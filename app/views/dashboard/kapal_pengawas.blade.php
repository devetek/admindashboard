@extends('shared.master-dashboard')

@section('content')
	<div class="col-md-3 col-sm-6 padding-1">
		<div id="site-visits" class="dash-widget-item bgm-teal">
			<div class="dash-widget-header">
				<div class="p-20">
					<div class="dash-widget-visits"></div>
				</div>
				
				<div class="dash-widget-title">Data 30 days</div>
				
				<ul class="actions actions-alt hide">
					<li class="dropdown">
						<a href="" data-toggle="dropdown">
							<i class="zmdi zmdi-more-vert"></i>
						</a>
						
						<ul class="dropdown-menu dropdown-menu-right">
							<li>
								<a href="">Refresh</a>
							</li>
							<li>
								<a href="">Manage Widgets</a>
							</li>
							<li>
								<a href="">Widgets Settings</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
			
			<div class="p-20">
				
				<small>Penangkapan</small>
				<h3 class="m-0 f-400">120</h3>
				
				<br/>
				
				<small>Diproses</small>
				<h3 class="m-0 f-400">79</h3>
				
				<br/>
				
				<small>Close</small>
				<h3 class="m-0 f-400">50</h3>
			</div>
		</div> 
</div>

<div class="col-md-9  padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/kapal_pengawas/map-kapal_pengawas')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
	<div class="card-header">
		<h2>Penangkapan per WPP (jan 2015 - sekarang)
			<small class="hidden">
				 <div class="row-fluid">
				   <div class="span12">
					 <div id="map" class="map"></div>
					 <select id="layer-select">
					   <option value="Aerial">Aerial</option>
					   <option value="AerialWithLabels" >Aerial with labels</option>
					   <option value="Road" selected>Road</option>
					   <option value="collinsBart">Collins Bart</option>
					   <option value="ordnanceSurvey">Ordnance Survey</option>
					 </select>
				   </div>
				 </div>
			</small>
		</h2>
		
		<ul class="actions">
		<li>
			<a id="reloadMapTangkap" href="#">
				<i class="zmdi zmdi-refresh-alt"></i>
			</a>
		</li>
		<li>
			<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
				<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
			</a>
		</li>
		<li>
			<a  data-widgster="restore" href="#">
				<i class="zmdi zmdi-window-restore"></i>
			</a>
		</li>
		<li>
			<a data-widgster="expand" href="#">
				<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
			</a>
		</li>
		<li>
			<a data-widgster="collapse" href="#">
				<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
			</a>
		</li>
		<li class="dropdown">
			<a href="" data-toggle="dropdown">
				<i class="zmdi zmdi-more-vert"></i>
			</a>
			<ul class="dropdown-menu dropdown-menu-right">
				<li>
					
					<a data-widgster="close" href="#">
						Close
					</a>		
				</li>
			</ul>
		</li>
	</ul>
	</div>
	<div class="card-body">
		<div class="body row">
			
		</div>
	</div>
</div>

</div>
<div class="col-md-8  padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/kapal_pengawas/chart-perbandingan')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
	<div class="card-header">
		<h2>Grafik Penangkapan 2015</h2> 
	<ul class="actions">
		<li>
			<a id="reloadMapPosisi" href="#">
				<i class="zmdi zmdi-refresh-alt"></i>
			</a>
		</li>
		<li>
			<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
				<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
			</a>
		</li>
		<li>
			<a  data-widgster="restore" href="#">
				<i class="zmdi zmdi-window-restore"></i>
			</a>
		</li>
		<li>
			<a data-widgster="expand" href="#">
				<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>                       
			</a>
		</li>
		<li>
			<a data-widgster="collapse" href="#">
				<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>                     
			</a>
		</li>
		<li class="dropdown">
			<a href="" data-toggle="dropdown">
				<i class="zmdi zmdi-more-vert"></i>
			</a>
			<ul class="dropdown-menu dropdown-menu-right">
				<li>
					
					<a data-widgster="close" href="#">
						2015
					</a>
					<a data-widgster="close" href="#">
						2014
					</a>
					<a data-widgster="close" href="#">
						2013
					</a>        
				</li>
			</ul>
		</li>
	</ul>
	</div>
	<div class="card-body">
		<div class="body">
			<div class="pl-body" id="GrafikPerbandingan" >
				 
			</div> 
		</div>
	</div>
</div>
</div>

<div class="col-sm-6 col-md-4 padding-1">
 
<div class="card widget" 
data-widgster-load="{{url('prototype/vms/list-suspect')}}"
data-widgster-autoload="true"
data-widgster-show-loader="false">
	<div class="card-header">
		<h2>List Gelar Operasi</h2>
				
		<ul class="actions">
		<li>
			<a data-widgster="load" href="#">
				<i class="zmdi zmdi-refresh-alt"></i>
			</a>
		</li>
		<li>
			<a data-widgster="fullscreen" href="#">
				<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
			</a>
		</li>
		<li>
			<a data-widgster="restore" href="#">
				<i class="zmdi zmdi-window-restore"></i>
			</a>
		</li>
		<li>
			<a data-widgster="expand" href="#">
				<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
			</a>
		</li>
		<li>
			<a data-widgster="collapse" href="#">
				<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
			</a>
		</li>
		<li class="dropdown">
			<a href="" data-toggle="dropdown">
				<i class="zmdi zmdi-more-vert"></i>
			</a>
			<ul class="dropdown-menu dropdown-menu-right">
				<li>
					<a onClick="OnloadDi('#all','page/detail-dataTangkap.html','#data')">View Detail</a>
					<a data-widgster="close" href="#">
						Close
					</a>		
				</li>
			</ul>
		</li>
	</ul>
	</div>
	
	<div class="card-body">
		<div class="body m-t-0 table-responsive" id="hostlistWG">
				
		</div>  
	</div>
</div> 
</div> 



@stop

@section('javascript')
	
    {{ HTML::style('public/vendors/openlayer/ol.css')}}
	{{ HTML::script('public/vendors/openlayer/ol.js')}}
	
		<script type="text/javascript">
		 
		 </script>
 @stop
