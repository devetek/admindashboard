			<div id="chartBakau" style="width:100%;"></div>
			<table class="table table-inner table-vmiddle table-bordered hidden">
				<thead>
					<tr>
						<th>Tahun</th>
						<th style="width: 60px">Jan</th>
						<th style="width: 60px">Feb</th>
						<th style="width: 60px">Mar</th>
						<th style="width: 60px">Apr</th>
						<th style="width: 60px">Mei</th>
						<th style="width: 60px">Jun</th>
						<th style="width: 60px">Jul</th>
						<th style="width: 60px">Agu</th>
						<th style="width: 60px">Sep</th>
						<th style="width: 60px">Okt</th>
						<th style="width: 60px">Nov</th>
						<th style="width: 60px">Des</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="f-500 c-cyan">2015</td>
						<td>3</td>
						<td>3</td>
						<td>4</td>
						<td>2</td>
						<td>1</td>
						<td>3</td>
						<td>5</td>
						<td>3</td>
						<td>4</td>
						<td>2</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td class="f-500 c-cyan">2014</td>
						<td>1</td>
						<td>2</td>
						<td>2</td>
						<td>2</td>
						<td>3</td>
						<td>2</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
					</tr>
				</tbody>
			</table>
			
			<script type="text/javascript">
				AutoResizeDiv('#chartBakau',50);

				var Data = [
					{"date": "Jan","2015": 3,"2014": 1},
					{"date": "Feb","2015": 3,"2014": 2},
					{"date": "Mar","2015": 4,"2014": 2},
					{"date": "Apr","2015": 2,"2014": 2},
					{"date": "Mei","2015": 1,"2014": 3},
					{"date": "Jun","2015": 3,"2014": 2},
					{"date": "Jul","2015": 5,"2014": 1},
					{"date": "Agu","2015": 3,"2014": 1},
					{"date": "Sep","2015": 4,"2014": 1},
					{"date": "Okt","2015": 2,"2014": 1},
					{"date": "Nov","2014": 1},
					{"date": "Des","2014": 1}];
				
				AmCharts.makeChart("chartBakau",
				{
					"type": "serial",
					"categoryField": "date",
					"chartCursor": {
						"categoryBalloonDateFormat": "{title} MMM"
					}, 
					"chartScrollbar": {},
					"trendLines": [],
					"graphs": [
						{
							"bullet": "round",
							"id": "AmGraph-3",
							"title": "2015",
							"valueField": "2015"
						},
						{
							"bullet": "square",
							"id": "AmGraph-4",
							"title": "2014",
							"valueField": "2014"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Jumlah"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"useGraphSettings": true
					},
					"dataProvider": Data
					}); 
			</script>
	