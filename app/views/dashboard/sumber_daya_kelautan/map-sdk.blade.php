			<form method="get" accept-charset="utf-8" action="http://www.djpsdkp.kkp.go.id/ppsdk/index.php/map/">    
				<p class="margin-5" style="padding-left : 15px;">
				        Pilihan Data UPT : <select name="upt_id">
						<option value="">- Spasial PSDKP -</option>
						<option value="1">Pos Pengawasan</option>
						<option selected="selected" value="2">Stasiun PSDKP</option>
						<option value="4">Satker PSDKP</option>
						<option value="3">Pangkalan PSDKP</option>
						</select>  
						        &nbsp;&nbsp;
						        Overlay Data : <select name="overlay_id">
						<option selected="selected" value="">- Overlay Data -</option>
						<option value="1">Kawasan Konservasi</option>
						<option value="2">WPP</option>
						<option value="3">Lokasi IUUF</option>
						<option value="4">Lokasi BMKT</option>
						<option value="5">Pelabuhan Perikanan</option>
						<option value="6">Pelabuhan Umum</option>
						<option value="7">Kilang Lepas Pantai</option>
						<option value="8">Pembangkit Listrik</option>
						<option value="9">Wisata Laut</option>
						</select>        &nbsp;&nbsp;
				        <input type="submit" value="Proses" name="submit">    
				    </p>
				</form>

			<div id="mapSDK" class="map"></div>
			
			<script type="text/javascript">
				AutoResizeDiv('#mapSDK',80);

				var container = document.getElementById('popup');
				var content = document.getElementById('popup-content');
				var closer = document.getElementById('popup-closer');

				closer.onclick = function() {
				  overlay.setPosition(undefined);
				  closer.blur();
				  return false;
				};

				var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
				  element: container,
				  autoPan: true,
				  autoPanAnimation: {
					duration: 250
				  }
				}));


				var map = new ol.Map({
				  layers: [
					new ol.layer.Tile({
					  source: new ol.source.OSM()
					}) 
				  ],
				  overlays: [overlay],
				  target: 'mapSDK',
				  controls: ol.control.defaults({
					attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
					  collapsible: false
					})
				  }),
				  view: new ol.View({
						center: ol.proj.transform([110,0], 'EPSG:4326', 'EPSG:3857'),
						zoom: 4,
						maxZoom:6,
						minZoom:4

				  })
				});


				var warnaBg2 = [
								  '#F0DD7F',
								  '#d11919',
								  '#d63232',
								  '#db4c4c',
								  '#e06666',
								  '#e57f7f',
								  '#ea9999',
								  '#efb2b2' 
								  ];

				var jenis='status';                  
				//var status=['Minor'='1','Mayor':2,'Critical':3];                  
				var status=['Minor','Mayor','Critical'];                  
				var stylePangan = function(feature, resolution) { 
						var text = status.indexOf(feature.get(jenis));
						var jari_jari = feature.get('jari_jari');  

						var Max = 2;
						var Min = 0;

						var total = (Max - Min);

						console.log(jari_jari);
						// console.log(total);
						// console.log(text);

						//makeLegen(warnaBg2 , total, jenis);

						var indexBg=1;

						if(text < total)
						{
							var indexBg=0;
						}

						else if(text < (total *2) )
						{
							var indexBg=1;
						}
						else if(text < (total *3) )
						{
							var indexBg=2;
						} 


						 var fill = new ol.style.Fill({
						   color: warnaBg2[indexBg]
						 });
						 var stroke = new ol.style.Stroke({
						   color: warnaBg2[indexBg],
						   width: jari_jari  
						 });
						  

						return [new ol.style.Style({
							stroke: new ol.style.Stroke({
							  color: '#fff',
							  width: 1
							}),

							fill: new ol.style.Fill({
							  color: warnaBg2[indexBg]
							}),
							image: new ol.style.Circle({
							   fill: fill,
							   stroke: stroke,
							   radius: 5
							 }) 
							
							// text: new ol.style.Text({
							//   text: text, 
							//   scale: 1, 
							//   fill: new ol.style.Fill({
							//     color: '#fff'
							//   }),

							//   stroke: new ol.style.Stroke({
							//     color: '#FFFF99',
							//     width: 1
							//   })
							// })

						  })];
					};



				//Tambah layar baru
				var LastDataSource = new ol.source.Vector({
					url: '{{url("public/geojson/geojson_pencemaran.geojson")}}',
					format: new ol.format.GeoJSON({ })
				});
				 
				var LastSource = new ol.source.Cluster({
				  source: LastDataSource
				});

				 


				var vectorLayer = new ol.layer.Vector({
				  source: LastDataSource,
				  opacity:0.7,
				  name:'lastposisi' ,
				  style:stylePangan,
				  opacity:0.8
				});

				map.addLayer(vectorLayer);

				//----------------------------
				//untuak klik detail map


				map.on('click', function(e) {
				  var coordinate = e.coordinate;
				  var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
						coordinate, 'EPSG:3857', 'EPSG:4326'));
					
				   map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
						//do something
						//infoBox.innerHTML="testtt,testt";
						//console.log(feature.get('val'));
						if(feature.get('lokasi'))
						{
							 content.innerHTML = '<p>Coordinate:</p><code>' + hdms + '</code>'; 
							 content.innerHTML+= '<p>lokasi : <b>'+ feature.get('lokasi') + '</b><p>';
							 content.innerHTML+= '<p>Kategori : <b>'+ feature.get('jenis') + '</b><p>';
							 content.innerHTML+= '<p>tanggal : <b>'+ feature.get('tanggal') + '</b><p>'; 
							 content.innerHTML+= '<p>jenis_pencemaran : <b>'+ feature.get('jenis_pencemaran') + '</b><p>'; 
							 content.innerHTML+= '<p>Penyebab : <b>'+ feature.get('penyebab') + '</b><p>'; 
							 content.innerHTML+= '<p>Status : <b>'+ feature.get('status') + '</b><p>'; 
							 content.innerHTML+= '<p>keterangan : <b>'+ feature.get('keterangan') + '</b><p>'; 
							   overlay.setPosition(coordinate);
							  
						}
						 
					}); 
				});

				//--------------------

								
				// untuak full screan
				$('#reloadMapPosisi').on('click',function()
				{
					var tinggi = ($(window).height() - 100);
					var lebar = ($(window).width() - 20);
					
					 $('#mapSDK canvas').attr('width',lebar);
					 $('#mapSDK canvas').attr('height',tinggi);
					 
					   if($('#mapSDK canvas').attr('width') == lebar)
					   {
							console.log(lebar);
							 map.updateSize();
					   }
						
					
				});
				
			</script>