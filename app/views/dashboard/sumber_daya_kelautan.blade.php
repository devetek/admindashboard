@extends('shared.master-dashboard')

@section('content')
 

	

	<div class="col-sm-6 col-md-6  padding-1">
		<div id="popup" class="ol-popup">
		  <a href="#" id="popup-closer" class="ol-popup-closer"></a>
		  <div id="popup-content"></div>
		</div>

		<div class="card widget" 
		data-widgster-load="{{url('prototype/sumber_daya_kelautan/map-sdk')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false"> 
			<div class="card-header">
				<h2>Kawasan Kelautan
					<small class="hidden">
						 <div class="row-fluid">
						   <div class="span12">
							 <div id="map" class="map"></div>
							 <select id="layer-select">
							   <option value="Aerial">Aerial</option>
							   <option value="AerialWithLabels">Aerial with labels</option>
							   <option value="Road" selected>Road</option>
							   <option value="collinsBart">Collins Bart</option>
							   <option value="ordnanceSurvey">Ordnance Survey</option>
							 </select>
						   </div>
						 </div>
					</small>
				</h2>
				
				<ul class="actions">
				<li>
					<a id="reloadMapPosisi" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a  data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
			</ul>
			</div>
			<div class="card-body">
				<div class="body">

				</div>
			</div>
		</div>
		
	</div>
   
	<div class="col-sm-6 col-md-3 padding-1 hidden">
		<div class="mini-charts-item bgm-lightgreen pointer">
			<div class="clearfix">
			   <div class="count">
					<small>Kerusakan Pencemaran</small>
					<h2>2 Kawasan</h2>
				</div>
			</div> 
		</div>

		 <div class="mini-charts-item bgm-bluegray pointer">
			<div class="clearfix">
				<div class="count">
					<small>Kerusakan Bakau</small>
					<h2>2 Kawasan</h2>
				</div>
			</div> 
		</div> 

		<div class="mini-charts-item bgm-lightgreen">
			<div class="clearfix">
				<div class="count">
					<small>Kerusakan Terumbu Karang</small>
					<h2>5 Kawasan</h2>
				</div>
			</div> 
		</div> 
		<div class="mini-charts-item bgm-lightgreen">
			<div class="clearfix">
				<div class="count">
					<small>Kawasan Konservasi</small>
					<h2>5 Kawasan</h2>
				</div>
			</div> 
		</div> 
	</div>

	<div class="col-sm-6 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/sumber_daya_kelautan/chart-pencemaran')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Grafik Pencemaran</h2>
				
				<ul class="actions">
				<li>
					<a id="" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a  data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
			</ul>
			</div>
			<div class="card-body">
				<div class="body">
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-sm-6 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/sumber_daya_kelautan/chart-bakau')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Grafik kerusakan bakau</h2>
				
				<ul class="actions">
				<li>
					<a id="" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a  data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
			</ul>
			</div>
			<div class="card-body">
				<div class="body">
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-sm-6 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/sumber_daya_kelautan/chart-terumbu')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Grafik Kerusakan Terumbu Karang</h2>
				
				<ul class="actions">
				<li>
					<a id="" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a  data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
			</ul>
			</div>
			<div class="card-body">
				<div class="body">
				</div>
			</div>
		</div>
		
	</div>
	
@stop

@section('javascript')
    {{ HTML::style('public/vendors/openlayer/ol.css')}}
	{{ HTML::script('public/vendors/openlayer/ol.js')}}
	
		<script type="text/javascript">
		 
		 </script>
		 @stop
