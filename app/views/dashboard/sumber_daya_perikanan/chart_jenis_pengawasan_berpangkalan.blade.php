<div style="width: 100%; height: 350px;" id="PengawasanBerpangkalan">
										 
</div>

<script type="text/javascript">
AutoResizeDiv('#PengawasanBerpangkalan',50);

jQuery(document).ready(function(){
	AmCharts.makeChart("PengawasanBerpangkalan",
{
	"type": "serial",
	"categoryField": "category",
	"angle": 30,
	"depth3D": 10,
	"colors": [
		"#4169E1",
		"#FF3300"
	],
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start"
	},
	"trendLines": [],
	"graphs": [
		{
			"balloonText": "[[title]]: [[value]]",
			"fillAlphas": 1,
			"id": "AmGraph-1",
			"title": "Taat",
			"type": "column",
			"valueField": "column-1"
		},
		{
			"balloonText": "[[title]]: [[value]]",
			"fillAlphas": 1,
			"id": "AmGraph-2",
			"title": "Tidak Taat",
			"type": "column",
			"valueField": "column-2"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"title": "Jumlah"
		}
	],
	"allLabels": [],
	"balloon": {},
	"legend": {
		"useGraphSettings": true,
		"maxColumns": 2
	},
	"titles": [
		{
			"id": "Title-1",
			"size": 12,
			"text": "Periode 01-01-2015 s/d 25-10-2015"
		}
	],
	"dataProvider": [
		{
			"category": "",
			"column-1": 100,
			"column-2": 5
		}
	]
}
	)
	});


</script>