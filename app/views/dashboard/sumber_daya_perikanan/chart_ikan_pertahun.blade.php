<div style="width: 100%; height: 350px;" id="HasilTangkapanTahunan">
										 
</div>

<script type="text/javascript">
	AutoResizeDiv('#HasilTangkapanTahunan',50);

	jQuery(document).ready(function(){
	AmCharts.makeChart("HasilTangkapanTahunan",
		{
	"type": "serial",
	"categoryField": "category",
	"angle": 30,
	"depth3D": 10,
	"colors": [
		"#00FFFF",
		"#3333CC"
	],
	"startDuration": 1,
	"theme": "light",
	"categoryAxis": {
		"gridPosition": "start",
		"minHorizontalGap": 76
	},
	"trendLines": [],
	"graphs": [
		{
			"balloonText": "Hasil Tangkapan  [[category]] [[title]] : [[value]]",
			"color": "#FF0000",
			"columnWidth": 0.45,
			"fillAlphas": 1,
			"fillColors": "#4169E1", 
			"id": "AmGraph-1",
			"labelText": "[[value]]",
			"lineColor": "#4169E1",
			"negativeFillColors": "#4169E1",
			"negativeLineAlpha": 0.28,
			"negativeLineColor": "#4169E1",
			"title": "Periode 2015",
			"type": "column",
			"valueField": "column-1"
		},
		{
			"balloonText": "Hasil Tangkapan  [[category]] [[title]] : [[value]]",
			"bullet": "round",
			"color": "#FF8000",
			"columnWidth": 0.5,
			"fillAlphas": 0.89,
			"fillColors": "#FF8000", 
			"gapPeriod": 1,
			"id": "AmGraph-2",
			"labelText": "[[value]]",
			"lineColor": "#FF8000",
			"lineThickness": 2,
			"title": "Periode 2014",
			"type": "column",
			"valueField": "column-2"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"title": "Jumlah (Kg)"
		}
	],
	"allLabels": [],
	"balloon": {},
	"legend": {
		"maxColumns": 2,
		"useGraphSettings": true
	},
	"titles": [
		{
			"id": "Title-1",
			"size": 15,
			"text": "Hasil Tangkapan Ikan Periode 2014-2015"
		}
	],
	"dataProvider": [
		{
			"category": "Jan",
			"column-1": 12563,
			"column-2": 23577
		},
		{
			"category": "Feb",
			"column-1": 74233,
			"column-2": 23577
		},
		{
			"category": "Mar",
			"column-1": 232556,
			"column-2": 357834
		},
		{
			"category": "Apr",
			"column-1": 40027,
			"column-2": 13457
		},
		{
			"category": "Mei",
			"column-1": 15861,
			"column-2": 46832
		},
		{
			"category": "Jun",
			"column-1": 35048,
			"column-2": 13572
		},
		{
			"category": "Jul",
			"column-1": 42973,
			"column-2": 13579
		},
		{
			"category": "Agu",
			"column-1": 209027,
			"column-2": 150027
		},
		{
			"category": "Sep",
			"column-1": 307894.65,
			"column-2": 209027
		},
		{
			"category": "Okt",
			"column-1": 23356,
			"column-2": 261454
		},
		{
			"category": "Nov",
			"column-1": 0,
			"column-2": 203578
		},
		{
			"category": "Des",
			"column-1": 0,
			"column-2": 150048
		}
	]
}	
	 
	 )
	});
</script>