<div style="width: 100%; height: 1050px;" id="HasilTangkapanBulanan">
										 
</div>

<script type="text/javascript"> 

jQuery(document).ready(function(){
	AmCharts.makeChart("HasilTangkapanBulanan",
{
	"type": "serial",
	"categoryField": "category",
	"rotate": true,
	"angle": 30,
	"depth3D": 0,
	"colors": [
		"#00FFFF",
		"#3333CC"
	],
	"startDuration": 1,
	"handDrawScatter": 3,
	"categoryAxis": {
		"gridPosition": "start"
	},
	"trendLines": [],
	"graphs": [
		{
			"balloonColor": "#FF8000",
			"balloonText": "[[category]]: [[value]]",
			"color": "#020202",
			"fillAlphas": 1,
			"fillColors": "#FF8000",
			"id": "AmGraph-1",
			"labelText": "[[value]]",
			"lineColor": "#FF8000",
			"title": "Periode 2015",
			"type": "column",
			"valueField": "column-1"
		},
		{
			"balloonText": "[[category]]: [[value]]",
			"bullet": "round",
			"id": "AmGraph-2",
			"labelText": "[[value]]",
			"lineThickness": 2,
			"title": "Periode 2014",
			"type": "step",
			"valueField": "column-2"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"title": ""
		}
	],
	"allLabels": [],
	"balloon": {
		"fadeOutDuration": 0
	},
	"legend": {
		"useGraphSettings": true
	},
	"titles": [
		{
			"id": "Title-1",
			"size": 15,
			"text": "Hasil Tangkapan Bulan Oktober Perjenis Ikan"
		}
	],
	"dataProvider": [
		{
			"category": "Setuhuk Hitam",
			"column-1": 3748,
			"column-2": 23299
		},
		{
			"category": "Madidihang",
			"column-1": 8842,
			"column-2": 63165
		},
		{
			"category": "Tuna Sirip Biru Selatan",
			"column-1": 24280,
			"column-2": 10027
		},
		{
			"category": "Ikan Tumbuk",
			"column-1": 20085,
			"column-2": 0
		},
		{
			"category": "Cakalang",
			"column-1": 6666,
			"column-2": 0
		},
		{
			"category": "Tenggiri",
			"column-1": 77832,
			"column-2": 73854.2
		},
		{
			"category": "Albakora",
			"column-1": 1,
			"column-2": 672.45
		},
		{
			"category": "Tenggiri,Calong",
			"column-1": 0,
			"column-2": 149096.35
		},
		{
			"category": "Setuhuk Putih",
			"column-1": 0,
			"column-2": 58880.65
		},
		{
			"category": "Setuhuk Hitam",
			"column-1": 3748,
			"column-2": 23299
		},
		{
			"category": "Madidihang",
			"column-1": 8842,
			"column-2": 63165
		},
		{
			"category": "Tuna Sirip Biru Selatan",
			"column-1": 24280,
			"column-2": 10027
		},
		{
			"category": "Ikan Tumbuk",
			"column-1": 20085,
			"column-2": 0
		},
		{
			"category": "Cakalang",
			"column-1": 6666,
			"column-2": 0
		},
		{
			"category": "Tenggiri",
			"column-1": 77832,
			"column-2": 73854.2
		},
		{
			"category": "Albakora",
			"column-1": 1,
			"column-2": 672.45
		},
		{
			"category": "Tenggiri,Calong",
			"column-1": 0,
			"column-2": 149096.35
		},
		{
			"category": "Setuhuk Putih",
			"column-1": 0,
			"column-2": 58880.65
		}
	]
}

	)
	});


</script>