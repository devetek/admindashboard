@extends('shared.master-dashboard')

@section('content')

<div class="col-sm-12 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/vms/map-vms')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Posisi Pelanggaran Kapal yang Ditindaklanjuti
					<small class="hidden">
						 <div class="row-fluid">
						   <div class="span12">
							 <div id="map" class="map"></div>
							 <select id="layer-select">
							   <option value="Aerial">Aerial</option>
							   <option value="AerialWithLabels">Aerial with labels</option>
							   <option value="Road" selected>Road</option>
							   <option value="collinsBart">Collins Bart</option>
							   <option value="ordnanceSurvey">Ordnance Survey</option>
							 </select>
						   </div>
						 </div>
					</small>
				</h2>
				<small>Informasi terakhir pertanggal <span id="tgl-update-map">-</span></small>
				
				<ul class="actions">
				<li>
					<a id="reloadMapPosisi" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a  data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
			</ul>
			</div>
			<div class="card-body">
				<div class="body">
				</div>
			</div>
		</div>
                        
		<script type="text/javascript">
			jQuery(document).ready(function(){
				var $widget_map = $('.widget-map')
				$widget_map.widgster();

				$widget_map.on("fullscreen.widgster", function(){
					$('.content-wrap').css({
						'-webkit-transform': 'none',
						'-ms-transform': 'none',
						transform: 'none',
						'margin': 0,
						'z-index': 2
					});
					jQuery("#lastPosisi").height("");
					//updateSizeMap();
				}).on("restore.widgster closed.widgster", function(){
					$('.content-wrap').css({
						'-webkit-transform': '',
						'-ms-transform': '',
						transform: '',
						margin: '',
						'z-index': ''
					});
					jQuery("#lastPosisi").height("");
					//map.updateSize();
				});
			});
		</script>
		
	</div>
   

	<div class="col-sm-12 col-md-12">
		<div class="card widget" 
		data-widgster-load="{{url('penanganan_pelanggaran/grafikPP')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Grafik Pelanggaran Tahunan</h2>
				
				<ul class="actions">
				<li>
					<a data-widgster="load" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				<li class="dropdown">
					<a href="" data-toggle="dropdown">
						<i class="zmdi zmdi-more-vert"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
						</li>
					</ul>
				</li>
			</ul>
			</div>
			
			<div class="card-body">
				<div class="body">
					<div class="pl-body" id="">
					
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="col-sm-12 col-md-12">
		<div class="card widget" 
		data-widgster-load="{{url('penanganan_pelanggaran/grafikPP')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Grafik Pelanggaran Tahunan</h2>
				
				<ul class="actions">
				<li>
					<a data-widgster="load" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				<li class="dropdown">
					<a href="" data-toggle="dropdown">
						<i class="zmdi zmdi-more-vert"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
						</li>
					</ul>
				</li>
			</ul>
			</div>
			
			<div class="card-body">
				<div class="body">
					<div class="pl-body" id="">
					
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-12">
		 <div class="card1 custom widget"
		data-widgster-load="{{url('prototype/penanganan_pelanggaran/list-pelanggaran')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="true">
			<div class="card-header">
				<h2>Daftar Keberangkatan Kapal Hari Ini</h2>
				
				<ul class="actions">
				<li>
					<a data-widgster="load" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				<li>
					<a data-widgster="expand" href="#">
						<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
					</a>
				</li>
				<li>
					<a data-widgster="collapse" href="#">
						<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
					</a>
				</li>
				<li class="dropdown">
					<a href="" data-toggle="dropdown">
						<i class="zmdi zmdi-more-vert"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
						</li>
					</ul>
				</li>
			</ul> 
			</div>
			
			<div class="card-body">
				<div class="body">
						
				</div>
			</div>
		</div>
	</div>


@stop

@section('javascript')

	<script type="text/javascript">
	 
	 </script>

 @stop
