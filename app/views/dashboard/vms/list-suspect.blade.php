	<div class="wrapper-indikasi">
		<table id="table-indikasi" class="table table-inner table-hover table-vmiddle">
			<thead>
				<tr> 
					<th>Nama Kapal</th>
					<th>GT</th>
					<th style="width: 60px">Pelanggaran</th>
				</tr>
			</thead>

			<tbody>
			@foreach($mListIndikasi as $item)
				<tr>
					<td onclick="LoadModal('{{url('dashboard/kapal/popup-kapal-detail')}}')">
						{{$item->nama}}
					</td>
					<td>{{$item->gt}}</td>
					<td>{{$item->jenis_pelanggaran}}</td>
				</tr>
			@endforeach
			</tbody>
		</table>  
	</div>
	{{ HTML::script('public/vendors/mkoryak-floatThead/jquery.floatThead.js')}}
	
	<script type="text/javascript"> 
		AutoResizeDiv('#listIndikasi',20);
		
		jQuery(document).ready(function(){
			var $tableIndikasi = jQuery('#table-indikasi');
			$tableIndikasi.floatThead({
				top: pageTop,
				scrollContainer: function($table){
					return $table.closest('.wrapper-indikasi');
				},
				position: 'absolute'
			});
			
			jQuery("#tgl-update-indikasi").html("{{date('d-m-Y H:i:s')}}");
		});
	</script>
	