			<div class="row">
				<div class="col-md-4">
					Group By
				</div>
				<div class="col-md-8" style="    text-align: right;">
					<select id="KapalCombo" class="selectpicker">
						<optgroup label="Kapal">
							<option value="Negara">Negara</option>
						</optgroup>
						<optgroup label="Perlengkapan">
							<option value="AlatTangkap" selected>Alat Tangkap</option>
							<option value="Provider">Provider</option>
						</optgroup>
					</select>
				</div>
			</div>
			<div id="chartKomposisi" style="width:100%; "></div>
			<script type="text/javascript">
				AutoResizeDiv('#chartKomposisi',100);

				var Negara = [{"category" :'Indonesia',"column-1" : 76.950916},{"category" :'Thailand',"column-1" : 8.414386},{"category" :'P. R. Of China',"column-1" : 4.953630},{"category" :'Taiwan',"column-1" : 3.981000},{"category" :'Philippines',"column-1" : 2.171454},{"category":"Lainnya","column-1": 3.528614}];
				var PemilikKapal = [{"category" :'BANDAR NELAYAN, PT',"column-1" :1.967881},{"category" :'ARABIKATAMA KHATULISTIWA FISHING INDUSTRY, PT',"column-1" :1.357159},{"category" :'BINAR SURYA BUANA, PT',"column-1" :1.063108},{"category" :'PATHEMAANG RAYA, PT',"column-1" :0.904773},{"category" :'TANGGUL MINA NUSANTARA. PT',"column-1" :0.814295},{"category":"Lainnya","column-1": 93.892784}];
				var AlatTangkap = [{"category":"Purse Seine (Pukat Cincin) Pelagis Kecil","column-1":"29.224157"},{"category":"Rawai Tuna","column-1":"14.612079"},{"category":"Pukat Ikan","column-1":"13.865641"},{"category":"Pengangkut","column-1":"12.621579"},{"category":"Bouke ami","column-1":"10.517982"},{"category":"Lainnya","column-1":"19.158562"}];
				var Provider = [{"category" :'SOGIndonesia',"column-1" :57.950690},{"category" :'CLS',"column-1" :20.402624},{"category" :'Amalgam',"column-1" :9.567971},{"category" :'PSN',"column-1" :6.378647},{"category" :'MSP',"column-1" :2.261932},{"category":"Lainnya","column-1":3.438136}];
				var Pelabuhan = [{"category" :'PU. Tanjung Benoa',"column-1" :5.021488},{"category" :'PP. Muara Angke, PP. Nizam Zachman Jakarta, PP. Su',"column-1" :4.433386},{"category" :'PP. Bitung',"column-1" :4.207193},{"category" :'PP. Barelang',"column-1" :3.619091},{"category" :'PP. Ambon',"column-1" :3.257182},{"category":"Lainnya","column-1":79.461660}];
			
				var chartGroup = AmCharts.makeChart("chartKomposisi",
				{
					"type": "pie",
					"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"labelText": "[[percents]]%",
					"startAngle": 0,
					"titleField": "category",
					"valueField": "column-1",
					"allLabels": [],
					"balloon": {},
					"titles": [],
					"autoMargins": false,
					"marginLeft": 0,
					"marginRight": 0,
					"pullOutRadius": 0,
					"dataProvider": Provider
				});
				jQuery(document).ready(function(){
					jQuery("#KapalCombo").change(function(){
						eval("chartGroup.dataProvider = " + $( this ).val() + ";");
						chartGroup.validateData();
					});
				});
			</script>
	