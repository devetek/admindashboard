	
	<div class="row">
		<div class="col-sm-12">
			<div class="col-sm-4">
				<div class="input-group form-group">
					<span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
					<div class="dtp-container fg-line">
						<input type='text' id="startDate" class="form-control" placeholder="Start Date">
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="input-group form-group">
					<span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
					<div class="dtp-container fg-line">
						<input type='text' id="startDate" class="form-control" placeholder="Start Date">
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="input-group form-group">
					<span class="input-group-addon"><i class="zmdi zmdi-calendar"></i></span>
					<div class="dtp-container fg-line">
						<input type='text' id="startDate" class="form-control" placeholder="Start Date">
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="input-group form-group">
					<input type='button' class="btn-primary" value="Cari">
				</div>
			</div>
		</div>
	</div>
	<div id="chartKapal" style="width:100%; height:295px;"></div>
	<table class="table table-inner table-vmiddle table-bordered hidden">
		<thead>
			<tr>
				<th>Pelanggaran</th>
				<th style="width: 60px">Jan</th>
				<th style="width: 60px">Feb</th>
				<th style="width: 60px">Mar</th>
				<th style="width: 60px">Apr</th>
				<th style="width: 60px">Mei</th>
				<th style="width: 60px">Jun</th>
				<th style="width: 60px">Jul</th>
				<th style="width: 60px">Agu</th>
				<th style="width: 60px">Sep</th>
				<th style="width: 60px">Okt</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="f-500 c-cyan">Pelanggaran Wilayah</td>
				<td>2</td>
				<td>5</td>
				<td>4</td>
				<td>2</td>
				<td>1</td>
				<td>7</td>
				<td>5</td>
				<td>2</td>
				<td>6</td>
				<td>2</td>
			</tr>
			<tr>
				<td class="f-500 c-cyan">Pelanggaran Operasional</td>
				<td>2</td>
				<td>1</td>
				<td>5</td>
				<td>5</td>
				<td>6</td>
				<td>5</td>
				<td>7</td>
				<td>3</td>
				<td>1</td>
				<td>1</td>
			</tr>
		</tbody>
	</table>
	
	<script type="text/javascript">

		AutoResizeDiv('#chartKapal',50);

		var Data = {{json_encode($mDataChartPelanggaran)}};
		
		AmCharts.makeChart("chartKapal",
		{
			"type": "serial",
			"categoryField": "date",
			"dataDateFormat": "YYYY-MM",
			"chartCursor": {
				"categoryBalloonDateFormat": "MMM YYYY"
			},
			"titles": [
				{
					"id": "Title-1",
					"size": 15,
					"text": "Data Pelanggaran Januari 2015 - Sekarang per Bulan"
				}
			],
			"chartScrollbar": {},
			"trendLines": [],
			"graphs": [
				{
					"bullet": "round",
					"id": "AmGraph-3",
					"title": "Pelanggaran Wilayah",
					"valueField": "Wilayah"
				},
				{
					"bullet": "square",
					"id": "AmGraph-4",
					"title": "Pelanggaran Operasional",
					"valueField": "Operasional"
				}
			],
			"guides": [],
			"valueAxes": [
				{
					"id": "ValueAxis-1",
					"title": "Axis title"
				}
			],
			"allLabels": [],
			"balloon": {},
			"legend": {
				"useGraphSettings": true
			},
			"dataProvider": Data
			}); 
	</script>
	