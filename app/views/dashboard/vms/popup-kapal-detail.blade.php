<div class="modal-content">
    <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 id="myModalLabel18" class="modal-title text-align-center fw-bold mt">Detail Kapal</h4>
    </div>
    <div class="modal-body bg-gray-lighter">
         <table class="table table-hover">
            <thead>
            <tr>
                <th>Detail</th>
                <th>Value</th> 
            </tr>
            </thead>
            <tbody>
                <tr> 
                    <td>ID Transmitter</td> 
                    <td>21947</td> 
                </tr> 
                <tr> 
                    <td>Nama Kapal</td> 
                    <td>KOFIAU 31</td> 
                </tr> 
                <tr> 
                    <td>Alat Tangkap</td> 
                    <td>Pukat Ikan</td> 
                </tr> 
                <tr> 
                    <td>Waktu Posisi Terakhir</td> 
                    <td>2014-11-28 18:04:00</td> 
                </tr>  
                <tr> 
                    <td>Koordinat Posisi Terakhir</td> 
                    <td>136.65 / -4.935</td> 
                </tr> 
                <tr> 
                    <td>Status</td> 
                    <td>KOFIAU 31</td> 
                </tr> 
                <tr> 
                    <td>Pelabuhan</td> 
                    <td>PU. Mimika</td> 
                </tr>   
  
                <tr> 
                    <td>Pemilik Kapal</td> 
                    <td>ANUGERAH BAHARI BERKAT ABADI, PT.</td> 
                </tr> 
             
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-gray" type="button">Close</button>
    </div>
</div>