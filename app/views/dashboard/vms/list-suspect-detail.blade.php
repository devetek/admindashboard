			<div class="card" style="margin-top: -30px;">
				 
						<div class="pl-body posisiAb" >
							<div id="MapKapalPengawas" style="height: 100%"> </div> 
						</div>  
			</div>

			<aside id="hotlist" class="toggled active">
				<div class="card">
					
					<div class="card-header">
						<h2>10 Hotlist Suspect Kapal</h2>
						<ul class="actions">
							<li class="dropdown action-show">
								<a href="#" onClick="hideToggle('hotlist')">
									<i class="zmdi zmdi-arrow-back"></i>
								</a> 
							</li> 
							<li class="dropdown action-show">
								<a href="#" onClick="closeLayer('#layer2')">
									<i class="zmdi zmdi-close"></i>
								</a> 
							</li>
						</ul>
					</div>
					
					<div class="card-body">
						<div class="card-body m-t-0 overflow" id="">
												

							<table class="table table-inner table-vmiddle " >
								<thead>
									<tr> 
										<th >View</th>
										<th >Nama Kapal</th>
										<th>GT</th>
										<th class="additional">Jenis</th>
										<th class="additional">Alat Tangkap</th>
										<th style="width: 60px">Pelanggaran</th>
									</tr>
								</thead>

								<tbody id="hostlistWGdetail">
									 
								</tbody>
							</table> 
						</div>
					</div>
				</div> 
			</aside>

			<script type="text/javascript">
				
				HostList('#hostlistWGdetail');

				function HostList(dima)
				{   
					var html=''; 

						for(var a=0;a < dataHotlist.length;a++ )
						{
							html+='<tr>';
								html+='<td view=""> <button lon="' + dataHotlist[a].lon + '" lat="' + dataHotlist[a].lat + '" class="posisidimap btn btn-default btn-icon waves-effect waves-circle waves-float"><i class="zmdi zmdi-arrow-back"></i></button></td>';
								html+='<td onclick="LoadModal(\'{{url("public/prototype/vms/popup-kapal-detail")}}\')">' + dataHotlist[a].nama + '</td>';
								html+='<td>' + dataHotlist[a].gt + '</td>';
								html+='<td class="additional">' + dataHotlist[a].jenis + '</td>';
								html+='<td class="additional">' + dataHotlist[a].alat_tangkap + '</td>';
								html+='<td>' + dataHotlist[a].jenis_pelanggaran + '</td>';

							html+='</tr>'; 

						}

					$('#hostlistWG').height($(window).height()/2 - 75 + 'px');
					$('#lastPosisi').height($(window).height()/2 - 75 + 'px');
					$(dima).html(html);
				} 


				var server= '';
				var styles = [
				  'Road',
				  'Aerial',
				  'AerialWithLabels',
				  'collinsBart',
				  'ordnanceSurvey'
				];
				var layers = [];
				var i, ii;
				for (i = 0, ii = styles.length; i < ii; ++i) {
				  layers.push(new ol.layer.Tile({
					visible: false,
					preload: Infinity,
					source: new ol.source.BingMaps({
					  key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3',
					  imagerySet: styles[i]
					  // use maxZoom 19 to see stretched tiles instead of the BingMaps
					  // "no photos at this zoom level" tiles
					  // maxZoom: 19
					})
				  }));
				}
				var mapFP = new ol.Map({ //full pengawas
				  layers: layers,
				  // Improve user experience by loading tiles while dragging/zooming. Will make
				  // zooming choppy on mobile or slow devices.
				  loadTilesWhileInteracting: true,
				  target: 'MapKapalPengawas',
				  view: new ol.View({
					center: ol.proj.transform([110,0], 'EPSG:4326', 'EPSG:3857'),
					zoom: 4
				  })
				});

				var select = document.getElementById('layer-select');
				function onChange() {
				  var style = select.value;
				  for (var i = 0, ii = layers.length; i < ii; ++i) {
					layers[i].setVisible(styles[i] === style);
				  }
				}
				select.addEventListener('change', onChange);
				onChange();


				function styleCluster(feature, resolution) {  
					style = [new ol.style.Style({
						  image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
							anchor: [0,0],
							opacity: 1,
							anchorXUnits: 'fraction',
							anchorYUnits: 'pixels',
							src: 'Boat-icon.png'
						  })),
							fill: new ol.style.Fill({
									  color: 'rgba(255, 255, 255, 0.2)'
									}),
									stroke: new ol.style.Stroke({
									  color: '#ffcc33',
									  width: 3
									})

						})];

			 
					return style;
				};
			 

			 
			var LastDataSource2 = new ol.source.Vector({
				url: "{{url('public/geojson/pengawas.geojson')}}",
				format: new ol.format.GeoJSON({ })
			});
			 

			var LastSource = new ol.source.Cluster({
			  source: LastDataSource2
			});

			 


			var lastPosisiLayer2 = new ol.layer.Vector({
			  source: LastDataSource2,
			  opacity:0.7,
			  name:'lastposisi',
			  visible:true
			});

			mapFP.addLayer(lastPosisiLayer2);


			// untuak tracking autoplay
			  function selectFrancePoints(feature, resolution)
			  {
				var radiusPoints = 8;
			   
				var textKdisisi = feature.get('name');
				var styles = [new ol.style.Style({
						fill: new ol.style.Fill({
							color: 'rgba(255, 255, 255, 0.2)'
						  }),
						  stroke: new ol.style.Stroke({
							color: '#ffcc33',
							width: 3
						  }),
				
							image: new ol.style.Circle({
								fill: new ol.style.Fill({
								  color: '#ff0000'
								}),
								stroke: new ol.style.Stroke({
								  color: '#ffccff'
								}),
								radius: 8
							  }),
							  text: 'bla'

							})
						
					  ];
				return styles;
			  }

			  
			   var trackSelect = new ol.interaction.Select({
				condition: ol.events.condition.altShiftKeysOnly,
				style: selectFrancePoints
			   });
			   mapFP.addInteraction(trackSelect);

			view = mapFP.getView(); 
				$( "tr td button").click(function() { 
					
					var lon = $(this).attr('lat'); // kebalik datanyo
					var lat = $(this).attr('lon');
					
					// var lon = lon.toFixed(3); 
					// var lat = lat.toFixed(3); 
					

					var lon = parseFloat(lon.replace(/\s/g, "").replace(",", ".")) + 0;
					var lat = parseFloat(lat.replace(/\s/g, "").replace(",", ".")) + 0;
				  

					var london = ol.proj.fromLonLat([lon,lat]);
					 var pan = ol.animation.pan({
						duration: 2000, 
						source: /** @type {ol.Coordinate} */ (view.getCenter())
					});
					mapFP.beforeRender(pan);
					view.setCenter(london); 
					

					//masuan ka inteaktion    
					trackSelect.getFeatures().clear();
					var features = trackSelect.getFeatures();  
					// now you have an ol.Collection of features that you can add features to 
					var feature = new ol.Feature({
						  geometry: new ol.geom.Point(ol.proj.transform([lon,lat], 'EPSG:4326', 'EPSG:3857')),
						  name: ''
					}); 
					features.push(feature);

				});

			</script>