<div class="card widget">
	<div class="card-header ch-alt m-b-20">
		<h2>Grafik Frekuensi Kapal per <span class="category_name"></span> </h2>
		<small>Kesuluruhan data Kapal per <span class="category_name"></span></small>
			<ul class="actions">
			<li>
				<a data-widgster="fullscreen" href="#">
					<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
				</a>
			</li>
			<li>
				<a data-widgster="restore" href="#">
					<i class="zmdi zmdi-window-restore"></i>
				</a>
			</li>
			<li>
				<a onClick="closeLayer('#layer2')">
					<i class="zmdi zmdi-close"></i>
				</a>
			</li>
		</ul>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div id="kapalGroup" class="table-responsive" style="height:500px">
				<table id="kapalByAlatTangkap" class="table table-striped">
					<thead>
						<tr>
							<th data-column-id="id" data-type="numeric" data-visible="false" data-identifier="true">ID</th>
							<th class="category_name" data-column-id="category"></th>
							<th data-column-id="persentase" data-type="numeric" data-order="desc">Persentase</th>
						</tr>
					</thead>
					<tbody id="dataDetailChart">
						
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-sm-6">
			<div id="kapalByAlatTangkapChart" style="width:100%;"></div>
		</div>
	</div>
</div>

<script type="text/javascript"> kapalByAlatTangkapChart
	AutoResizeDiv('#kapalByAlatTangkapChart',0);

	jQuery(".category_name").html(jQuery("#KapalCombo option:selected").text());
	jQuery(document).ready(function(){
		var objectData = new Object();

		var NegaraData = "<tr><td>1</td><td>Indonesia</td><td>76.950916</td></tr><tr><td>2</td><td>Thailand</td><td>8.414386</td></tr><tr><td>3</td><td>P. R. Of China</td><td>4.953630</td></tr><tr><td>4</td><td>Taiwan</td><td>3.981000</td></tr><tr><td>5</td><td>Philippines</td><td>2.171454</td></tr><tr><td>6</td><td>Jepang</td><td>1.311920</td></tr><tr><td>7</td><td>Belize</td><td>0.339290</td></tr><tr><td>8</td><td>Australia</td><td>0.294051</td></tr><tr><td>9</td><td>Fuzhou (China)</td><td>0.294051</td></tr><tr><td>10</td><td>-</td><td>0.203574</td></tr><tr><td>11</td><td>Panama</td><td>0.203574</td></tr><tr><td>12</td><td>Korea Selatan</td><td>0.135716</td></tr><tr><td>13</td><td>Vietnam</td><td>0.113097</td></tr><tr><td>14</td><td>Dalian (China)</td><td>0.090477</td></tr><tr><td>15</td><td>Singapura</td><td>0.067858</td></tr><tr><td>16</td><td>Honduras</td><td>0.067858</td></tr><tr><td>17</td><td>Qing Dao (China)</td><td>0.045239</td></tr><tr><td>18</td><td>Korea</td><td>0.045239</td></tr><tr><td>19</td><td>Cambodia</td><td>0.045239</td></tr><tr><td>20</td><td>Saudi Arabia</td><td>0.045239</td></tr><tr><td>21</td><td>Hongkong</td><td>0.045239</td></tr><tr><td>22</td><td>Kamboja</td><td>0.022619</td></tr><tr><td>23</td><td>Malaysia</td><td>0.022619</td></tr><tr><td>24</td><td>Sierra Leone</td><td>0.022619</td></tr><tr><td>25</td><td>India</td><td>0.022619</td></tr><tr><td>26</td><td>Kaoh Shiung (Taiwan)</td><td>0.022619</td></tr><tr><td>27</td><td>Myanmar</td><td>0.022619</td></tr><tr><td>28</td><td>Belanda</td><td>0.022619</td></tr><tr><td>29</td><td>RRC</td><td>0.022619</td></tr>";
		
		var AlatTangkapData = "<tr><td>1</td><td>Purse Seine (Pukat Cincin) Pelagis Kecil</td><td>29.224157</td></tr><tr><td>2</td><td>Rawai Tuna</td><td>14.612079</td></tr><tr><td>3</td><td>Pukat Ikan</td><td>13.865641</td></tr><tr><td>4</td><td>Pengangkut</td><td>12.621579</td></tr><tr><td>5</td><td>Bouke ami</td><td>10.517982</td></tr><tr><td>6</td><td>Jaring insang oseanik</td><td>3.95838</td></tr><tr><td>7</td><td>Pukat cincin Pelagis Besar dengan satu kapal</td><td>3.505994</td></tr><tr><td>8</td><td>Pancing Cumi (squid jigging)</td><td>2.488125</td></tr><tr><td>9</td><td>Rawai dasar</td><td>2.465506</td></tr><tr><td>10</td><td>Huhate</td><td>1.922642</td></tr><tr><td>11</td><td>Pukat Udang</td><td>1.130966</td></tr><tr><td>12</td><td>Jaring liong bun</td><td>1.017869</td></tr><tr><td>13</td><td>Pancing Ulur</td><td>0.99525</td></tr><tr><td>14</td><td>Pukat cincin grup pelagis besar </td><td>0.927392</td></tr><tr><td>15</td><td>Pukat cincin grup pelagis kecil</td><td>0.610722</td></tr><tr><td>16</td><td>Jaring insang hanyut (Driftnets)</td><td>0.090477</td></tr><tr><td>17</td><td>Payang</td><td>0.045239</td></tr>";
		
		var ProviderData = "<tr><td>1</td><td>SOGIndonesia</td><td>57.950690</td></tr><tr><td>2</td><td>CLS</td><td>20.402624</td></tr><tr><td>3</td><td>Amalgam</td><td>9.567971</td></tr><tr><td>4</td><td>PSN</td><td>6.378647</td></tr><tr><td>5</td><td>MSP</td><td>2.261932</td></tr><tr><td>6</td><td></td><td>2.103596</td></tr><tr><td>7</td><td>0</td><td>1.334540</td></tr>";
		
		var PelabuhanData = "<tr><td>1</td><td>Purse Seine (Pukat Cincin) Pelagis Kecil</td><td>29.224157</td></tr><tr><td>2</td><td>Rawai Tuna</td><td>14.612079</td></tr><tr><td>3</td><td>Pukat Ikan</td><td>13.865641</td></tr><tr><td>4</td><td>Pengangkut</td><td>12.621579</td></tr><tr><td>5</td><td>Bouke ami</td><td>10.517982</td></tr><tr><td>6</td><td>Jaring insang oseanik</td><td>3.95838</td></tr><tr><td>7</td><td>Pukat cincin Pelagis Besar dengan satu kapal</td><td>3.505994</td></tr><tr><td>8</td><td>Pancing Cumi (squid jigging)</td><td>2.488125</td></tr><tr><td>9</td><td>Rawai dasar</td><td>2.465506</td></tr><tr><td>10</td><td>Huhate</td><td>1.922642</td></tr><tr><td>11</td><td>Pukat Udang</td><td>1.130966</td></tr><tr><td>12</td><td>Jaring liong bun</td><td>1.017869</td></tr><tr><td>13</td><td>Pancing Ulur</td><td>0.99525</td></tr><tr><td>14</td><td>Pukat cincin grup pelagis besar </td><td>0.927392</td></tr><tr><td>15</td><td>Pukat cincin grup pelagis kecil</td><td>0.610722</td></tr><tr><td>16</td><td>Jaring insang hanyut (Driftnets)</td><td>0.090477</td></tr><tr><td>17</td><td>Payang</td><td>0.045239</td></tr>";
		
		jQuery("#dataDetailChart").html(eval(jQuery("#KapalCombo").val() + "Data"));
		
		var pieChart = AmCharts.makeChart("kapalByAlatTangkapChart",
		{
			"type": "pie",
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			"labelText": "[[percents]]%",
			"startAngle": 0,
			"titleField": "category",
			"valueField": "column-1",
			
			"legend": {
				"align": "center",
				"labelText": "[[title]] ",
				"markerType": "circle",
				"valueText": ""
			},
			"allLabels": [],
			"balloon": {},
			"titles": [],
			"autoMargins": false,
			"marginLeft": 0,
			"marginRight": 0,
			"pullOutRadius": 0,
			"dataProvider": eval(jQuery("#KapalCombo").val())
			}
		);
		
		function reloadChart(objectData){
			pieChart.dataProvider = [];
			jQuery.each( objectData, function( key, item ) {
				pieChart.dataProvider.push({
					"category": item.category,
					"column-1": item.persentase
				});			
			});
			pieChart.validateData();
		}

		$("#kapalByAlatTangkap").bootgrid({
			/*
			ajax: true,
			post: function ()
			{
				* To accumulate custom parameter with the request object *
				return {
					id: "b0df282a-0d67-40e5-8558-c9e93b7befed"
				};
			},*/
			rowCount: -1,
			navigation:0,    // Turn of paging.
			caseSensitive:false,
			//url: "data-kapal-alat-tangkap.php",
			selection: true,
			multiSelect: true,
			rowSelect: true,
			keepSelection: true
		}).on("selected.rs.jquery.bootgrid", function(e, rows)
		{
			for (var i = 0; i < rows.length; i++)
			{
				objectData[rows[i].id] = {
					"category" : rows[i].category,
					"persentase" : rows[i].persentase
				};
			}
			reloadChart(objectData);
		}).on("deselected.rs.jquery.bootgrid", function(e, rows)
		{
			for (var i = 0; i < rows.length; i++)
			{
				delete objectData[rows[i].id];
			}
			reloadChart(objectData);
		});

		reloadWidget();
	});

</script>
