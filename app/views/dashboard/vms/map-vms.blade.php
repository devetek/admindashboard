			<div class="pl-body"  style="width: 100%; height: 100%; " id="lastPosisi">
				 
			</div> 

			<script type="text/javascript">
				AutoResizeDiv('#lastPosisi',20);
				 var styles = [
					  'Road',
					  'Aerial',
					  'AerialWithLabels',
					  'collinsBart',
					  'ordnanceSurvey'
					];
					var layers = [];
					var i, ii;
					for (i = 0, ii = styles.length; i < ii; ++i) {
					  layers.push(new ol.layer.Tile({
						visible: false,
						preload: Infinity,
						source: new ol.source.BingMaps({
						  key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3',
						  imagerySet: styles[i]
						  // use maxZoom 19 to see stretched tiles instead of the BingMaps
						  // "no photos at this zoom level" tiles
						  // maxZoom: 19
						})
					  }));
					}
					var map = new ol.Map({
					  layers: layers,
					  // Improve user experience by loading tiles while dragging/zooming. Will make
					  // zooming choppy on mobile or slow devices.
					  loadTilesWhileInteracting: true,
					  target: 'lastPosisi',
					  view: new ol.View({
						center: ol.proj.transform([110,0], 'EPSG:4326', 'EPSG:3857'),
						zoom: 4
					  })
					});

					var select = document.getElementById('layer-select');
					function onChange() {
					  var style = select.value;
					  for (var i = 0, ii = layers.length; i < ii; ++i) {
						layers[i].setVisible(styles[i] === style);
					  }
					}
					select.addEventListener('change', onChange);
					onChange();
					
					function updateSizeMap(){
						map.updateSize();
					}
					
					var maxFeatureCount;
						var styleCache = {};
						function calculateClusterInfo(resolution) {
						  maxFeatureCount = 0;
						  var features = lastPosisiLayer.getSource().getFeatures();
					 

						  var feature, radius;
						  for (var i = features.length - 1; i >= 0; --i) {
							feature = features[i];
							var originalFeatures = feature.get('features'); 

							var extent = ol.extent.createEmpty();
							for (var j = 0, jj = originalFeatures.length; j < jj; ++j) {
							  ol.extent.extend(extent, originalFeatures[j].getGeometry().getExtent());
							}
							maxFeatureCount = Math.max(maxFeatureCount, jj);
							radius =( 0.5 * (ol.extent.getWidth(extent) + ol.extent.getHeight(extent))/ resolution);
							 
							feature.set('radius', radius);  
						  }
						}

						var currentResolution;
						function styleCluster(feature, resolution) { 
							var styleCache = {};
							cfeatures = feature.get('features'); 

							if (resolution != currentResolution) {
								calculateClusterInfo(resolution);
								currentResolution = resolution;
							  }

							var size = feature.get('features').length;
						   
								 
								if(size == 1)
								{                 
									var src = "{{url('public/vendors/openlayer/Styles/Icons/Transportation/boat.png')}}";
									if(cfeatures[0].get('src').length)
									{
										src = cfeatures[0].get('src');
									}
									
									var style = styleCache[size];
									if (!style) {

										style = [new ol.style.Style({
										  image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
											anchor: [cfeatures[0].get('picturescalex'), cfeatures[0].get('picturescaley')],
											opacity: 1,
											anchorXUnits: 'fraction',
											anchorYUnits: 'pixels',
											src: server +  src
										  }))
										})];
									}
									styleCache[size] = style;

								}
								else
								{
									var style = styleCache[size];
									if (!style) {

										radius =feature.get('radius');
										if(feature.get('radius') <= 9 || typeof(feature.get('radius')) =='undefined'){
											radius = 8;
										}

										style = [new ol.style.Style({
													image: new ol.style.Circle({
													  radius: radius,
													  stroke: new ol.style.Stroke({
														color: '#fff',
														width: 1,
														opacity: 0.4
													  }),
													  fill: new ol.style.Fill({
														color: '#3399CC'
													  })
													}),
													text: new ol.style.Text({
													  text: size.toString(),
													  fill: new ol.style.Fill({
														color: '#fff'
													  })
													})
												  })];
									}
									styleCache[size] = style;
								}

								
							//}
							return style;
						};


					 
					var LastDataSource = new ol.source.Vector({
						url: "{{url('public/geojson/last_posisi.geojson')}}",
						format: new ol.format.GeoJSON({ })
					});

					// var geoJSONFormatddd =  new ol.format.GeoJSON({ })
					// var LastDataSource = new ol.source.Vector({
					// 	    strategy: ol.loadingstrategy.bbox,
					// 	    format: new ol.format.GeoJSON(),
					// 	    loader: function(extent, resolution, projection) {
					// 	    var url = 'last_posisi.json';
					// 	    $.ajax({  
					// 	      url: url,
					// 	      useDefaultXhrHeader: false,
					// 	      method:'POST',
					// 	      success: function(data) { 
					// 	        var features = geoJSONFormat.readFeatures(data,{featureProjection: 'EPSG:3857'});

					// 	      	//console.log(features);
					// 			LastDataSource.addFeatures(features);
					// 	      }
					// 	    }); 
					// 	  } 
					// 	});

					var LastSource = new ol.source.Cluster({
					  source: LastDataSource
					});

					 


					var lastPosisiLayer = new ol.layer.Vector({
					  source: LastSource,
					  opacity:0.7,
					  name:'lastposisi',
					  style: styleCluster
					   ,visible:true
					});

					map.addLayer(lastPosisiLayer);


					// untuak full screan
					$('#reloadMapPosisi').on('click',function()
					{
						var tinggi = ($(window).height() - 100);
						var lebar = ($(window).width() - 20);
						
						 $('#lastPosisi canvas').attr('width',lebar);
						 $('#lastPosisi canvas').attr('height',tinggi);
						 
						   if($('#lastPosisi canvas').attr('width') == lebar)
						   {
								 map.updateSize();
						   }
					});



					// untuak tracking autoplay
					  function selectFrancePoints(feature, resolution)
					  {
						var radiusPoints = 8;
						if(typeof ConfInfo.semua_radius != 'undefined')
							{
							  var radiusPoints = ConfInfo.semua_radius;
							}


						var textKdisisi = feature.get('name');
						var styles = [new ol.style.Style({
								fill: new ol.style.Fill({
									color: 'rgba(255, 255, 255, 0.2)'
								  }),
								  stroke: new ol.style.Stroke({
									color: '#ffcc33',
									width: 3
								  }),
						
									image: new ol.style.Circle({
										fill: new ol.style.Fill({
										  color: '#ff0000'
										}),
										stroke: new ol.style.Stroke({
										  color: '#ffccff'
										}),
										radius: 8
									  }),
									  text: styleTextReplay(4,textKdisisi)

									})
								
							  ];
						return styles;
					  }

					  
					   var trackSelect = new ol.interaction.Select({
						//condition: ol.events.condition.altShiftKeysOnly,
						style: selectFrancePoints
					   });
					   map.addInteraction(trackSelect);

			</script>