<div id="mapHomepage"></div>
<div id="popup" class="ol-popup">
<a href="#" id="popup-closer" class="ol-popup-closer"></a>
  <div id="popup-content"></div>
</div>
 
 <div id="toogle-icon" class="toggled">
	<button onClick="hideShowToggle('hotlist')" class="btn bgm-cyan btn-float waves-effect waves-circle waves-float">
		<i class="zmdi zmdi-map"></i>
	</button>
 </div>
 
 <div id="hotlist" class="toggled" style="display:none"> 
	<div class="card-header">
		<h2>Overlays Layer</h2>
		<ul class="actions">
			<li class="dropdown action-show">
				<a href="#" onClick="hideShowToggle('hotlist')">
					<i class="zmdi zmdi-close"></i>
				</a> 
			</li>  
		</ul>
	</div>
					
	<div class="card">		
		<div class="card-body card-padding">	
			
			<div  role="tablist" aria-multiselectable="true" id="panel-accordion" class="panel-group">
				<div class="panel panel-collapse">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								Base Layer
							</a>
						</h4>
					</div>
 
	 
					<div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							<div class="radio m-b-15">
                                <label>
                                    <input type="radio" name="base-layer" value="Aerial">
                                    <i class="input-helper"></i>
                                    Bing Aerial
                                </label>
                            </div>
							
                            <div class="radio m-b-15">
                                <label>
                                    <input type="radio" name="base-layer" value="AerialWithLabels">
                                    <i class="input-helper"></i>
                                    Bing Aerial with labels
                                </label>
                            </div>
                            
                            <div class="radio m-b-15">
                                <label>
                                    <input type="radio" selected name="base-layer" value="Road">
                                    <i class="input-helper"></i>
                                    Bing Road
                                </label>
                            </div>
                            
                            <div class="radio m-b-15">
                                <label>
                                    <input type="radio" name="base-layer" value="collinsBart">
                                    <i class="input-helper"></i>
                                    Bing Collins Bart
                                </label>
                            </div>
                            
                            <div class="radio m-b-15">
                                <label>
                                    <input type="radio" name="base-layer" value="ordnanceSurvey">
                                    <i class="input-helper"></i>
                                    Bing Ordnance Survey
                                </label>
                            </div>
                            
                             
						</div>
					</div>
				</div>
				 
				 
				<div class="panel panel-collapse">
					<div class="panel-heading" role="tab" id="headingTwo">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								Zone WPP
							</a>
						</h4>
					</div>
					<div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
						<div class="panel-body">
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    WPP R1 712 
                                </label>
                            </div>
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    WPP R1 714
                                </label>
                            </div>
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    WPP R1 712 
                                </label>
                            </div>
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    WPP R1 714
                                </label>
                            </div>
							
						</div>
					</div>
				</div>
				
				<div class="panel panel-collapse">
					<div class="panel-heading" role="tab" id="headingTwo">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwoVMS" aria-expanded="false" aria-controls="collapseTwo">
								VMS
							</a>
						</h4>
					</div>
					<div id="collapseTwoVMS" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
						<div class="panel-body">
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    Posisi Kapal Terpantau 
                                </label>
                            </div>
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    Posisi Kapal Pegawas 
                                </label>
                            </div>
							
						</div>
					</div>
				</div>				
				
				<div class="panel panel-collapse">
					<div class="panel-heading" role="tab" id="headingTwo">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwoSDK" aria-expanded="false" aria-controls="collapseTwo">
								SDK
							</a>
						</h4>
					</div>
					<div id="collapseTwoSDK" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
						<div class="panel-body">
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    Pencemaran Laut 
                                </label>
                            </div>
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    kerusakan lahan bakau 
                                </label>
                            </div>
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    terumbu karang 
                                </label>
                            </div>
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    konservasi 
                                </label>
                            </div>
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    bmkt 
                                </label>
                            </div> 
							
						</div>
					</div> 
					
				</div>

				
				<div class="panel panel-collapse">
					<div class="panel-heading" role="tab" id="headingTwo">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwoKP" aria-expanded="false" aria-controls="collapseTwo">
								Kapal Pengawas
							</a>
						</h4>
					</div>
					<div id="collapseTwoKP" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
						<div class="panel-body">
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    gelar operai 
                                </label>
                            </div>
							
							<div class="checkbox  m-b-15">
                                <label>
                                    <input type="checkbox" name="vms-layer" value="terpantau">
                                    <i class="input-helper"></i>
                                    penakapan 
                                </label>
                            </div>
							 
							
						</div>
					</div> 
					
				</div>				
				 
			</div>
			<hr>
			<button onClick="tampilLegend()" class="btn btn-default btn-icon-text waves-effect"><i class="zmdi zmdi-pin-help"></i> Show Legend</button>
			
		</div>
	</div>
	
 </div>
 
 <div id="legenHomepage" style="display:none" class="col-md-3"> 
	<div class="card">	
		<div class="card-header">
			<h2>Overlays Layer</h2>
			<ul class="actions">
				<li class="dropdown action-show">
					<a href="#" onClick="tampilLegend()">
						<i class="zmdi zmdi-close"></i>
					</a> 
				</li>  
			</ul>
		</div>
					
		
		<div id="legen" class="card-body card-padding"> </div>
		  
	</div>
 
 </div>
 
 <script type="text/javascript"> 
		$( "#legenHomepage" ).draggable();
		
		function hideShowToggle(id)
		{
			if($('#' + id).hasClass('deactive'))
			{
				$('#' + id).removeClass('deactive');
				$('#' + id).show(400); 
				$('#toogle-icon').hide(400);
			}
			else
			{
				$('#' + id).addClass('deactive');		
				
				$('#' + id).hide(400);
				$('#toogle-icon').show(400);
			}
		}
		
		function tampilLegend()
		{
			if ( $('#legenHomepage').is(':visible' ))
			{
				$('#legenHomepage').hide();
			}
			else
			{
				$('#legenHomepage').show();
			}
			customHeight('#legenHomepage .card',1,50); 
			customHeight('#legen',1,150); 
		}
		
		
		customHeight('#panel-accordion',2,100);
		customHeight('.card1',2,0);
		customHeight('#mapHomepage',2,0); 
		  
		
		var container = document.getElementById('popup');
		var content = document.getElementById('popup-content');
		var closer = document.getElementById('popup-closer');

		closer.onclick = function() {
		  overlay.setPosition(undefined);
		  closer.blur();
		  return false;
		};

		var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
		  element: container,
		  autoPan: true,
		  autoPanAnimation: {
			duration: 250
		  }
		}));

		 var server= '';
			var styles = [
			  'Road',
			  'Aerial',
			  'AerialWithLabels',
			  'collinsBart',
			  'ordnanceSurvey'
			];
			var layers = [];
			var i, ii;
			for (i = 0, ii = styles.length; i < ii; ++i) {
			  layers.push(new ol.layer.Tile({
				visible: false,
				preload: Infinity,
				source: new ol.source.BingMaps({
				  key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3',
				  imagerySet: styles[i]
				  // use maxZoom 19 to see stretched tiles instead of the BingMaps
				  // "no photos at this zoom level" tiles
				  
				})
			  }));
			}
			
			var minZoom = function()
			{
				if(MaterHeight > 900)
				{
					return 4;
				}
				else
				{
					return 3;
				}
			}
			
			var map = new ol.Map({
			  layers: layers,
			  // Improve user experience by loading tiles while dragging/zooming. Will make
			  // zooming choppy on mobile or slow devices.
			  loadTilesWhileInteracting: true,
			  overlays: [overlay],
			  target: 'mapHomepage',
			  view: new ol.View({
				center: ol.proj.transform([110,0], 'EPSG:4326', 'EPSG:3857'),
				zoom: 5,
				minZoom: minZoom(),
				maxZoom: 19
			  })
			});
 
			$('input[type=radio][name=base-layer]').change(function() {
				var style = this.value;
				console.log('sdsd000');
				for (var i = 0, ii = layers.length; i < ii; ++i) {
					layers[i].setVisible(styles[i] === style);
				}
			});
			
			layers[0].setVisible(styles[0]);

			
			
			function updateSizeMap(){
				map.updateSize();
			}
			
			
		//layer point
		var LastDataSource2 = new ol.source.Vector({
			url: "{{url('public/geojson/pengawas.geojson')}}",
			format: new ol.format.GeoJSON({ })
		});
		 

		var LastSource = new ol.source.Cluster({
		  source: LastDataSource2
		});

		 


		var lastPosisiLayer2 = new ol.layer.Vector({
		  source: LastDataSource2,
		  opacity:0.7,
		  name:'lastposisi',
		  visible:true
		});

		map.addLayer(lastPosisiLayer2);

 
		function selectFrancePoints(feature, resolution)
		{
			var radiusPoints = 8;
		   
			var textKdisisi = feature.get('name');
			var styles = [new ol.style.Style({
					fill: new ol.style.Fill({
						color: 'rgba(255, 255, 255, 0.2)'
					  }),
					  stroke: new ol.style.Stroke({
						color: '#ffcc33',
						width: 3
					  }),
			
						image: new ol.style.Circle({
							fill: new ol.style.Fill({
							  color: '#ff0000'
							}),
							stroke: new ol.style.Stroke({
							  color: '#ffccff'
							}),
							radius: 8
						  }),
						  text: 'bla'

						})
					
				  ];
			return styles;
		}
			  


		/**
		 * Add a click handler to the map to render the popup.
		 */


		map.on('click', function(e) {
		  var coordinate = e.coordinate;
		  var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
				coordinate, 'EPSG:3857', 'EPSG:4326'));
			
		   map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
				//do something
				//infoBox.innerHTML="testtt,testt";
				//console.log(feature.get('val'));
				if(feature.get('line-width') > 0)
				{
					 content.innerHTML = '<p>Detail Pengangkapan Kapal:</p><code>' + hdms + '</code>';
					 content.innerHTML+= '<p>Jumlah Kapal : <b>'+ feature.get('line-width') + '</b><p>';
					 content.innerHTML+= '<p>WPP : <b>'+ feature.get('name') + '</b><p>';
					   overlay.setPosition(coordinate);
					  
				}
				else
				{

				}
				

			}); 
		});



		//map
		var valueData=[]; 
		
		var jenis='line-width';                  
		var stylePangan = function(feature, resolution) { 
				var text = feature.get(jenis);
				//text = text.replace(',','.');
				console.log('sds');

				cekBanyakValue(jenis);

				var Max = Math.max.apply(Math,valueData);
				var Min = Math.min.apply(Math,valueData);

				var total = (Max - Min) / 8;

				// console.log(Min);
				// console.log(total);
				// console.log(text);

				makeLegen(warnaBg2 , total, jenis);

				var indexBg=0;

				if(text < total)
				{
					var indexBg=0;
				}

				else if(text < (total *2) )
				{
					var indexBg=1;
				}
				else if(text < (total *3) )
				{
					var indexBg=2;
				}
				else if(text < (total *4) )
				{
					var indexBg=3;
				}
				else if(text < (total *5) )
				{
					var indexBg=4;
				}
				else if(text < (total *6) )
				{
					var indexBg=5;
				}
				else if(text < (total *7) )
				{
					var indexBg=6;
				}
				else
				{
					var indexBg=7; //samo jo index 8
				}

				//var color = ['#D2527F','#663399','#446CB3','#4183D7','#52B3D9','#22A7F0','#81CFE0']
				return [new ol.style.Style({
					stroke: new ol.style.Stroke({
					  color: '#52403C',
					  width: 0.1
					}),
					fill: new ol.style.Fill({
					  color: warnaBg2[indexBg]
					}),
					
					text: new ol.style.Text({
					  text: text, 
					  scale: 1, 
					  fill: new ol.style.Fill({
						color: '#fff'
					  }),

					  stroke: new ol.style.Stroke({
						color: '#FFFF99',
						width: 1
					  })
					})

				  })];
			};

			function  cekBanyakValue(data)
			{
				valueData=[];      
				vectorLayer.getSource().forEachFeature(function(e) {
					valueData.push(e.get(data) );  
				});
				//console.log(valueData);
				 
			}

			function makeLegen(color,interval,judul)
			{
				var legen = document.getElementById('legen');
				var html='<span>Keterangan</span>';
					html+='<table class="legen-table table table-bordered">';
				
				var count = 5 ;
				  for(a=0;a<8;a++)
				  {
					html+='<tr>';
					html+='<td style="background :' + color[a] + '; padding: 10px; */"></td>';
					html+='<td> <= ' +  (count)  +' Kapal</td>';
					count = count + 5;
					//html+='<td> <= ' +  (interval * (a + 1))  +' Kapal</td>';
					html+='</tr>';
				  }
				  
					html+='<tr>';
					html+='<td style="background : #cc0000; padding: 10px; */"></td>';
					count = count - 5;
					html+='<td> > ' +  (count)  +' Kapal</td>';
					//html+='<td> <= ' +  (interval * (a + 1))  +' Kapal</td>';
					html+='</tr>';

				
				html+='</table><br>';
				legen.innerHTML=html;

			}

			$('#reloadMapTangkap').on('click',function()
			{
				
				var tinggi = ($(window).height() - 100);
				var lebar = ($(window).width() - 20);
				
				 $('#maptangkap canvas').attr('width',lebar);
				 $('#maptangkap canvas').attr('height',tinggi);
				  map.updateSize();
				  
				  console.log(tinggi);
				   // if($('#maptangkap canvas').attr('width') == lebar)
				   // {
						// console.log(lebar);
						 // map.updateSize();
				   // }
					
				
			});


		 
		var LastDataSource = new ol.source.Vector({
			url: "{{url('public/geojson/zone_pelanggaran.geojson')}}",
			format: new ol.format.GeoJSON({ })
		});
		 
		var LastSource = new ol.source.Cluster({
		  source: LastDataSource
		});

		 


		var vectorLayer = new ol.layer.Vector({
		  source: LastDataSource,
		  opacity:0.7,
		  name:'lastposisi' ,
		  style:stylePangan,
		  opacity:0.5
		});

		map.addLayer(vectorLayer);
	</script>