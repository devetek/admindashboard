			<div id="1chartdiv" style="width:100%; height:400px;"></div>
			<script type="text/javascript">
				AmCharts.makeChart("1chartdiv",
				{
					"type": "pie",
					"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"startAngle": 244.8,
					"hideLabelsPercent": 10,
					"titleField": "category",
					"valueField": "column-1",
					"allLabels": [],
					"balloon": {},
					"titles": [],
					"dataProvider": {{$Model}}
				}
			)
			</script>
	