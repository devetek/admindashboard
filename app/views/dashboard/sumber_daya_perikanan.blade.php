@extends('shared.master-dashboard')

@section('content')

	<div class="container" id="data">
				<div class="mini-charts col-sm-12">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div class="mini-charts-item bgm-cyan" style="cursor: pointer;">
                                    <div class="clearfix">
                                        <div class="count">
                                            <small>Jumlah SLO</small>
                                            <h2>2,078</h2>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                            
                            <div class="col-sm-6 col-md-3">
                                <div class="mini-charts-item bgm-lightgreen" style="cursor: pointer;">
                                    <div class="clearfix">
                                        <div class="count">
                                            <small>HPK Keberangkatan (Laik)</small>
                                            <h2>10,744</h2>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                            
                            <div class="col-sm-6 col-md-3">
                                <div class="mini-charts-item bgm-orange" style="cursor: pointer;">
                                    <div class="clearfix">
                                         <div class="count">
                                            <small>HPK Kedatangan</small>
                                            <h2>4,576</h2>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                            
                            <div class="col-sm-6 col-md-3">
                                <div class="mini-charts-item bgm-bluegray" style="cursor: pointer;">
                                    <div class="clearfix">
                                        <div class="count">
                                            <small>Jumlah Tangkapan</small>
                                            <h2>48,431,856.42</h2>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>

				
					<div class="col-sm-12">
                        <div class="card widget"
							<div class="card widget"
							data-widgster-load="{{url('prototype/sumber_daya_perikanan/chart_ikan_pertahun')}}"
							data-widgster-autoload="true"
							data-widgster-show-loader="true">
							<div class="card-header">
								<h2>Grafik Perbandingan Hasil Tangkapan Pertahun</h2>
								
								<ul class="actions">
								<li>
									<a data-widgster="load" href="#">
										<i class="zmdi zmdi-refresh-alt"></i>
									</a>
								</li>
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li>
								<li class="dropdown">
									<a href="" data-toggle="dropdown">
										<i class="zmdi zmdi-more-vert"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
										</li>
									</ul>
								</li>
							</ul> 
							</div>
							
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-8">
                        <div class="card2 widget"
						data-widgster-load="{{url('prototype/sumber_daya_perikanan/chart_jenis_ikan_perbulan')}}"
						data-widgster-autoload="true"
						data-widgster-show-loader="true">
                            <div class="card-header">
                                <h2>Grafik Perbandingan Hasil Tangkapan Ikan Perbulan</h2>
                                
                                <ul class="actions">
								<li>
									<a data-widgster="load" href="#">
										<i class="zmdi zmdi-refresh-alt"></i>
									</a>
								</li>
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li>
								<li class="dropdown">
									<a href="" data-toggle="dropdown">
										<i class="zmdi zmdi-more-vert"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
										</li>
									</ul>
								</li>
							</ul> 
							</div>
                            
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
                        </div>
                    </div>
					 
					<div class="col-sm-4">
                         <!-- Picture List -->
						<div class="card widget"
						data-widgster-load="{{url('prototype/sumber_daya_perikanan/chart_jenis_pengawasan_p3')}}"
						data-widgster-autoload="true"
						data-widgster-show-loader="true">
							<div class="card-header">
								<h2>Grafik Pengawasan P3</h2>
								
								<ul class="actions">
								<li>
									<a data-widgster="load" href="#">
										<i class="zmdi zmdi-refresh-alt"></i>
									</a>
								</li>
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li>
								<li class="dropdown">
									<a href="" data-toggle="dropdown">
										<i class="zmdi zmdi-more-vert"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
										</li>
									</ul>
								</li>
							</ul> 
							</div>
							
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-4">
                         <!-- Picture List -->
						<div class="card widget"
						data-widgster-load="{{url('prototype/sumber_daya_perikanan/chart_jenis_pengawasan_berpangkalan')}}"
						data-widgster-autoload="true"
						data-widgster-show-loader="true">
							<div class="card-header">
								<h2>Grafik Data Ketaatan Berpangkalan</h2>
								
								<ul class="actions">
								<li>
									<a data-widgster="load" href="#">
										<i class="zmdi zmdi-refresh-alt"></i>
									</a>
								</li>
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li>
								<li class="dropdown">
									<a href="" data-toggle="dropdown">
										<i class="zmdi zmdi-more-vert"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
										</li>
									</ul>
								</li>
							</ul> 
							</div>
							
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-4">
                	 <!-- Picture List -->
						<div class="card widget"
						data-widgster-load="{{url('prototype/sumber_daya_perikanan/chart_jenis_pengawasan_budidaya')}}"
						data-widgster-autoload="true"
						data-widgster-show-loader="true">
							<div class="card-header">
								<h2>Pengawasan Usaha Budidaya</h2>
								
								<ul class="actions">
								<li>
									<a data-widgster="load" href="#">
										<i class="zmdi zmdi-refresh-alt"></i>
									</a>
								</li>
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li>
								<li class="dropdown">
									<a href="" data-toggle="dropdown">
										<i class="zmdi zmdi-more-vert"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
										</li>
									</ul>
								</li>
							</ul> 
							</div>
							
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						 <div class="card1 custom widget"
						data-widgster-load="{{url('prototype/sumber_daya_perikanan/detail_keberangkatan_kapal')}}"
						data-widgster-autoload="true"
						data-widgster-show-loader="true">
                            <div class="card-header">
                                <h2>Daftar Keberangkatan Kapal Hari Ini</h2>
                                
                                <ul class="actions">
								<li>
									<a data-widgster="load" href="#">
										<i class="zmdi zmdi-refresh-alt"></i>
									</a>
								</li>
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li>
								<li class="dropdown">
									<a href="" data-toggle="dropdown">
										<i class="zmdi zmdi-more-vert"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
										</li>
									</ul>
								</li>
							</ul> 
							</div>
                            
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
                        </div>
					</div>

					<script type="text/javascript">
					 	customHeight('.card1',1,0);
					 </script>					

@stop

@section('javascript')

		
		 @stop
