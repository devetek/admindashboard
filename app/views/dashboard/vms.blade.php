@extends('shared.master-dashboard')

@section('content')
	
	
	<div class="col-sm-6 col-md-2 padding-1">
		<div class="mini-charts-item bgm-lightgreen pointer" onclick="LoadLayer('#layer2','{{url('dashborad/vms/list-SKAT-detail')}}','#data')">
			<div class="clearfix">
			   <div class="count">
					<small>Total SKAT berlaku</small>
					<h2><span class="jumlah">{{$mJumlahSkatAktif}}</span> Kapal</h2> 
				</div>
			</div> 
		</div>

		 <div class="mini-charts-item bgm-bluegray pointer" onclick="LoadLayer('#layer2','{{url('dashborad/vms/list-skat_nonaktif-detail')}}','#data')">
			<div class="clearfix">
				<div class="count">
					<small>Total SKAT akan habis</small>
					<h2><span class="jumlah">{{$mJumlahSkatAkanHabis}}</span> Kapal</h2>
				</div>
			</div> 
		</div> 

		<div class="mini-charts-item bgm-lightgreen" onclick="LoadLayer('#layer2','{{url('dashborad/vms/list-kapal_terpantau-detail')}}','#data')">
			<div class="clearfix">
				<div class="count">
					<small>Kapal Terpantau</small>
					<h2><span class="jumlah">{{$mJumlahKapalTerpantau}}</span> Kapal</h2>
				</div>
			</div> 
		</div> 

		 <div class="mini-charts-item bgm-orange" onclick="LoadLayer('#layer2','{{url('dashborad/vms/list-kapal_tidak_terpantau-detail')}}','#data')">
			<div class="clearfix">
				<div class="count">
					<small>Kapal Tidak Terpantau</small>
					<h2><span class="jumlah">{{$mJumlahKapalNonTerpantau}}</span> Kapal</h2>
				</div>
			</div> 
		</div> 
	</div>

	<div class="col-sm-6 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/vms/map-vms')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Posisi Kapal Pengawas
					<small class="hidden">
						 <div class="row-fluid">
						   <div class="span12">
							 <div id="map" class="map"></div>
							 <select id="layer-select">
							   <option value="Aerial">Aerial</option>
							   <option value="AerialWithLabels">Aerial with labels</option>
							   <option value="Road" selected>Road</option>
							   <option value="collinsBart">Collins Bart</option>
							   <option value="ordnanceSurvey">Ordnance Survey</option>
							 </select>
						   </div>
						 </div>
					</small>
				</h2>
				<small>Informasi terakhir pertanggal <span id="tgl-update-map">-</span></small>
				
				<ul class="actions">
				<li>
					<a id="reloadMapPosisi" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a  data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
			</ul>
			</div>
			<div class="card-body">
				<div class="body">
				</div>
			</div>
		</div>
                        
		<script type="text/javascript">
			jQuery(document).ready(function(){
				var $widget_map = $('.widget-map')
				$widget_map.widgster();

				$widget_map.on("fullscreen.widgster", function(){
					$('.content-wrap').css({
						'-webkit-transform': 'none',
						'-ms-transform': 'none',
						transform: 'none',
						'margin': 0,
						'z-index': 2
					});
					jQuery("#lastPosisi").height("");
					//updateSizeMap();
				}).on("restore.widgster closed.widgster", function(){
					$('.content-wrap').css({
						'-webkit-transform': '',
						'-ms-transform': '',
						transform: '',
						margin: '',
						'z-index': ''
					});
					jQuery("#lastPosisi").height("");
					//map.updateSize();
				});
			});
		</script>
		
	</div>
   

	<div class="col-sm-6 col-md-4 padding-1">
		 
		<div class="card widget" 
		data-widgster-load="{{url('vms/listIndikasiPelanggaran')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>List Indikasi Pelanggaran</h2>
				<small>Informasi terakhir pertanggal <span id="tgl-update-indikasi">-</span></small>
				<ul class="actions">
				<li>
					<a data-widgster="load" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				<li class="dropdown">
					<a href="" data-toggle="dropdown">
						<i class="zmdi zmdi-more-vert"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>	
						</li>
					</ul>
				</li>
			</ul>
			</div>
			
			<div class="card-body">
				<div class="body m-t-0 wrapper" id="listIndikasi">
				
				</div>
			</div>
		</div> 
	</div> 
	
	 <div class="col-sm-6 col-md-8 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('vms/GrafikPelanggaran')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Grafik Indikasi Pelanggaran</h2>
				<small>Periode tanggal <span id="tgl-periode-grafik">-</span></small>
				<ul class="actions">
				<li>
					<a data-widgster="load" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				<li class="dropdown">
					<a href="" data-toggle="dropdown">
						<i class="zmdi zmdi-more-vert"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
						</li>
					</ul>
				</li>
			</ul>
			</div>
			
			<div class="card-body">
				<div class="body">
					<div class="pl-body" id="">
						<div style="width: 100%;" id="chartdivchartdiv">
						 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="col-sm-6 col-md-4 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('vms/chartKomposisi')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
		<div class="card-header">
			<h2>Grafik Komposisi Kapal</h2>
			<small>Informasi diambil dari data 5 Teratas dan Lainnya</small>
			 
			<ul class="actions">
				<li>
					<a data-widgster="load" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				<li class="dropdown">
					<a href="" data-toggle="dropdown">
						<i class="zmdi zmdi-more-vert"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a onClick="LoadLayer('#layer2','{{url('prototype/vms/chart-komposisi-detail')}}','#data')">View Detail</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="card-body card-padding m-t-0">
			<div class="body">
			</div>
		</div>
	</div> 
		
	</div>   
                     
@stop

@section('javascript')
	
    {{ HTML::style('public/vendors/openlayer/ol.css')}}
	{{ HTML::script('public/vendors/openlayer/ol.js')}}
	
	<script type="text/javascript">
	 
	 </script>

@stop
