			<div class="col-md-9">
			     

				 <div id="popup" class="ol-popup">
				  <a href="#" id="popup-closer" class="ol-popup-closer"></a>
				  <div id="popup-content"></div>
				</div>
			<div class="pl-body" id="maptangkap"  > </div> 
			</div> 
			<div id="legen" class="col-md-3">
			</div>

			<script type="text/javascript">
				AutoResizeDiv('#maptangkap',20);
				AutoResizeDiv('#legen',20);

				 var container = document.getElementById('popup');
					var content = document.getElementById('popup-content');
					var closer = document.getElementById('popup-closer');

					closer.onclick = function() {
					  overlay.setPosition(undefined);
					  closer.blur();
					  return false;
					};

					var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
					  element: container,
					  autoPan: true,
					  autoPanAnimation: {
						duration: 250
					  }
					}));

					 var server= '';
						var styles = [
						  'Road',
						  'Aerial',
						  'AerialWithLabels',
						  'collinsBart',
						  'ordnanceSurvey'
						];
						var layers = [];
						var i, ii;
						for (i = 0, ii = styles.length; i < ii; ++i) {
						  layers.push(new ol.layer.Tile({
							visible: false,
							preload: Infinity,
							source: new ol.source.BingMaps({
							  key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3',
							  imagerySet: styles[i]
							  // use maxZoom 19 to see stretched tiles instead of the BingMaps
							  // "no photos at this zoom level" tiles
							  // maxZoom: 19
							})
						  }));
						}
						var map = new ol.Map({
						  layers: layers,
						  // Improve user experience by loading tiles while dragging/zooming. Will make
						  // zooming choppy on mobile or slow devices.
						  loadTilesWhileInteracting: true,
						  overlays: [overlay],
						  target: 'maptangkap',
						  view: new ol.View({
							center: ol.proj.transform([110,0], 'EPSG:4326', 'EPSG:3857'),
							zoom: 4
						  })
						});

						var select = document.getElementById('layer-select');
						function onChange() {
						  var style = select.value;
						  for (var i = 0, ii = layers.length; i < ii; ++i) {
							layers[i].setVisible(styles[i] === style);
						  }
						}
						select.addEventListener('change', onChange);
						onChange();
						
						function updateSizeMap(){
							map.updateSize();
						}


					/**
					 * Add a click handler to the map to render the popup.
					 */


					map.on('click', function(e) {
					  var coordinate = e.coordinate;
					  var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
							coordinate, 'EPSG:3857', 'EPSG:4326'));
						
					   map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
							//do something
							//infoBox.innerHTML="testtt,testt";
							//console.log(feature.get('val'));
							if(feature.get('line-width') > 0)
							{
								 content.innerHTML = '<p>Detail Pengangkapan Kapal:</p><code>' + hdms + '</code>';
								 content.innerHTML+= '<p>Jumlah Kapal : <b>'+ feature.get('line-width') + '</b><p>';
								 content.innerHTML+= '<p>WPP : <b>'+ feature.get('name') + '</b><p>';
								   overlay.setPosition(coordinate);
								  
							}
							else
							{

							}
							

						}); 
					});



					//map
					var valueData=[]; 
					var warnaBg2 = [
									  '#f4cccc', 
									  '#efb2b2',
									  '#ea9999',
									  '#e57f7f',
									  '#e06666',
									  '#db4c4c',
									  '#d63232',
									  '#d11919'
									  ];
					var jenis='line-width';                  
					var stylePangan = function(feature, resolution) { 
							var text = feature.get(jenis);
							//text = text.replace(',','.');
							console.log('sds');

							cekBanyakValue(jenis);

							var Max = Math.max.apply(Math,valueData);
							var Min = Math.min.apply(Math,valueData);

							var total = (Max - Min) / 8;

							// console.log(Min);
							// console.log(total);
							// console.log(text);

							makeLegen(warnaBg2 , total, jenis);

							var indexBg=0;

							if(text < total)
							{
								var indexBg=0;
							}

							else if(text < (total *2) )
							{
								var indexBg=1;
							}
							else if(text < (total *3) )
							{
								var indexBg=2;
							}
							else if(text < (total *4) )
							{
								var indexBg=3;
							}
							else if(text < (total *5) )
							{
								var indexBg=4;
							}
							else if(text < (total *6) )
							{
								var indexBg=5;
							}
							else if(text < (total *7) )
							{
								var indexBg=6;
							}
							else
							{
								var indexBg=7; //samo jo index 8
							}

							//var color = ['#D2527F','#663399','#446CB3','#4183D7','#52B3D9','#22A7F0','#81CFE0']
							return [new ol.style.Style({
								stroke: new ol.style.Stroke({
								  color: '#52403C',
								  width: 0.1
								}),
								fill: new ol.style.Fill({
								  color: warnaBg2[indexBg]
								}),
								
								text: new ol.style.Text({
								  text: text, 
								  scale: 1, 
								  fill: new ol.style.Fill({
									color: '#fff'
								  }),

								  stroke: new ol.style.Stroke({
									color: '#FFFF99',
									width: 1
								  })
								})

							  })];
						};

						function  cekBanyakValue(data)
						{
							valueData=[];      
							vectorLayer.getSource().forEachFeature(function(e) {
								valueData.push(e.get(data) );  
							});
							//console.log(valueData);
							 
						}

						function makeLegen(color,interval,judul)
						{
							var legen = document.getElementById('legen');
							var html='<span>Keterangan</span>';
								html+='<table class="legen-table table table-bordered">';
							
							var count = 5 ;
							  for(a=0;a<8;a++)
							  {
								html+='<tr>';
								html+='<td style="background :' + color[a] + '; padding: 10px; */"></td>';
								html+='<td> <= ' +  (count)  +' Kapal</td>';
								count = count + 5;
								//html+='<td> <= ' +  (interval * (a + 1))  +' Kapal</td>';
								html+='</tr>';
							  }
							  
								html+='<tr>';
								html+='<td style="background : #cc0000; padding: 10px; */"></td>';
								count = count - 5;
								html+='<td> > ' +  (count)  +' Kapal</td>';
								//html+='<td> <= ' +  (interval * (a + 1))  +' Kapal</td>';
								html+='</tr>';

							
							html+='</table>';
							legen.innerHTML=html;

						}

						$('#reloadMapTangkap').on('click',function()
						{
							
							var tinggi = ($(window).height() - 100);
							var lebar = ($(window).width() - 20);
							
							 $('#maptangkap canvas').attr('width',lebar);
							 $('#maptangkap canvas').attr('height',tinggi);
							  map.updateSize();
							  
							  console.log(tinggi);
							   // if($('#maptangkap canvas').attr('width') == lebar)
							   // {
									// console.log(lebar);
									 // map.updateSize();
							   // }
								
							
						});


					 
					var LastDataSource = new ol.source.Vector({
						url: "{{url('public/geojson/zone_pelanggaran.geojson')}}",
						format: new ol.format.GeoJSON({ })
					});
					 
					var LastSource = new ol.source.Cluster({
					  source: LastDataSource
					});

					 


					var vectorLayer = new ol.layer.Vector({
					  source: LastDataSource,
					  opacity:0.7,
					  name:'lastposisi' ,
					  style:stylePangan,
					  opacity:0.5
					});

					map.addLayer(vectorLayer);
			</script>