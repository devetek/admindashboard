			<div id="GrafikPerbandingan" style="width:100%; "></div>
			<script type="text/javascript">
				
				AutoResizeDiv('#GrafikPerbandingan',20);
				var Data = [{"category": "Jan","column-1" : "16"
							},{"category": "Feb","column-1" : "8"
							},{"category": "Mar","column-1" : "15"
							},{"category": "Apr","column-1" : "10"
							},{"category": "Mei","column-1" : "6"
							},{"category": "2015-06","column-1" : "9"
							},{"category": "2015-07","column-1" : "2"
							},{"category": "2015-08","column-1" : "12"
							},{"category": "2015-10","column-1" : "1"
							}];
				
				AmCharts.makeChart("GrafikPerbandingan",
					 {
						"type": "serial",
						"categoryField": "category",
						"dataDateFormat": "YYYY-MM",
						"startDuration": 1,
						"categoryAxis": {
							"gridPosition": "start"
						},
						"chartCursor": {},
						"chartScrollbar": {},
						"trendLines": [],
						"graphs": [
							{"fillAlphas": 1,
								"id": "AmGraph-1",
								"title": "graph 1",
								"type": "column",
								"valueField": "column-1"
							}
						],
						"guides": [],
						"valueAxes": [
							{"id": "ValueAxis-1",
								"title": "Jumlah Kapal"
							}
						],
						"allLabels": [],
						"balloon": {}, 
						"dataProvider": Data
							}
						);
			</script>
	