				<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
					<div style="width:800px" class="modal-dialog">
						<div class="modal-content">
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						  </div>
						</div> 
					</div> <!-- /.modal-dialog -->
				</div>

	<div class="row">
		<div class="col-sm-6">
			<div id="kapalSipigroup" class="table-responsive" >
				<table id="kapalSipi" class="table table-striped">
					<thead>
						 <tr>
		                    <th  data-column-id="id" data-visible="false">id</th>
		                    <th  data-column-id="negara">Negara</th>
		                    <th  data-column-id="jumlah">Jumlah</th> 
		                </tr>
					</thead>
					 <tbody>
                      <tr>
						  <td>noned</td>
						  <td>-</td>
						  <td>5</td>
						</tr>
						<tr>
						  <td>Australia</td>
						  <td>Australia</td>
						  <td>2</td>
						</tr>
						<tr>
						  <td>Indonesia</td>
						  <td>Indonesia</td>
						  <td>43</td>
						</tr>
						<tr>
						  <td>Jepang</td>
						  <td>Jepang</td>
						  <td>1</td>
						</tr>
						<tr>
						  <td>China</td>
						  <td>P. R. Of China</td>
						  <td>1</td>
						</tr>
						<tr>
						  <td>Philippines</td>
						  <td>Philippines</td>
						  <td>3</td>
						</tr>
						<tr>
						  <td>SaudiArabia</td>
						  <td>Saudi Arabia</td>
						  <td>1</td>
						</tr>
						<tr>
						  <td>Taiwan</td>
						  <td>Taiwan</td>
						  <td>1</td>
						</tr>
						<tr>
						  <td>Thailand</td>
						  <td>Thailand</td>
						  <td>3</td>
						</tr>
						<tr>
						  <td>Vietnam</td>
						  <td>Vietnam</td>
						  <td>1</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
		<div class="col-sm-6">
			<div  id="kapalSipiDetail"class="table-responsive" >
			<table   class="table table-striped">
				<thead>
					 <tr>
						  <th data-column-id="operator"  data-formatter="operator">OPERATOR</th>
						  <th data-column-id="pemilik">PEMILIK KAPAL</th>
						  <th data-column-id="tanggal_sipi">TANGGAL SIPI</th>
						  <th data-column-id="tanggal_akhir_sipi">TANGGAL AKHIR SIPI</th>
						  <th data-column-id="jumlah">JUMLAH</th>
					</tr>
				</thead>
				  <tbody id="kapalSipiDetailData">
					
				  </tbody>
				</table>
			</div>
		</div>
	</div>
	
<script type="text/javascript">
	customHeight('#kapalSipigroup',2,100);
	customHeight('#kapalSipiDetail',2,100);


	var noned = "<tr><td >Hengky Pramono</td><td>SIM LIE HWE</td><td> </td><td> </td><td>1</td></tr><tr><td >Liono</td><td>LIONO</td><td> </td><td> </td><td>1</td></tr><tr><td >Perikanan Nusantara (Persero), P</td><td>PERIKANAN SAMODRA BESAR, PT</td><td> </td><td> </td><td>1</td></tr><tr><td >Sanjaya Budiono</td><td>SANJAYA BUDIONO</td><td> </td><td> </td><td>1</td></tr><tr><td >Sunarjo Sukardi</td><td>SUNARJO SUKARDI</td><td> </td><td> </td><td>1</td></tr>";
	var Australia = "<tr><td >West Iria Fishing Industries, PT</td><td>WEST IRIAN FISHING INDUSTRIES, PT</td><td> </td><td> </td><td>1</td></tr><tr><td >West Irian Fishing Industries, P</td><td>WEST IRIAN FISHING INDUSTRIES, PT</td><td> </td><td> </td><td>1</td></tr>";
	var Jepang = "<tr><td >Riko Dian Jayatama</td><td>RICO DIAN JAYATAMA, PT</td><td> </td><td> </td><td>1</td></tr>";
	var China = "<tr><td >Samudra Fishery Indonesia, PT</td><td>SAMUDRA FISHERY INDONESIA, PT</td><td> </td><td> </td><td>1</td></tr>";
	var Philippines = "<tr><td >KURNIA LAUT, PT</td><td>RIMAR MUTIARA SAMUDERA INDONESIA, PT</td><td> </td><td> </td><td>1</td></tr><tr><td >Rimar Mutiara Samudera Indonesia</td><td>RIMAR MUTIARA SAMUDERA INDONESIA, PT</td><td> </td><td> </td><td>1</td></tr><tr><td ></td><td>RIMAR MUTIARA SAMUDERA INDONESIA, PT</td><td> </td><td> </td><td>1</td></tr>";
	var SaudiArabia = "<tr><td >Sinar Pesona Laut, PT</td><td>SINAR PESONA LAUT, PT</td><td> </td><td> </td><td>1</td></tr>";
	var Taiwan = "<tr><td >Global Pahala Samudera, PT</td><td>GLOBAL PAHALA SAMUDERA, PT</td><td> </td><td> </td><td>1</td></tr>";
	var Thailand = "<tr><td >Jaringan Barelang, PT</td><td>JARINGAN BARELANG, PT</td><td> </td><td> </td><td>1</td></tr><tr><td >Pusaka Benjina Armada, PT</td><td>PUSAKA BENJINA ARMADA , PT</td><td> </td><td> </td><td>1</td></tr><tr><td >Pusaka Benjina Nusantara, PT</td><td>PUSAKA BENJINA NUSANTARA, PT</td><td> </td><td> </td><td>1</td></tr>";
	var Vietnam = "<tr><td >Maritim Indonesia RAya, PT</td><td>MARITIM INDONESIA RAYA, PT</td><td> </td><td> </td><td>1</td></tr>";
	var Indonesia = "<tr><td >ANTHONY ANDRIANA</td><td>ANTHONY ANDRIANA</td><td> </td><td> </td><td>1</td></tr><tr><td >Adi Nugraha</td><td>ADI NUGRAHA</td><td> </td><td> </td><td>1</td></tr><tr><td >Andi Asri</td><td>SENG LAI SUSANTO</td><td> </td><td> </td><td>1</td></tr><tr><td >Arabikatama Khatulistiwa Fishing</td><td>ARABIKATAMA KHATULISTIWA FISHING INDUSTRY, PT</td><td> </td><td> </td><td>2</td></tr><tr><td >CIAN LIE</td><td>CIAN LIE</td><td> </td><td> </td><td>1</td></tr><tr><td >Charlie Wijaya Tuna, PT</td><td>ALSUM KAMPAR SEMESTA, PT</td><td> </td><td> </td><td>1</td></tr><tr><td >Cilacap Samudera Fishing Industr</td><td>CILACAP SAMUDERA FISHING INDUSTRY, PT</td><td> </td><td> </td><td>5</td></tr><tr><td >Cilacap Samudra Fishing Industry</td><td>CILACAP SAMUDERA FISHING INDUSTRY, PT</td><td> </td><td> </td><td>1</td></tr><tr><td >Eduard Thenderan</td><td>EDUARD THENDERAN</td><td> </td><td> </td><td>1</td></tr><tr><td >Emil Arifin</td><td>SANJANGO RAYA PERMAI, PT.</td><td> </td><td> </td><td>1</td></tr><tr><td >HORISON SULYSTIO</td><td>NGATINAH , Ny</td><td> </td><td> </td><td>1</td></tr><tr><td >Halim Als Tiua Tjen</td><td>HALIM Als TIUA TJEN</td><td> </td><td> </td><td>1</td></tr><tr><td >Hery</td><td>HERY</td><td> </td><td> </td><td>1</td></tr><tr><td >Heryanto</td><td>HERYANTO</td><td> </td><td> </td><td>1</td></tr><tr><td >ICINRAB BAHARI TIMUR,PT</td><td>ICINRAB BAHARI TIMUR, PT</td><td> </td><td> </td><td>1</td></tr><tr><td >Intimas Surya, PT</td><td>INTIMAS SURYA, PT</td><td> </td><td> </td><td>1</td></tr><tr><td >Irawan Sugiyono</td><td>IRAWAN SUGIYONO</td><td> </td><td> </td><td>1</td></tr><tr><td >Ja Ho</td><td>JA HO</td><td> </td><td> </td><td>1</td></tr><tr><td >Jemmy Santoso</td><td>JEMMY SANTOSO</td><td> </td><td> </td><td>1</td></tr><tr><td >Jusman Herman</td><td>KUNCORO KUSUMO</td><td> </td><td> </td><td>1</td></tr><tr><td >Marten Suhandy</td><td>MARTEN SUHANDY</td><td> </td><td> </td><td>1</td></tr><tr><td >Mina</td><td>MINA</td><td> </td><td> </td><td>1</td></tr><tr><td >SUTARMIN</td><td>SUTARMIN</td><td> </td><td> </td><td>1</td></tr><tr><td >Sari Cakalang, PT</td><td>SARI CAKALANG , PT</td><td> </td><td> </td><td>2</td></tr><tr><td >Sartomo Sakti, PT</td><td> </td><td> </td><td> </td><td>1</td></tr><tr><td >Sugito Handoko</td><td>PANCA ASTARI SAMUDERA, PT</td><td> </td><td> </td><td>1</td></tr><tr><td >TJAI ALI</td><td>TJAI ALI</td><td> </td><td> </td><td>1</td></tr><tr><td >Teng Hoo</td><td>TENG HOO</td><td> </td><td> </td><td>1</td></tr><tr><td >Tiu Lat</td><td>GODJALI</td><td> </td><td> </td><td>1</td></tr><tr><td >Tjetjep</td><td>MARTAKA DHARMA , H</td><td> </td><td> </td><td>2</td></tr><tr><td >West Irian Fishing Indutries, PT</td><td>WEST IRIAN FISHING INDUSTRIES, PT</td><td> </td><td> </td><td>1</td></tr><tr><td >Wilopo</td><td>WILOPO</td><td> </td><td> </td><td>3</td></tr><tr><td >Yulius Hartanto</td><td>GO TJAI TEK</td><td> </td><td> </td><td>1</td></tr><tr><td ></td><td>ARIANTI MEGAH SURYA, PT</td><td> </td><td> </td><td>1</td></tr><tr><td ></td><td>DONNY HERVIS</td><td> </td><td> </td><td>1</td></tr><tr><td ></td><td>SUANDI KASDI</td><td> </td><td> </td><td>1</td></tr><tr><td ></td><td>ROBERT</td><td> </td><td> </td><td>1</td></tr><tr><td ></td><td>TJONG GIOK BIE</td><td> </td><td> </td><td>1</td></tr><tr><td ></td><td>ARABIKATAMA KHATULISTIWA FISHING INDUSTRY, PT</td><td> </td><td> </td><td>1</td></tr><tr><td ></td><td>JONGKI SOUISA</td><td> </td><td> </td><td>1</td></tr><tr><td ></td><td>PERINTIS JAYA INTERNASIONAL, PT</td><td> </td><td> </td><td>1</td></tr><tr><td ></td><td>SALAM</td><td> </td><td> </td><td>1</td></tr><tr><td ></td><td>GUNARI. H</td><td> </td><td> </td><td>1</td></tr>";
		var bootgridKapal;
		function reloadtable(data){
			if(bootgridKapal!=null) bootgridKapal.bootgrid('destroy');
			jQuery("#kapalSipiDetailData").html(eval(data));
			bootgridKapal = $("#kapalSipiDetail").bootgrid({
				rowCount: -1,
				caseSensitive: false,
	            formatters: {
                    	"operator": function(column,row){
                    		return "<span style='cursor:pointer;' onClick='LoadModalKapal()'><a href='#'>" + row.operator + "</a></span>";
                    	}
                    },
			});
		}
		
		function LoadModalKapal()
		{ 
			var url = 'page/detail-kapal.html';

			$('#myModal').removeData("modal");
			$('#myModal').modal({remote: url});       
		}
		
	jQuery(document).ready(function(){  

		$("#kapalSipi").bootgrid({
			rowCount: -1,
			caseSensitive:false,
			rowSelect: true,
		}).on("click.rs.jquery.bootgrid", function(e,columns, row)
		{
			reloadtable(row.id);
		}); 
		reloadtable("");
	});

</script>
