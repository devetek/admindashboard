			<div id="chartTPP" style="width:100%; height:295px;"></div>
			<table class="table table-inner table-vmiddle table-bordered">
				<thead>
					<tr>
						<th>Tahun</th>
						<th style="width: 60px">Jan</th>
						<th style="width: 60px">Feb</th>
						<th style="width: 60px">Mar</th>
						<th style="width: 60px">Apr</th>
						<th style="width: 60px">Mei</th>
						<th style="width: 60px">Jun</th>
						<th style="width: 60px">Jul</th>
						<th style="width: 60px">Agu</th>
						<th style="width: 60px">Sep</th>
						<th style="width: 60px">Okt</th>
						<th style="width: 60px">Nov</th>
						<th style="width: 60px">Des</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="f-500 c-cyan">Tahap 1</td>
						<td>3</td>
						<td>3</td>
						<td>4</td>
						<td>2</td>
						<td>1</td>
						<td>3</td>
						<td>5</td>
						<td>3</td>
						<td>4</td>
						<td>2</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td class="f-500 c-cyan">Tahap 2</td>
						<td>1</td>
						<td>2</td>
						<td>2</td>
						<td>2</td>
						<td>3</td>
						<td>2</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>1</td>
						<td>0</td>
						<td>0</td>
					</tr>
				</tbody>
			</table>
			
			<script type="text/javascript">
				var Data = [
					{"date": "Jan","2015": 3,"2014": 1},
					{"date": "Feb","2015": 3,"2014": 2},
					{"date": "Mar","2015": 4,"2014": 2},
					{"date": "Apr","2015": 2,"2014": 2},
					{"date": "Mei","2015": 1,"2014": 3},
					{"date": "Jun","2015": 3,"2014": 2},
					{"date": "Jul","2015": 5,"2014": 1},
					{"date": "Agu","2015": 3,"2014": 1},
					{"date": "Sep","2015": 4,"2014": 1},
					{"date": "Okt","2015": 2,"2014": 1}];
				
				AmCharts.makeChart("chartTPP",
				{
					"type": "serial",
					"categoryField": "date",
					"chartCursor": {
						"categoryBalloonDateFormat": "MM"
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Grafik Penanganan Pelanggaran"
						}
					],
					"chartScrollbar": {},
					"trendLines": [],
					"graphs": [
						{
							"bullet": "round",
							"id": "AmGraph-3",
							"title": "Tahap 1",
							"valueField": "2015"
						},
						{
							"bullet": "square",
							"id": "AmGraph-4",
							"title": "Tahap 2",
							"valueField": "2014"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Axis title"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"useGraphSettings": true
					},
					"dataProvider": Data
					}); 
			</script>
	