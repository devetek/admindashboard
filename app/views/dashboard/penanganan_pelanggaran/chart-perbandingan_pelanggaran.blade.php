			<div id="chartTPP" style="width:100%; height:295px;"></div>
			<table class="table table-inner table-vmiddle table-bordered">
				<thead>
					<tr>
						<th>Jenis Pelanggaran</th>
						<th style="width: 60px">Jan</th>
						<th style="width: 60px">Feb</th>
						<th style="width: 60px">Mar</th>
						<th style="width: 60px">Apr</th>
						<th style="width: 60px">Mei</th>
						<th style="width: 60px">Jun</th>
						<th style="width: 60px">Jul</th>
						<th style="width: 60px">Agu</th>
						<th style="width: 60px">Sep</th>
						<th style="width: 60px">Okt</th>
						<th style="width: 60px">Nov</th>
						<th style="width: 60px">Des</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="f-500 c-cyan">Tahap 1</td>
						<td>3</td>
						<td>3</td>
						<td>4</td>
						<td>2</td>
						<td>1</td>
						<td>3</td>
						<td>5</td>
						<td>3</td>
						<td>4</td>
						<td>2</td>
						<td>0</td>
						<td>0</td>
					</tr>
				</tbody>
			</table>
			
			<script type="text/javascript">
				var Data = {{json_encode($mDataChartPP)}};
				
				AmCharts.makeChart("chartTPP",
				{
					"type": "serial",
					"categoryField": "date",
					"chartCursor": {
						"categoryBalloonDateFormat": "MM"
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Grafik Penanganan Pelanggaran"
						}
					],
					"chartScrollbar": {},
					"trendLines": [],
					"graphs": [
						@foreach($mDataChartPP as $item)
							{
							"bullet": "round",
							"id": "AmGraph-3",
							"title": "Tahap 1",
							"valueField": "2015"
							},
						@endforeach
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Axis title"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"useGraphSettings": true
					},
					"dataProvider": Data
					}); 
			</script>
	