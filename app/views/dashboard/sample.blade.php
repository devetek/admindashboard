@extends('shared.master')

@section('content')

<div class="card widget" 
	data-widgster-load="{{url('dashboard/getChart/pie')}}"
    data-widgster-autoload="true"
	data-widgster-show-loader="false">
	<div class="card-header">
		<h2>Sales Statistics <small>Vestibulum purus quam scelerisque, mollis nonummy metus</small></h2>
		<ul class="actions">
			<li>
				<a data-widgster="load" href="#">
					<i class="zmdi zmdi-refresh-alt"></i>
				</a>
			</li>
			<li>
                <a data-widgster="fullscreen" href="#">
					<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
				</a>
			</li>
			<li>
                <a data-widgster="restore" href="#">
					<i class="zmdi zmdi-window-restore"></i>
				</a>
			</li>
			<li>
				<a data-widgster="expand" href="#">
					<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
				</a>
			</li>
			<li>
				<a data-widgster="collapse" href="#">
					<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
				</a>
			</li>
			<li class="dropdown">
				<a href="" data-toggle="dropdown">
					<i class="zmdi zmdi-more-vert"></i>
				</a>
				<ul class="dropdown-menu dropdown-menu-right">
					<li>
						<a data-widgster="close" href="#">
							Close
						</a>		
					</li>
				</ul>
			</li>
		</ul>
	</div>
	<div class="card-body m-t-0">
        <div class="body">
		</div>
	</div>
</div> 

@stop

@section('javascript')

        {{ HTML::script('public/vendors/bower_components/flot/jquery.flot.js')}}
        {{ HTML::script('public/vendors/bower_components/flot/jquery.flot.resize.js')}}
        {{ HTML::script('public/vendors/bower_components/flot.curvedlines/curvedLines.js')}}
        {{ HTML::script('public/vendors/sparklines/jquery.sparkline.min.js')}}

        {{ HTML::script('public/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}
        {{ HTML::script('public/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js')}}
        {{ HTML::script('public/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')}}

        {{ HTML::script('public/js/flot-charts/curved-line-chart.js')}}
        {{ HTML::script('public/js/flot-charts/line-chart.js')}}
        {{ HTML::script('public/js/charts.js')}}

        {{ HTML::script('public/vendors/amcharts/amcharts.js')}}
        {{ HTML::script('public/vendors/amcharts/pie.js')}}
        {{ HTML::script('public/vendors/amcharts/serial.js')}}
        
		{{ HTML::script('public/vendors/widgster/widgster.js')}}
		{{ HTML::script('public/vendors/jquery-ui/ui/core.js')}}
		{{ HTML::script('public/vendors/jquery-ui/ui/widget.js')}}
		{{ HTML::script('public/devetek/widget.js')}}
	
		<script type="text/javascript">
		
		</script>
 @stop
