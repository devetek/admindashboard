@extends('shared.master')

@section('content')
    <div class="card">
        <form class="form-horizontal"  action="" method="POST" role="form">
            <div class="card-header">
                <h2>User Data <small>User Login Info</small></h2>
            </div>
			<div class="card-body card-padding">
				<fieldset>
					<legend>User Login Info</legend>
					<input type="hidden" name="id" value="{{$Model->getId()}}">
					<div class="form-group">
						<label class="col-sm-2 control-label">Username</label>
						<div class="col-sm-4">
							<input type="text" name="username" placeholder="Username" size="16" class="form-control" id="prepended-input" value="{{$Model->getUsername()}}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Email</label>
						<div class="col-sm-4">
							<input type="text" name="email" placeholder="Email" size="16" class="form-control" id="prepended-input" value="{{$Model->getEmail()}}">
						</div>
					</div>
					<div class="form-group">
						<label for="password-field" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-4">
							<input type="password" name="password" placeholder="Password" id="password-field" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="password-field" class="col-sm-2 control-label">Repeat Password</label>
						<div class="col-sm-4">
							<input type="password" name="repassword" placeholder="Repeat Password" id="password-field" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Is Enabled</label>
						<div class="col-sm-4">
							<div class="select">
								{{Form::select('is_enabled', $IsEnabledCombo, $Model->IsEnabled(),array('id'=>'IsEnabledCombo', 'class'=>'form-control'))}}
							</div>
						</div>
					</div>
				</fieldset>
	
				<fieldset>
					<legend>User Info</legend>
					<div class="form-group">
						<label class="col-sm-2 control-label">Name</label>
						<div class="col-sm-4">
							<input type="text" name="name" class="form-control" placeholder="Name" value="{{$Model->getName()}}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">User Group</label>
						<div class="col-sm-4">
							<div class="select">
								{{Form::select('user_group_id', $UserGroupCombo, $Model->getUserGroup()!=null ? $Model->getUserGroup()->getId() : "",array('id'=>'UserGroupCombo', 'class'=>'form-control'))}}
							</div>
						</div>
					</div>
				</fieldset>

				<div class="form-actions text-right">
					<button class="btn btn-primary waves-effect" type="submit" name="submit" value="Save" />Save</button>
					<button class="btn btn-default waves-effect" type="button" onClick="location.href='{{url('user')}}'" >Cancel</button>
				</div>

            </div>
        </form>
    </div>
@stop

@section('javascript')
	
@stop
