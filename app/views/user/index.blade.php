@extends('shared.master')

@section('content')

    <div class="card">
        <form class="form-horizontal" role="form">
            <div class="card-header">
                <h2>User List Data <small>List For Login User</small></h2>
            </div>
            <div class="card-body card-padding">
				<div class="table-responsive">
                    <table id="data-table-basic" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th data-column-id="commands" data-formatter="commands" data-sortable="false" style="width: 70px !important;"></th>
								<th data-column-id="id" data-visible="false"></th>
								<th data-column-id="username" class="text-center" data-order="desc">Username</th>
								<th data-column-id="email" class="text-center">Email</th>
								<th data-column-id="name" class="text-center">Name</th>
								<th data-column-id="usergroup" class="text-center">User Group</th>
								<th data-column-id="isenabled" class="text-center">Is Enabled</th>
								<th data-column-id="lastlogin" class="text-center">Last Login</th>
                            </tr>
                        </thead>
                        <tbody>
						@if ($Model != null)
                    		@foreach ($Model as $item)
                            <tr>
								<td nowrap></td>
								<td>{{ $item->getId() }}</td>
								<td><a href="{{ url('user/detail', $item->getId()) }}">{{ $item->getUsername() }}</a></td>
								<td>{{ $item->getEmail() }}</td>
								<td>{{ $item->getName() }}</td>
								<td>{{ $item->getUserGroup()!=null ? $item->getUserGroup()->getName() : "-" }}</td>
								<td class="text-center">{{ $item->IsEnabled() ? "TRUE" : "FALSE" }}</td>
								<td class="text-center">{{ $item->getLastLogin() }}</td>
                            </tr>
                            @endforeach
						@else
							<tr>
								<td colspan="7"> <span class="fw-semi-bold">there is no data in database</span> </td>
							</tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <br />
				<div class="form-actions text-right">
					<button class="btn btn-default" type="button" onClick="location.href='{{url('user/create')}}'" >Create New</button>
				</div>
            </div>
        </form>
    </div>

@stop

@section('javascript')
	<script type="text/javascript">
	$(document).ready(function(){
				$("#data-table-basic").bootgrid({
					"columnDefs": [
						{
							"render": function ( data, type, row ) {
								return data +' ('+ row[3]+')';
							},
							"targets": 1
						},
						{ "visible": false,  "targets": [ 3 ] }
					],
					css: {
						icon: 'zmdi icon',
						iconColumns: 'zmdi-format-list-numbered ',
						iconDown: 'zmdi-expand-more',
						iconRefresh: 'zmdi-refresh',
						iconUp: 'zmdi-expand-less'
					},
					caseSensitive: false,
					formatters: {
							"commands": function(column, row) {
								return "<button type=\"button\" class=\"btn btnDelete btn-primary btn-xs mb-xs\" data-row-id=\"" + row.id + "\" url=\"{{ url('user/delete') }}/" + row.id + "\"><i class=\"zmdi zmdi-delete zmdi-hc-fw\"></i></button> " + 
									"<button type=\"button\" class=\"btn btn-primary btn-xs mb-xs\" data-row-id=\"" + row.id + "\" onclick=\"location.href='{{ url('user/edit') }}/" + row.id + "'\">><span class=\"zmdi zmdi-edit\"></span></button>";
							},
							"name": function(column,row){
								return "<a href=\"{{ url('user/detail')}}/" + row.id + "\">" + row.name + "</a>";
							}
						},
				}).on("loaded.rs.jquery.bootgrid", function (e)
				{
					jQuery('.btnDelete').click(function(ev){
						var url = jQuery(this).attr("url");
						swal({   
							allowOutsideClick: true,
							title: "Are you sure to Delete it?",   
							text: "You will not be able to recover this data!",   
							type: "warning",   
							showCancelButton: true,   
							confirmButtonColor: "#DD6B55",   
							confirmButtonText: "Yes, delete it!",   
							closeOnConfirm: false 
						}, function(isConfirm){
							if(isConfirm){
								location.href = url;
							}
						});
					});
				});;
			});
	</script>
@stop
