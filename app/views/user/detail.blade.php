@extends('shared.master')

@section('content')
	
    <div class="card">
        <form class="form-horizontal" role="form">
            <div class="card-header">
                <h2>User Data <small>User Login Info</small></h2>
            </div>
			<div class="card-body card-padding">
				<fieldset>
					<legend>User Login Info</legend>
					<div class="form-group">
						<label class="col-sm-2">Username</label>
						<div class="col-sm-4">
							: {{$Model->getUsername()}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Email</label>
						<div class="col-sm-4">
							: {{$Model->getEmail()}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Is Enabled</label>
						<div class="col-sm-4">
							: {{$Model->IsEnabled() ? "TRUE" : "FALSE" }}
						</div>
					</div>
				</fieldset>
	
				<fieldset>
					<legend>User Info</legend>
					<div class="form-group">
						<label class="col-sm-2">Name</label>
						<div class="col-sm-4">
							: {{$Model->getName()}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">User Group</label>
						<div class="col-sm-4">
							: {{$Model->getUserGroup()!=null ? $Model->getUserGroup()->getName() : "-"}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Last Login</label>
						<div class="col-sm-4">
							: {{$Model->getLastLogin()}}
						</div>
					</div>
				</fieldset>

				<div class="form-actions text-right">
					<button class="btn btn-primary waves-effect" type="button" onClick="location.href='{{ url(isset($ProfilePage) ? 'profile/edit' : 'user/edit', $Model->getId()) }}'">Edit</button>
					<button class="btn btn-default waves-effect" type="button" onClick="location.href='{{url('user')}}'" >Close</button>
				</div>

            </div>
        </form>
    </div>

@stop

@section('javascript')
	<script type="text/javascript">
	
	
	</script>
@stop
