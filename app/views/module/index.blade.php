@extends('shared.master')

@section('content')
	<div class="card">
        <form class="form-horizontal" role="form">
            <div class="card-header">
                <h2>Module List Data <small>Group of Function Info, this group affected to how Function List will be Displayed as menu in left menu</small></h2>
            </div>
            <div class="card-body card-padding">
				<div class="table-responsive">
                    <table id="data-table-basic" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th data-column-id="commands" data-formatter="commands" data-sortable="false" style="width: 70px !important;"></th>
								<th data-column-id="id" data-visible="false"></th>
								<th data-column-id="name" data-formatter="name">Name</th>
								<th data-column-id="icon" data-formatter="icon" data-sortable="false">Icon</th>
								<th data-column-id="sequence"  class="text-center">Sequence</th>
								<th data-column-id="isshow"  class="text-center">Is Show</th>
								<th data-column-id="isenabled"  class="text-center">Is Enabled</th>
                            </tr>
                        </thead>
                        <tbody>
						@if ($Model != null)
                    		@foreach ($Model as $item)
                            <tr>
								<td class="text-center" nowrap></td>
								<td>{{ $item->getId() }}</td>
								<td>{{ $item->getName() }}</td>
								<td class="text-center">{{ $item->getIcon() }}</td>	
								<td class="text-center">{{ $item->getSequence() }}</td>
								<td class="text-center">{{ $item->IsShow() ? "TRUE" : "FALSE" }}</td>
								<td class="text-center">{{ $item->IsEnabled() ? "TRUE" : "FALSE" }}</td>							</tr>
                            @endforeach
						@else
							<tr>
								<td colspan="7"> <span class="fw-semi-bold">there is no data in database</span> </td>
							</tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <br />
				<div class="form-actions text-right">
					<button class="btn btn-primary" type="button" onClick="location.href='{{url('module/create')}}'" >Create New</button>
				</div>
            </div>
        </form>
    </div>


@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function(){
	        $("#data-table-basic").bootgrid({
     			"columnDefs": [
		            {
		                "render": function ( data, type, row ) {
		                    return data +' ('+ row[3]+')';
		                },
		                "targets": 1
		            },
		            { "visible": false,  "targets": [ 3 ] }
		        ],
	            css: {
	                icon: 'zmdi icon',
	                iconColumns: 'zmdi-format-list-numbered ',
	                iconDown: 'zmdi-expand-more',
	                iconRefresh: 'zmdi-refresh',
	                iconUp: 'zmdi-expand-less'
	            },
	            caseSensitive: false,
	            formatters: {
                        "commands": function(column, row) {
                            return "<button type=\"button\" class=\"btn btn-primary btnDelete btn-xs mb-xs\" data-row-id=\"" + row.id + "\" url=\"{{ url('module/delete') }}/" + row.id + "\"><i class=\"zmdi zmdi-delete zmdi-hc-fw\"></i></button> " + 
								"<button type=\"button\" class=\"btn btn-primary btn-xs mb-xs\" data-row-id=\"" + row.id + "\" onclick=\"location.href='{{ url('module/edit') }}/" + row.id + "'\">><span class=\"zmdi zmdi-edit\"></span></button>";
                        },
                        "icon": function (column, row) {
		                    return "<i class=\"zmdi "+ row.icon +"\"></i>";
	                    },
                    	"name": function(column,row){
                    		return "<a href=\"{{ url('module/detail')}}/" + row.id + "\">" + row.name + "</a>";
                    	}
                    },
	        }).on("loaded.rs.jquery.bootgrid", function (e)
			{
				jQuery('.btnDelete').click(function(ev){
					var url = jQuery(this).attr("url");
					swal({   
						allowOutsideClick: true,
						title: "Are you sure to Delete it?",   
						text: "You will not be able to recover this data!",   
						type: "warning",   
						showCancelButton: true,   
						confirmButtonColor: "#DD6B55",   
						confirmButtonText: "Yes, delete it!",   
						closeOnConfirm: false 
					}, function(isConfirm){
						if(isConfirm){
							location.href = url;
						}
					});
				});
			});
        });
	
	
	</script>
@stop
