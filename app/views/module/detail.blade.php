@extends('shared.master')

@section('content')
	<div class="card">
        <form class="form-horizontal" role="form">
            <div class="card-header">
                <h2>Module Data <small>Group of Function Info, this group affected to how Function List will be Displayed as menu in left menu</small></h2>
            </div>
			<div class="card-body card-padding">
				<fieldset>
					<legend>Module Info</legend>
					<div class="form-group">
						<label class="col-sm-2">Name</label>
						<div class="col-sm-4">
							: {{$Model->getName()}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Icon</label>
						<div class="col-sm-4">
							: <i class="zmdi {{ $Model->getIcon() }}"></i>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Sequence</label>
						<div class="col-sm-4">
							: {{ $Model->getSequence() }}</i>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Is Show</label>
						<div class="col-sm-4">
							: {{$Model->IsShow() ? "TRUE" : "FALSE" }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Is Enabled</label>
						<div class="col-sm-4">
							: {{$Model->IsEnabled() ? "TRUE" : "FALSE" }}
						</div>
					</div>
				</fieldset>

				<div class="form-actions text-right">
					<button class="btn btn-primary waves-effect" type="button" onClick="location.href='{{ url('module/edit', $Model->getId()) }}'">Edit</button>
					<button class="btn btn-default waves-effect" type="button" onClick="location.href='{{url('module')}}'" >Close</button>
				</div>

            </div>
        </form>
    </div>

@stop

@section('javascript')
	<script type="text/javascript">
	
	
	</script>
@stop
