@extends('shared.master')

@section('content')
	<div class="card">
        <form class="form-horizontal"  action="" method="POST" role="form">
            <div class="card-header">
                <h2>Function Info Data <small>Function Info of declaration which function can be accessed</small></h2>
            </div>
			<div class="card-body card-padding">
				<fieldset>
					<legend>Function Info</legend>
					<input type="hidden" name="id" value="{{$Model->getId()}}">
					<div class="form-group">
						<label class="col-sm-2 control-label">Name</label>
						<div class="col-sm-4">
								<input type="text" name="name" placeholder="Name" size="16" class="form-control" value="{{$Model->getName()}}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Module</label>
						<div class="col-sm-4">
							<div class="select">
								{{Form::select('module_id', $ModuleCombo, ($Model->getModule()!=null ? $Model->getModule()->getId() :""),array('id'=>'ModuleCombo','class'=>'form-control'))}}
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Icon</label>
						<div class="col-sm-4">
							<div class="input-group">
								<input type="hidden" id="Icon" name='icon' value="{{$Model->getIcon()}}">
								<input type="text" id="txtIcon" class="form-control" value="{{$Model->getIcon()}}" disabled="disabled">
								<span class="input-group-btn">
									<button id="iconpicker" class="btn btn-default" role="iconpicker" data-placement="right"></button>
								</span>    
								<script>
									$(document).ready(function(){
										$('#iconpicker').iconpicker({
											iconset: 'materialdesign', 
											cols: 10,
											rows: 5,
											icon: '{{$Model->getIcon()}}',
											selectedClass: 'btn-success',
										}).on('change', function(e) {
											$("#txtIcon").val(e.icon);
											$("#Icon").val(e.icon);
										});
									});
								</script>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Sequence</label>
						<div class="col-sm-4">
							<input type="number" name="sequence" class="form-control" placeholder="Sequence" min="0" value="{{$Model->getSequence()}}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Url</label>
						<div class="col-sm-4">
							<input type="text" name="url" placeholder="URL" size="16" class="form-control" id="prepended-input" value="{{$Model->getUrl()}}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Is Show</label>
						<div class="col-sm-4">
							<div class="select">
								{{Form::select('is_show', $IsShowCombo, $Model->IsShow(),array('id'=>'IsShowCombo', 'class'=>'form-control'))}}
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Is Enabled</label>
						<div class="col-sm-4">
							<div class="select">
								{{Form::select('is_enabled', $IsEnabledCombo, $Model->IsEnabled(),array('id'=>'IsEnabledCombo', 'class'=>'form-control'))}}
							</div>
						</div>
					</div>
				</fieldset>

				<div class="form-actions text-right">
					<button class="btn btn-primary waves-effect" type="submit" name="submit" value="Save" />Save</button>
					<button class="btn btn-default waves-effect" type="button" onClick="location.href='{{url('functioninfo')}}'" >Cancel</button>
				</div>

            </div>
        </form>
    </div>

@stop

@section('javascript')
	{{ HTML::style('public/vendors/bootstrap-iconpicker/css/bootstrap-iconpicker.min.css')}}
	{{ HTML::script('public/vendors/bootstrap-iconpicker/js/iconset/iconset-materialdesign-2.1.2.js')}}
	{{ HTML::script('public/vendors/bootstrap-iconpicker/js/bootstrap-iconpicker.min.js')}}
@stop
