@extends('shared.master')

@section('content')
		<div class="card">
        <form class="form-horizontal" role="form">
            <div class="card-header">
                <h2>Function Info Data <small>Function Info of declaration which function can be accessed</small></h2>
            </div>
			<div class="card-body card-padding">
				<fieldset>
					<legend>User Group Info</legend>
					<div class="form-group">
						<label class="col-sm-2">Name</label>
						<div class="col-sm-4">
							: {{$Model->getName()}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Module</label>
						<div class="col-sm-4">
							: {{ $Model->getModule()!=null ? $Model->getModule()->getName() : "-" }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Icon</label>
						<div class="col-sm-4">
							: <i class="zmdi {{ $Model->getIcon() }}"></i>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Sequence</label>
						<div class="col-sm-4">
							: {{ $Model->getSequence() }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Url</label>
						<div class="col-sm-4">
							: {{ $Model->getUrl() }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Is Show</label>
						<div class="col-sm-4">
							: {{$Model->IsShow() ? "TRUE" : "FALSE" }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Is Enabled</label>
						<div class="col-sm-4">
							: {{$Model->IsEnabled() ? "TRUE" : "FALSE" }}
						</div>
					</div>
				</fieldset>

				<div class="form-actions text-right">
					<button class="btn btn-primary waves-effect" type="button" onClick="location.href='{{ url('functioninfo/edit', $Model->getId()) }}'">Edit</button>
					<button class="btn btn-default waves-effect" type="button" onClick="location.href='{{url('functioninfo')}}'" >Close</button>
				</div>

            </div>
        </form>
    </div>

@stop

@section('javascript')
	<script type="text/javascript">
	
	
	</script>
@stop
