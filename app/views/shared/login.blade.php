<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
    <title>{{$DevData["AppLoginTitle"]}}</title>
	<!-- Vendor CSS -->
    {{ HTML::style('public/vendors/bower_components/animate.css/animate.min.css')}}
    {{ HTML::style('public/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}
		
	<!-- CSS -->
    {{ HTML::style('public/css/app.min.1.css')}}
    {{ HTML::style('public/css/app.min.2.css')}}
    {{ HTML::style('public/devetek/application.material.login.theme.css')}}
 
	<!-- as of IE9 cannot parse css files with more that 4K classes separating in two files -->
    <!--[if IE 9]>
        <link href="css/application-ie9-part2.css" rel="stylesheet">
    <![endif]-->
    
	<link rel='stylesheet' id='roboto-css'  href='http://fonts.googleapis.com/css?family=Roboto%3A400%2C700%2C300&#038;ver=4.0.1' type='text/css' media='all' />
	<link rel='stylesheet' id='abel-css'  href='http://fonts.googleapis.com/css?family=Abel&#038;ver=4.0.1' type='text/css' media='all' />
	<link rel="shortcut icon" href="{{ url('public/favicon.ico')}}">

    <meta name="description" content="">
    <meta name="author" content="">
	
    <script>
        /* yeah we need this empty stylesheet here. It's cool chrome & chromium fix
         chrome fix https://code.google.com/p/chromium/issues/detail?id=167083
         https://code.google.com/p/chromium/issues/detail?id=332189
         */
    </script>
	</head>
	<body class="login-content">
        <!-- Login -->
		<header>
			<div id="top-menu">
			</div>
			<div class="title">
				<div class="image-wrap">
					<a href="http://kkp.go.id/"><img src="http://kkp.go.id/assets/themes/kpi/images/logo-admin.png" class="on-mobile" alt=""></a>
				</div>
				<div class="title-wrap">
					<h2>KEMENTERIAN KELAUTAN DAN PERIKANAN REPUBLIK INDONESIA</h2>
					<h3>Ministry of Marine Affairs and Fisheries Republic of Indonesia</h3>
				</div>
			</div>
		</header>
		
		
        <div class="lc-block toggled" id="l-login">
			<form id="LoginForm" class="mt-lg" action="" method="post" >
			<ul class="error-messages"> 
				@if(count($errors)>1)
					<li>{{ $errors->first('username') }}</li>
					<li>{{ $errors->first('pswd') }}</li> 
				@endif
			</ul>
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                <div class="fg-line">
					<input type="text" name="username" class="form-control" placeholder="Username">
                </div>
            </div>
            
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-lock"></i></span>
                <div class="fg-line">
					<input name="pswd" class="form-control" id="pswd" type="password" placeholder="Password">
                </div>
            </div>
            
            <div class="clearfix"></div>
            <input type="submit" class="hide">
			<?php
			/*
				<div class="checkbox">
					<label>
						<input type="checkbox" value="">
						<i class="input-helper"></i>
						Keep me signed in
					</label>
				</div>
			*/
			?>
            <a onClick="document.getElementById('LoginForm').submit();" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></a>
            </form>
        </div>
        
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <!-- Javascript Libraries -->
		{{ HTML::script('public/vendors/bower_components/jquery/dist/jquery.min.js')}}
		{{ HTML::script('public/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}
		
		{{ HTML::script('public/vendors/bower_components/Waves/dist/waves.min.js')}}
        
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        
        {{ HTML::script('public/js/functions.js')}}
        
    </body>
	<!-- common app js -->
	
</html>