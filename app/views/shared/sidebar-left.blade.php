<div class="sidebar-inner c-overflow">
                <div class="profile-menu">
                    <a href="">
                        <div class="profile-pic">
                            <img src="{{url('public/kkp-logo.png')}}" alt="">
                        </div>

                        <div class="profile-info">
                            {{$userInfo->getName()}}

                            <i class="zmdi zmdi-arrow-drop-down"></i>
                        </div>
                    </a>

                    <ul class="main-menu">
                        <li>
                            <a href=""><i class="zmdi zmdi-account"></i> View Profile</a>
                        </li>
                        <?php /*
						<li>
                            <a href=""><i class="zmdi zmdi-input-antenna"></i> Privacy Settings</a>
                        </li> */
						?>
                        <li>
                            <a href="{{url('/profile')}}"><i class="zmdi zmdi-settings"></i>Account Settings</a>
                        </li>
                        <li>
                            <a href="{{url('/logout')}}"><i class="zmdi zmdi-time-restore"></i> Logout</a>
                        </li>
                    </ul>
                </div>
				
		<?php
			$ActiveFunctionId = "";
			$ActiveFunctionName = "";
			$ActiveModuleId = "";
			if(isset($ActiveFunction)){
				$ActiveFunctionId = $ActiveFunction->getId();
				$ActiveFunctionName = $ActiveFunction->getName();
				$ActiveModuleId = $ActiveFunction->getModule()->getId();
			}
		?>
				
                <ul class="main-menu">
            @if(isset($functioninfo) && $functioninfo!=NULL && count($functioninfo['module']) > 0)
                    @foreach ($functioninfo['module'] as $module)
                        @if(count($functioninfo[$module->getId()]) == 1)
                            <li {{$ActiveFunctionId == $functioninfo[$module->getId()][0]->getId() ? "class='active'" : "" }}>
                                <a href="{{url($functioninfo[$module->getId()][0]->getUrl())}}">
                                    <i class="zmdi {{$functioninfo[$module->getId()][0]->getIcon()}}"></i> 
                                    {{$functioninfo[$module->getId()][0]->getName()}}
                                </a>
                            </li>
                        @else
                            <li class="sub-menu  {{$ActiveModuleId == $module->getId() ? 'active toggled' : '' }}">
                                <a href="">
                                    <i class="zmdi {{$module->getIcon()}}"></i> 
                                    {{$module->getName()}}
                                </a>

                                <ul>
                                @foreach ($functioninfo[$module->getId()] as $function)
                                    <li>
                                        <a {{$ActiveFunction == $function->getId() ? "class='active'" : "" }} href="{{url($function->getUrl())}}">
                                            <i class="zmdi {{$function->getIcon()}}"></i> 
                                            {{$function->getName()}}
                                        </a>
                                    </li>
                                @endforeach
                                </ul>
                            </li>
                            
                        @endif
                    @endforeach
            @endif
                </ul>
            </div>