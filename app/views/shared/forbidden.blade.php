@extends('shared.master')

@section('content')
		<div class="card">
        <form class="form-horizontal"  action="" method="POST" role="form">
            <div class="card-header">
                <h2>Alert Messages</h2>
            </div>
			<div class="card-body card-padding">
				<div class="alert alert-danger alert-sm">
                    <i class="fa fa-warning fa-fw"></i> <span class="fw-semi-bold">Forbidden Access:</span> You dont have permission to this page!.
                </div>
            </div>
        </form>
    </div>
		
@stop
