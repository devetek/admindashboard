<!DOCTYPE html>
<html lang="en">
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>{{$DevData["AppTitle"]}}</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ url('public/favicon.ico')}}">

    <!-- Vendor CSS -->
    {{ HTML::style('public/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css')}}
    {{ HTML::style('public/vendors/bower_components/animate.css/animate.min.css')}}
    {{ HTML::style('public/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css')}}
    {{ HTML::style('public/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}
    {{ HTML::style('public/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css')}}

    <!-- CSS -->
    {{ HTML::style('public/css/app.min.1.css')}}
    {{ HTML::style('public/css/app.min.2.css')}}

    {{ HTML::style('public/devetek/application.material.theme.css')}}
    
	<script>
        /* yeah we need this empty stylesheet here. It's cool chrome & chromium fix
         chrome fix https://code.google.com/p/chromium/issues/detail?id=167083
         https://code.google.com/p/chromium/issues/detail?id=332189
         */
    </script>
	

<!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <!-- Javascript Libraries -->
        {{ HTML::script('public/vendors/bower_components/jquery/dist/jquery.min.js')}}
        {{ HTML::script('public/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}

        {{ HTML::script('public/vendors/bower_components/moment/min/moment.min.js')}}
        {{ HTML::script('public/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js')}}
        {{ HTML::script('public/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js')}}
        {{ HTML::script('public/vendors/bower_components/Waves/dist/waves.min.js')}}
        {{ HTML::script('public/vendors/bootstrap-growl/bootstrap-growl.min.js')}}
        {{ HTML::script('public/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js')}}
        {{ HTML::script('public/vendors/bower_components/autosize/dist/autosize.min.js')}}
		
		{{ HTML::script("public/vendors/bootgrid/jquery.bootgrid.min.js")}}
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        
        @yield('javascript')
    
</head>
<body>
	
<header id="header">
	@include('shared.navbar')
</header>
        
    <section id="main">
        <aside id="sidebar">
			@include('shared.sidebar-left')
        </aside>

        <aside id="chat">
			@include('shared.sidebar-right')
        </aside>
        
        <section id="content">
			<div class="container" id="layer1">
				@if(isset($ActiveFunction))
				<ol class="breadcrumb">
					<li>{{$ActiveFunction->getModule() == null ? "" : $ActiveFunction->getModule()->getName()}}</li>
					<li class="active">{{$ActiveFunction == null ? "" : $ActiveFunction->getName()}}</li>
				</ol>
				@else
				<ol class="breadcrumb">
					<li>Dashboard System</li>
					<li class="active">Forbidden Access</li>
				</ol>
				@endif
				@if($DevMessage != null)
					{{$DevMessage}}
				@endif

				@yield('content')

			</div>
			<div class="container" id="layer2">
			
			</div>
        </section>
    </section>
        	

    <footer id="footer">
        <h4><small>Copyright &copy; {{$DevData["CompanyName"]}}</small></h4>
        <small>{{$DevData["CompanyName2"]}}</small>
        <ul class="f-menu hide" >
            <li><a href="">Home</a></li>
            <li><a href="">Dashboard</a></li>
            <li><a href="">Reports</a></li>
            <li><a href="">Support</a></li>
            <li><a href="">Contact</a></li>
        </ul>
    </footer>

        {{ HTML::script('public/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js')}}
		{{ HTML::script('public/vendors/widgster/widgster.js')}}
		{{ HTML::script('public/vendors/jquery-ui/ui/core.js')}}
		{{ HTML::script('public/vendors/jquery-ui/ui/widget.js')}}
		{{ HTML::script('public/devetek/functions.js')}}
		
        {{ HTML::script('public/js/functions.js')}}
        {{ HTML::script('public/js/demo.js')}}
		<script>
			@if(isset($justLogin))
				notify('Welcome back Mallinda Hollaway', 'inverse');
			@endif
		</script>
</body>
</html>