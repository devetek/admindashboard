@extends('shared.master')

@section('content')
	<div class="card">
        <form class="form-horizontal" role="form">
            <div class="card-header">
                <h2>Lookup Data <small>Group of Configuration Lookup Detail, this group is configuration for each your module</small></h2>
            </div>
			<div class="card-body card-padding">
				<fieldset>
					<legend>Lookup Info</legend>
					<div class="form-group">
						<label class="col-sm-2">Name</label>
						<div class="col-sm-4">
							: {{$Model->getName()}}
						</div>
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Lookup Detail</legend>
					<table class="table table-bordered table-sm mt-sm mb-0">
					<thead>
						<tr>
							<th style="width: 70px !important;">#</th>
							<th class="text-center">Name</th>
							<th class="text-center">Value</th>
						</tr>
					</thead>
					<tbody>
					@if ($Model->getLookupDetail() != null)
						<?php $i = 1 ?>
						@foreach ($Model->getLookupDetail() as $item)
						<tr>
							<td class="text-center" nowrap>{{$i++}}</td>
							<td>{{ $item->getName() }}</td>
							<td>{{ $item->getValue() }}</td>
						</tr>
                        @endforeach
					@else
						<tr>
							<td colspan="7"> <span class="fw-semi-bold">there is no data in database</span> </td>
						</tr>
                    @endif
					</tbody>
				</table>
				</fieldset>

				<div class="form-actions text-right">
					<button class="btn btn-primary waves-effect" type="button" onClick="location.href='{{ url('lookup/edit', $Model->getId()) }}'">Edit</button>
					<button class="btn btn-default waves-effect" type="button" onClick="location.href='{{url('lookup')}}'" >Close</button>
				</div>

            </div>
        </form>
    </div>

@stop

@section('javascript')
	<script type="text/javascript">
	
	
	</script>
@stop
