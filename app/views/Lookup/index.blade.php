@extends('shared.master')

@section('content')
	<div class="card">
        <form class="form-horizontal" role="form">
            <div class="card-header">
                <h2>Lookup List Data <small>Group of Configuration Lookup Detail, this group is configuration for each your module</small></h2>
            </div>
            <div class="card-body card-padding">
				<div class="table-responsive">
                    <table id="data-table-basic" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th data-column-id="commands" data-formatter="commands" data-sortable="false" style="width: 70px !important;"></th>
								<th data-column-id="id" data-visible="false"></th>
								<th data-column-id="name" data-formatter="name">Name</th>
                            </tr>
                        </thead>
                        <tbody>
						@if ($Model != null)
                    		@foreach ($Model as $item)
                            <tr>
								<td class="text-center" nowrap></td>
								<td>{{ $item->getId() }}</td>
								<td>{{ $item->getName() }}</td>							
							</tr>
                            @endforeach
						@else
							<tr>
								<td colspan="7"> <span class="fw-semi-bold">there is no data in database</span> </td>
							</tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <br />
				<?php /*<div class="form-actions text-right">
					<button class="btn btn-primary" type="button" onClick="location.href='{{url('module/create')}}'" >Create New</button>
				</div> */?>
            </div>
        </form>
    </div>


@stop

@section('javascript')
	<script type="text/javascript">
		$(document).ready(function(){
	        $("#data-table-basic").bootgrid({
     			"columnDefs": [
		            {
		                "render": function ( data, type, row ) {
		                    return data +' ('+ row[3]+')';
		                },
		                "targets": 1
		            },
		            { "visible": false,  "targets": [ 3 ] }
		        ],
	            css: {
	                icon: 'zmdi icon',
	                iconColumns: 'zmdi-format-list-numbered ',
	                iconDown: 'zmdi-expand-more',
	                iconRefresh: 'zmdi-refresh',
	                iconUp: 'zmdi-expand-less'
	            },
	            caseSensitive: false,
	            formatters: {
                        "commands": function(column, row) {
                            return "<button type=\"button\" class=\"btn btn-primary btn-xs mb-xs\" data-row-id=\"" + row.id + "\" onclick=\"location.href='{{ url('lookup/edit') }}/" + row.id + "'\">><span class=\"zmdi zmdi-edit\"></span></button>";
                        },
                    	"name": function(column,row){
                    		return "<a href=\"{{ url('lookup/detail')}}/" + row.id + "\">" + row.name + "</a>";
                    	}
                    },
	        });
        });
	
	
	</script>
@stop
