@extends('shared.master-dashboard')

@section('content')
 

<div class="col-md-4  padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/kapal_pengawas/pie')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
		<div class="card-header">
			<h2>Persentase Hasil Operasi</h2>		
			<ul class="actions">
			<li>
				<a id="reloadMapPosisi" href="#">
					<i class="zmdi zmdi-refresh-alt"></i>
				</a>
			</li>
			<li>
				<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
					<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
				</a>
			</li>
			<li>
				<a  data-widgster="restore" href="#">
					<i class="zmdi zmdi-window-restore"></i>
				</a>
			</li>
			 	
		</ul>
		</div>
		<div class="card-body">
			<div class="body">
				
			</div>
		</div>
	</div>

</div>


<div class="col-md-8  padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/kapal_pengawas/map-kapal_pengawas')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
		<div class="card-header">
			<h2>Peta Lokasi Gelar Operasi(Data Terakhir)</h2>		
			<ul class="actions">
			<li>
				<a id="reloadMapPosisi" href="#">
					<i class="zmdi zmdi-refresh-alt"></i>
				</a>
			</li>
			<li>
				<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
					<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
				</a>
			</li>
			<li>
				<a  data-widgster="restore" href="#">
					<i class="zmdi zmdi-window-restore"></i>
				</a>
			</li>
			<li>
				<a data-widgster="expand" href="#">
					<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
				</a>
			</li>
			<li>
				<a data-widgster="collapse" href="#">
					<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
				</a>
			</li>
			<li class="dropdown">
				<a href="" data-toggle="dropdown">
					<i class="zmdi zmdi-more-vert"></i>
				</a>
				<ul class="dropdown-menu dropdown-menu-right">
					<li>
						
						<a data-widgster="close" href="#">
							Close
						</a>		
					</li>
				</ul>
			</li>
		</ul>
		</div>
		<div class="card-body">
			<div class="body">
				
			</div>
		</div>
	</div>

</div>
<div class="col-md-6  padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/kapal_pengawas/chart-perbandingan')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
	<div class="card-header">
		<h2>Grafik Perbadigan Gelar Operasi</h2> 
	<ul class="actions">
		<li>
			<a id="reloadMapPosisi" href="#">
				<i class="zmdi zmdi-refresh-alt"></i>
			</a>
		</li>
		<li>
			<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
				<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
			</a>
		</li>
		<li>
			<a  data-widgster="restore" href="#">
				<i class="zmdi zmdi-window-restore"></i>
			</a>
		</li>
		<li>
			<a data-widgster="expand" href="#">
				<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>                       
			</a>
		</li>
		<li>
			<a data-widgster="collapse" href="#">
				<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>                     
			</a>
		</li>
		<li class="dropdown">
			<a href="" data-toggle="dropdown">
				<i class="zmdi zmdi-more-vert"></i>
			</a>
			<ul class="dropdown-menu dropdown-menu-right">
				<li>
					
					<a data-widgster="close" href="#">
						2015
					</a>
					<a data-widgster="close" href="#">
						2014
					</a>
					<a data-widgster="close" href="#">
						2013
					</a>        
				</li>
			</ul>
		</li>
	</ul>
	</div>
	<div class="card-body">
		<div class="body">
			<div class="pl-body" id="GrafikPerbandingan" >
				 
			</div> 
		</div>
	</div>
</div>
</div>

<div class="col-sm-6 col-md-6 padding-1">
 
<div class="card widget" 
data-widgster-load="{{url('prototype/kapal_pengawas/list-gelar-operasi')}}"
data-widgster-autoload="true"
data-widgster-show-loader="false">
	<div class="card-header">
		<h2>List Data Hasil Gelar Operasi</h2>
				
		<ul class="actions">
		<!-- <li>
			<a data-widgster="load" href="#">
				<i class="zmdi zmdi-refresh-alt"></i>
			</a>
		</li> -->
		<li>
			<a data-widgster="fullscreen" href="#">
				<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
			</a>
		</li>
		<li>
			<a data-widgster="restore" href="#">
				<i class="zmdi zmdi-window-restore"></i>
			</a>
		</li>
		<li>
			<a data-widgster="expand" href="#">
				<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
			</a>
		</li>
		<li>
			<a data-widgster="collapse" href="#">
				<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
			</a>
		</li> 
	</ul>
	</div>
	
	<div class="card-body">
		<div class="body autoOverflow"> 
		</div>
	</div>
</div> 
</div> 



@stop

@section('javascript')
	
    {{ HTML::style('public/vendors/openlayer/ol.css')}}
	{{ HTML::script('public/vendors/openlayer/ol.js')}}

	<!-- tambahan -->
	{{ HTML::script('public/devetek/mapsStyle.js')}} 
	
		<script type="text/javascript">
		 
		 </script>
 @stop
