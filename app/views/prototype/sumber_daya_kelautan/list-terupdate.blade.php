 
<div class="card-header" style="padding:5px">
   
    <div class="row">   
        <div class="col-sm-4">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">  
                        <select name="jenisSdk" class="form-control">
                            <option value="konservasi">Kawasa Konservasi</option> 
                            <option value="pencemaran">Pencemaran laut</option> 
                            <option value="karang">kerusakan terumbu karang</option> 
                            <option value="bakau">Kerusakan Bakau</option> 
                            <option value="bmkt">BMKT</option> 
                             
                        </select>
                    </div>
                </div>
            </div>
        </div> 

        <div class="col-sm-4">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">
                        <select name="tahun" disabled class="form-control">
                            <option value="0">Pilih Tahun</option>
                            <?php for($a=2005;$a <= date('Y') ;$a++){ ?>
                             
                                <option value="<?php echo $a ?>"><?php echo $a ?></option>

                            <?php } ?>

                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-sm-4">
            <button class="btn btn-primary btn-sm waves-effect btn-filter-data" type="submit">Filter</button>
        </div>

    </div>

    <script type="text/javascript">
        
        customHeight('.autoOverflow',2,80); 
        dataSdk(); 

        $('.btn-filter-data').click(function(){
            dataSdk();                
        })

        $('select[name=jenisSdk]').change(function (){
            if($('select[name=jenisSdk]').val=='konservasi')
            {
                $('select[name=tahun]').prop('disabled', true);
            }
            else
            {
                $('select[name=tahun]').prop('disabled', false);
            }
        })
        

        function dataSdk()
        {
            var jenisSdk = $('select[name=jenisSdk]').val();
            var tahun = $('select[name=tahun]').val();
          
            var url =  jenisSdk + '/table/'+ tahun; 
            $('#dataSdk').load(urlServer + 'apisdk/'+ url); 
        }

    </script>

</div>

<div class="table-responsive" style="overflow: hidden;" tabindex="3">
    <table class="table table-hover" id="dataSdk">
         
    </table>
</div>
                 