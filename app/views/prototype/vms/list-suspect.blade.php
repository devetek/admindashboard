			 
			<table class="table table-inner table-hover table-vmiddle" >
				<thead>
					<tr> 
						<th>Nama Kapal</th>
						<th>GT</th>
						<th style="width: 60px">Pelanggaran</th>
					</tr>
				</thead>

				<tbody id="dataHostlist">
					 
				</tbody>
			</table>  
			<script type="text/javascript"> 
				var dataHotlist = [
                      {
                        "nama":"MITRA NELAYAN",
                        "gt":130,
                        "lon":18.5294,
                        "lat":118.2078,
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Pukat Ikan",
						"jenis_pelanggaran":"Pelanggaran Wilayah"
                      },
                      {
                        "nama":"MUTIARA MINA 1",
                        "gt":115,
                        "lon":-7.1722 ,
                        "lat":112.7314,
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Pukat Ikan",
						"jenis_pelanggaran":"Pelanggaran Operasional"
                      },
                      {
                        "nama":"WOGEKEL - 11",
                        "gt":195,
                        "lon":-8.7431,
                        "lat":115.2075,
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Pukat Ikan",
						"jenis_pelanggaran":"Pelanggaran Wilayah"
                      },
                      {
                        "nama":"OMBRE 62",
                        "gt":305,
                        "lon":1,
                        "lon":140.309, 
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Pukat Ikan",
						"jenis_pelanggaran":"Pelanggaran Wilayah"
                      },
                      {
                        "nama":"KOFIAU 32",
                        "gt":298,
                        "lon":-9.564 ,
                        "lat":130.309,
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Pukat Ikan",
						"jenis_pelanggaran":"Pelanggaran Operasional"
                      },
                      {
                        "nama":"DOFIOR 156",
                        "gt":298,
                        "lon":3.78 ,
                        "lat":98.713,
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Pukat Ikan",
						"jenis_pelanggaran":"Pelanggaran Wilayah"
                      },
                      {
                        "nama":"DOFIOR 158",
                        "gt":298,
                        "lon":-6.1 ,
                        "lat":106.804,
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Pukat Ikan",
						"jenis_pelanggaran":"Pelanggaran Wilayah"
                      },
                      {
                        "nama":"ASIA BAGUS - 28",
                        "gt":127,
                        "lon":1.4383,
                        "lat":125.2136,
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Pancing Cumi (squid jigging)",
						"jenis_pelanggaran":"Pelanggaran Wilayah"
                      },
                      {
                        "nama":"SUMBER BARU -II",
                        "gt":38,
                        "lon":1.8506,
                        "lat":104.64,
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Jaring insang oseanik",
						"jenis_pelanggaran":"Pelanggaran Wilayah"
                      },
                      {
                        "nama":"MITRAMAS 9",
                        "gt":628,
                        "lon":-8.7419 ,
                        "lat":115.2075,
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Pengangkut",
						"jenis_pelanggaran":"Pelanggaran Wilayah"
                      },
                      {
                        "nama":"TRANS MITRAMAS 111",
                        "gt":425,
                        "lon": -4.7994 ,
                        "lat": 136.6356,
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Pengangkut",
						"jenis_pelanggaran":"Pelanggaran Wilayah"
                      },
                      {
                        "nama":"BANDAR NELAYAN - 126",
                        "gt":133,
                        "lon":4.2072,
                        "lat":123.6611,
                        "jenis":"penangkap ikan",
                        "alat_tangkap":"Rawai Tuna",
						"jenis_pelanggaran":"Pelanggaran Wilayah"
                      }
                    ];
        
					function LoadHostList(dima)
					{	
						var html=''; 

							for(var a=0;a < dataHotlist.length;a++ )
							{
								html+='<tr>';
									html+='<td onclick="LoadModal(\'{{url("public/prototype/vms/popup-kapal-detail")}}\')">' + dataHotlist[a].nama + '</td>';
									html+='<td>' + dataHotlist[a].gt + '</td>';
									//html+='<td>' + dataHotlist[a].jenis + '</td>';
									//html+='<td>' + dataHotlist[a].alat_tangkap + '</td>';
									html+='<td>' + dataHotlist[a].jenis_pelanggaran + '</td>';

								html+='</tr>'; 

							}

						//$('#hostlistWG').height($(window).height()/2 - 75 + 'px');
						//$('#lastPosisi').height($(window).height()/2 - 75 + 'px');
						$(dima).html(html);
					}
					LoadHostList('#dataHostlist'); 
			</script>
	