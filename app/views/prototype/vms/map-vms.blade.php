  <div id="mapSDK"></div>
<div id="popup" class="ol-popup hidden">
<a href="#" id="popup-closer" class="ol-popup-closer"></a>
  <div id="popup-content"></div>
</div>
 
 <div id="toogle-icon" class="toggled">
	<button onClick="hideShowToggle('hotlist')" class="btn bgm-cyan btn-float waves-effect waves-circle waves-float">
		<i class="zmdi zmdi-map"></i>
	</button>
 </div>
 
 <div id="hotlist" class="toggled" style="display:none">

 <div role="tabpanel"> 						                            

    <ul role="tablist" class="tab-nav" style="overflow: hidden;" tabindex="1">
        <li class="active"><a data-toggle="tab" role="tab" aria-controls="home11" href="#home11" aria-expanded="true">Layer</a></li>
        <li class=""><a data-toggle="tab" role="tab" aria-controls="profile11" href="#profile11" aria-expanded="false">Information</a></li>
        <li>
         	<a href="#" onClick="hideShowToggle('hotlist')">
				<i class="zmdi zmdi-close"></i>
			</a>
		</li>
    </ul>
  
    <div class="tab-content">
        <div id="home11" class="tab-pane active" role="tabpanel">
         			
			<div class="card">		
				<div class="card-body card-padding">
					 
					<div class="row">   
				        <div class="col-sm-6">
				            <div class="form-group" style="margin:1px">
				                <div class="fg-line">
				                    <div class="select">
				                        <select name="statusVMS" class="form-control"> 
				                            
				                                <option value="1">Aktif</option>
				                                <option value="0">Tidak Aktif</option>
				                             
				                        </select>
				                    </div>
				                </div>
				            </div>
				        </div>

				        <div class="col-sm-6">
				            <div class="form-group" style="margin:1px">
				                <div class="fg-line">
				                    <div class="select">  
				                        <?php 
				                        	$sql=" select alat_tangkap from ADF_DW_J_TVESSEL_STATUS group by alat_tangkap";
				                        	$alat_tangkap = DB::select($sql); 
				                         ?>
				                        <select name="alat_tangkapVMS" class="form-control">
				                        	<option value="">Semua alat tangkap</option>
				                        	<?php foreach($alat_tangkap as $x){ ?>
				                            <option value="<?php echo $x->alat_tangkap ?>"><?php echo $x->alat_tangkap ?></option>                              
				                            <?php } ?>
				                        </select>
				                    </div>
				                </div>
				            </div>
				        </div> 
						
						<div class="col-sm-6">
				            <div class="form-group" style="margin:1px">
				                <div class="fg-line">
				                    <div class="select">  
				                        <?php 
				                        	$sql=" select negara from ADF_DW_J_TVESSEL_STATUS group by negara";
				                        	$negara = DB::select($sql); 
				                         ?>
				                        <select name="negaraVMS" class="form-control">
				                        	<option value="">Semua Negara</option>
				                        	<?php foreach($negara as $x){ ?>
				                            <option value="<?php echo $x->negara ?>"><?php echo $x->negara ?></option>                              
				                            <?php } ?>
				                        </select>
				                    </div>
				                </div>
				            </div>
				        </div> 
 
				        <div class="col-sm-6">
				            <button class="pull-right btn btn-primary btn-sm waves-effect btn-filter-map" type="submit">Filter</button>
				        </div>

				    </div>

					 <!-- Boss -->
					<hr>	
					
					<div  role="tablist" aria-multiselectable="true" id="panel-accordion" class="panel-group">
						<div class="panel panel-collapse">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Base Layer
									</a>
								</h4>
							</div>
		 
			 
							<div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="radio m-b-15">
		                                <label>
		                                    <input type="radio" name="base-layer" value="Aerial">
		                                    <i class="input-helper"></i>
		                                    Bing Aerial
		                                </label>
		                            </div>
									
		                            <div class="radio m-b-15">
		                                <label>
		                                    <input type="radio" name="base-layer" value="AerialWithLabels">
		                                    <i class="input-helper"></i>
		                                    Bing Aerial with labels
		                                </label>
		                            </div>
		                            
		                            <div class="radio m-b-15">
		                                <label>
		                                    <input type="radio" selected name="base-layer" value="Road">
		                                    <i class="input-helper"></i>
		                                    Bing Road
		                                </label>
		                            </div>
		                            
		                            <div class="radio m-b-15">
		                                <label>
		                                    <input type="radio" name="base-layer" value="collinsBart">
		                                    <i class="input-helper"></i>
		                                    Bing Collins Bart
		                                </label>
		                            </div>
		                            
		                            <div class="radio m-b-15">
		                                <label>
		                                    <input type="radio" name="base-layer" value="ordnanceSurvey">
		                                    <i class="input-helper"></i>
		                                    Bing Ordnance Survey
		                                </label>
		                            </div>
		                            
		                             
								</div>
							</div>
						</div>
						 
						 
						<div class="panel panel-collapse">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										Zone WPP
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body" id="DataZone">
									 
									
									 <div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="14" name="wpp-layer" id="checkbox14">
								        <i class="input-helper"></i>
								            WPP-RI 711                     14        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="15"  name="wpp-layer" id="checkbox15">
								        <i class="input-helper"></i>
								            WPP-RI 712                     15        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="16" name="wpp-layer" id="checkbox16">
								        <i class="input-helper"></i>
								            WPP-RI 713                     16        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="17" name="wpp-layer" id="checkbox17">
								        <i class="input-helper"></i>
								            WPP-RI 714                     17        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="18" name="wpp-layer" id="checkbox18">
								        <i class="input-helper"></i>
								            WPP-RI 715                     18        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="19" name="wpp-layer" id="checkbox19">
								        <i class="input-helper"></i>
								            WPP-RI 716                     19        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="20" name="wpp-layer" id="checkbox20">
								        <i class="input-helper"></i>
								            WPP-RI 717                     20        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="21" name="wpp-layer" id="checkbox21">
								        <i class="input-helper"></i>
								            WPP-RI 718                     21        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="22" name="wpp-layer" id="checkbox22">
								        <i class="input-helper"></i>
								            WPP-RI 571                     22        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="23" name="wpp-layer" id="checkbox23">
								        <i class="input-helper"></i>
								            WPP-RI 573                     23        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="24" name="wpp-layer" id="checkbox24">
								        <i class="input-helper"></i>
								            WPP-RI 572                     24        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="41" name="wpp-layer" id="checkbox41">
								        <i class="input-helper"></i>
								            Teritorial                     41        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="42" name="wpp-layer" id="checkbox42">
								        <i class="input-helper"></i>
								            ZEE                            42        </label>
								    </div>
									
								</div>
							</div>
						</div>
						 			
						 
					</div>
					<hr>
					<button onClick="tampilLegend()" class="btn btn-default btn-icon-text waves-effect"><i class="zmdi zmdi-pin-help"></i> Show Grafik</button>
					
				</div>
			</div>


         </div>
        <div id="profile11" class="tab-pane" role="tabpanel"> 
        	<div class="card">		
				<div id="idInformation" class="card-body card-padding">

				</div>	
			</div>	

        </div>
         
    </div>
</div>


	
	
 </div>
 
 <div id="legenHomepage" style="display:none" class="col-md-3"> 
	<div class="card">	
		<div class="card-header">
			<h2>Grafik</h2>
			<ul class="actions">
				<li class="dropdown action-show">
					<a href="#" onClick="tampilLegend()">
						<i class="zmdi zmdi-close"></i>
					</a> 
				</li>  
			</ul>
		</div>
					
		
		<div id="legen" class="card-body card-padding"> </div>
		  
	</div>
 
 </div>
 
 <script type="text/javascript"> 
		 
		function hideShowToggle(id)
		{
			if($('#' + id).hasClass('deactive'))
			{
				$('#' + id).removeClass('deactive');
				$('#' + id).show(400); 
				$('#toogle-icon').hide(400);
			}
			else
			{
				$('#' + id).addClass('deactive');		
				
				$('#' + id).hide(400);
				$('#toogle-icon').show(400);
			}
		}
		
		function tampilLegend()
		{
			if ( $('#legenHomepage').is(':visible' ))
			{
				$('#legenHomepage').hide();
			}
			else
			{
				$('#legenHomepage').show();
			}
			customHeight('#legenHomepage .card',1,50); 
			customHeight('#legen',1,150); 
		}
		
		
 
		AutoResizeDiv('#mapSDK',20); 
		  
		
		var container = document.getElementById('popup');
		var content = document.getElementById('popup-content');
		var closer = document.getElementById('popup-closer');

		closer.onclick = function() {
		  overlay.setPosition(undefined);
		  closer.blur();
		  return false;
		};

		var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
		  element: container,
		  autoPan: true,
		  autoPanAnimation: {
			duration: 250
		  }
		}));

		 var server= '';
			var styles = [
			  'Road',
			  'Aerial',
			  'AerialWithLabels',
			  'collinsBart',
			  'ordnanceSurvey'
			];
			var layers = [];
			var i, ii;
			for (i = 0, ii = styles.length; i < ii; ++i) {
			  layers.push(new ol.layer.Tile({
				visible: false,
				preload: Infinity,
				source: new ol.source.BingMaps({
				  key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3',
				  imagerySet: styles[i]
				  // use maxZoom 19 to see stretched tiles instead of the BingMaps
				  // "no photos at this zoom level" tiles
				  
				})
			  }));
			}
			
			var minZoom = function()
			{
				if(MaterHeight > 900)
				{
					return 4;
				}
				else
				{
					return 3;
				}
			}
			
			var map = new ol.Map({
			  layers: layers,
			  // Improve user experience by loading tiles while dragging/zooming. Will make
			  // zooming choppy on mobile or slow devices.
			  loadTilesWhileInteracting: true,
			  overlays: [overlay],
			  target: 'mapSDK',
			  view: new ol.View({
				center: ol.proj.transform([110,0], 'EPSG:4326', 'EPSG:3857'),
				zoom: 5,
				minZoom: minZoom(),
				maxZoom: 19
			  })
			});
 
			$('input[type=radio][name=base-layer]').change(function() {
				var style = this.value; 
				for (var i = 0, ii = layers.length; i < ii; ++i) {
					layers[i].setVisible(styles[i] === style);
				}
			});
			
			layers[0].setVisible(styles[0]);

			 
			  


		/**
		 * Add a click handler to the map to render the popup.
		 */

		var idInformation = document.getElementById('idInformation');
		map.on('click', function(e) {
			map.updateSize();

		  var coordinate = e.coordinate;
		  var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
				coordinate, 'EPSG:3857', 'EPSG:4326'));
			
		   map.forEachFeatureAtPixel(e.pixel, function (feature, layer) { 
				if(feature.get('idVms'))
				{
					$('#idInformation').load(urlServer + 'apivms/detailkapal/' + feature.get('idVms'));	
					
					//masuan ka inteaktion    
					pointSelect.getFeatures().clear();
					var features = pointSelect.getFeatures();  
					// now you have an ol.Collection of features that you can add features to 
					var featurePoint = new ol.Feature({
						  geometry: new ol.geom.Point(coordinate),
						  name: ''
					}); 
					features.push(featurePoint); 
									   
				} 
				else
				{

				} 

			}); 
		}); 

	</script>
	


	<script type="text/javascript"> 
		//DSS
		var vmsSource = new ol.source.Vector({
			url: "<?php echo url('public/apivms/mapkapal?statusVMS=1')?>",
			format: new ol.format.GeoJSON({ })
		}); 

		var vmSlayer = new ol.layer.Vector({
		  	source: vmsSource,
		  	opacity:1,
		  	style:stylePoint, 
		  	name:'pengawas',
		  	visible:true
		}); 
		map.addLayer(vmSlayer);
		
		 

		//zone wpp 
		var wppSource = new ol.source.Vector({
			url: "{{url('public/apiv1/datazone')}}",
			format: new ol.format.GeoJSON({ })
		}); 

		var wvmSlayer = new ol.layer.Vector({
		  	source: wppSource,
		  	style: styleFunctionZone,
		  	opacity:0.7,
		  	name:'wpp',
		  	visible:true
		}); 
		map.addLayer(wvmSlayer);

		//togle klik atau indak layar overflow 
		function loadSourceZone(sourceLama,url) {
			
			sourceLama.clear(true);
				var sourceZone = new ol.source.Vector({
				  	url: url,
				    format: new ol.format.GeoJSON({ })
				});

				wvmSlayer.setSource(sourceZone);

		}
		//akir refres data


		var DataZone = document.getElementById('DataZone');
					
			DataZone.addEventListener('click', function() {
				
				var getData = '?id=';
				 $('#DataZone input[type="checkbox"]:checked').each(function() {
				 		var val = $(this).val();
				 		getData+=val + ','; 			  		 
				 	});
				 	getData+='0'; 

				var url = "{{url('public/apiv1/datazone')}}" + getData;
		 	console.log(url);
		 	loadSourceZone(wvmSlayer.getSource(),url); 

		  }, false);


		function loadSourcePp(sourceLama,url) {
			
			sourceLama.clear(true);
				var sourceZone = new ol.source.Vector({
				  	url: url,
				    format: new ol.format.GeoJSON({ })
				});

				vmSlayer.setSource(sourceZone);

		} 

		 
        $('.btn-filter-map').click(function(){
            dataVmsmap();                
        })
 
        function dataVmsmap()
        {
            var negaraVMS = $('select[name=negaraVMS]').val();
            var alat_tangkapVMS = $('select[name=alat_tangkapVMS]').val();
            var statusVMS = $('select[name=statusVMS]').val();
	        var url="?statusVMS=" + statusVMS;


	        if(alat_tangkapVMS !='')
	        {
	        	url+='&alat_tangkapVMS=' +  alat_tangkapVMS;
	        }

	        if(negaraVMS !='')
	        {
	        	url+='&negaraVMS=' +  negaraVMS;
	        }

            url = urlServer + 'apivms/mapkapal'+ url;  
            
 			loadSourcePp(vmSlayer.getSource(),url);
        }


	   	var pointSelect = new ol.interaction.Select({
			condition: ol.events.condition.altShiftKeysOnly,
			style: selectFrancePoints
	   		});
	    map.addInteraction(pointSelect);

		// view = mapFP.getView(); 
		// $( "tr td button").click(function() { 
			
		// 	var lon = $(this).attr('lat'); // kebalik datanyo
		// 	var lat = $(this).attr('lon');
			
		// 	// var lon = lon.toFixed(3); 
		// 	// var lat = lat.toFixed(3); 
			

		// 	var lon = parseFloat(lon.replace(/\s/g, "").replace(",", ".")) + 0;
		// 	var lat = parseFloat(lat.replace(/\s/g, "").replace(",", ".")) + 0;
		  

		// 	var london = ol.proj.fromLonLat([lon,lat]);
		// 	 var pan = ol.animation.pan({
		// 		duration: 2000, 
		// 		source: /** @type {ol.Coordinate} */ (view.getCenter())
		// 	});
		// 	mapFP.beforeRender(pan);
		// 	view.setCenter(london); 
			

		// 	//masuan ka inteaktion    
		// 	trackSelect.getFeatures().clear();
		// 	var features = trackSelect.getFeatures();  
		// 	// now you have an ol.Collection of features that you can add features to 
		// 	var feature = new ol.Feature({
		// 		  geometry: new ol.geom.Point(ol.proj.transform([lon,lat], 'EPSG:4326', 'EPSG:3857')),
		// 		  name: ''
		// 	}); 
		// 	features.push(feature);

		// });

 
	</script>
