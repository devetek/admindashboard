<div class="card widget">
	<div class="card-header ch-alt m-b-20">
		<h2><?php echo count($data) ?> Data SKAT <?php echo $judul ?> </h2>
		<small>Kesuluruhan data Kapal dengan SKAT yang <?php echo $judul ?></small>
			
			<?php if($jenis =='table'){ ?>	
				<ul class="actions"> 
					<li class="dropdown">
		                <a data-toggle="dropdown" href="">
		                    <i class="zmdi zmdi-download"></i>
		                </a>
		                
		                <ul class="dropdown-menu dropdown-menu-right">
		                    <li>
		                        <a href="<?php echo url('apivms/sekat/'.$status.'/pdf') ?>" target="_blank">Pdf</a>
		                        <a href="<?php echo url('apivms/sekat/'.$status.'/excel') ?>" target="_blank">Excel</a>
		                    </li> 
		                </ul>
		            </li>

					<li>
						<a onClick="closeLayer('#layer2') ">
							<i class="zmdi zmdi-close"></i>
						</a>
					</li>
				</ul>
			<?php } ?>

	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="table-responsive" style="height:400px">
				<table id="kapalSKATAktif" class="table table-striped">
					<thead>
						 <tr> 
		                    <th>No</th>
                            <th>Nama Kapal</th>  
                            <th>No TRX</th>  
                            <th>Vessel ID</th>  
                            <th>Tgl Aktif</th>  
                            <th>Tgl Habis</th>  
                            <th>GT</th>   
		                </tr>
                    </thead>
                    <tbody>
						<?php $n=1; foreach ($data as  $x) { ?>
							<tr onclick="LoadModal('<?php echo url('apivms/detailkapal/'.$x->transmitter_id.'/modal') ?>')">
								<td><?php echo $n++ ?></td>
								<td><?php echo $x->name ?></td>
								<td><?php echo $x->transmitter_id ?></td>
								<td><?php echo $x->vessel_id ?></td>
								<td><?php echo $x->transmitter_pairing_begin_date ?></td>
								<td><?php echo $x->transmitter_pairing_end_date ?></td>
								<td><?php echo $x->gt ?></td>
							</tr>
						<?php } ?>
					</tbody>			  
				</table>
			</div>
		</div>
		  
	</div>
</div>
 
 <script type="text/javascript">

 	customHeight('.table-responsive','1.2','0');
 	$(document).ready(function() {
        $('#kapalSKATAktif').dataTable(
    		 {
			 	"info": true,
			 	"fixedHeader": true,
			 "bLengthChange": false,
			 "iDisplayLength": 500
			}
    	); 
 

	} );
 </script>