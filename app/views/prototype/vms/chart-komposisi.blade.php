<div class="card-sub-header" style="padding:2px 5px"> 
    <div class="row">	
		<div class="col-sm-8">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">
                        <select name="pilihanPie" class="form-control"> 
                            
                            <option value="alat_tangkap">Alat Tangkap</option>
                            <option value="negara">Negara</option> 
 
                        </select>
                    </div>
                </div>
            </div>
        </div> 
        <div class="col-sm-4">
            <button class="pull-right btn btn-primary btn-sm waves-effect btn-filter-tangkap" type="submit">Filter</button>
        </div>

	</div>

    <script type="text/javascript">
        
        AutoResizeDiv('.autoOverflow',20);  

    </script>

</div>	


<div id="Vmspie" style="width:100%; "></div>
<script type="text/javascript">

	AutoResizeDiv('#Vmspie',70);  
	 
</script>

<script type="text/javascript">
         
    var data='[]';
    var dataPie = $('select[name=pilihanPie]').val();
    var urlPie = urlServer + "apivms/kapalpie/" + dataPie;
    var lokasiPie='Vmspie';

    ChartPie();

 
    $('.btn-filter-tangkap').click(function(){ 
              
       dataPie = $('select[name=pilihanPie]').val(); 
       urlPie = urlServer + "apivms/kapalpie/" + dataPie;
       ChartPie();

    }) 
 

</script>

	