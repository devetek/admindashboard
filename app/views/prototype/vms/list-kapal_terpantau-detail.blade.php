<div class="card widget">
	<div class="card-header ch-alt m-b-20">
		<h2><?php echo count($data) ?> Data Kapal <?php echo $judul ?> </h2>
		<small>Kesuluruhan data Kapal dengan Keaktifan yang <?php echo $judul ?></small>
			<?php if($jenis =='table'){ ?>	
				<ul class="actions"> 
					<li class="dropdown">
		                <a data-toggle="dropdown" href="">
		                    <i class="zmdi zmdi-download"></i>
		                </a>
		                
		                <ul class="dropdown-menu dropdown-menu-right">
		                    <li>
		                        <a href="<?php echo url('apivms/kapal/'.$status.'/pdf') ?>" target="_blank">Pdf</a>
		                        <a href="<?php echo url('apivms/kapal/'.$status.'/excel') ?>" target="_blank">Excel</a>
		                    </li> 
		                </ul>
		            </li>

					<li>
						<a onClick="closeLayer('#layer2') ">
							<i class="zmdi zmdi-close"></i>
						</a>
					</li>
				</ul>
			<?php } ?>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="table-responsive" style="height:400px">
				<table id="kapalSKATAktif" class="table table-striped">
					<thead>
						 <tr> 
		                    <th>No</th>
                            <th>Nama Kapal</th>  
                            <th>No TRX</th>  
                            <th>Speed</th>  
                            <th>Negara</th>  
                            <th>ALKAP</th>  
                            <th>GT</th>   
                            <th>SIPI</th>   
		                </tr>
                    </thead>
                    <tbody> 
						<?php $n=1; foreach ($data as  $x) { ?>
							<tr onclick="LoadModal('<?php echo url('apivms/detailkapal/'.$x->no_transmitter.'/modal') ?>')">
								<td><?php echo $n++ ?></td>
								<td><?php echo $x->nama_kapal ?></td>
								<td><?php echo $x->id_vessel ?></td>
								<td><?php echo round($x->speed/10,2) ?></td>
								<td><?php echo $x->negara ?></td>
								<td><?php echo $x->alat_tangkap ?></td>
								<td><?php echo $x->ukuran_gt ?></td>
								<td><?php echo $x->no_sipi ?></td>
							</tr>
						<?php } ?>
					</tbody>			  
				</table>
			</div>
		</div>
		  
	</div>
</div>
 
 <script type="text/javascript">

 	customHeight('.table-responsive','1.2','0');
 	$(document).ready(function() {
        $('#kapalSKATAktif').dataTable(
    		 {
			 	"info": true,
			 	"fixedHeader": true,
			 "bLengthChange": false,
			 "iDisplayLength": 500
			}
    	); 
 

	} );
 </script>