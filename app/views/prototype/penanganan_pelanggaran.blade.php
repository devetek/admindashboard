@extends('shared.master-dashboard')

@section('content')

	<div class="col-md-4 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/penanganan_pelanggaran/pie')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Persentase Penanganan Pelanggaran</h2>
				
				<ul class="actions">
				<li>
					<a data-widgster="load" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				 
			</ul>
			</div>
			
			<div class="card-body">
				<div class="body">
					<div class="pl-body" id="">
					
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="col-md-8 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/penanganan_pelanggaran/map')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Peta Lokasi Pelangaran</h2>
				
				<ul class="actions"> 
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				 
			</ul>
			</div>
			
			<div class="card-body">
				<div class="body">
					<div class="pl-body" id="">
					
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-sm-12 padding-1">
		 <div class="card1 custom widget"
		data-widgster-load="{{url('prototype/penanganan_pelanggaran/list-pelanggaran')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="true">
            <div class="card-header">
                <h2>Data Penanganan Pelangaran</h2>
                
                <ul class="actions"> 
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				<li>
					<a data-widgster="expand" href="#">
						<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
					</a>
				</li>
				<li>
					<a data-widgster="collapse" href="#">
						<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
					</a>
				</li> 
			</ul> 
			</div>
            
			<div class="card-body">
				<div class="body autoOverflow">
						
				</div>
			</div>
        </div>
	</div>


@stop

@section('javascript')
    {{ HTML::style('public/vendors/openlayer/ol.css')}}
	{{ HTML::script('public/vendors/openlayer/ol.js')}}

	{{ HTML::script('public/devetek/mapsStyle.js')}} 
	
		<script type="text/javascript">
		 
		 </script>
		 @stop
