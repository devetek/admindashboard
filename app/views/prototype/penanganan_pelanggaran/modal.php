<div class="modal-content">
    <div class="modal-header">
        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
        <h4 id="myModalLabel18" class="modal-title text-align-center fw-bold mt"><?php echo $judul ?></h4>
    </div>
    <div class="modal-body bg-gray-lighter">
          <?php echo $data ?>
    </div>
    <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-gray" type="button">Close</button>
    </div>
</div>