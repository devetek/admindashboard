	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		<div style="width:800px" class="modal-dialog">
			<div class="modal-content">
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div> 
		</div> <!-- /.modal-dialog -->
	</div>
	<div class="row">   
        <div class="col-sm-3">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">  
                        <?php 
                        	$sql=" select wilayah from ADF_PP_PIDANA_TANGKAP group by wilayah";
                        	$wilayah = DB::select($sql); 
                         ?>
                        <select name="wilayah" class="form-control">
                        	<option value="">Semua wilayah</option>
                        	<?php foreach($wilayah as $x){ ?>
                            <option value="<?php echo $x->wilayah ?>"><?php echo $x->wilayah ?></option>                              
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div> 
		
		<div class="col-sm-3">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">  
                        <?php 
                        	$sql=" select proses_hukum from ADF_PP_PIDANA_TANGKAP group by proses_hukum";
                        	$proses_hukum = DB::select($sql); 
                         ?>
                        <select name="proses_hukum" class="form-control">
                        	<option value="">Semua Proses Hukum</option>
                        	<?php foreach($proses_hukum as $x){ ?>
                            <option value="<?php echo $x->proses_hukum ?>"><?php echo $x->proses_hukum ?></option>                              
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        </div> 

        <div class="col-sm-3">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">
                        <select name="tahun" class="form-control"> 
                            <?php for($a=date('Y');$a >= 2005 ;$a--){ ?>
                             
                                <option value="<?php echo $a ?>">Tahun <?php echo $a ?></option>

                            <?php } ?>

                        </select>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-sm-2">
            <button class="pull-right btn btn-primary btn-sm waves-effect btn-filter-data" type="submit">Filter</button>
        </div>

    </div>

    <script type="text/javascript">
        
        customHeight('.autoOverflow',2,80); 
        dataPP(); 

        $('.btn-filter-data').click(function(){
            dataPP();                
        })
 
        function dataPP()
        {
            var wilayah = $('select[name=wilayah]').val();
            var proses_hukum = $('select[name=proses_hukum]').val();
            var tahun = $('select[name=tahun]').val();
	        var url="?tahun=" + tahun;

	        

	        if(wilayah !='')
	        {
	        	url+='&wilayah=' +  wilayah;
	        }

	        if(proses_hukum !='')
	        {
	        	url+='&proses_hukum=' +  proses_hukum;
	        }

            $('#dataPP').load(urlServer + 'apipp/listpp/tabel'+ url); 
        }

    </script>

</div>


	<div class="row">
		<div class="col-sm-12">
			<div id="kapalSipigroup" class="table-responsive" >
				<table id="kapalSipi" class="table table-striped">
					<thead>
						 <tr>
		                    <th  data-column-id="id" data-visible="false">no</th>
		                    <th  data-column-id="negara">Nama Kapal</th>
		                    <th  data-column-id="negara">Bendera</th>
		                    <th  data-column-id="negara">Jml ABK</th>
		                    <th  data-column-id="jumlah">ALKAB</th> 
		                    <th  data-column-id="jumlah">Kp. Penangkap</th> 
		                    <th  data-column-id="jumlah">Adhock</th> 
		                    <th  data-column-id="jumlah">Proses Hukum</th>  
		                </tr>
					</thead>
					 <tbody id="dataPP"> 
						 
					</tbody>
				</table>
			</div>
		</div>
		 
	</div>
	
 