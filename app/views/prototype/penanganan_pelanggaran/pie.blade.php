<div class="card-header" style="padding:2px 5px"> 
    <div class="row">	
		<div class="col-sm-4">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">
                        <select name="tahun" class="form-control"> 
                            <?php for($a=date('Y');$a >= 2005 ;$a--){ ?>
                             
                            	<option value="<?php echo $a ?>">Tahun <?php echo $a ?></option>

                            <?php } ?>

                        </select>
                    </div>
                </div>
            </div>
        </div>

         <div class="col-sm-4">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">
                        <select name="jenis" class="form-control"> 
                            <option value="KEBANGSAAN_BENDERA_KAPAL">BENDERA KAPAL</option>
                            <option value="WILAYAH">WILAYAH</option>
                            <option value="INSTANSI_PENARIMA_ADHOCK">ADHOCK</option> 
                            <option value="PROSES_HUKUM">PROSES HUKUM</option> 

                        </select>
                    </div>
                </div>
            </div>
        </div>
         
        <div class="col-sm-4">
            <button class="pull-right btn btn-primary btn-sm waves-effect btn-filter-tangkap" type="submit">Filter</button>
        </div>

	</div>

    <script type="text/javascript">
        
        AutoResizeDiv('.autoOverflow',20);  

    </script>

</div>	


<div id="pieKp" style="width:100%; "></div>
<script type="text/javascript">
	
	AutoResizeDiv('#pieKp',70); 
	
	var tahun = $('select[name=tahun]').val(); 
    var jenis = $('select[name=jenis]').val(); 

    JudulPie = 'Tahun: ' + tahun +' Berdasarkan "' + jenis.replace(/\_/g,' ') + '"';

    var data='[]';
    var dataPie = tahun  + '/' + jenis;
    var urlPie = urlServer + "apipp/pppie/" + dataPie;
    var lokasiPie='pieKp';

    ChartPie();

 
    $('.btn-filter-tangkap').click(function(){ 
        
        var tahun = $('select[name=tahun]').val(); 
        var jenis = $('select[name=jenis]').val();

        JudulPie = 'Tahun: ' + tahun +' Berdasarkan "' + jenis.replace(/\_/g,' ') + '"';

        dataPie = tahun  + '/' + jenis; 
        urlPie = urlServer + "apipp/pppie/" + dataPie;
        ChartPie();

    }) 
</script>
 

	