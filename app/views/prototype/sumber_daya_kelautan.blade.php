@extends('shared.master-dashboard')

@section('content')
 

	<div class="col-sm-4 padding-1">
		<div class="card1 widget" 
		data-widgster-load="{{url('prototype/sumber_daya_kelautan/list-terupdate')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Data Terbaru</h2>
				
				<ul class="actions">
				<li>
					<a id="" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a  data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
			</ul>
			</div>
			<div class="card-body">
				<div class="body autoOverflow" >
				</div>
			</div>
		</div>
		
	</div>

	<div class="col-sm-8 padding-1">
  
		<div id="popup" class="ol-popup">
		  <a href="#" id="popup-closer" class="ol-popup-closer"></a>
		  <div id="popup-content"></div>
		</div>
 
		<div class="card widget" 
		data-widgster-load="{{url('prototype/sumber_daya_kelautan/map-sdk')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Maps</h2>
				
				<ul class="actions"> 
				<li>
					<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a  data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
			</ul>
			</div>
			<div class="card-body">
				<div class="body">
				</div>
			</div>
		</div>
        
	</div> 

 
	<div class="col-sm-8 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/sumber_daya_kelautan/chart-terumbu')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Grafik Perbadingan Pencemaran/Kerusakan/konservasi</h2>
				
				<ul class="actions">
				<li>
					<a id="" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a  data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
			</ul>
			</div>
			<div class="card-body">
				<div class="body">
				</div>
			</div>
		</div>
		
	</div>

	
	
@stop

@section('javascript')
    {{ HTML::style('public/vendors/openlayer/ol.css')}}
	{{ HTML::script('public/vendors/openlayer/ol.js')}}

	{{ HTML::script('public/devetek/mapsStyle.js')}} 
	
		<script type="text/javascript">
		 
		 </script>
		 @stop
