@extends('shared.master-dashboard')

@section('content')
	
	
	<div class="col-sm-6 col-md-2 padding-1">
		<div onclick="LoadLayer('#layer2','<?php echo url('apivms/sekat/1/table') ?>','#data')" class="mini-charts-item bgm-lightgreen pointer" >
			<div class="clearfix">
			   <div class="count">
					<small>Total SKAT berlaku</small>
					<h2 id="skatAktif">-</h2> 
				</div>
			</div> 
		</div>

		 <div class="mini-charts-item bgm-bluegray pointer" onclick="LoadLayer('#layer2','<?php echo url('apivms/sekat/0/table') ?>','#data')">
			<div class="clearfix">
				<div class="count">
					<small>Total SKAT akan habis</small>
					<h2 id="skatAkan">-</h2>
				</div>
			</div> 
		</div> 

		<div class="mini-charts-item bgm-lightgreen pointer" onclick="LoadLayer('#layer2','<?php echo url('apivms/kapal/1/table') ?>','#data')">
			<div class="clearfix">
				<div class="count">
					<small>Kapal Terpantau</small>
					<h2 id="terpantau">-</h2>
				</div>
			</div> 
		</div> 

		 <div class="mini-charts-item bgm-orange pointer" onclick="LoadLayer('#layer2','<?php echo url('apivms/kapal/0/table') ?>','#data')">
			<div class="clearfix">
				<div class="count">
					<small>Kapal Tidak Terpantau</small>
					<h2 id="tidakterpantau">-</h2>
				</div>
			</div> 
		</div> 
	</div>

	<script type="text/javascript"> 
    	function loadWidgeetData()
    	{
    		$('#skatAktif').load('{{url("apivms/sekat/1/count")}}'); 
    		$('#skatAkan').load('{{url("apivms/sekat/0/count")}}'); 
    		$('#terpantau').load('{{url("apivms/kapal/1/count")}}'); 
    		$('#tidakterpantau').load('{{url("apivms/kapal/0/count")}}'); 
    		 
    	}
    	loadWidgeetData();

    </script>

	<div class="col-sm-6 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/vms/map-vms')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Posisi Kapal Ikan pertanggal 27 Oktober 2015 </h2>
				
				<ul class="actions">
				<li>
					<a id="reloadMapPosisi" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a  data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
			</ul>
			</div>
			<div class="card-body">
				<div class="body">
				</div>
			</div>
		</div>
                        
		<script type="text/javascript">
			jQuery(document).ready(function(){
				var $widget_map = $('.widget-map')
				$widget_map.widgster();

				$widget_map.on("fullscreen.widgster", function(){
					$('.content-wrap').css({
						'-webkit-transform': 'none',
						'-ms-transform': 'none',
						transform: 'none',
						'margin': 0,
						'z-index': 2
					});
					jQuery("#lastPosisi").height("");
					//updateSizeMap();
				}).on("restore.widgster closed.widgster", function(){
					$('.content-wrap').css({
						'-webkit-transform': '',
						'-ms-transform': '',
						transform: '',
						margin: '',
						'z-index': ''
					});
					jQuery("#lastPosisi").height("");
					//map.updateSize();
				});
			});
		</script>
		
	</div>
   

	<div class="col-sm-6 col-md-4 padding-1">
		 
		<div class="card widget" 
		data-widgster-load="{{url('prototype/vms/list-suspect')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>18 Indikasi Pelanggaran 27 Oktober 2015</h2>
						
				<ul class="actions">
				<li>
					<a data-widgster="load" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				<li class="dropdown">
					<a href="" data-toggle="dropdown">
						<i class="zmdi zmdi-more-vert"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>	
						</li>
					</ul>
				</li>
			</ul>
			</div>
			
			<div class="card-body">
				<div class="body m-t-0 table-responsive" id="hostlistWG">
				
				</div>
			</div>
		</div> 
	</div> 
	
	 <div class="col-sm-6 col-md-8 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/vms/chart-kapal')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Grafik Indikasi Pelanggaran Tahunan</h2>
				
				<ul class="actions">
				<li>
					<a data-widgster="load" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				<li class="dropdown">
					<a href="" data-toggle="dropdown">
						<i class="zmdi zmdi-more-vert"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
						</li>
					</ul>
				</li>
			</ul>
			</div>
			
			<div class="card-body">
				<div class="body">
					<div class="pl-body" id="">
						<div style="width: 100%;" id="chartdivchartdiv">
						 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="col-sm-6 col-md-4 padding-1">
		<div class="card widget" 
		data-widgster-load="{{url('prototype/vms/chart-komposisi')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
		<div class="card-header">
			<h2>Grafik Komposisi Kapal</h2>
			<small>Informasi diambil dari data 5 Teratas dan Lainnya</small>
			 
			<ul class="actions">
				<li>
					<a data-widgster="load" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
				<li class="dropdown">
					<a href="" data-toggle="dropdown">
						<i class="zmdi zmdi-more-vert"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a onClick="LoadLayer('#layer2','{{url('prototype/vms/chart-komposisi-detail')}}','#data')">View Detail</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="card-body card-padding m-t-0">
			<div class="body">
			</div>
		</div>
	</div> 
		
	</div>   
                     
@stop

@section('javascript')
	
    {{ HTML::style('public/vendors/openlayer/ol.css')}}
	{{ HTML::script('public/vendors/openlayer/ol.js')}}

	{{ HTML::script('public/devetek/mapsStyle.js')}} 
	
	<script type="text/javascript">
	 
	 </script>

@stop
