 
<div class="card-header">
    <h2>2015</h2>

    <div class="row">	
		<div class="col-sm-4">
            <div class="form-group">
                <div class="fg-line">
                    <div class="select">
                        <select name="tahun" class="form-control">
                            <option value="0">Pilih Tahun</option>
                            <?php for($a=2005;$a <= date('Y') ;$a++){ ?>
                             
                            	<option value="<?php echo $a ?>"><?php echo $a ?></option>

                            <?php } ?>

                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <div class="fg-line">
                    <div class="select">
 

                        <select name="gelarOperasi" class="form-control">
                              <option value="0">Semua Operasi</option> 
                             <option value="GELAROPERASIMANDIRIKAPALPENGAWAS">Operasi Madiri</option> 
                             <option value="GELAROPERASIKAPALPENGAWASDENGANBAKORKAMLA">Bakamla</option> 
                             <option value="GELAROPERASIKAPALPENGAWASPANGKALANPSDKP">Pangkalan PSDKP</option> 
                             <option value="GELAROPERASIRUTINKAPALPENGAWAS">Kapal Pengawas</option> 
                             <option value="GELAROPERASIAUSINDOKAPALPENGAWAS">ASIDO</option> 
                             <option value="GELAROPERASIMALINDOKAPALPENGAWAS">MALINDO</option> 

                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <button class="btn btn-primary btn-sm waves-effect btn-filter-gelar" type="submit">Filter</button>
        </div>

	</div>

    <script type="text/javascript">
        
        AutoResizeDiv('.autoOverflow',20); 
        $('#gelarOpr').load(urlServer + 'apikp/gelaroperasi?tahun=2015');

        $('.btn-filter-gelar').click(function(){

                var gelarOperasi = $('select[name=gelarOperasi]').val();
                var tahun = $('select[name=tahun]').val();
              
                var url = '?tahun=' + tahun + '&gelarOpr='+ gelarOperasi; 
                $('#gelarOpr').load(urlServer + 'apikp/gelaroperasi'+ url);
        })

    </script>

</div>

<div class="table-responsive" style="overflow: hidden;" tabindex="3">
    <table class="table table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama K. Pengawas</th>
                <th>Hari OPS</th>
                <th>Riksa</th>
                <th>Tangkap</th>
            </tr>
        </thead>
        <tbody id="gelarOpr">
            <tr>
                <td>1</td>
                <td>Alexandra</td>
                <td>Christopher</td>
                <td>@makinton</td>
                <td>Ducky</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Madeleine</td>
                <td>Hollaway</td>
                <td>@hollway</td>
                <td>Cheese</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Sebastian</td>
                <td>Johnston</td>
                <td>@sebastian</td>
                <td>Jaycee</td>
            </tr>
            <tr>
                <td>4</td>
                <td>Mitchell</td>
                <td>Christin</td>
                <td>@mitchell4u</td>
                <td>AdskiDeAnus</td>
            </tr>
            <tr>
                <td>5</td>
                <td>Elizabeth</td>
                <td>Belkitt</td>
                <td>@belkitt</td>
                <td>Goat</td>
            </tr>
            <tr>
                <td>6</td>
                <td>Benjamin</td>
                <td>Parnell</td>
                <td>@wayne234</td>
                <td>Pokie</td>
            </tr>
            <tr>
                <td>7</td>
                <td>Katherine</td>
                <td>Buckland</td>
                <td>@anitabelle</td>
                <td>Wokie</td>
            </tr>
            <tr>
                <td>8</td>
                <td>Nicholas</td>
                <td>Walmart</td>
                <td>@mwalmart</td>
                <td>Spike</td>
            </tr>
        </tbody>
    </table>
</div>
                 