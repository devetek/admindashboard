			

			<div id="GrafikPerbandingan" style="width:100%; "></div>
			<script type="text/javascript">
				
				AutoResizeDiv('#GrafikPerbandingan',20);
				 
				AmCharts.makeChart("GrafikPerbandingan",
					 {
						"type": "serial",
						"categoryField": "category",
						"startDuration": 1,
						"categoryAxis": {
							"gridPosition": "start"
						},
						"trendLines": [],
						"graphs": [
							 
							{
								"balloonText": "[[title]] of [[category]]:[[value]]",
								"fillAlphas": 1,
								"id": "AmGraph-2",
								"title": "RIKSA",
								"type": "column",
								"valueField": "RIKSA"
							},
							{
								"balloonText": "[[title]] of [[category]]:[[value]]",
								"id": "AmGraph-3",
								"title": "Tangkap KII",
								"type": "column",
								"valueField": "Tangkap KII"
							},
							{
								"balloonText": "[[title]] of [[category]]:[[value]]",
								"id": "AmGraph-4",
								"title": "Tangkap KIA",
								"type": "column",
								"valueField": "Tangkap KIA"
							},
							{
								"balloonText": "[[title]] of [[category]]:[[value]]",
								"id": "AmGraph-5",
								"title": "Tangkap Total",
								"type": "column",
								"valueField": "Tangkap Total"
							}
						],
						"guides": [],
						"valueAxes": [
							{
								"id": "ValueAxis-1",
								"title": "Jumlah"
							}
						],
						"allLabels": [],
						"balloon": {},
						"legend": {
							"useGraphSettings": true
						}, 
						"dataProvider": [
							{
								"category": "2005",
								"RIKSA": "344",
								"Tangkap KII": "91",
								"Tangkap KIA": "24",
								"Tangkap Total": "115"
							},
							{
								"category": "2006",
								"RIKSA": "1447",
								"Tangkap KII": "83",
								"Tangkap KIA": "49",
								"Tangkap Total": "132"
							},
							{
								"category": "2007",
								"RIKSA": "2207",
								"Tangkap KII": "95",
								"Tangkap KIA": "91",
								"Tangkap Total": "186"
							},
							{
								"category": "2008",
								"RIKSA": "2178",
								"Tangkap KII": "119",
								"Tangkap KIA": "124",
								"Tangkap Total": "243"
							},
							{
								"category": "2009",
								"RIKSA": "3961",
								"Tangkap KII": "78",
								"Tangkap KIA": "125",
								"Tangkap Total": "203"
							},
							{
								"category": "2010",
								"RIKSA": "2255",
								"Tangkap KII": "24",
								"Tangkap KIA": "159",
								"Tangkap Total": "183"
							},
							{
								"category": "2011",
								"RIKSA": "3348",
								"Tangkap KII": "30",
								"Tangkap KIA": "76",
								"Tangkap Total": "106"
							},
							{
								"category": "2012",
								"RIKSA": "4326",
								"Tangkap KII": "42",
								"Tangkap KIA": "70",
								"Tangkap Total": "112"
							},
							{
								"category": "2013",
								"RIKSA": "3871",
								"Tangkap KII": "24",
								"Tangkap KIA": "44",
								"Tangkap Total": "68"
							},
							{
								"category": "2014",
								"RIKSA": "2044",
								"Tangkap KII": "22",
								"Tangkap KIA": "16",
								"Tangkap Total": "38"
							},
							{
								"category": "2015",
								"RIKSA": "4559",
								"Tangkap KII": "41",
								"Tangkap KIA": "53",
								"Tangkap Total": "94"
							}
						]
					}
											);
			</script>