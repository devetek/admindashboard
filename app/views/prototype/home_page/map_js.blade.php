<script type="text/javascript">
		//layer VMS
		var vmsSource = new ol.source.Vector({
			url: "<?php echo url('public/apivms/mapkapal?statusVMS=1')?>",
			format: new ol.format.GeoJSON({ })
		}); 

		var vmSlayer = new ol.layer.Vector({
		  	source: vmsSource,
		  	opacity:1,
		  	style:stylePoint, 
		  	name:'pengawas',
		  	visible:true
		}); 
		map.addLayer(vmSlayer);
		
		var vmsSource2 = new ol.source.Vector({
			url: "<?php echo url('public/apivms/mapkapal?statusVMS=0')?>",
			format: new ol.format.GeoJSON({ })
		}); 

		var vmSlayer2 = new ol.layer.Vector({
		  	source: vmsSource2,
		  	opacity:1,
		  	style:stylePoint, 
		  	name:'pengawas',
		  	visible:true
		}); 
		map.addLayer(vmSlayer2);



		//pengawas
		var pegawasSource = new ol.source.Vector({
			url: "{{url('public/geojson/pengawas.geojson')}}",
			format: new ol.format.GeoJSON({ })
		}); 

		var pengawasLayer = new ol.layer.Vector({
		  	source: pegawasSource,
		  	opacity:0.7,
		  	style:stylePengawas,
		  	name:'pengawas',
		  	visible:true
		}); 
		map.addLayer(pengawasLayer);



		//DSS
		var dssiSource = new ol.source.Vector({
			url: "{{url('public/apisdp/datadss')}}",
			format: new ol.format.GeoJSON({ })
		}); 

		var dssLayer = new ol.layer.Vector({
		  	source: dssiSource,
		  	opacity:1,
		  	style:pointDss,
		  	name:'pengawas',
		  	visible:false
		}); 
		map.addLayer(dssLayer);


		//Kp
		//tangkap
		var tangkapSource = new ol.source.Vector({
			url: "{{url('public/apikp/tangkap')}}",
			format: new ol.format.GeoJSON({ })
		}); 

		var tangkapLayer = new ol.layer.Vector({
		  	source: tangkapSource,
		  	opacity:1,
		  	style:stylePoint,
		  	name:'pengawas',
		  	visible:true
		}); 
		map.addLayer(tangkapLayer);
		 

		//zone wpp 
		var wppSource = new ol.source.Vector({
			url: "{{url('public/apiv1/datazone')}}",
			format: new ol.format.GeoJSON({ })
		}); 

		var wppLayer = new ol.layer.Vector({
		  	source: wppSource,
		  	style: styleFunctionZone,
		  	opacity:0.7,
		  	name:'wpp',
		  	visible:true
		}); 
		map.addLayer(wppLayer);

		//togle klik atau indak layar overflow 
		function loadSourceZone(sourceLama,url) {
			
			sourceLama.clear(true);
				var sourceZone = new ol.source.Vector({
				  	url: url,
				    format: new ol.format.GeoJSON({ })
				});

				wppLayer.setSource(sourceZone);

		}
		//akir refres data


		var DataZone = document.getElementById('DataZone');
					
			DataZone.addEventListener('click', function() {
				
				var getData = '?id=';
				 $('#DataZone input[type="checkbox"]:checked').each(function() {
				 		var val = $(this).val();
				 		getData+=val + ','; 			  		 
				 	});
				 	getData+='0'; 

				var url = "{{url('public/apiv1/datazone')}}" + getData;
		 	console.log(url);
		 	loadSourceZone(wppLayer.getSource(),url); 

		  }, false);


		var rubah = document.getElementById('hotlist');
			
		rubah.addEventListener('click', function() {

			if($('input[name=vmsLayer]:checked').length > 0)
			{ 
				vmSlayer.setVisible(true);
			}
			else
			{
				vmSlayer.setVisible(false);
			}

			if($('input[name=vmsLayerdua]:checked').length > 0)
			{ 
				console.log('sas')
				vmSlayer2.setVisible(true)
			}
			else
			{
				vmSlayer2.setVisible(false);
			} 

			if($('input[name=pengawasLayer]:checked').length > 0)
			{
				pengawasLayer.setVisible(true);
			}
			else
			{
				pengawasLayer.setVisible(false);
			}

			//SDP

			if($('input[name=Dsslayer]:checked').length > 0)
			{
				dssLayer.setVisible(true);
			}
			else
			{
				dssLayer.setVisible(false);
			}
			//KP

			if($('input[name=Kplayer]:checked').length > 0)
			{
				tangkapLayer.setVisible(true);
			}
			else
			{
				tangkapLayer.setVisible(false);
			}


		});	
</script>