<div id="mapHomepage"></div>
<div id="popup" class="ol-popup">
<a href="#" id="popup-closer" class="ol-popup-closer"></a>
  <div id="popup-content"></div>
</div>
 
 <div id="toogle-icon" class="toggled">
	<button onClick="hideShowToggle('hotlist')" class="btn bgm-cyan btn-float waves-effect waves-circle waves-float">
		<i class="zmdi zmdi-map"></i>
	</button>
 </div>
 
 <div id="hotlist" class="toggled" style="display:none">

 <div role="tabpanel"> 						                            

    <ul role="tablist" class="tab-nav" style="overflow: hidden;" tabindex="1">
        <li class="active"><a data-toggle="tab" role="tab" aria-controls="home11" href="#home11" aria-expanded="true">Layer</a></li>
        <li class=""><a data-toggle="tab" role="tab" aria-controls="profile11" href="#profile11" aria-expanded="false">Information</a></li>
        <li>
         	<a href="#" onClick="hideShowToggle('hotlist')">
				<i class="zmdi zmdi-close"></i>
			</a>
		</li>
    </ul>
  
    <div class="tab-content">
        <div id="home11" class="tab-pane active" role="tabpanel">
         			
			<div class="card">		
				<div class="card-body card-padding">	
					
					<div  role="tablist" aria-multiselectable="true" id="panel-accordion" class="panel-group">
						<div class="panel panel-collapse">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Base Layer
									</a>
								</h4>
							</div>
		 
			 
							<div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="radio m-b-15">
		                                <label>
		                                    <input type="radio" name="base-layer" value="Aerial">
		                                    <i class="input-helper"></i>
		                                    Bing Aerial
		                                </label>
		                            </div>
									
		                            <div class="radio m-b-15">
		                                <label>
		                                    <input type="radio" name="base-layer" value="AerialWithLabels">
		                                    <i class="input-helper"></i>
		                                    Bing Aerial with labels
		                                </label>
		                            </div>
		                            
		                            <div class="radio m-b-15">
		                                <label>
		                                    <input type="radio" selected name="base-layer" value="Road">
		                                    <i class="input-helper"></i>
		                                    Bing Road
		                                </label>
		                            </div>
		                            
		                            <div class="radio m-b-15">
		                                <label>
		                                    <input type="radio" name="base-layer" value="collinsBart">
		                                    <i class="input-helper"></i>
		                                    Bing Collins Bart
		                                </label>
		                            </div>
		                            
		                            <div class="radio m-b-15">
		                                <label>
		                                    <input type="radio" name="base-layer" value="ordnanceSurvey">
		                                    <i class="input-helper"></i>
		                                    Bing Ordnance Survey
		                                </label>
		                            </div>
		                            
		                             
								</div>
							</div>
						</div>
						 
						 
						<div class="panel panel-collapse">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										Zone WPP
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body" id="DataZone">
									 
									
									 <div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="14" name="wpp-layer" id="checkbox14">
								        <i class="input-helper"></i>
								            WPP-RI 711                     14        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="15"  name="wpp-layer" id="checkbox15">
								        <i class="input-helper"></i>
								            WPP-RI 712                     15        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="16" name="wpp-layer" id="checkbox16">
								        <i class="input-helper"></i>
								            WPP-RI 713                     16        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="17" name="wpp-layer" id="checkbox17">
								        <i class="input-helper"></i>
								            WPP-RI 714                     17        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="18" name="wpp-layer" id="checkbox18">
								        <i class="input-helper"></i>
								            WPP-RI 715                     18        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="19" name="wpp-layer" id="checkbox19">
								        <i class="input-helper"></i>
								            WPP-RI 716                     19        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="20" name="wpp-layer" id="checkbox20">
								        <i class="input-helper"></i>
								            WPP-RI 717                     20        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="21" name="wpp-layer" id="checkbox21">
								        <i class="input-helper"></i>
								            WPP-RI 718                     21        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="22" name="wpp-layer" id="checkbox22">
								        <i class="input-helper"></i>
								            WPP-RI 571                     22        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="23" name="wpp-layer" id="checkbox23">
								        <i class="input-helper"></i>
								            WPP-RI 573                     23        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="24" name="wpp-layer" id="checkbox24">
								        <i class="input-helper"></i>
								            WPP-RI 572                     24        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="41" name="wpp-layer" id="checkbox41">
								        <i class="input-helper"></i>
								            Teritorial                     41        </label>
								    </div>
								 	
									<div class="checkbox m-b-15"><label>
								        <input type="checkbox" value="42" name="wpp-layer" id="checkbox42">
								        <i class="input-helper"></i>
								            ZEE                            42        </label>
								    </div>
									
								</div>
							</div>
						</div>
						
						<div class="panel panel-collapse">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwoVMS" aria-expanded="false" aria-controls="collapseTwo">
										VMS
									</a>
								</h4>
							</div>
							<div id="collapseTwoVMS" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									
									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="vmsLayer" checked>
		                                    <i class="input-helper"></i>
		                                    Posisi Kapal Terpantau 
		                                </label>
		                            </div> 

									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="vmsLayerdua" checked>
		                                    <i class="input-helper"></i>
		                                    Posisi Kapal Tidak Terpantau 
		                                </label>
		                            </div>
									
									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="pengawasLayer" value="terpantau">
		                                    <i class="input-helper"></i>
		                                    Posisi Kapal Pegawas 
		                                </label>
		                            </div>
									
								</div>
							</div>
						</div>				
						
						<div class="panel panel-collapse">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwoSDK" aria-expanded="false" aria-controls="collapseTwo">
										Sumber Daya Kelautan
									</a>
								</h4>
							</div>
							<div id="collapseTwoSDK" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									
									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="vms-layer" value="terpantau">
		                                    <i class="input-helper"></i>
		                                    Pencemaran Laut 
		                                </label>
		                            </div>
									
									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="vms-layer" value="terpantau">
		                                    <i class="input-helper"></i>
		                                    kerusakan lahan bakau 
		                                </label>
		                            </div>
									
									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="vms-layer" value="terpantau">
		                                    <i class="input-helper"></i>
		                                    terumbu karang 
		                                </label>
		                            </div> 
		                            
									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="vms-layer" value="terpantau">
		                                    <i class="input-helper"></i>
		                                    Tambang Pasir 
		                                </label>
		                            </div> 

									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="vms-layer" value="terpantau">
		                                    <i class="input-helper"></i>
		                                    konservasi 
		                                </label>
		                            </div>

									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="vms-layer" value="terpantau">
		                                    <i class="input-helper"></i>
		                                    Boom Ikan 
		                                </label>
		                            </div>

									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="vms-layer" value="terpantau">
		                                    <i class="input-helper"></i>
		                                    BMKT 
		                                </label>
		                            </div> 
									
								</div>
							</div> 
							
						</div>

						
						<div class="panel panel-collapse">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwoSdp" aria-expanded="false" aria-controls="collapseTwo">
										Sumber Daya Perikanan 
									</a>
								</h4>
							</div>
							<div id="collapseTwoSdp" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									
									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="Dsslayer">
		                                    <i class="input-helper"></i>
		                                    Pelabuhan Perikanan  
		                                </label>
		                            </div>
									 
									 
									
								</div>
							</div> 
							
						</div>
						<div class="panel panel-collapse">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwoKP" aria-expanded="false" aria-controls="collapseTwo">
										Kapal Pengawas 
									</a>
								</h4>
							</div>
							<div id="collapseTwoKP" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									 
									<div class="checkbox  m-b-15">
		                                <label>
		                                    <input type="checkbox" name="Kplayer">
		                                    <i class="input-helper"></i>
		                                    Penakapan 1 Tahun Terakhir 
		                                </label>
		                            </div>
									 
									
								</div>
							</div> 
							
						</div>  
					<hr>
					<button onClick="tampilLegend()" class="btn btn-default btn-icon-text waves-effect"><i class="zmdi zmdi-pin-help"></i> Show Legend</button>
				</div>
				
			</div>
			</div>
		</div>

		<div id="profile11" class="tab-pane" role="tabpanel"> 
		        	<div class="card">		
						<div id="idInformation" class="card-body card-padding">

						</div>	
					</div>	

		        </div>
	
 </div>
 
 <div id="legenHomepage" style="display:none" class="col-md-3"> 
	<div class="card">	
		<div class="card-header">
			<h2>Overlays Layer</h2>
			<ul class="actions">
				<li class="dropdown action-show">
					<a href="#" onClick="tampilLegend()">
						<i class="zmdi zmdi-close"></i>
					</a> 
				</li>  
			</ul>
		</div>
					
		
		<div id="legen" class="card-body card-padding"> </div>
		  
	</div>
 
 </div>
 
 <script type="text/javascript"> 
		$( "#legenHomepage" ).draggable();
		
		function hideShowToggle(id)
		{
			if($('#' + id).hasClass('deactive'))
			{
				$('#' + id).removeClass('deactive');
				$('#' + id).show(400); 
				$('#toogle-icon').hide(400);
			}
			else
			{
				$('#' + id).addClass('deactive');		
				
				$('#' + id).hide(400);
				$('#toogle-icon').show(400);
			}
		}
		
		function tampilLegend()
		{
			if ( $('#legenHomepage').is(':visible' ))
			{
				$('#legenHomepage').hide();
			}
			else
			{
				$('#legenHomepage').show();
			}
			customHeight('#legenHomepage .card',1,50); 
			customHeight('#legen',1,150); 
		}
		
		
		customHeight('#panel-accordion',2,100); 
		customHeight('.card1',2,0);
		customHeight('#mapHomepage',2,0); 
		  
		
		var container = document.getElementById('popup');
		var content = document.getElementById('popup-content');
		var closer = document.getElementById('popup-closer');

		closer.onclick = function() {
		  overlay.setPosition(undefined);
		  closer.blur();
		  return false;
		};

		var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
		  element: container,
		  autoPan: true,
		  autoPanAnimation: {
			duration: 250
		  }
		}));

		 var server= '';
			var styles = [
			  'Road',
			  'Aerial',
			  'AerialWithLabels',
			  'collinsBart',
			  'ordnanceSurvey'
			];
			var layers = [];
			var i, ii;
			for (i = 0, ii = styles.length; i < ii; ++i) {
			  layers.push(new ol.layer.Tile({
				visible: false,
				preload: Infinity,
				source: new ol.source.BingMaps({
				  key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3',
				  imagerySet: styles[i]
				  // use maxZoom 19 to see stretched tiles instead of the BingMaps
				  // "no photos at this zoom level" tiles
				  
				})
			  }));
			}
			
			var minZoom = function()
			{
				if(MaterHeight > 900)
				{
					return 4;
				}
				else
				{
					return 3;
				}
			}
			
			var map = new ol.Map({
			  layers: layers,
			  // Improve user experience by loading tiles while dragging/zooming. Will make
			  // zooming choppy on mobile or slow devices.
			  loadTilesWhileInteracting: true,
			  overlays: [overlay],
			  target: 'mapHomepage',
			  view: new ol.View({
				center: ol.proj.transform([110,0], 'EPSG:4326', 'EPSG:3857'),
				zoom: 5,
				minZoom: minZoom(),
				maxZoom: 19
			  })
			});
 
			$('input[type=radio][name=base-layer]').change(function() {
				var style = this.value;
				console.log('sdsd000');
				for (var i = 0, ii = layers.length; i < ii; ++i) {
					layers[i].setVisible(styles[i] === style);
				}
			});
			
			layers[0].setVisible(styles[0]);

			
			
			function updateSizeMap(){
				map.updateSize();
			}
			
			  


		/**
		 * Add a click handler to the map to render the popup.
		 */

		 var pointSelect = new ol.interaction.Select({
			condition: ol.events.condition.altShiftKeysOnly,
			style: selectFrancePoints
	   		});
	    map.addInteraction(pointSelect);

		map.on('click', function(e) {
			map.updateSize();

		  var coordinate = e.coordinate;
		  var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
				coordinate, 'EPSG:3857', 'EPSG:4326'));
			
		   map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
				if(feature.get('idVms'))
				{
					$('#idInformation').load(urlServer + 'apivms/detailkapal/' + feature.get('idVms'));	
					
					//masuan ka inteaktion    
					pointSelect.getFeatures().clear();
					var features = pointSelect.getFeatures();  
					// now you have an ol.Collection of features that you can add features to 
					var featurePoint = new ol.Feature({
						  geometry: new ol.geom.Point(coordinate),
						  name: ''
					}); 
					features.push(featurePoint); 
									   
				}
				else if(feature.get('idDss'))
				{
					$('#idInformation').load(urlServer + 'apisdp/detaildss/' + feature.get('idDss'));					   
					
					pointSelect.getFeatures().clear();
					var features = pointSelect.getFeatures();  
					// now you have an ol.Collection of features that you can add features to 
					var featurePoint = new ol.Feature({
						  geometry: new ol.geom.Point(coordinate),
						  name: ''
					}); 
					features.push(featurePoint);

				} 
				else if(feature.get('idKp'))
				{
					$('#idInformation').load(urlServer + 'apikp/detailtangkap/' + feature.get('idKp')); 
				
					//masuan ka inteaktion    
					pointSelect.getFeatures().clear();
					var features = pointSelect.getFeatures();  
					// now you have an ol.Collection of features that you can add features to 
					var featurePoint = new ol.Feature({
						  geometry: new ol.geom.Point(coordinate),
						  name: ''
					}); 
					features.push(featurePoint); 
				} 
				else
				{

				}
				

			}); 
		});



		//map
		var valueData=[]; 
		
		var jenis='line-width';                  
		var stylePangan = function(feature, resolution) { 
				var text = feature.get(jenis);
				//text = text.replace(',','.');
				console.log('sds');

				cekBanyakValue(jenis);

				var Max = Math.max.apply(Math,valueData);
				var Min = Math.min.apply(Math,valueData);

				var total = (Max - Min) / 8;

				// console.log(Min);
				// console.log(total);
				// console.log(text);

				makeLegen(warnaBg2 , total, jenis);

				var indexBg=0;

				if(text < total)
				{
					var indexBg=0;
				}

				else if(text < (total *2) )
				{
					var indexBg=1;
				}
				else if(text < (total *3) )
				{
					var indexBg=2;
				}
				else if(text < (total *4) )
				{
					var indexBg=3;
				}
				else if(text < (total *5) )
				{
					var indexBg=4;
				}
				else if(text < (total *6) )
				{
					var indexBg=5;
				}
				else if(text < (total *7) )
				{
					var indexBg=6;
				}
				else
				{
					var indexBg=7; //samo jo index 8
				}

				//var color = ['#D2527F','#663399','#446CB3','#4183D7','#52B3D9','#22A7F0','#81CFE0']
				return [new ol.style.Style({
					stroke: new ol.style.Stroke({
					  color: '#52403C',
					  width: 0.1
					}),
					fill: new ol.style.Fill({
					  color: warnaBg2[indexBg]
					}),
					
					text: new ol.style.Text({
					  text: text, 
					  scale: 1, 
					  fill: new ol.style.Fill({
						color: '#fff'
					  }),

					  stroke: new ol.style.Stroke({
						color: '#FFFF99',
						width: 1
					  })
					})

				  })];
			};

			function  cekBanyakValue(data)
			{
				valueData=[];      
				vectorLayer.getSource().forEachFeature(function(e) {
					valueData.push(e.get(data) );  
				});
				//console.log(valueData);
				 
			}

			function makeLegen(color,interval,judul)
			{
				var legen = document.getElementById('legen');
				var html='<span>Keterangan</span>';
					html+='<table class="legen-table table table-bordered">';
				
				var count = 5 ;
				  for(a=0;a<8;a++)
				  {
					html+='<tr>';
					html+='<td style="background :' + color[a] + '; padding: 10px; */"></td>';
					html+='<td> <= ' +  (count)  +' Kapal</td>';
					count = count + 5;
					//html+='<td> <= ' +  (interval * (a + 1))  +' Kapal</td>';
					html+='</tr>';
				  }
				  
					html+='<tr>';
					html+='<td style="background : #cc0000; padding: 10px; */"></td>';
					count = count - 5;
					html+='<td> > ' +  (count)  +' Kapal</td>';
					//html+='<td> <= ' +  (interval * (a + 1))  +' Kapal</td>';
					html+='</tr>';

				
				html+='</table><br>';
				legen.innerHTML=html;

			}

			$('#reloadMapTangkap').on('click',function()
			{
				
				var tinggi = ($(window).height() - 100);
				var lebar = ($(window).width() - 20);
				
				 $('#maptangkap canvas').attr('width',lebar);
				 $('#maptangkap canvas').attr('height',tinggi);
				  map.updateSize();
				  
				  console.log(tinggi);
				   // if($('#maptangkap canvas').attr('width') == lebar)
				   // {
						// console.log(lebar);
						 // map.updateSize();
				   // }
					
				
			});


		//layar posisi kapal Pengawas 
		// var LastDataSource = new ol.source.Vector({
		// 	url: "{{url('public/geojson/zone_pelanggaran.geojson')}}",
		// 	format: new ol.format.GeoJSON({ })
		// }); 

		// var vectorLayer = new ol.layer.Vector({
		//   source: LastDataSource,
		//   opacity:0.7,
		//   name:'lastposisi' ,
		//   style:stylePangan,
		//   opacity:0.5
		// });

		// map.addLayer(vectorLayer);

 

		$('#reloadMapPosisi').on('click',function()
		{ 
			map.updateSize();
			 
		});

	</script>
	@include("prototype/home_page/map_js") 