@extends('shared.master-dashboard')

@section('content')
 
				<div class="mini-charts col-sm-12 padding-1">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <div onclick="LoadLayer('#layer2','<?php echo url('apisdp/dataslo') ?>','#data')" class="mini-charts-item bgm-cyan" style="cursor: pointer;">
                                    <div class="clearfix">
                                        <div class="count">
                                            <small>Jumlah SLO</small> 
                                            <h2 id="countSLO">2,078</h2>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                            
                            <div class="col-sm-6 col-md-3">
                                <div class="mini-charts-item bgm-bluegray" style="cursor: pointer;">
                                    <div class="clearfix">
                                        <div class="count">
                                            <small>Jumlah Tangkapan</small>
                                            <h2 id="countTANGKAP">-</h2>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                            
                            <div class="col-sm-6 col-md-3">
                                <div onclick="LoadLayer('#layer2','<?php echo url('apisdp/dataobat') ?>','#data')" class="mini-charts-item bgm-lightgreen" style="cursor: pointer;">
                                    <div class="clearfix">
                                        <div class="count">
                                            <small>Pemeriksaan Obat</small>
                                            <h2 id="countOBAT">0*</h2>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                            
                            <div class="col-sm-6 col-md-3">
                                <div onclick="LoadLayer('#layer2','<?php echo url('apisdp/dataobat') ?>','#data')" class="mini-charts-item bgm-orange" style="cursor: pointer;">
                                    <div class="clearfix">
                                         <div class="count">
                                            <small>Pemeriksaan Pakan</small>
                                            <h2 id="countPAKAN">0*</h2>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </div>
                            
                        </div>
                    </div>

                    <script type="text/javascript"> 
                    	function loadWidgeetData()
                    	{
                    		$('#countSLO').load('{{url("apisdp/countdata/slo")}}');
                    		$('#countOBAT').load('{{url("apisdp/countdata/obat")}}');
                    		$('#countPAKAN').load('{{url("apisdp/countdata/pakan")}}');
                    		$('#countTANGKAP').load('{{url("apisdp/countdata/tangkap")}}');
                    	}
                    	loadWidgeetData();

                    </script>

				
					<div class="col-sm-8 padding-1">
                        <div class="card widget"
							<div class="card widget"
							data-widgster-load="{{url('prototype/sumber_daya_perikanan/map_slo')}}"
							data-widgster-autoload="true"
							data-widgster-show-loader="true">
							<div class="card-header">
								<h2>Posisi DSS/SLO</h2>
								
								<ul class="actions"> 
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li> 
							</ul> 
							</div>
							
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-4 padding-1">
                         <!-- Picture List -->
						<div class="card widget"
						data-widgster-load="{{url('prototype/sumber_daya_perikanan/chart_jenis_pengawasan_p3')}}"
						data-widgster-autoload="true"
						data-widgster-show-loader="true">
							<div class="card-header">
								<h2>Grafik Pengawasan P3</h2>
								
								<ul class="actions">
								<li>
									<a data-widgster="load" href="#">
										<i class="zmdi zmdi-refresh-alt"></i>
									</a>
								</li>
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li>
								<li class="dropdown">
									<a href="" data-toggle="dropdown">
										<i class="zmdi zmdi-more-vert"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
										</li>
									</ul>
								</li>
							</ul> 
							</div>
							
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-8 padding-1">
                        <div class="card1 widget"
						data-widgster-load="{{url('prototype/sumber_daya_perikanan/chart_jenis_ikan_perbulan')}}"
						data-widgster-autoload="true"
						data-widgster-show-loader="true">
                            <div class="card-header">
                                <h2>Grafik Perbandingan Hasil Tangkapan Ikan Perbulan</h2>
                                
                                <ul class="actions">
								<li>
									<a data-widgster="load" href="#">
										<i class="zmdi zmdi-refresh-alt"></i>
									</a>
								</li>
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li>
								<li class="dropdown">
									<a href="" data-toggle="dropdown">
										<i class="zmdi zmdi-more-vert"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
										</li>
									</ul>
								</li>
							</ul> 
							</div>
                            
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
                        </div>
                    </div>
 
					<div class="col-sm-4 padding-1">
                         <!-- Picture List -->
						<div class="card widget"
						data-widgster-load="{{url('prototype/sumber_daya_perikanan/chart_jenis_pengawasan_berpangkalan')}}"
						data-widgster-autoload="true"
						data-widgster-show-loader="true">
							<div class="card-header">
								<h2>Grafik Data Ketaatan Berpangkalan</h2>
								
								<ul class="actions">
								<li>
									<a data-widgster="load" href="#">
										<i class="zmdi zmdi-refresh-alt"></i>
									</a>
								</li>
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li>
								<li class="dropdown">
									<a href="" data-toggle="dropdown">
										<i class="zmdi zmdi-more-vert"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
										</li>
									</ul>
								</li>
							</ul> 
							</div>
							
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-4 padding-1">
                	 <!-- Picture List -->
						<div class="card widget"
						data-widgster-load="{{url('prototype/sumber_daya_perikanan/chart_jenis_pengawasan_budidaya')}}"
						data-widgster-autoload="true"
						data-widgster-show-loader="true">
							<div class="card-header">
								<h2>Pengawasan Usaha Budidaya</h2>
								
								<ul class="actions">
								<li>
									<a data-widgster="load" href="#">
										<i class="zmdi zmdi-refresh-alt"></i>
									</a>
								</li>
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li>
								<li class="dropdown">
									<a href="" data-toggle="dropdown">
										<i class="zmdi zmdi-more-vert"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
										</li>
									</ul>
								</li>
							</ul> 
							</div>
							
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12 padding-1">
						 <div class="card1 custom widget"
						data-widgster-load="{{url('prototype/sumber_daya_perikanan/detail_keberangkatan_kapal')}}"
						data-widgster-autoload="true"
						data-widgster-show-loader="true">
                            <div class="card-header">
                                <h2>Daftar Keberangkatan Kapal Hari Ini</h2>
                                
                                <ul class="actions">
								<li>
									<a data-widgster="load" href="#">
										<i class="zmdi zmdi-refresh-alt"></i>
									</a>
								</li>
								<li>
									<a data-widgster="fullscreen" href="#">
										<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
									</a>
								</li>
								<li>
									<a data-widgster="restore" href="#">
										<i class="zmdi zmdi-window-restore"></i>
									</a>
								</li>
								<li>
									<a data-widgster="expand" href="#">
										<i class="zmdi zmdi-chevron-down zmdi-hc-fw"></i>						
									</a>
								</li>
								<li>
									<a data-widgster="collapse" href="#">
										<i class="zmdi zmdi-chevron-up zmdi-hc-fw"></i>						
									</a>
								</li>
								<li class="dropdown">
									<a href="" data-toggle="dropdown">
										<i class="zmdi zmdi-more-vert"></i>
									</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li>
											<a onClick="LoadLayer('#layer2','{{url('prototype/vms/list-suspect-detail')}}','#data')">View Detail</a>
										</li>
									</ul>
								</li>
							</ul> 
							</div>
                            
							<div class="card-body">
								<div class="body">
										
								</div>
							</div>
                        </div>
					</div>

					<script type="text/javascript">
					 	customHeight('.card1',1,0);
					 </script>					

@stop

@section('javascript')
    {{ HTML::style('public/vendors/openlayer/ol.css')}}
	{{ HTML::script('public/vendors/openlayer/ol.js')}}

	{{ HTML::script('public/devetek/mapsStyle.js')}} 
	
		<script type="text/javascript">
		 
		 </script>
		 @stop
