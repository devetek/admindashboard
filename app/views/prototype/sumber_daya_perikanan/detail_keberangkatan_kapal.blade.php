	<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		<div style="width:800px" class="modal-dialog">
			<div class="modal-content">
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div> 
		</div> <!-- /.modal-dialog -->
	</div>
 	
 	 
    <div class="row">   
        <div class="col-sm-4">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">  
                        <select name="jenisKapal" class="form-control">
                            <option value="ADF_SDP_KBRNGKTN_ANGKUT">Kapal Angkut</option> 
                            <option value="ADF_SDP_KBRNGKTN_TANGKAP">Kapal tangkap</option>   
                        </select>
                    </div>
                </div>
            </div>
        </div> 
 
        
        <div class="col-sm-4">
            <button class="btn btn-primary btn-sm waves-effect btn-filter-data" type="submit">Filter</button>
        </div>

    </div>

    <script type="text/javascript">
        
        customHeight('.autoOverflow',2,80); 
        tableKeberangkatan(); 

        $('.btn-filter-data').click(function(){
            tableKeberangkatan();                
        }) 
        

        function tableKeberangkatan()
        {
            var jenisKapal = $('select[name=jenisKapal]').val();  
            $('#tableKeberangkatan').load(urlServer + 'apisdp/kapalberangkat/'+ jenisKapal); 
        }

    </script>


	<div class="row">

		<div class="col-sm-12">
			<div id="kapalSipigroup" class="table-responsive" >
				<table id="kapalSipi" class="table table-striped">
					<thead>
						 <tr>
		                    <th  data-column-id="id" data-visible="false">No</th>
		                    <th  data-column-id="negara">Nama Kapal</th>
		                    <th  data-column-id="jumlah">Bendera</th> 
		                    <th  data-column-id="jumlah">Nahkoda</th> 
		                    <!-- <th  data-column-id="jumlah">P. Pangkalan</th>  -->
		                    <th  data-column-id="jumlah">SATKER</th> 
		                    <th  data-column-id="jumlah">No SIPI</th> 
		                    <th  data-column-id="jumlah">Temapat Periksa</th> 
		                    <th  data-column-id="jumlah">Analisa</th> 
		                </tr>
					</thead>
					 <tbody id="tableKeberangkatan">
                      	

					</tbody>
				</table>
			</div>
		</div>
		  
	</div>
	
 