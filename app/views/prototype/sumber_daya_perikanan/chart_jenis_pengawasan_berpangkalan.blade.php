<div style="width: 100%; height: 350px;" id="PengawasanBerpangkalan">
										 
</div>

<script type="text/javascript">
AutoResizeDiv('#PengawasanBerpangkalan',50);

jQuery(document).ready(function(){
	AmCharts.makeChart("PengawasanBerpangkalan",
 		{
			"type": "pie",
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			"innerRadius": "40%",
			"titleField": "category",
			"valueField": "column-1",
			"theme": "light",
			"allLabels": [],
			"balloon": {},
			"legend": {
				"align": "center",
				"markerType": "circle"
			},
			"titles": [],
			"dataProvider": [
				{
					"category": "Taat",
					"column-1": 6
				},
				{
					"category": "Tidak",
					"column-1": 2
				}
			]
		}
	)
	});


</script>