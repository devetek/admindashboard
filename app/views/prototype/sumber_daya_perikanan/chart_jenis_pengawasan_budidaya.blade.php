<div class="card-header" style="padding:2px 5px"> 
    <div class="row">	
		<div class="col-sm-4">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">
                        <select name="tahunBudidaya" class="form-control"> 
                            <?php for($a=date('Y');$a >= 2005 ;$a--){ ?>
                             
                            	<option value="<?php echo $a ?>"><?php echo $a ?></option>

                            <?php } ?>

                        </select>
                    </div>
                </div>
            </div>
        </div>

         <div class="col-sm-4">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">
                        <select name="jenisBudidaya" class="form-control"> 
                            <option value="JENIS_USAHA">Jenis Budidaya</option> 
                        </select>
                    </div>
                </div>
            </div>
        </div>
         
        <div class="col-sm-4">
            <button class="pull-right btn btn-primary btn-sm waves-effect btn-filter-budidaya" type="submit">Filter</button>
        </div>

	</div>

    <script type="text/javascript">
        
        //AutoResizeDiv('.autoOverflow',20);  

    </script>

</div>	


<div id="pieKp" style="width:100%; "></div>
<script type="text/javascript">
	
	AutoResizeDiv('#pieKp',70); 
	
	function  AmChartspiesdp2(dataPie)
	{  

		AmCharts.makeChart("pieKp",
			 {
				"type": "pie",
				"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
				"innerRadius": "40%",
				"labelText": " [[percents]]%",
				"minRadius": 7,
				"titleField": "category",
				"valueField": "column-1",
				"theme": "light",
				"allLabels": [],
				"balloon": {},
				"legend": {
					"align": "center",
					"markerType": "circle"
				},
				"titles": [],
				 "dataLoader": {
				    "url": dataPie,
				    "format": "json"
				  }


			}
		);
	}
</script>

<script type="text/javascript">
         
    var data='[]';
    Sdp2pie('{{ date('Y') }}',$('select[name=jenisBudidaya]').val());


    $('.btn-filter-budidaya').click(function(){  
             
       var tahunBudidaya = $('select[name=tahunBudidaya]').val(); 
       var jenisBudidaya = $('select[name=jenisBudidaya]').val(); 
       Sdp2pie(tahunBudidaya,jenisBudidaya); 

    })

    function Sdp2pie(tahun,jenis)
    {
	    AmChartspiesdp2(urlServer + "apisdp/budayapie/" + tahun  + '/' + jenis);
    }
 
</script>

	