<div class="card-header" style="padding:2px 5px"> 
    <div class="row">	
		<div class="col-sm-3">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">
                        <select name="tahun" class="form-control"> 
                            <?php for($a=date('Y') ;$a >= 2010  ;$a--){ ?>
                             
                            	<option value="<?php echo $a ?>"><?php echo $a ?></option>

                            <?php } ?>

                        </select>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="col-sm-3">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select">
                        <select name="bulan" class="form-control"> 
                            <?php for($a=1 ;$a <= 12  ;$a++){ ?>
                             
                            	<option value="<?php echo $a ?>">bulan <?php echo $a ?></option>

                            <?php } ?>

                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group" style="margin:1px">
                <div class="fg-line">
                    <div class="select"> 
                        <select name="jenis" class="form-control"> 
                            <option value="0">Kapal Tangkap</option>
                            <option value="1">Kapal Pengangkut</option> 
                        </select>
                    </div>
                </div>
            </div>
        </div>
         
        <div class="col-sm-2">
            <button class="pull-right btn btn-primary btn-sm waves-effect btn-filter-tangkap" type="submit">Filter</button>
        </div>

	</div>

    

</div>	

<div style="width: 100%; " id="HasilTangkapanBulanan">
										 
</div>

<script type="text/javascript"> 
	

   	customHeight('#HasilTangkapanBulanan',2,120); 
   	customHeight('.card1',2,20); 

	function AmChartspieKp(dataPie)
	{
		AmCharts.makeChart("HasilTangkapanBulanan",
		{
				"type": "serial",
				"categoryField": "category",
				"startDuration": 1,
				"categoryAxis": {
					"gridPosition": "start",
					"labelRotation":100
				},
				"chartCursor": {},
				"chartScrollbar": {},
				"trendLines": [],
				"graphs": [
					{
						"fillAlphas": 1,
						"fillColors": "#FF8000",
						"id": "AmGraph-1",
						"title": "graph 1",
						"type": "column",
						"valueField": "column-1"
					}
				],
				"guides": [],
				"valueAxes": [
					{
						"id": "ValueAxis-1",
						"title": "Axis title"
					}
				],
				"allLabels": [],
				"balloon": {},
				"titles": [
					{
						"id": "Title-1",
						"size": 15,
						"text": "Chart Title"
					}
				],
				 "dataLoader": {
				    "url": dataPie,
				    "format": "json"
				  }
			}
		);
	};


	

</script>

<script type="text/javascript">
       
        var data='[]';
        Tangkappie('{{ date('Y') }}','{{ date('m') }}',$('select[name=jenis]').val());


        $('.btn-filter-tangkap').click(function(){ 
                 
           var tahun = $('select[name=tahun]').val(); 
           var bulan = $('select[name=bulan]').val(); 
           var jenis = $('select[name=jenis]').val(); 
           Tangkappie(tahun,bulan,jenis); 
  
        })

        function Tangkappie(tahun,bulan,jenis)
        {
		    $.get(urlServer + "apisdp/dataikan/" + tahun  + '/' + jenis, function(data, status){
		        data = data;
		        AmChartspieKp(urlServer + "apisdp/dataikan/" + tahun  + '/'+ bulan  + '/' + jenis);
		    });
        }

    </script>