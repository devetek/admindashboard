<div style="width: 100%; height: 350px;" id="PengawasanP3">
										 
</div>

<script type="text/javascript">

AutoResizeDiv('#PengawasanP3',50);

jQuery(document).ready(function(){
	AmCharts.makeChart("PengawasanP3",
{
	"type": "serial",
	"categoryField": "category",
	"angle": 30,
	"depth3D": 10,
	"colors": [
		"#4169E1",
		"#FF3300"
	],
	"startDuration": 1,
	"categoryAxis": {
		"gridPosition": "start"
	},
	"trendLines": [],
	"graphs": [
		{
			"balloonText": "[[title]]: [[value]]",
			"fillAlphas": 1,
			"id": "AmGraph-1",
			"title": "Pemeriksaan",
			"type": "column",
			"valueField": "column-1"
		},
		{
			"balloonText": "[[title]]: [[value]]",
			"fillAlphas": 1,
			"id": "AmGraph-2",
			"title": "Kesesuaian dg UU",
			"type": "column",
			"valueField": "column-2"
		}
	],
	"guides": [],
	"valueAxes": [
		{
			"id": "ValueAxis-1",
			"title": "Jumlah"
		}
	],
	"allLabels": [],
	"balloon": {},
	"legend": {
		"useGraphSettings": true,
		"maxColumns": 2
	},
	"titles": [
		{
			"id": "Title-1",
			"size": 11,
			"text": " Periode 01-01-2015 s/d 25-10-2015"
		}
	],
	"dataProvider": [
		{
			"category": "Impor",
			"column-1": 3,
			"column-2": 2
		},
		{
			"category": "Verifikasi UPI",
			"column-1": 23,
			"column-2": 22
		},
		{
			"category": "Uji Petik Tradisional",
			"column-1": 6,
			"column-2": 6
		}
	]
}
	)
	});


</script>