<div class="card widget">
	<div class="card-header ch-alt m-b-20">
		<h2><?php echo count($data) ?> Data Kapal SLO Hari Ini</h2>
		<small>Kesuluruhan data Kapal SLO</small>
			<ul class="actions">
			<li>
				<a data-widgster="fullscreen" href="#">
					<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
				</a>
			</li>
			<li>
				<a data-widgster="restore" href="#">
					<i class="zmdi zmdi-window-restore"></i>
				</a>
			</li>
			<li>
				<a onClick="closeLayer('#layer2') ">
					<i class="zmdi zmdi-close"></i>
				</a>
			</li>
		</ul>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="table-responsive" style="height:400px">
				<table id="kapalRFMO" class="table table-striped">
					<thead>
						 <tr> 
		                    <th  data-column-id="id" data-visible="false">id</th>
                            <th  data-column-id="NEGARA">Nama Kapal</th>  
                            <th  data-column-id="JUMLAH">GT</th>  
                            <th  data-column-id="JUMLAH">No SIPI</th>  
                            <th  data-column-id="JUMLAH">ALKAP</th>  
                            <th  data-column-id="JUMLAH">NO HPK</th>  
                            <th  data-column-id="JUMLAH">Tujuan</th>  
                            <th  data-column-id="JUMLAH">No SLO</th>  
                            <th  data-column-id="JUMLAH">TMP Terbit</th>  
                            <th  data-column-id="JUMLAH">Bendera</th>  
		                </tr>
                    </thead>
                    <tbody>
                    	<?php $n=1; foreach($data as $x){ ?>
							<tr>
							  <td class="col0"><?php echo $n++ ?></td>
							  <td class="col0"><?php echo $x->nama_kapal ?></td>
							  <td class="col0"><?php echo $x->gt_kapal ?></td>
							  <td class="col0"><?php echo $x->no_sipi ?></td>
							  <td class="col0"><?php echo $x->kapal_nama_alat_tangkap ?></td>
							  <td class="col0"><?php echo $x->no_hpk ?></td>
							  <td class="col0"><?php echo $x->tujuan ?></td>
							  <td class="col0"><?php echo $x->no_register_slo ?></td>
							  <td class="col0"><?php echo $x->tempat_terbit ?></td>
							  <td class="col0"><?php echo $x->bendera_kapal ?></td> 
							</tr>
						<?php } ?>
						 
					</tbody>			  
				</table>
			</div>
		</div>
		 
		 
	</div>
</div>

<script type="text/javascript">

 
</script>
