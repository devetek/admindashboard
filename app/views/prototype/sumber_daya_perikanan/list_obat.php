<div class="card widget">
	<div class="card-header ch-alt m-b-20">
		<h2><?php echo count($data) ?> Data Pengawasan Obat</h2>
		<small>Kesuluruhan data hari ini</small>
			<ul class="actions">
			<li>
				<a data-widgster="fullscreen" href="#">
					<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
				</a>
			</li>
			<li>
				<a data-widgster="restore" href="#">
					<i class="zmdi zmdi-window-restore"></i>
				</a>
			</li>
			<li>
				<a onClick="closeLayer('#layer2') ">
					<i class="zmdi zmdi-close"></i>
				</a>
			</li>
		</ul>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="table-responsive" style="height:400px">
				<table id="kapalRFMO" class="table table-striped">
					<thead>
						 <tr> 
		                    <th  data-column-id="id" data-visible="false">id</th>
                            <th  data-column-id="NEGARA">Nama Perusahaan</th>  
                            <th  data-column-id="JUMLAH">Alamat</th>   
                            <th  data-column-id="JUMLAH">Obat</th>   
                            <th  data-column-id="JUMLAH">Tempat Periksa</th>    
                            <th  data-column-id="JUMLAH">Analisa</th>    
		                </tr>
                    </thead>
                    <tbody>
                    	<?php $n=1; foreach($data as $x){ ?>
							<tr> 
							  <td class="col0"><?php echo $n++ ?></td>
							  <td class="col0"><?php echo $x->nama_perusahaan ?></td>
							  <td class="col0"><?php echo $x->alamat ?></td>
							  <td class="col0"><?php echo $x->nama_obat ?></td>
							  <td class="col0"><?php echo $x->tempat_periksa ?></td>
							  <td class="col0"><?php echo $x->analisa_ket ?></td> 
							</tr>
						<?php } ?>
						 
					</tbody>			  
				</table>
			</div>
		</div>
		 
		 
	</div>
</div>

<script type="text/javascript">

 
</script>
