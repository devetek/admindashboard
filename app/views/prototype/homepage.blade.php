@extends('shared.master-dashboard')

@section('content')
	
	<div class="col-sm-12 padding-1">
		<div class="card1 widget" 
		data-widgster-load="{{url('prototype/home_page/map')}}"
		data-widgster-autoload="true"
		data-widgster-show-loader="false">
			<div class="card-header">
				<h2>Maps</h2>
				
				<ul class="actions">
				<li>
					<a id="reloadMapPosisi" href="#">
						<i class="zmdi zmdi-refresh-alt"></i>
					</a>
				</li>
				<li>
					<a id="fullLastPosisi" data-widgster="fullscreen" href="#">
						<i class="zmdi zmdi-fullscreen-alt zmdi-hc-fw"></i>
					</a>
				</li>
				<li>
					<a  data-widgster="restore" href="#">
						<i class="zmdi zmdi-window-restore"></i>
					</a>
				</li>
			</ul>
			</div>
			<div class="card-body">
				<div class="body">
				</div>
			</div>
		</div>
        
	</div> 

@stop

@section('javascript')
	
    {{ HTML::style('public/vendors/openlayer/ol.css')}}
	{{ HTML::script('public/vendors/openlayer/ol.js')}} 
	{{ HTML::script('public/devetek/mapsStyle.js')}} 
		
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script type="text/javascript">
			
		</script>
 @stop
