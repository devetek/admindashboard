@extends('shared.master')

@section('content')
    <div class="card">
        <form class="form-horizontal"  action="" method="POST" role="form">
            <div class="card-header">
                <h2>User Group Data <small>User group for user info, this group affected to which function that user info can access</small></h2>
            </div>
			<div class="card-body card-padding">
				<fieldset>
					<legend>User Group Info</legend>
					<input type="hidden" name="id" value="{{$Model->getId()}}">
					<div class="form-group">
						<label class="col-sm-2 control-label">Name</label>
						<div class="col-sm-4">
							<input type="text" name="name" placeholder="Name" size="16" class="form-control" id="prepended-input" value="{{$Model->getName()}}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Description</label>
						<div class="col-sm-4">
							<textarea class="form-control auto-size" name='description' placeholder="Description">{{$Model->getDescription()}}</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Is Enabled</label>
						<div class="col-sm-4">
							<div class="select">
								{{Form::select('is_enabled', $IsEnabledCombo, $Model->IsEnabled(),array('id'=>'IsEnabledCombo', 'class'=>'form-control'))}}
							</div>
						</div>
					</div>
				</fieldset>
						<fieldset>
						<legend>Privilege Info</legend>
						<table class="table table-bordered table-sm mt-sm mb-0">
						<thead>
							<tr>
								<?php //<th style="width: 70px !important;">#</th> ?>
								<th class="text-center">Function Name</th>
								<?php //<th class="text-center">Module Name</th> ?>
								<th class="text-center">Is Allow Create</th>
								<th class="text-center">Is Allow Read</th>
								<th class="text-center">Is Allow Update</th>
								<th class="text-center">Is Allow Delete</th>
							</tr>
						</thead>
						<tbody id="tableData">
						@if ($Model->getPrivilegeInfo() != null)
							<?php $i = 1 ?>
							@foreach ($Model->getPrivilegeInfo() as $item)
							<tr>
								<?php //<td class="text-center" nowrap>{{$i++}}</td> ?>
								<?php //<td><td>{{ $item->getFunctionInfo() != null ? $item->getFunctionInfo()->getName() : ""}}</td></td> ?>
								<?php //<td>{{ $item->getModule() != null ? $item->getModule()->getName() : ""}}</td> ?>
								<input type="hidden" name="privilege_info[]" value="">
								<input type="hidden" name="privilege_info_id[]" value="{{$item->getId()}}">
								<td class="text-center">
									<div class="select">
										{{Form::select('function_info[]', $ModuleFunctionCombo, $item->getFunctionInfo() !=null ? $item->getFunctionInfo()->getId() : "",array('id'=>'ModuleFunctionCombo' . $i, 'class'=>'form-control'))}}
									</div>
								</td>
								<td class="text-center">
									<div class="select">
										{{Form::select('is_allow_create[]', $IsAllowCreateCombo, $item->IsAllowCreate(),array('id'=>'IsAllowCreateCombo' . $i, 'class'=>'form-control'))}}
									</div>
								</td>
								<td class="text-center">
									<div class="select">
										{{Form::select('is_allow_read[]', $IsAllowReadCombo, $item->IsAllowRead(),array('id'=>'IsAllowReadCombo' . $i, 'class'=>'form-control'))}}
									</div>
								</td>
								<td class="text-center">
									<div class="select">
										{{Form::select('is_allow_update[]', $IsAllowUpdateCombo, $item->IsAllowUpdate(),array('id'=>'IsAllowUpdateCombo' . $i, 'class'=>'form-control'))}}
									</div>
								</td>
								<td class="text-center">
									<div class="select">
										{{Form::select('is_allow_delete[]', $IsAllowDeleteCombo, $item->IsAllowDelete(),array('id'=>'IsAllowDeleteCombo' . $i, 'class'=>'form-control'))}}
									</div>
								</td>
								<td class="text-center">
									<div onClick="deleteRow(this)">
										<i class="zmdi zmdi-close"></i>
									</div>
								</td>
							</tr>
                            @endforeach
                        @endif
						</tbody>
					</table>
					</fieldset>
					<br />
					<script>
						function addRow() {
							newRow = "<tr>" + 
									"<input type=\"hidden\" name=\"privilege_info[]\" value=\"\"><input type=\"hidden\" name=\"privilege_info_id[]\" value=\"\">"+
									"<td class=\"text-center\">" +
									"<div class=\"select\">" +
									'{{Form::select('function_info[]', $ModuleFunctionCombo, "",array('id'=>'ModuleFunctionCombo', 'class'=>'form-control'))}}' +
									"</div></td>" +
									"<td class=\"text-center\"><div class=\"select\">" +
									'{{Form::select('is_allow_create[]', $IsAllowCreateCombo, false,array('id'=>'IsAllowCreateCombo', 'class'=>'form-control'))}}' +
									"</div></td>" +
									"<td class=\"text-center\"><div class=\"select\">" +
									'{{Form::select('is_allow_read[]', $IsAllowReadCombo, false,array('id'=>'IsAllowReadCombo', 'class'=>'form-control'))}}' +
									"</div></td>" +
									"<td class=\"text-center\"><div class=\"select\">" +
									'{{Form::select('is_allow_update[]', $IsAllowUpdateCombo, false,array('id'=>'IsAllowUpdateCombo', 'class'=>'form-control'))}}' +
									"</div></td>" +
									"<td class=\"text-center\"><div class=\"select\">" +
									'{{Form::select('is_allow_delete[]', $IsAllowDeleteCombo, false,array('id'=>'IsAllowDeleteCombo', 'class'=>'form-control'))}}' +
									"</div></td>" +
									"<td><div onClick=\"deleteRow(this)\"><i class=\"zmdi zmdi-close\"></i></div></td>" +
									"</tr>"
							jQuery(newRow).appendTo('#tableData');
						}
						function deleteRow(el){
							jQuery(el).parent().parent().remove()
						}
					</script>
					
					<button onClick="addRow()" type="button" class="btn btn-default" data-toggle="modal" data-target="#functionInfoModal">
						Add More
					</button>


				<div class="form-actions text-right">
					<button class="btn btn-primary waves-effect" type="submit" name="submit" value="Save" />Save</button>
					<button class="btn btn-default waves-effect" type="button" onClick="location.href='{{url('usergroup')}}'" >Cancel</button>
				</div>

            </div>
        </form>
    </div>

@stop

@section('javascript')
	<script>

	</script>
@stop
