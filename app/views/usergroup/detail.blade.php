@extends('shared.master')

@section('content')

    <div class="card">
        <form class="form-horizontal" role="form">
            <div class="card-header">
                <h2>User Group Data <small>User group for user info, this group affected to which function that user info can access</small></h2>
            </div>
			<div class="card-body card-padding">
				<fieldset>
					<legend>User Group Info</legend>
					<div class="form-group">
						<label class="col-sm-2">Name</label>
						<div class="col-sm-4">
							: {{$Model->getName()}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Description</label>
						<div class="col-sm-4">
							: {{$Model->getDescription()}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2">Is Enabled</label>
						<div class="col-sm-4">
							: {{$Model->isEnabled() ? "TRUE" : "FALSE" }}
						</div>
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Privilege Info</legend>
					<table class="table table-bordered table-sm mt-sm mb-0">
					<thead>
						<tr>
							<th style="width: 70px !important;">#</th>
							<th class="text-center">Function Name</th>
							<th class="text-center">Module Name</th>
							<th class="text-center">Is Allow Create</th>
							<th class="text-center">Is Allow Read</th>
							<th class="text-center">Is Allow Update</th>
							<th class="text-center">Is Allow Delete</th>
						</tr>
					</thead>
					<tbody>
					@if ($Model->getPrivilegeInfo() != null)
						<?php $i = 1 ?>
						@foreach ($Model->getPrivilegeInfo() as $item)
						<tr>
							<td class="text-center" nowrap>{{$i++}}</td>
							<td>{{ $item->getFunctionInfo() != null ? $item->getFunctionInfo()->getName() : ""}}</td>
							<td>{{ $item->getModule() != null ? $item->getModule()->getName() : ""}}</td>
							<td class="text-center">{{ $item->isAllowCreate() ? "TRUE" : "FALSE"}}</td>
							<td class="text-center">{{ $item->isAllowRead() ? "TRUE" : "FALSE"}}</td>
							<td class="text-center">{{ $item->isAllowUpdate() ? "TRUE" : "FALSE"}}</td>
							<td class="text-center">{{ $item->isAllowDelete() ? "TRUE" : "FALSE"}}</td>
						</tr>
                        @endforeach
					@else
						<tr>
							<td colspan="7"> <span class="fw-semi-bold">there is no data in database</span> </td>
						</tr>
                    @endif
					</tbody>
				</table>
				</fieldset>

				<div class="form-actions text-right">
					<button class="btn btn-primary waves-effect" type="button" onClick="location.href='{{ url('usergroup/edit', $Model->getId()) }}'">Edit</button>
					<button class="btn btn-default waves-effect" type="button" onClick="location.href='{{url('usergroup')}}'" >Close</button>
				</div>

            </div>
        </form>
    </div>

@stop

@section('javascript')
	<script type="text/javascript">
	
	
	</script>
@stop
