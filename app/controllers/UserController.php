<?php

class UserController extends DevetekController {

	/*
		Variable Declaration
	*/

	const _FUNCTION_ID = 'user_info';

	/*
		Constructor
	*/
	
	public function __construct(){
        parent::__construct();
		$this->loadDefaultValue();
        $this->loadServices();
    }

	/*
		Public Method (Page Controller)
	*/
	
	public function index(){			
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }
		
		$this->restoreTempData();
		$data['Model'] = Array();
		
		try {
			$data['Model'] = $this->mService->getUserInfos();
			return $this->makeView('user/index', $data);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->makeView('user/index', $data);
        }
	}

	public function create(){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowCreate()){ return Redirect::to('forbidden'); }

		$user = new UserInfo();

        try{
            $user->setIsEnabled(0);
			return $this->createInputView($user);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->createInputView($user);
        }
	}

	public function processcreate(){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowCreate()){ return Redirect::to('forbidden'); }

		$user = new UserInfo();
        
        try{
			$user = $this->bindData();
			if($this->validateInput(Input::all())==FALSE) return $this->createInputView($user);
			$this->mService->SetUserInformation($this->userInformation());
			$newUserId = $this->mService->insertUserInfo($user);
			if($newUserId==null){
				$this->addModelError($this->mService->GetErrors());
				return $this->createInputView($user);
			}
			return Redirect::to('user/detail/'.$newUserId->getId());
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->createInputView($user);
        }
	}
	
	public function detail($id=null){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }

        try {
            $result = $this->mService->getUserInfo($id);
            if($result==null) {
				$this->addModelError("Data Not Found");
				$this->assignToTempData();
                Return Redirect::to('user');
            }
            $data['Model'] = $result;
            return $this->makeView('user/detail', $data);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('user');
        }
	}

	public function edit($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowUpdate()){ return Redirect::to('forbidden'); }
        
        $user = new UserInfo();
        
        try {
			$user = $this->mService->getUserInfo($id);
			if($user==null) {
				$this->addModelError("Data Not Found");
				$this->assignToTempData();
                Return Redirect::to('user');
            }
			return $this->createInputView($user);
		} catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('user');
        }
	}

	public function processedit($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowUpdate()){ return Redirect::to('forbidden'); }
        
        $user = new UserInfo();
		$user = $this->bindData();
        
        try {
			if($this->validateInput(Input::all())==FALSE) return $this->createInputView($user);
			$this->mService->SetUserInformation($this->userInformation());
			if($this->mService->updateUserInfo($user,$user->getId())==true){
				Return Redirect::to('user/detail/'.$user->getId());
			}
			return $this->createInputView($user);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('user');
        }
	}

	public function profile(){
        if (!$this->isLogin()){ return Redirect::to('login'); }
		
        try {
            $result = $this->mService->getUserInfo($this->userInformation()->getId());
            if($result==null) {
				$this->addModelError("Data Not Found");
				$this->assignToTempData();
                Return Redirect::to('forbidden');
            }
            $data['Model'] = $result;
			$data['ProfilePage'] = true;
            return $this->makeView('user/detail', $data);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('forbidden');
        }
	}

	public function updateprofile($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        
        $user = new UserInfo();
        
        try {
			$user = $this->mService->getUserInfo($this->userInformation()->getId());
			if($user==null) {
				$this->addModelError("Data Not Found");
				$this->assignToTempData();
                Return Redirect::to('forbidden');
            }
			return $this->createInputView($user);
		} catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('user');
        }
	}

	public function processupdateprofile($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        
        $user = new UserInfo();
		$user = $this->bindData();
        
        try {
			if($this->validateInput(Input::all())==FALSE) return $this->createInputView($user);
			$this->mService->SetUserInformation($this->userInformation());
			if($this->mService->updateUserInfo($user,$user->getId())==true){
				Return Redirect::to('user/detail/'.$user->getId());
			}
			return $this->createInputView($user);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('user');
        }
	}
	
	public function delete($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowDelete()){ return Redirect::to('forbidden'); }

        try {
            $this->mService->deleteUserInfo($id);
            
            Return Redirect::to('user');
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
            Return Redirect::to('user');
        }
	}
	
	/*
		Private Method
	*/
	
	private function loadComboIsEnabled($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $arr_data[true] = 'TRUE';
        $arr_data[false] = 'FALSE';
        
        $this->data['IsEnabledCombo'] = $arr_data;
    }

	private function loadComboGroup($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $userGroupService = new UserGroupService();
        $userGroups = $userGroupService->getUserGroups();
        foreach ($userGroups as $item){
            $arr_data[$item->getId()] = $item->getName();
        }
        
        $this->data['UserGroupCombo'] = $arr_data;
    }

    private function createInputView($user=null){
        $this->loadComboIsEnabled();
        $this->loadComboGroup();
		$this->data['Model'] = $user;
		
        return $this->makeView('user/input', $this->data);
    }
    
    private function bindData(){
        $data = Input::all();
        
        $user = New UserInfo();
        $user->setId($data["id"]);
        $user->setUsername($data["username"]);
        $user->setEmail($data["email"]);
        $user->setName($data["name"]);
		$password = $data["password"];
		if(!is_null($password) && !empty($password)){
			$user->setPassword($password);
		}
			$group = new UserGroup();
			$group->setId($data["user_group_id"]);
			$group->setIsLoaded(true);
        $user->setGroup($group);
		$user->setIsEnabled($data["is_enabled"]);

        return $user;
    }
 
    private function validateInput($data){
        
        if($data['password']!=$data['repassword']){
            PageData::addModelError('Password not match');
        }
		
        if($data==null) return false;
        $rules = array(
            'username' => 'alpha|required',
            'email' => 'required|email',
            'name' => 'required'
        );

        $this->validator = Validator::make($data, $rules);
        if($this->validator->fails()) $this->assignValidatorToModelState($this->validator);
        return $this->validator->passes();
    }
	
	private function loadDefaultValue(){
        $this->setFunctionId(self::_FUNCTION_ID);
    }

    private function loadServices(){
        $this->mService = new UserInfoService();
    }
	
}