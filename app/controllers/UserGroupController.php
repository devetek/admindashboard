<?php

class UserGroupController extends DevetekController {

	/*
		Variable Declaration
	*/

	const _FUNCTION_ID = 'user_group';

	/*
		Constructor
	*/
	
	public function __construct(){
        parent::__construct();
		$this->loadDefaultValue();
        $this->loadServices();
    }

	/*
		Public Method (Page Controller)
	*/
	
	public function index(){			
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }
		$this->restoreTempData();
		$data['Model'] = Array();
		
		try {
			$data['Model'] = $this->mService->getUserGroups();
			return $this->makeView('usergroup/index', $data);
        } catch (Exception $exc) {
            $this->addModelError($exc->getMessage());
            return $this->makeView('usergroup/index', $data);
        }
	}

	public function create(){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowCreate()){ return Redirect::to('forbidden'); }

		$userGroup = new UserGroup();
        try{
			$userGroup->setCreatedDate(date('d-m-Y H:i:s'));
			$userGroup->setCreatedBy($this->UserInformation());
			return $this->createInputView($userGroup);
        } catch (Exception $exc) {
            $this->addModelError($exc->getMessage());
            return $this->createInputView($userGroup);
        }
	}

	public function processcreate(){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowCreate()){ return Redirect::to('forbidden'); }

		$userGroup = new UserGroup();
        try{
			$userGroup = $this->bindData();
			if($this->validateInput(Input::all())==FALSE) return $this->createInputView($userGroup);
			$this->mService->setUserInformation($this->UserInformation());
			$insertedUserGroup = $this->mService->insertUserGroup($userGroup);
			if($insertedUserGroup==null){
				$this->addModelError($this->mService->GetErrors());
				return $this->createInputView($userGroup);
			}
			return Redirect::to('usergroup/detail/'.$insertedUserGroup->getId());
        } catch (Exception $exc) {
            $this->addModelError($exc->getMessage());
            return $this->createInputView($userGroup);
        }
	}
	
	public function detail($id=null){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }

        try {
            $result = $this->mService->getUserGroup($id);
            if($result==null) {
				$this->addModelError("Data Not Found");
				$this->assignToTempData();
                Return Redirect::to('usergroup');
            }
            $data['Model'] = $result;
            return $this->makeView('usergroup/detail', $data);
        } catch (Exception $exc) {
			$this->addModelError($exc->getMessage());
			$this->assignToTempData();
			return Redirect::to('usergroup');
        }

	}

	public function edit($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowUpdate()){ return Redirect::to('forbidden'); }
        
        $userGroup = new UserGroup();
        
        try {
			$userGroup = $this->mService->getUserGroup($id);
			if($userGroup==null) {
				$this->addModelError("User Group Not Found");
				$this->assignToTempData();
                Return Redirect::to('usergroup');
            }
			return $this->createInputView($userGroup);
		} catch (Exception $exc) {
			$this->addModelError($exc->getMessage());
			$this->assignToTempData();
			return Redirect::to('usergroup');
        }
	}

	public function processedit($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowUpdate()){ return Redirect::to('forbidden'); }
        
        $userGroup = new UserGroup();
		$userGroup = $this->bindData();
        
        try {
			if($this->validateInput(Input::all())==FALSE) return $this->createInputView($userGroup);
			$this->mService->setUserInformation($this->userInformation());
			if($this->mService->updateUserGroup($userGroup,$userGroup->getId())==true){
				Return Redirect::to('usergroup/detail/'.$userGroup->getId());
			}
			return $this->createInputView($userGroup);
        } catch (Exception $exc) {
			$this->addModelError($exc->getMessage());
			$this->assignToTempData();
			return Redirect::to('usergroup');
        }
	}

	
	public function delete($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowDelete()){ return Redirect::to('forbidden'); }

        try {
            $this->mService->deleteUserGroup($id);
            
            Return Redirect::to('usergroup');
        } catch (Exception $exc) {
			$this->addModelError($exc->getMessage());
			$this->assignToTempData();
            Return Redirect::to('usergroup');
        }
	}
	
	/*
		Private Method
	*/
	
    private function loadComboModuleFunction($show_all = false){
		$arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $functionInfoService = new FunctionInfoService();
        $functionInfos = $functionInfoService->getFunctionInfos();
        foreach ($functionInfos as $item){
			$module = $item->getModule();
			if($module != null){
				$arr_data[$item->getId()] = $module->getName() ." - ". $item->getName();
			}
        }
        
        $this->data['ModuleFunctionCombo'] = $arr_data;
	}
	
    private function loadComboIsAllowCreate($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $arr_data[true] = 'TRUE';
        $arr_data[false] = 'FALSE';
        
        $this->data['IsAllowCreateCombo'] = $arr_data;
    }
	
    private function loadComboIsAllowRead($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $arr_data[true] = 'TRUE';
        $arr_data[false] = 'FALSE';
        
        $this->data['IsAllowReadCombo'] = $arr_data;
    }
    
    private function loadComboIsAllowUpdate($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $arr_data[true] = 'TRUE';
        $arr_data[false] = 'FALSE';
        
        $this->data['IsAllowUpdateCombo'] = $arr_data;
    }

    private function loadComboIsAllowDelete($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $arr_data[true] = 'TRUE';
        $arr_data[false] = 'FALSE';
        
        $this->data['IsAllowDeleteCombo'] = $arr_data;
    }

    private function loadComboIsEnabled($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $arr_data[true] = 'TRUE';
        $arr_data[false] = 'FALSE';
        
        $this->data['IsEnabledCombo'] = $arr_data;
    }
    
    private function loadComboModule($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $moduleService = new ModuleService();
        $modules = $moduleService->getModules();
        foreach ($modules as $item){
            $arr_data[$item->getId()] = $item->getName();
        }
        
        $this->data['ModulesCombo'] = $arr_data;
    }
    
    private function loadComboFunctionInfo($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $functionInfoService = new FunctionInfoService();
        $functionInfo = $functionInfoService->getFunctionInfos();
        foreach ($functionInfo as $item){
            $arr_data[$item->getId()] = $item->getName();
        }
        
        $this->data['FunctionInfoCombo'] = $arr_data;
    }

    private function createInputView($userGroup=null){
        //$this->loadComboModule();
        //$this->loadComboFunctionInfo();
		$this->loadComboModuleFunction();
        $this->loadComboIsEnabled();
        $this->loadComboIsAllowCreate();
        $this->loadComboIsAllowRead();
        $this->loadComboIsAllowUpdate();
        $this->loadComboIsAllowDelete();
		$this->data['Model'] = $userGroup;
		
        return $this->makeView('usergroup/input', $this->data);
    }
    
    private function bindData(){
        $data = Input::all();
        
        $userGroup = New UserGroup();
        $userGroup->setId($data["id"]);
        $userGroup->setName($data["name"]);
        $userGroup->setDescription($data["description"]);
        $userGroup->setIsEnabled($data["is_enabled"]);
        $privilegeInfoIds = [];
        $privilegeInfoIds = Input::get('privilege_info', array());
        for($i=0; $i<count($privilegeInfoIds); $i++) {
            $privilegeInfo = new PrivilegeInfo();
            $privilegeInfo->setId($data["privilege_info_id"][$i]);
            
            $Group = New UserGroup();
            $Group->setId($userGroup->getId());
            $Group->setIsLoaded(true);
            $privilegeInfo->setUserGroup($Group);
            
            $functions = new FunctionInfo();
            $functions->setId($data["function_info"][$i]);
            $functions->setIsLoaded(false);
            $privilegeInfo->setFunctionInfo($functions);
            
            $module = new Module();
            if($privilegeInfo->getFunctionInfo()!=null && $privilegeInfo->getFunctionInfo()->getModule()!=null)
            $module->setId($privilegeInfo->getFunctionInfo()->getModule()->getId());
            $module->setIsLoaded(true);
            $privilegeInfo->setModule($module);
            
            $privilegeInfo->setIsAllowCreate($data["is_allow_create"][$i]);
            $privilegeInfo->setIsAllowRead($data["is_allow_read"][$i]);
            $privilegeInfo->setIsAllowUpdate($data["is_allow_update"][$i]);
            $privilegeInfo->setIsAllowDelete($data["is_allow_delete"][$i]);
            $privilegeInfo->setIsLoaded(true);
            
            $privilegeInfos[] = $privilegeInfo;
        }
        $userGroup->setPrivilegeInfo($privilegeInfos);
        return $userGroup;
    }
 
    private function validateInput($data){
        
        if($data==null) return false;
        $rules = array(
            'name' => 'required'
        );

        $messages = array(
            'name' => 'Group Name is Required.'
        );

        $this->validator = Validator::make($data, $rules);
        if($this->validator->fails()) $this->assignValidatorToModelState($this->validator);
        return $this->validator->passes();
    }
	
	private function loadDefaultValue(){
        $this->setFunctionId(self::_FUNCTION_ID);
    }

    private function loadServices(){
        $this->mService = new UserGroupService();
    }
	
}