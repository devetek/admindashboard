<?php

class IpinSdp extends Controller { 


	//var $remote='@dbwh';
	public static $remote=''; 

	function conver($kordinat)
	{    
		$kordinat = str_replace("", " ", $kordinat); 
		$kordinat = str_replace("'", "º", $kordinat); 

		$vars = explode("-",$kordinat);
		$vars = explode("º",$vars[0]);
 		 
 		$vars1 = substr($vars[0], -6,-3);
 		$vars2 = substr($vars[0], -3) + 0; 
 		return  $vars1 +((($vars2*60) + (1)) / 3600);
 		 
 		//return  $vars[0] ; 
	}
 
 
 	public function getDatadss()
	{   
		//$sql="select * from "data_slo"@mysql5";
		$sql="select * from ADF_MPELABUHANDSS where id > 0";
		if(isset($_GET['kelas']))
		{
			$sql.=" AND kelas in (".$_GET['kelas'].")";
		}

		$final_data =  DB::connection('oracle')->select($sql);
		 		 
		$data='';
		 
 

		$data.='
			{ "type": "FeatureCollection",
				"features": [
		';
			$a=0;
			foreach($final_data as $x)
			{  
				
				$warnaBg = array(
			          '#0036D9', 
			          '#FFC926',
			          '#D93600',
			          '#004000',
			          '#FFFB6C',
			          '#D90000',
			          '#EC50B1'
			          );

				$color = $x->kelas;
				$warna = '';

				if($color=='PPN')
			    {
			      $warna  = $warnaBg[0];
			    }
			    else if($color=='PPS')
			    {
			      $warna  = $warnaBg[1];
			    }
			    else if($color=='PPI')
			    {
			      $warna  = $warnaBg[2];
			    }
			    else if($color=='CPPI')
			    {
			      $warna  = $warnaBg[3];
			    }
			    else if($color=='PPP')
			    {
			      $warna  = $warnaBg[4];
			    }
			    else if($color=='PPI PUD')
			    {
			      $warna  = $warnaBg[5];
			    }
			    else if($color=='PP')
			    {
			      $warna  = $warnaBg[6];
			    }

				 
				$data.='
				  { "type": "Feature",
					"geometry":  
						{
							"type": "Point", 
							 "coordinates": ['.round($x->bujur ,3).', '.round($x->lintang ,3).']
						},
					"properties": 
						{ 
							"idDss": "'.$x->id.'" , 
							"kelas": "'.$warna.'"  
						}
 
					}
					';
					$a++;
					if(count($final_data) != $a)
					{
						$data.=",";
					}
	 
			}
					   
		$data.='	]
			}
		'; 
		 

		return $data;
	} 

	function getDetaildss($id)
	{
		$sql="select * from ADF_MPELABUHANDSS where id='".$id."' ";
		$final_data =  DB::connection('oracle')->select($sql);

		 
		$data ='<table class="table table-hover">';
		$no =1;
		 

		foreach($final_data as $key=>$x)
		{  
			$warnaBg = array(
			  '#6D8FFF', 
			  '#F57D5A',
			  '#0DE23C',
			  '#57E8EB',
			  '#FFFB6C',
			  '#8960FF',
			  '#EC50B1'
			  );

			$color = $x->kelas;
			$warna = '';

			if($color=='PPN')
		    {
		      $warna  = $warnaBg[0];
		    }
		    else if($color=='PPS')
		    {
		      $warna  = $warnaBg[1];
		    }
		    else if($color=='PPI')
		    {
		      $warna  = $warnaBg[2];
		    }
		    else if($color=='CPPI')
		    {
		      $warna  = $warnaBg[3];
		    }
		    else if($color=='PPP')
		    {
		      $warna  = $warnaBg[4];
		    }
		    else if($color=='PPI PUD')
		    {
		      $warna  = $warnaBg[5];
		    }
		    else if($color=='PP')
		    {
		      $warna  = $warnaBg[6];
		    }

			$data.='<tr>
		                <td>Nama Pelabuhan</td>
		                <td>:</td>
		                <td>'.$x->nama_pelabuhan.'</td> 
		            </tr>'; 

		    $data.='<tr>
		                <td>Propinsi</td>
		                <td>:</td>
		                <td>'.$x->propinsi.'</td> 
		            </tr>'; 

		    
		    $data.='<tr>
		                <td>Kab/Kot.</td>
		                <td>:</td>
		                <td>'.$x->kota_kabupaten.'</td> 
		            </tr>'; 

		    
		    $data.='<tr>
		                <td>Kelas</td>
		                <td>:</td>
		                <td style="background:'.$warna.'">'.$x->kelas.'</td> 
		            </tr>'; 

		    
		    $data.='<tr>
		                <td>WPP</td>
		                <td>:</td>
		                <td>'.$x->wpp.'</td> 
		            </tr>'; 

		    
		    $data.='<tr>
		                <td>Alamat</td>
		                <td>:</td>
		                <td>'.$x->alamat.'</td> 
		            </tr>'; 
		    
		    $data.='<tr>
		                <td>Lintang</td>
		                <td>:</td>
		                <td>'.$x->lintang.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>Bujur</td>
		                <td>:</td>
		                <td>'.$x->bujur.'</td> 
		            </tr>'; 

		    
		    $data.='<tr>
		                <td>Status</td>
		                <td>:</td>
		                <td>'.$x->deskripsi_status.'</td> 
		            </tr>'; 

		     

		}
		$data.="</table>";  
		
		return $data;  
		 
	}



	public function getCountdata($jenis=null)
	{   
		$sql="";

		if($jenis=='slo')
		{
			$sql="select count(tanggal_terbit) as jml 
					FROM ADF_SDP_DATA_SLO 
					where  trunc(tanggal_terbit) = trunc(sysdate-368)
					group by tanggal_terbit
					";
			 
		} 
		else if($jenis=='obat')
		{
			$sql="select count(tgl_periksa) as jml 
					FROM ADF_SDP_UPI_PERIKSA
 
					where  trunc(tgl_periksa) = trunc(sysdate-287)
					group by tgl_periksa
					"; 
		} 

		else if($jenis=='pakan')
		{
			$sql="select count(tgl_periksa) as jml 
					FROM ADF_SDP_UPI_PERIKSA
 
					where  trunc(tgl_periksa) = trunc(sysdate-286)
					group by tgl_periksa
					";

			 
		}

		else if($jenis=='tangkap')
		{
			$sql="select count(tgl_pemeriksaan) as jml 
					FROM ADF_SDP_DATA_KEDATANGAN
 
					where  trunc(tgl_pemeriksaan) = trunc(sysdate-286)
					group by tgl_pemeriksaan
					";

			 
		}

		$final_data =  DB::connection('oracle')->select($sql);
		
		if(count($final_data) > 0)
		{
			return $final_data[0]->jml;
		}
		else
		{
			return 0;
		}
		
		
	}

	public function getDataslo()
	{   
		$sql="select * 
					FROM ADF_SDP_DATA_SLO 
					where  trunc(tanggal_terbit) = trunc(sysdate-368)					 
					";

		$data['data'] =  DB::connection('oracle')->select($sql);

		return View::make('prototype/sumber_daya_perikanan/list_slo',$data);
		 	 
	}

	public function getDataobat()
	{   
		$sql="select * FROM ADF_SDP_UPI_PERIKSA a left join ADF_SDP_UPI_PERIKSA_OBAT b on a.ID_PEMERIKSAAN=b.ID_PEMERIKSAAN
			 where trunc(a.tgl_periksa) = trunc(sysdate-286)";

		$data['data'] =  DB::connection('oracle')->select($sql);

		return View::make('prototype/sumber_daya_perikanan/list_obat',$data);
		 	 
	}

	public function getDatapakan()
	{   
		$sql="select * FROM ADF_SDP_UPI_PERIKSA a left join ADF_SDP_UPI_PERIKSA_PAKAN b on a.ID_PEMERIKSAAN=b.ID_PEMERIKSAAN
			 where trunc(a.tgl_periksa) = trunc(sysdate-286)";

		$data['data'] =  DB::connection('oracle')->select($sql);

		return View::make('prototype/sumber_daya_perikanan/list_pakan',$data);
		 	 
	}

	public function getDataikan($tahun=null,$bulan=null,$jenis=null)
	{   
		
		$sql="SELECT b.nama_ikan, sum(b.jumlah) as jml
				from ADF_SDP_DATA_KEDATANGAN a, ADF_SDP_DATA_KEDATANGAN_HASIL b
				where a.id_kedatangan=b.id_kedatangan
				and a.id_jenis_kapal='".$jenis."'";

		 
		$sql.=" AND to_char( a.tgl_pemeriksaan, 'yyyy' )  = ".$tahun.""; 	
		$sql.=" AND to_char( a.tgl_pemeriksaan, 'mm' )  = ".$bulan.""; 	

		$sql.="	group by b.nama_ikan "; 
		  

		$final_data =  DB::connection('oracle')->select($sql);
 
		$data ='[';  
		$n=0;

			foreach($final_data as $key=>$x)
			{   

				$data.='
					{
					"category": "'.$x->nama_ikan.'",
					"column-1": "'.$x->jml.'",
					"column-2": "'.$x->jml.'"
					}';


				if(count($final_data)-1 > $n++)
				{
					$data.=",";
				}
			}

		$data.=']'; 

		return $data;
	}

	function getKapalberangkat($table='ADF_SDP_KBRNGKTN_ANGKUT')
	{
		$sql="select * 
					FROM ".$table." a 
					left join MTR_SATKER b on a.id_satker=b.id_satker
					where  trunc(a.tgl_pemeriksaan) = trunc(sysdate-361)					 
					";

		$final_data =  DB::connection('oracle')->select($sql);
		
		$data ='<table class="table table-hover">';
		$no =1;

		foreach ($final_data as $key => $x) {

			 $data.='<tr>
						  <td>'.$no++.'</td> 
						  <td>'.$x->nama_kapal.'</td> 
						  <td>'.$x->bendera_kapal.'</td> 
						  <td>'.$x->nama_nahkoda.'</td> 
						  <td>'.$x->nama_satker.'</td> 
						  <td>'.$x->no_sipi.'</td> 
						  <td>'.$x->tempat_pemeriksaan.'</td> 
						  <td>'.$x->hasil_analisa.'</td> 
						</tr>';
		}
		$data.='</table>';

		return $data;
	}

	function getBudayapie($tahun,$jenis)
	{
		
		$sql="select count(".$jenis.") as jml, ".$jenis." as jenis from ADF_SDP_UPI_PERIKSA where to_char(tgl_periksa,'yyyy')=".$tahun." group by ".$jenis; 
		 
		$final_data =  DB::connection('oracle')->select($sql);
 
		$data ='[';  
		$n=0;

			foreach($final_data as $key=>$x)
			{  
				$data.='
					{
					"category": "'.$x->jenis.'",
					"column-1": "'.$x->jml.'"
					}'; 

				if(count($final_data)-1 > $n++)
				{
					$data.=",";
				}
			}

		$data.=']'; 

		return $data;

	}
		  


 
 

	 
	 
 }
