<?php

class LookupController extends DevetekController {

	/*
		Variable Declaration
	*/

	const _FUNCTION_ID = 'lookup';

	/*
		Constructor
	*/
	
	public function __construct(){
        parent::__construct();
		$this->loadDefaultValue();
        $this->loadServices();
    }

	/*
		Public Method (Page Controller)
	*/
	
	public function index(){			
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }

		$this->restoreTempData();
		$data['Model'] = Array();
		
		try {
			$data['Model'] = $this->mService->getLookups();
			return $this->makeView('lookup/index', $data);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->makeView('lookup/index', $data);
        }
	}

	public function create(){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowCreate()){ return Redirect::to('forbidden'); }

		$lookup = new Lookup();

        try{
			$lookup->setIsEnabled(0);
			$lookup->setIsShow(0);
			return $this->createInputView($lookup);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->createInputView($lookup);
        }
	}

	public function processcreate(){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowCreate()){ return Redirect::to('forbidden'); }

		$lookup = new Lookup();

        try{
			$lookup = $this->bindData();
			if($this->validateInput(Input::all())==FALSE) return $this->createInputView($lookup);
			$this->mService->setUserInformation($this->UserInformation());
			$insertedLookup = $this->mService->insertLookup($lookup);
            if($insertedLookup==null){
				$this->addModelError($this->mService->GetErrors());
				return $this->createInputView($lookup);
			}
			return Redirect::to('lookup/detail/'.$insertedLookup->GetId());
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->createInputView($lookup);
            }
	}
	
	public function detail($id=null){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }

        try {
            $result = $this->mService->getLookup($id);
            if($result==null) {
				$this->addModelError("Data Not Found");
				$this->assignToTempData();
                Return Redirect::to('lookup');
            }
            $data['Model'] = $result;
            return $this->makeView('lookup/detail', $data);
        } catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('lookup');
        }

	}

	public function edit($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowUpdate()){ return Redirect::to('forbidden'); }
        
        $lookup = new Lookup();
        
        try {
			$lookup = $this->mService->getLookup($id);
            if($lookup==null) {
				$this->addModelError("Module Not Found");
				$this->assignToTempData();
                Return Redirect::to('module');
            }
			if($id == 'icon'){
				$images = array();
				$valid_images = array('jpg','png','gif');
				$filepath = public_path().'/icon';
				if(is_dir($filepath)){
					foreach(scandir($filepath) as $file){
						$ext = pathinfo($file, PATHINFO_EXTENSION);
						if(in_array($ext, $valid_images)){
							array_push($images, $file);
						}   
					}
				}
				$this->data['images'] = $images;
			}
			return $this->createInputView($lookup);
		} catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('module');
        }
	}

	public function processedit($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowUpdate()){ return Redirect::to('forbidden'); }
        
        $lookup = new Lookup();
		$lookup = $this->bindData();
        
        try {
			if($this->validateInput(Input::all())==FALSE) return $this->createInputView($lookup);
			$this->mService->setUserInformation($this->userInformation());
			if($this->mService->updateLookup($lookup,$lookup->getId())==true){
				Return Redirect::to('lookup/detail/'.$lookup->getId());
			}
			return $this->createInputView($lookup);
        } catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('module');
        }
	}

	
	public function delete($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowDelete()){ return Redirect::to('forbidden'); }

        try {
            $this->mService->deleteLookup($id);
            
            Return Redirect::to('module');
        } catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
            Return Redirect::to('module');
        }
	}
	
	/*
		Private Method
	*/

    private function createInputView($lookup=null){
		$this->data['Model'] = $lookup;
		
        return $this->makeView('lookup/input', $this->data);
    }
    
    private function bindData(){
        $data = Input::all();
        $lookup = New Lookup();
        $lookup->setId($data["id"]);
        $lookup->setName($data["name"]);
		$lookupDetails = [];
        $lookupDetails = Input::get('lookup_detail', array());
        for($i=0; $i<count($lookupDetails); $i++) {
            $lookupDetail = new LookupDetail();
            $lookupDetail->setId($data["lookup_detail_id"][$i]);
            $lookupDetail->setName($data["lookup_detail_name"][$i]);
            $lookupDetail->setValue($data["lookup_detail_value"][$i]);
            
				$lookup = new Lookup();
				$lookup->setId($data["id"]);
				$lookup->IsLoaded(true);
            $lookupDetail->setLookup($lookup);
            $lookupDetail->setIsLoaded(true);
            
            $itemLookupDetails[] = $lookupDetail;
        }
        $lookup->setLookupDetail($itemLookupDetails);  
		D::p($lookup,true);
        return $lookup;
    }
 
    private function validateInput($data){
        
        if($data==null) return false;
        $rules = array(
            'name' => 'required'
        );

        $messages = array(
            'name' => 'Lookup Name is Required.'
        );

        $this->validator = Validator::make($data, $rules, $messages);
        if($this->validator->fails()) $this->assignValidatorToModelState($this->validator);
        return $this->validator->passes();
    }
	
	private function loadDefaultValue(){
         $this->setFunctionId(self::_FUNCTION_ID);
    }

    private function loadServices(){
        $this->mService = new LookupService();
    }
	
}