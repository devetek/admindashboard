<?php

class IpinKp extends Controller { 


	//var $remote='@dbwh';
	public static $remote=''; 

	function conver($kordinat)
	{  
		$kordinat = str_replace('’', 'º', $kordinat);
		$kordinat = str_replace('&;', 'º', $kordinat);

		$vars = explode("º",$kordinat);
 		
 		if(isset($vars[1]))
 		{
 			return  $vars[0] +((($vars[1]*60) + (1)) / 3600);
 		}
 		return  $vars[0] ;
		

	}

	public function getTangkap()
	{    
		$final_data =  DB::connection('oracle')->table('ADF_KP_TANGKAP')
					 ->take(1000)->get();
		  
		$data ='
			{ "type": "FeatureCollection",
				"features": [
		';
			$a=0;
			foreach($final_data as $x)
			{  
				 
				$LINTANG = $this->conver($x->lintang);
				$BUJUR = $this->conver($x->bujur);

				$icon = url()."/img/melanggar.png";
			

				$data.='
				  { "type": "Feature",
					"geometry":  
						{
							"type": "Point", 
							 "coordinates": ['.round($BUJUR ,3).', '.round($LINTANG ,3).']
						},
					"properties": 
						{ 
							"idKp": "'.$x->id_kp_tangkap.'",
							"nama": "'.$x->nama_kapal.'",
							"gt": "'.$x->gt.'",
							"icon": "'.$icon.'"
						}
 
					}
					';
					$a++;
					if(count($final_data) != $a)
					{
						$data.=",";
					}
	 
			}
				   
		$data.='	]
			}
		'; 
		return $data;
	}

	function getDetailtangkap($id)
	{
		$sql="select * from ADF_KP_TANGKAP where ID_KP_TANGKAP='".$id."' ";
		$final_data =  DB::connection('oracle')->select($sql);

		 
		$data ='<table class="table table-hover">';
		$no =1;
		 

		foreach($final_data as $key=>$x)
		{  
			// print_r('<pre>');
			// print_r($x);
			$data.='<tr>
		                <td>Nama Kapal</td>
		                <td>:</td>
		                <td>'.$x->nama_kapal.'</td> 
		            </tr>'; 

		    $data.='<tr>
		                <td>GT</td>
		                <td>:</td>
		                <td>'.$x->gt.'</td> 
		            </tr>'; 

		    
		    $data.='<tr>
		                <td>Negara</td>
		                <td>:</td>
		                <td>'.$x->kebangsaan_kapal.'</td> 
		            </tr>'; 

		    
		    $data.='<tr>
		                <td>Nama Kapal</td>
		                <td>:</td>
		                <td>'.$x->nama_kapal.'</td> 
		            </tr>'; 

		    
		    $data.='<tr>
		                <td>Tanggal</td>
		                <td>:</td>
		                <td>'.$x->tgl_periksa.'</td> 
		            </tr>'; 

		    
		    $data.='<tr>
		                <td>lintang</td>
		                <td>:</td>
		                <td>'.$x->lintang.'</td> 
		            </tr>'; 
		    
		    $data.='<tr>
		                <td>bujur</td>
		                <td>:</td>
		                <td>'.$x->bujur.'</td> 
		            </tr>'; 

		    
		    $data.='<tr>
		                <td>Nama Kapal Penangkap</td>
		                <td>:</td>
		                <td>'.$x->nama_kapal_penangkap.'</td> 
		            </tr>'; 

		    $data.='<tr>
		                <td>alkap</td>
		                <td>:</td>
		                <td>'.$x->alkap.'</td> 
		            </tr>';

		    $data.='<tr>
		                <td>perairan</td>
		                <td>:</td>
		                <td>'.$x->perairan.'</td> 
		            </tr>';

		    $data.='<tr>
		                <td>duga langgar</td>
		                <td>:</td>
		                <td>'.$x->duga_langgar.'</td> 
		            </tr>'; 

		    $data.='<tr>
		                <td>tindak lanjut</td>
		                <td>:</td>
		                <td>'.$x->alkap.'</td> 
		            </tr>'; 
		    
		    $data.='<tr>
		                <td>keterangan</td>
		                <td>:</td>
		                <td>'.$x->keterangan.'</td> 
		            </tr>'; 

		    $data.='<tr>
		                <td>wni/wna</td>
		                <td>:</td>
		                <td>'.$x->jlh_awak_wni.'/'.$x->jlh_awak_wna.'</td> 
		            </tr>';  

		}
		$data.="</table>";  
		
		return $data;  
		 
	}

	//untuk pie grafik
	function getTangkappie($tahun,$jenis)
	{
		
		$sql="select count(".$jenis.") as jml, ".$jenis." as jenis from ADF_KP_TANGKAP where TH='".$tahun."' group by ".$jenis; 
		 
		$final_data =  DB::connection('oracle')->select($sql);
 
		$data ='[';  
		$n=0;

			foreach($final_data as $key=>$x)
			{  
				$data.='
					{
					"category": "'.$x->jenis.'",
					"column-1": "'.$x->jml.'"
					}'; 

				if(count($final_data)-1 > $n++)
				{
					$data.=",";
				}
			}

		$data.=']'; 

		return $data;

	}


	//hasil gelar operasi
	public function getGelaroperasi()
	{    
		$sql="select * from ADF_KP_GELAR_OPERASI where TAHUN not like '0' ";

		if(isset($_GET['tahun']))
		{
			if($_GET['tahun'] !=0)
			{
				$sql.="and trim(tahun) like '".$_GET['tahun']."' ";
			}
		}

		if(isset($_GET['gelarOpr']))
		{  
			if($_GET['gelarOpr'] != 0) 
			{ 
				$sql.="and replace (NAMA_GELAR_OPERASI,' ','') = '".str_replace(' ','',trim($_GET['gelarOpr']))."' ";
			}
		}

		$final_data =  DB::connection('oracle')->select($sql);
		 
		//print_r($final_data);
		$data='';
		$no=1;
		foreach($final_data as $x)
		{  
			$data.='<tr>
                <td>'. ($no++) .'</td>
                <td>'.$x->nama_kapal_pengawas.'</td>
                <td>'.$x->hari_ops.'</td>
                <td>'.$x->hsl_riksa_jml.'</td> 
                <td>'.$x->hsl_tangkap_jml.'</td> 
            </tr>'; 
		}
		
				   
		  
		return $data;
	}
 
 

	 
	 
 }
