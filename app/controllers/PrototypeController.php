<?php

class PrototypeController extends DevetekController {

	/*
		Variable Declaration
	*/

	const _FUNCTION_ID = 'dashboard';

	/*
		Constructor
	*/
	
	public function __construct(){
        parent::__construct();
		$this->loadDefaultValue();
        $this->loadServices();
    }

	/*
		Public Method (Page Controller)
	*/
	
	public function index($page = null){	
		if($page == null){
			return Redirect::to('/homepage');
		}
	
		$this->setFunctionId($page);	
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }
		
		return $this->makeView('prototype/'.$page, array());
	}
	
	public function page($folder = null,$page = null){	
		if($folder == null || $page == null){
			return Redirect::to('/404');
		}
	
		if (!$this->isLogin()){ return Redirect::to('login'); }
		
		return $this->makeView('prototype/'.$folder.'/'.$page, array());
	}
	
	/*
		Private Method
	*/
	
	private function loadDefaultValue(){
        $this->setFunctionId(self::_FUNCTION_ID);
    }

    private function loadServices(){
        $this->mService = new FunctionInfoService();
    }
	
}