<?php

class IpinSdk extends Controller { 


	//var $remote='@dbwh';
	public static $remote=''; 

	function conver($kordinat)
	{    
		$kordinat = str_replace("", " ", $kordinat); 
		$kordinat = str_replace("'", "º", $kordinat); 

		$vars = explode("-",$kordinat);
		$vars = explode("º",$vars[0]);
 		 
 		$vars1 = substr($vars[0], -6,-3);
 		$vars2 = substr($vars[0], -3) + 0; 
 		return  $vars1 +((($vars2*60) + (1)) / 3600);
 		 
 		//return  $vars[0] ; 
	}
 
 
 	public function getKonservasi($jenis=null)
	{   
		$sql="Select * from ADF_SDK_KAWASAN_KONSERVASI where id_konservasi is not null ";

		if(isset($_GET['awal']) and isset($_GET['akhir']))
		{
			// tidak ada pilihan tanggal/tahun di DB
			//$sql.=" and order_date BETWEEN TO_DATE ('2014/02/01', 'yyyy/mm/dd')";
		}

		$final_data =  DB::connection('oracle')->table('ADF_SDK_KAWASAN_KONSERVASI')
					 ->take(1000)->get();
		 		 
		$data='';
		if($jenis=='table')
		{	

			$data.="<tr>";	
				$data.="<th>#</th>";	
				$data.="<th>NAMA KAWASAN</th>";	
				$data.="<th>PROVINSI</th>";	
				$data.="<th>LUAS KAWASAN</th>";	
			$data.="</tr>";	

			$n=1;	
			foreach($final_data as $x)
			{ 	
				$url = "'".url('apisdk/detailkonservasi/'.$x->id_konservasi.'/modal')."'";
				$url = 'onclick="LoadModal('.$url.')"';
				$data.="<tr ".$url.">";	
					$data.="<td>".$n++."</td>";	 
					$data.="<td>".$x->nama_kawasan."</td>";	 
					$data.="<td>".$x->provinsi."</td>";	 
					$data.="<td>".$x->luas_kawasan."</td>";	 
				$data.="</tr>";	
			}
		}  
		else
		{
 

			$data.='
				{ "type": "FeatureCollection",
					"features": [
			';
				$a=0;
				foreach($final_data as $x)
				{  
					
					$LINTANG = $this->conver($x->latitude_lintang);
					if( $LINTANG > 90 )
			 		{
			 			$LINTANG = substr( $LINTANG,1);
			 		} 
					$BUJUR = $this->conver($x->longitude_bujur);
					if( $BUJUR > 180 )
			 		{
			 			$BUJUR = substr( $BUJUR,1);
			 		}

					$data.='
					  { "type": "Feature",
						"geometry":  
							{
								"type": "Point", 
								 "coordinates": ['.round($BUJUR ,3).', '.round($LINTANG ,3).']
							},
						"properties": 
							{ 
								"id_konservasi": "'.$x->id_konservasi.'",
								"icon": "https://cdn0.iconfinder.com/data/icons/eco/500/bio_eco_energy_farmer_flower_green_leaf_nature_plant_tree-48.png"
							}
	 
						}
						';
						$a++;
						if(count($final_data) != $a)
						{
							$data.=",";
						}
		 
				}
					   
			$data.='	]
				}
			'; 
		}

		return $data;
	}
 
	function getDetailkonservasi($id,$jenis=null)
	{
		$sql="select * from ADF_SDK_KAWASAN_KONSERVASI where id_konservasi='".$id."' ";
		$final_data =  DB::connection('oracle')->select($sql);
		 
		$data ='<table class="table table-hover">';
		$no =1;
		 

		foreach($final_data as $key=>$x)
		{  
 
			$data.='<tr>
		                <td>Provinsi</td>
		                <td>:</td>
		                <td>'.$x->provinsi.'</td> 
		            </tr>'; 
  
		    $data.='<tr>
		                <td>Nama Kawasan</td>
		                <td>:</td>
		                <td>'.$x->nama_kawasan.'</td> 
		            </tr>'; 
  
		    $data.='<tr>
		                <td>Kota</td>
		                <td>:</td>
		                <td>'.$x->kabupaten_kota.'</td> 
		            </tr>'; 
  
		    $data.='<tr>
		                <td>Dasar Hukum</td>
		                <td>:</td>
		                <td>'.$x->dasar_hukum.'</td> 
		            </tr>'; 
  
		    $data.='<tr>
		                <td>Luas</td>
		                <td>:</td>
		                <td>'.$x->luas_kawasan.'</td> 
		            </tr>'; 
  
		    $data.='<tr>
		                <td>LT</td>
		                <td>:</td>
		                <td>'.$x->latitude_lintang.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>LB</td>
		                <td>:</td>
		                <td>'.$x->longitude_bujur.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>Tipe</td>
		                <td>:</td>
		                <td>'.$x->tipe_kawasan.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>Kondisi Umum</td>
		                <td>:</td>
		                <td>'.$x->kondisi_umum.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>Provinsi</td>
		                <td>:</td>
		                <td>'.$x->provinsi.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>Geografis</td>
		                <td>:</td>
		                <td>'.$x->letak_geografis.'</td> 
		            </tr>';  

		}
		$data.="</table>";

		if($jenis==null)
		{
			return $data;
		}
		else //modal
		{
			$isi['data']= $data;
			$isi['judul']= 'Detail Kawasan Konservasi'; 
			return View::make('prototype/vms/modal',$isi);
		}  
		
		  
		 
	}


	public function getPencemaran($jenis=null,$tahun=null)
	{   
		$final_data =  DB::connection('oracle')->table('ADF_SDK_DATA_PENCEMARAN')
					 ->where('tahun','=',$tahun)
					 ->take(1000)->get();
		 		 
		$data='';
		if($jenis=='table')
		{	

			$data.="<tr>";	
				$data.="<th>#</th>";	
				$data.="<th>LOKASI</th>";	
				$data.="<th>PENCEMARAN</th>";	 
			$data.="</tr>";	

			$n=1;	
			foreach($final_data as $x)
			{ 
				$data.="<tr>";	
					$data.="<td>".$n++."</td>";	 
					$data.="<td>".$x->lokasi."</td>";	 
					$data.="<td>".$x->sumber_pencemar."</td>"; 	 
				$data.="</tr>";	
			}
		}  
		else
		{
 
 			
		}

		return $data;
	}
 
 

	 
	 
 }
