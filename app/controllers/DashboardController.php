<?php

class DashboardController extends DevetekController {

	/*
		Variable Declaration
	*/

	const _FUNCTION_ID = 'dashboard';

	/*
		Constructor
	*/
	
	public function __construct(){
        parent::__construct();
		$this->loadDefaultValue();
        $this->loadServices();
    }

	/*
		Public Method (Page Controller)
	*/
	
	public function index($page = null){			
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }
		
		if($page == null){
			return Redirect::to('/404');
		}
		
		return $this->makeView('dashboard/'.$page, array());
	}

	public function getChart($page = null){			
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }
		
		if($page == null){
			return Redirect::to('/404');
		}
		
		$data['Model'] = $this->getJson($page);
		
		return $this->makeView('dashboard/'.$page, $data);
	}

	public function getJson($id = null){			
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }
		
		if($id == null){
			return Redirect::to('/404');
		}
	
		//$data["Model"] = eval("return \$vmsService->get$id();");
	
		$dashboardDao = new DashboardDao();
		$data = Array
		(
				(object) array
				(
					"category" => "Purse Seine (Pukat Cincin) Pelagis Kecil",
					"column-1" => "29.224157"
				),(object) array
				(
					"category" => "Rawai Tuna",
					"column-1" => "14.612079"
				),(object) array
				(
					"category" => "Pukat Ikan",
					"column-1" => "13.865641"
				),(object) array
				(
					"category" => "Pengangkut",
					"column-1" => "12.621579"
				),(object) array
				(
					"category" => "Bouke ami",
					"column-1" => "10.517982"
				),(object) array
				(
					"category" => "Lainnya",
					"column-1" => "19.158562"
				)
		);
		//$data["Model"] = $dashboardDao->getData();
		
		return json_encode($data);
	}
	
	/*
		Private Method
	*/
    
	private function loadDefaultValue(){
         $this->setFunctionId(self::_FUNCTION_ID);
    }

    private function loadServices(){
        $this->mService = new ModuleService();
        print_r( $this->mService);
    }
	
}