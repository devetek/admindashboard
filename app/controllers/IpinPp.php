<?php

class IpinPp extends Controller { 


	//var $remote='@dbwh';
	public static $remote=''; 

	function conver($kordinat)
	{  
		$kordinat = str_replace('’', 'º', $kordinat);
		$kordinat = str_replace('&;', 'º', $kordinat);

		$vars = explode("º",$kordinat);
 		
 		if(isset($vars[1]))
 		{
 			return  $vars[0] +((($vars[1]*60) + (1)) / 3600);
 		}
 		return  $vars[0] ; 
	}

	public function getListpp($jenis=null)
	{    
		$sql="select * from ADF_PP_PIDANA_TANGKAP where id_pidana_tangkap > 0 ";

		if(isset($_GET['tahun']))
		{
			$sql.=" and to_char(TANGGAL_KEJADIAN,'yyyy')=".$_GET['tahun']."";
		}

		if(isset($_GET['wilayah']))
		{
			$sql.=" and  wilayah='".$_GET['wilayah']."'";
		} 

		if(isset($_GET['proses_hukum']))
		{
			$sql.=" and proses_hukum = '".$_GET['proses_hukum']."'";
		} 

			  
		
		$final_data =  DB::connection('oracle')->select($sql);
		
		if($jenis=='tabel')
		{
			$data="";
 
			$no=1;
			foreach($final_data as $key=>$x)
			{	
				$url = "'".url('apipp/detailpp/'.$x->id_pidana_tangkap.'/modal')."'";
				$modal = 'onclick="LoadModal('.$url.')"';
		        $data.="<tr ".$modal.">";
		        	$data.="<td>".$no++."</td>";
		        	$data.="<td>".$x->nama_kapal."</td>";
		        	$data.="<td>".$x->kebangsaan_bendera_kapal."</td>";
		        	$data.="<td>".$x->jumlah_abk."</td>";
		        	$data.="<td>".$x->alkap."</td>";
		        	$data.="<td>".$x->kapal_penangkap."</td>";
		        	$data.="<td>".$x->instansi_penarima_adhock."</td>"; 
		        	$data.="<td>".$x->proses_hukum."</td>"; 
		        $data.="</tr>";	
			}


		}
		else
		{
			$data ='
				{ "type": "FeatureCollection",
					"features": [
			';
				$a=0;
				foreach($final_data as $x)
				{  
					 
					$LINTANG = $this->conver($x->lintang);
					$BUJUR = $this->conver($x->bujur);

					$icon = "http://icons.iconarchive.com/icons/icons-land/vista-map-markers/16/Map-Marker-Marker-Outside-Chartreuse-icon.png";

					$data.='
					  { "type": "Feature",
						"geometry":  
							{
								"type": "Point", 
								 "coordinates": ['.round($BUJUR ,3).', '.round($LINTANG ,3).']
							},
						"properties": 
							{ 
								"idPp": "'.$x->id_pidana_tangkap.'",  
								"icon": "'.$icon.'"
							}
	 
						}
						';
						$a++;
						if(count($final_data) != $a)
						{
							$data.=",";
						}
		 
				}
					   
			$data.='	]
				}
			'; 
		}

		


		return $data;
	}

	function getDetailpp($id,$jenis=null) //proses***
	{
		$sql="select * from ADF_PP_PIDANA_TANGKAP where id_pidana_tangkap = '".$id."' ";
		$final_data =  DB::connection('oracle')->select($sql);

		 
		$data ='<table width="100%" class="table table-hover">';
		$no =1;
		 

		foreach($final_data as $key=>$x)
		{  
			// print_r('<pre>');
			// print_r($x);
			$data.='<tr>
		                <td width="40%">NAMA KAPAL</td>
		                <td width="4%">:</td>
		                <td width="56%">'.$x->nama_kapal.'</td> 
		            </tr>'; 

		    $data.='<tr>
		                <td>WILAYAH</td>
		                <td>:</td>
		                <td>'.$x->wilayah.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>TANGGAL</td>
		                <td>:</td>
		                <td>'.$x->tanggal_kejadian.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>JUMLAH ABK</td>
		                <td>:</td>
		                <td>'.$x->jumlah_abk.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>ALKAP</td>
		                <td>:</td>
		                <td>'.$x->alkap.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>BENDERA</td>
		                <td>:</td>
		                <td>'.$x->kebangsaan_bendera_kapal.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>NO_SIPI</td>
		                <td>:</td>
		                <td>'.$x->no_sipi.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>KAPAL P.</td>
		                <td>:</td>
		                <td>'.$x->kapal_penangkap.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>PROSES HUKUM</td>
		                <td>:</td>
		                <td>'.$x->proses_hukum.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>ADHOCK</td>
		                <td>:</td>
		                <td>'.$x->instansi_penarima_adhock.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>BARANG BUKTI</td>
		                <td>:</td>
		                <td>'.$x->barang_bukti.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>NAMA PENYIDIK</td>
		                <td>:</td>
		                <td>'.$x->nama_penyidik.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>PELANGGARAN</td>
		                <td>:</td>
		                <td>'.$x->jenis_pelanggaran.'</td> 
		            </tr>';
		    $data.='<tr>
		                <td>PASAL</td>
		                <td>:</td>
		                <td>'.$x->pasal_yang_dilanggar.'</td> 
		            </tr>';
		     
   

		}
		$data.="</table>";  
		
		if($jenis==null)
		{
			return $data; 
		}
		else
		{
			$isi['data']= $data;
			$isi['judul']= 'Detail Penagganan Pleanggaran';
			return View::make('prototype/penanganan_pelanggaran/modal',$isi);
		}
		 
		 
	}

	//untuk pie grafik
	function getPppie($tahun,$jenis)
	{
		
		$sql="select count(".$jenis.") as jml, ".$jenis." as jenis from ADF_PP_PIDANA_TANGKAP where to_char(TANGGAL_KEJADIAN,'yyyy')=".$tahun." group by ".$jenis; 
		 
		$final_data =  DB::connection('oracle')->select($sql);
 
		$data ='[';  
		$n=0;

			foreach($final_data as $key=>$x)
			{  
				$data.='
					{
					"category": "'.$x->jenis.'",
					"column-1": "'.$x->jml.'"
					}'; 

				if(count($final_data)-1 > $n++)
				{
					$data.=",";
				}
			}

		$data.=']'; 

		return $data;

	}


	//hasil gelar operasi
	public function getGelaroperasi()
	{    
		$sql="select * from ADF_KP_GELAR_OPERASI where TAHUN not like '0' ";

		if(isset($_GET['tahun']))
		{
			if($_GET['tahun'] !=0)
			{
				$sql.="and trim(tahun) like '".$_GET['tahun']."' ";
			}
		}

		if(isset($_GET['gelarOpr']))
		{  
			if($_GET['gelarOpr'] != 0) 
			{ 
				$sql.="and replace (NAMA_GELAR_OPERASI,' ','') = '".str_replace(' ','',trim($_GET['gelarOpr']))."' ";
			}
		}

		$final_data =  DB::connection('oracle')->select($sql);
		 
		//print_r($final_data);
		$data='';
		$no=1;
		foreach($final_data as $x)
		{  
			$data.='<tr>
                <td>'. ($no++) .'</td>
                <td>'.$x->nama_kapal_pengawas.'</td>
                <td>'.$x->hari_ops.'</td>
                <td>'.$x->hsl_riksa_jml.'</td> 
                <td>'.$x->hsl_tangkap_jml.'</td> 
            </tr>'; 
		}
		
				   
		  
		return $data;
	}
 
 

	 
	 
 }
