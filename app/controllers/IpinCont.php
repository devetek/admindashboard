<?php

class IpinCont extends Controller { 
	
	//var $remote='@dbwh';
	public static $remote=''; 
	public function getKapalvms()
	{  
			 // $sql="select a.report_date, b.*,c.*  
				// 		from  DW_MOBILE_STATUS".self::$remote." a
				// 		left join DW_TRS_VMS".self::$remote." b on a.TRANSMITTER_NO = b.NO_TRANSMITTER                   
				// 		left join dw_ship_style".self::$remote." c on trim(b.alat_tangkap) = trim(c.alatangkap) 
				// 		where  trunc(report_date) = trunc(sysdate-640)";
	 
				// $final_data =  DB::connection('oracle')->select($sql);
		$final_data =  DB::connection('oraclevms')->table('DW_TRS_VMS')->take(1000)->get();
		  
		$data ='
			{ "type": "FeatureCollection",
				"features": [
		';
			$a=0;
			foreach($final_data as $x)
			{  
				  
				$data.='
				  { "type": "Feature",
					"geometry":  
						{
							"type": "Point", "coordinates": ['.round($x->last_longitude ,3).', '.round($x->last_latitude ,3).']
						},
					"properties": 
						{
							"id": "'.$x->no_transmitter.'",
							"name": "'.$x->nama_kapal_vms.'"
						}
 
					}
					';
					$a++;
					if(count($final_data) != $a)
					{
						$data.=",";
					}
	 
			}
				   
		$data.='	]
			}
		'; 
		return $data;
	}

	public function getKapalpengawas()
	{  
			 // $sql="select a.report_date, b.*,c.*  
				// 		from  DW_MOBILE_STATUS".self::$remote." a
				// 		left join DW_TRS_VMS".self::$remote." b on a.TRANSMITTER_NO = b.NO_TRANSMITTER                   
				// 		left join dw_ship_style".self::$remote." c on trim(b.alat_tangkap) = trim(c.alatangkap) 
				// 		where  trunc(report_date) = trunc(sysdate-640)";
	 
				// $final_data =  DB::connection('oracle')->select($sql);
		$final_data =  DB::connection('oraclevms')->table('DW_TRS_VMS')->take(1000)->get();
		  
		$data ='
			{ "type": "FeatureCollection",
				"features": [
		';
			$a=0;
			foreach($final_data as $x)
			{  
				  
				$data.='
				  { "type": "Feature",
					"geometry":  
						{
							"type": "Point", "coordinates": ['.round($x->last_longitude ,3).', '.round($x->last_latitude ,3).']
						},
					"properties": 
						{
							"id": "'.$x->no_transmitter.'",
							"name": "'.$x->nama_kapal_vms.'"
						}
 
					}
					';
					$a++;
					if(count($final_data) != $a)
					{
						$data.=",";
					}
	 
			}
				   
		$data.='	]
			}
		'; 
		return $data;
	}

	public function getDatazone()
	{	
		$where = '';
		$get=array();
		if(isset($_GET['id'])){
			$get = explode(',',$_GET['id']);
			
			 
			$n=1;
			for($a=0;$a < (count($get) -1);$a++) // karena titambahan 1 -> 0
			{
				$val=$get[$a];
				$where.= "'".$val."'";
				
				$n++;
				if(count($get) != $n)
				{
					$where.= ",";
				}
			} 
		}
		

		$sql="select a.id,a.ZONE_NAME,a.zone_type,   
				d.LINE_STYLE , d.LINE_COLOR ,d.FILL_STYLE,d.FILL_COLOR,d.TRANSPARENT,d.LINE_WIDTH,d.FILLTRANSPARENT
				from dw_zone a, 
				DW_ZONE_STYLE d
			where   a.id=d.zone_id and a.zone_type != 2
		";
		//echo $sql;

		if(count($get) > 1)
		{	
			$sql.=" and a.id in (".$where.")";
		}
		else
		{
			$sql.=" and a.id in (0)";
		}

		$data_zone = DB::connection('oraclewarehouse')
					->select($sql); 

		$js='
			{
			  "type": "FeatureCollection",
			  "features": [
			  ';

			  $n=1;
			  foreach ($data_zone as $key => $v) 
			  {
			   
			  	$zone_type = $v->zone_type==2 ? "Point" :  "Polygon";
				$js.='
				    {
				      "type": "Feature",
				      "properties": { 
				        "name": "'.$v->zone_name.'",
				        "fill-color": "'.$v->fill_color.'",
				        "line-color": "'.$v->line_color.'",
				        "fill-opacity": '.$v->filltransparent.',
				        "line-width": '.$v->line_width.',
				        "id": "'.$v->id.'"
				      },
				      "geometry": {
				        "type": "'.$zone_type .'",
				        "coordinates":  ';
				          
				          if($v->zone_type !=2)
				          {
				          		$js.='[ [';
				          }

				          $sql="select * from dw_zone_detail where zone_id = ".$v->id;
				          $data_zone2 = DB::connection('oraclewarehouse')
							->select($sql);

							$n2=1;
							foreach ($data_zone2 as $key => $value2) 
							{
								$js.='
						            
						            [
						              '.($value2->longitude +0).',
						              '.($value2->latitude + 0).'
						            ]';


						            if(count($data_zone2) > $n2++)
								    {
								    	$js.=',';
								    }

						    }

						if($v->zone_type !=2)
				          {
				          		$js.='] ]';
				          } 

				        $js.='	 
				      }
				    }
				    ';

				    if(count($data_zone) > $n++)
				    {
				    	$js.=',';
				    }
				}


		$js.='	    
			  ]
			}';

		return $js;

	}

 

	 

	function getConfig()
	{
		$sql="select * from sig_setting where setting like 'speedline' ";
		$data['ConfSpeed']= DB::connection('oracle')->select($sql)[0]->isinyo;

		$sql="select * from sig_setting where setting like 'infolabel' ";  
		$data['ConfInfo']= DB::connection('oracle')->select($sql)[0]->isinyo;  
		return $data;

	}

	 
	 
 }
