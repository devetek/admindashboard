<?php

class KPController extends DevetekController {

	/*
		Variable Declaration
	*/

	const _FUNCTION_ID = 'kapal_pengawas';

	/*
		Constructor
	*/
	
	public function __construct(){
        parent::__construct();
		$this->loadDefaultValue();
        $this->loadServices();
    }

	/*
		Public Method (Page Controller)
	*/
	
	public function index(){			
		//if (!$this->isLogin()){ return Redirect::to('login'); }
        //if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }
		
		//Data Summary
		$data['mJumlahSkatAktif'] = $this->mService->getJumlahSKAT(1);
		$data['mJumlahSkatAkanHabis'] = $this->mService->getJumlahSKAT(3);
		$data['mJumlahKapalTerpantau'] = $this->mService->getJumlahKapalTerpantau(1);
		$data['mJumlahKapalNonTerpantau'] = $this->mService->getJumlahKapalTerpantau(2);
		
		//Data Peta
		$data['mJumlahKapalNonTerpantau'] = $this->mService->getJumlahKapalTerpantau(2);
		
		return $this->makeView('dashboard/vms', $data);
	}

	public function listIndikasiPelanggaran(){
		//Data Indikasi Pelanggaran
		$data['mListIndikasi'] = $this->mService->getListIndikasi();
		$data['mJumlahIndikasiPelanggaran'] = count($data['mListIndikasi']);
		
		return $this->makeView('dashboard/vms/list-suspect', $data);
	}

	public function grafikPelanggaran(){
		$startDate;
		$endDate;
		$periode;
		$data['mDataChartPelanggaran'] = 
		$this->mService->getDataChartPelanggaran($startDate,$endDate,$periode);
		
		return $this->makeView('dashboard/vms/chart-kapal', $data);
	}
	
	public function chartKomposisi(){
		$type;
		$data['mDataChartKomposisi'] = $this->mService->getDataChartKomposisi($type);
		
		return $this->makeView('dashboard/vms/chart-komposisi', $data);
	}
	
	public function getPage($page = null){			
		// if (!$this->isLogin()){ return Redirect::to('login'); }
        // if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }
		
		if($page == null){
			return Redirect::to('/404');
		}
		
		return eval("\$this->$page();");
	}

	public function getJson($id = null){			
		// if (!$this->isLogin()){ return Redirect::to('login'); }
        // if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }
		
		$data;
		
		if($id == null){
			return json_encode($data);
		}
		
		return json_encode(eval("return \$vmsService->get$id();"));
	}
	
	public function getDataTable($id = null){			
		// if (!$this->isLogin()){ return Redirect::to('login'); }
        // if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }
		
		if($id == null){
			return Redirect::to('/404');
		}
	
		$vmsService = new VMSService();

		
		return json_encode($data);
	}
	
	/*
		Private Method
	*/
    
	private function loadDefaultValue(){
         $this->setFunctionId(self::_FUNCTION_ID);
    }

    private function loadServices(){
        $this->mService = new KPService();
    }
	
}