<?php

class IpinVms extends Controller { 


	//var $remote='@dbwh';
	public static $remote=''; 
	public static $limit='7'; 
	public static $sekarang= '2015-06-01 8:30:25'; 

	function getSekat($status,$jenis)
	{  
		$data = array();
		# 1-> aktif, 0 -> akan habis
		if($status == 0)
		{
			$sql="SELECT * from ADF_VMS_SKAT_SHIP where trunc(TRANSMITTER_PAIRING_END_DATE) >= trunc(SYSDATE) and trunc(TRANSMITTER_PAIRING_END_DATE) <= trunc(SYSDATE + ".self::$limit.") ";

			$data = DB::connection('oracle')->select($sql);
		}
		else if($status == 1)
		{
			$sql="SELECT * from ADF_VMS_SKAT_SHIP where trunc(TRANSMITTER_PAIRING_END_DATE) >= trunc(SYSDATE) ";

			$data = DB::connection('oracle')->select($sql);
		}

		$datas['status'] = $status;
		$datas['jenis'] = $jenis;
		$datas['data'] = $data;
		$datas['judul'] = $status==0? 'Akan habis' : 'Aktif';

		if($jenis=='count')
		{
			return count($data).' Kapal' ;
		}
		else if($jenis=='table')
		{ 
			return View::make('prototype/vms/list-skat-detail',$datas);
		} 
		else if($jenis=='pdf')
		{	 
	 		$html = View::make('prototype/vms/list-skat-detail',$datas);
			return PDF::load($html, 'A4', 'portrait')->show(); 
		} 
		else if($jenis=='excel')
		{
			Excel::create($datas['judul'], function($excel) use ($datas) {

			    $excel->sheet('data', function($sheet) use ($datas) {

			        $sheet->loadView('prototype/vms/list-skat-detail',$datas);

			    }); 

			})->export('xls');
 
		}  
	}

	function getKapal($status,$jenis)
	{  
		$data = array();
		# 1-> aktif, 0 -> akan habis
		if($status == 0)
		{
			$sql="SELECT * 
					from ADF_DW_J_TVESSEL_STATUS 
					where REPORTDATE < TO_DATE('".self::$sekarang."', 'YYYY-MM-DD HH:MI:SS')";

			$data = DB::connection('oracle')->select($sql);
		}
		else if($status == 1)
		{
			$sql="SELECT * from ADF_DW_J_TVESSEL_STATUS 
					where REPORTDATE >= TO_DATE('".self::$sekarang."', 'YYYY-MM-DD HH:MI:SS') ";
 
			$data = DB::connection('oracle')->select($sql);
		}


		$datas['status'] = $status;
		$datas['jenis'] = $jenis;
		$datas['data'] = $data;
		$datas['judul'] = $status==0? 'Tidak Aktif' : 'Aktif';

		if($jenis=='count')
		{
			return count($data).' Kapal' ;
		} 

		else if($jenis=='table')
		{ 
			return View::make('prototype/vms/list-kapal_terpantau-detail',$datas);
		} 
		else if($jenis=='pdf')
		{	 
	 		$html = View::make('prototype/vms/list-kapal_terpantau-detail',$datas);
			return PDF::load($html, 'A4', 'portrait')->show(); 
		} 
		else if($jenis=='excel')
		{
			Excel::create($datas['judul'], function($excel) use ($datas) {

			    $excel->sheet('data', function($sheet) use ($datas) {

			        $sheet->loadView('prototype/vms/list-kapal_terpantau-detail',$datas);

			    }); 

			})->export('xls');
 
		} 
	}


	function getMapkapal()
	{   

		$sql="SELECT * 
					from ADF_DW_J_TVESSEL_STATUS 
					where negara IS NOT NULL ";

		$icon='';
		# 1-> aktif, 0 -> akan habis
		if(isset($_GET['statusVMS']) and $_GET['statusVMS']==0)
		{
			$icon = "http://icons.iconarchive.com/icons/paomedia/small-n-flat/16/map-marker-icon.png";
			$sql.=" AND REPORTDATE < TO_DATE('".self::$sekarang."', 'YYYY-MM-DD HH:MI:SS')"; 
		}
		else if(isset($_GET['statusVMS']) and $_GET['statusVMS']==1)
		{
			$icon = "http://icons.iconarchive.com/icons/icons-land/vista-map-markers/16/Map-Marker-Marker-Outside-Chartreuse-icon.png";
			$sql.=" AND REPORTDATE >= TO_DATE('".self::$sekarang."', 'YYYY-MM-DD HH:MI:SS') ";
		}

		 
		if(isset($_GET['negaraVMS']))
		{
			$sql.=" AND negara like '".$_GET['negaraVMS']."' "; 
		}

		if(isset($_GET['alat_tangkapVMS']))
		{
			$sql.=" AND ALAT_TANGKAP like '".$_GET['alat_tangkapVMS']."' "; 
		} 

		$final_data =  DB::connection('oracle')->select($sql);

		$data='
			{ "type": "FeatureCollection",
				"features": [
		';

			$a=0;
			foreach($final_data as $x)
			{ 

				$data.='
				  { "type": "Feature",
					"geometry":  
						{
							"type": "Point", 
							 "coordinates": ['.round($x->last_longitude + 0 ,3).', '.round($x->last_latitude + 0,3).']
						},
					"properties": 
						{ 
							"idVms": "'.$x->no_transmitter.'" , 
							"icon": "'.$icon.'"  
						}
 
					}
					';
					$a++;
					if(count($final_data) != $a)
					{
						$data.=",";
					}
	 

			}

			$data.='	]
			}
		'; 
		return $data;
	}


	function getDetailkapal($id,$jenis=null) //proses***
	{
		$sql="select * from ADF_DW_J_TVESSEL_STATUS where no_transmitter = '".$id."' ";
		$final_data =  DB::connection('oracle')->select($sql);

		 
		$data ='<table width="100%" class="table table-hover">';
		$no =1;
		 

		foreach($final_data as $key=>$x)
		{   
			$data.='<tr>
		                <td width="40%">NAMA KAPAL</td>
		                <td width="4%">:</td>
		                <td width="56%">'.$x->nama_kapal.'</td> 
		            </tr>'; 

		    $data.='<tr>
		                <td>NO TRANSMITTER</td>
		                <td>:</td>
		                <td>'.$x->no_transmitter.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>LAST LONGITUDE</td>
		                <td>:</td>
		                <td>'.$x->last_longitude.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>LAST LATITUDE</td>
		                <td>:</td>
		                <td>'.$x->last_latitude.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>REPORTDATE</td>
		                <td>:</td>
		                <td>'.$x->reportdate.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>SPEED</td>
		                <td>:</td>
		                <td>'.$x->speed.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>UKURAN GT</td>
		                <td>:</td>
		                <td>'.$x->ukuran_gt.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>ALAT TANGKAP</td>
		                <td>:</td>
		                <td>'.$x->alat_tangkap.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>NO SIPI</td>
		                <td>:</td>
		                <td>'.$x->no_sipi.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>AKHIR SIPI</td>
		                <td>:</td>
		                <td>'.$x->tanggal_akhir_sipi.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>PELABUHAN PANGKALAN</td>
		                <td>:</td>
		                <td>'.$x->nama_pelabuhan_pangkalan.'</td> 
		            </tr>'; 
		    $data.='<tr>
		                <td>NAMA WPP</td>
		                <td>:</td>
		                <td>'.$x->nama_wpp.'</td> 
		            </tr>';  
		    $data.='<tr>
		                <td>NEGARA</td>
		                <td>:</td>
		                <td>'.$x->negara.'</td> 
		            </tr>'; 
		      
   

		}
		$data.="</table>";  
		
		if($jenis==null)
		{
			return $data; 
		}
		else if('pdf')
		{
			$isi['data']= $data;
			$isi['judul']= 'Detail Kapal'; 
			return View::make('prototype/vms/modal',$isi);
		}
		else if('excel')
		{
			$isi['data']= $data;
			$isi['judul']= 'Detail Kapal'; 
			return View::make('prototype/vms/modal',$isi);
		}
		else
		{
			$isi['data']= $data;
			$isi['judul']= 'Detail Kapal'; 
			return View::make('prototype/vms/modal',$isi);
		}
		 
		 
	}

	public function getKapalpie($jenis=null)
	{   
		
		$sql="select count(".$jenis.") as jml, ".$jenis." as jenis from ADF_DW_J_TVESSEL_STATUS where ".$jenis." IS NOT NULL   group by ".$jenis; 
		 
		$final_data =  DB::connection('oracle')->select($sql);
 
		$data ='[';  
		$n=0;

			foreach($final_data as $key=>$x)
			{  
				$data.='
					{
					"category": "'.$x->jenis.'",
					"column-1": "'.$x->jml.'"
					}'; 

				if(count($final_data)-1 > $n++)
				{
					$data.=",";
				}
			}

		$data.=']'; 


		return $data;
	}
 
 

	 
	 
 }
