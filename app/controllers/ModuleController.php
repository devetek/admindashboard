<?php

class ModuleController extends DevetekController {

	/*
		Variable Declaration
	*/

	const _FUNCTION_ID = 'module';

	/*
		Constructor
	*/
	
	public function __construct(){
        parent::__construct();
		$this->loadDefaultValue();
        $this->loadServices();
    }

	/*
		Public Method (Page Controller)
	*/
	
	public function index(){			
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }

		$this->restoreTempData();
		$data['Model'] = Array();
		
		try {
			$data['Model'] = $this->mService->getModules();
			return $this->makeView('module/index', $data);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->makeView('module/index', $data);
        }
	}

	public function create(){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowCreate()){ return Redirect::to('forbidden'); }

		$module = new Module();

        try{
			$module->setIsEnabled(0);
			$module->setIsShow(0);
			return $this->createInputView($module);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->createInputView($Module);
        }
	}

	public function processcreate(){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowCreate()){ return Redirect::to('forbidden'); }

		$module = new Module();

        try{
			$module = $this->bindData();
			if($this->validateInput(Input::all())==FALSE) return $this->createInputView($module);
			$this->mService->setUserInformation($this->UserInformation());
			$insertedModule = $this->mService->insertModule($module);
            if($insertedModule==null){
				$this->addModelError($this->mService->GetErrors());
				return $this->createInputView($module);
			}
			return Redirect::to('module/detail/'.$insertedModule->GetId());
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->createInputView($module);
            }
	}
	
	public function detail($id=null){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }

        try {
            $result = $this->mService->getModule($id);
            if($result==null) {
				$this->addModelError("Data Not Found");
				$this->assignToTempData();
                Return Redirect::to('module');
            }
            $data['Model'] = $result;
            return $this->makeView('module/detail', $data);
        } catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('module');
        }

	}

	public function edit($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowUpdate()){ return Redirect::to('forbidden'); }
        
        $Module = new Module();
        
        try {
			$Module = $this->mService->getModule($id);
            if($Module==null) {
				$this->addModelError("Module Not Found");
				$this->assignToTempData();
                Return Redirect::to('module');
            }
			return $this->createInputView($Module);
		} catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('module');
        }
	}

	public function processedit($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowUpdate()){ return Redirect::to('forbidden'); }
        
        $Module = new Module();
		$Module = $this->bindData();
        
        try {
			if($this->validateInput(Input::all())==FALSE) return $this->createInputView($Module);
			$this->mService->setUserInformation($this->userInformation());
			if($this->mService->updateModule($Module,$Module->getId())==true){
				Return Redirect::to('module/detail/'.$Module->getId());
			}
			return $this->createInputView($Module);
        } catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('module');
        }
	}

	
	public function delete($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowDelete()){ return Redirect::to('forbidden'); }

        try {
            $this->mService->deleteModule($id);
            
            Return Redirect::to('module');
        } catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
            Return Redirect::to('module');
        }
	}
	
	/*
		Private Method
	*/
    
    private function loadParentModule($show_all = false, $ModuleId = ""){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $arr_data[''] = '';
        $ModuleFilter = new ModuleFilter();
        $ModuleFilter->setNotId($ModuleId);
        $Modules = $this->mService->GetModules($ModuleFilter);
        foreach ($Modules as $item) {
            $arr_data[$item->getId()] = $item->getName();
        }
        
        $this->data['ParentModuleCombo'] = $arr_data;
    }

    private function loadComboIsShow($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $arr_data[true] = 'TRUE';
        $arr_data[false] = 'FALSE';
        
        $this->data['IsShowCombo'] = $arr_data;
    }

    private function loadComboIsEnabled($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $arr_data[true] = 'TRUE';
        $arr_data[false] = 'FALSE';
        
        $this->data['IsEnabledCombo'] = $arr_data;
    }

    private function createInputView($Module=null){
        $this->loadParentModule(false, $Module->getId());
        $this->loadComboIsEnabled();
        $this->loadComboIsShow();
		$this->data['Model'] = $Module;
		
        return $this->makeView('module/input', $this->data);
    }
    
    private function bindData(){
        $data = Input::all();
        
        $Module = New Module();
        $Module->setId($data["id"]);
        $Module->setName($data["name"]);
        $Module->setIcon($data["icon"]);
        $Module->setSequence($data["sequence"]);
        $Module->setIsShow($data["is_show"]);
        $Module->setIsEnabled($data["is_enabled"]);
                $ParentModule = new Module();
                $ParentModule->setId($data["parent_module_id"]);
                $ParentModule->setIsLoaded(true);
        $Module->setParentModule($ParentModule);
        
        return $Module;
    }
 
    private function validateInput($data){
        
        if($data==null) return false;
        $rules = array(
            'name' => 'required'
        );

        $messages = array(
            'name' => 'Module Name is Required.'
        );

        $this->validator = Validator::make($data, $rules, $messages);
        if($this->validator->fails()) $this->assignValidatorToModelState($this->validator);
        return $this->validator->passes();
    }
	
	private function loadDefaultValue(){
         $this->setFunctionId(self::_FUNCTION_ID);
    }

    private function loadServices(){
        $this->mService = new ModuleService();
    }
	
}