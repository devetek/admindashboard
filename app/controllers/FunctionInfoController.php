<?php

class FunctionInfoController extends DevetekController {

	/*
		Variable Declaration
	*/

	const _FUNCTION_ID = 'function_info';

	/*
		Constructor
	*/
	
	public function __construct(){
        parent::__construct();
		$this->loadDefaultValue();
        $this->loadServices();
    }

	/*
		Public Method (Page Controller)
	*/
	
	public function index(){			
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }
		$this->restoreTempData();
		$data['Model'] = Array();
		
		try {
			$data['Model'] = $this->mService->getFunctionInfos();
			return $this->makeView('functioninfo/index', $data);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->makeView('functioninfo/index', $data);
        }
	}

	public function create(){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowCreate()){ return Redirect::to('forbidden'); }

		$functionInfo = new FunctionInfo();
        try{
			$functionInfo->setCreatedDate(date('d-m-Y H:i:s'));
			$functionInfo->setCreatedBy($this->UserInformation());
			return $this->createInputView($functionInfo);
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->createInputView($functionInfo);
        }
	}

	public function processcreate(){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowCreate()){ return Redirect::to('forbidden'); }

		$functionInfo = new FunctionInfo();
        try{
			$functionInfo = $this->bindData();
			if($this->validateInput(Input::all())==FALSE) return $this->createInputView($functionInfo);
			$this->mService->setUserInformation($this->UserInformation());
			$insertedFunctionInfo = $this->mService->insertFunctionInfo($functionInfo);
			if($insertedFunctionInfo==null){
				$this->addModelError($this->mService->GetErrors());
				return $this->createInputView($functionInfo);
			}
			return Redirect::to('functioninfo/detail/'.$insertedFunctionInfo->getId());
        } catch (Exception $exc) {
            $this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
            return $this->createInputView($functionInfo);
        }
	}
	
	public function detail($id=null){
        if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowRead()){ return Redirect::to('forbidden'); }

        try {
            $result = $this->mService->getFunctionInfo($id);
            if($result==null) {
				$this->addModelError("Function Info Not Found");
				$this->assignToTempData();
                Return Redirect::to('functioninfo');
            }
            $data['Model'] = $result;
            return $this->makeView('functioninfo/detail', $data);
        } catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('functioninfo');
        }

	}

	public function edit($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowUpdate()){ return Redirect::to('forbidden'); }
        
        $functionInfo = new FunctionInfo();
        
        try {
			$functionInfo = $this->mService->getFunctionInfo($id);
            if($functionInfo==null) {
				$this->addModelError("Module Not Found");
				$this->assignToTempData();
                Return Redirect::to('functioninfo');
            }
			return $this->createInputView($functionInfo);
		} catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('functioninfo');
        }
	}

	public function processedit($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowUpdate()){ return Redirect::to('forbidden'); }
        
        $functionInfo = new FunctionInfo();
		$functionInfo = $this->bindData();
        
        try {
			if($this->validateInput(Input::all())==FALSE) return $this->createInputView($functionInfo);
			$this->mService->setUserInformation($this->userInformation());
			if($this->mService->updateFunctionInfo($functionInfo,$functionInfo->getId())==true){
				Return Redirect::to('functioninfo/detail/'.$functionInfo->getId());
			}
			return $this->createInputView($functionInfo);
        } catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
			return Redirect::to('functioninfo');
        }
	}

	
	public function delete($id){
		if (!$this->isLogin()){ return Redirect::to('login'); }
        if (!$this->isAllowDelete()){ return Redirect::to('forbidden'); }

        try {
            $this->mService->deleteFunctionInfo($id);
            
            Return Redirect::to('functioninfo');
        } catch (Exception $exc) {
			$this->addModelError("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
			$this->assignToTempData();
            Return Redirect::to('functioninfo');
        }
	}
	
	/*
		Private Method
	*/
	
    private function loadComboModule($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $moduleService = new ModuleService();
        $module = $moduleService->getModules();
        foreach ($module as $item){
            $arr_data[$item->getId()] = $item->getName();
        }
        
        $this->data['ModuleCombo'] = $arr_data;
    }

    private function loadComboIsShow($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $arr_data[true] = 'TRUE';
        $arr_data[false] = 'FALSE';
        
        $this->data['IsShowCombo'] = $arr_data;
    }

    private function loadComboIsEnabled($show_all = false){
        $arr_data = array();
        
        if ($show_all) $arr_data[''] = 'Pilih Nilai';
        $arr_data[true] = 'TRUE';
        $arr_data[false] = 'FALSE';
        
        $this->data['IsEnabledCombo'] = $arr_data;
    }

    private function createInputView($functionInfo=null){
        $this->loadComboIsEnabled();
        $this->loadComboIsShow();
        $this->loadComboModule();
		$this->data['Model'] = $functionInfo;
		
        return $this->makeView('functioninfo/input', $this->data);
    }
    
    private function bindData(){
        $data = Input::all();
        
        $functionInfo = New FunctionInfo;
        $functionInfo->setId($data["id"]);
        $functionInfo->setName($data["name"]);
            $module =  new Module();
            $module->setId($data["module_id"]);
            $module->setIsLoaded(true);
        $functionInfo->setModule($module);
        $functionInfo->setIcon($data["icon"]);
        $functionInfo->setSequence($data["sequence"]);
        $functionInfo->setUrl($data["url"]);
        $functionInfo->setIsShow($data["is_show"]);
        $functionInfo->setIsEnabled($data["is_enabled"]);

        return $functionInfo;
    }
 
    private function validateInput($data){
        
        if($data==null) return false;
        $rules = array(
            'name' => 'required'
        );

        $messages = array(
            'name' => 'Name is Required.'
        );

        $this->validator = Validator::make($data, $rules);
        if($this->validator->fails()) $this->assignValidatorToModelState($this->validator);
        return $this->validator->passes();
    }
	
	private function loadDefaultValue(){
        $this->setFunctionId(self::_FUNCTION_ID);
    }

    private function loadServices(){
        $this->mService = new FunctionInfoService();
    }
	
}