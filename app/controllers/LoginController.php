<?php

class LoginController extends DevetekController {

	/*
		Public Method (Page Controller)
	*/

    public function index(){ 
		if ($this->isLogin()){ return Redirect::to('home'); }
        return $this->makeView('shared/login',array());
    }

	public function doLogin(){
        /* 
			process the form
			validate the info, create rules for the inputs
		*/
        $rules = array(
                'username' => 'required|min:3', // make sure the email is an actual email
                'pswd' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
			return Redirect::to('/')
					->withErrors($validator) // send back all errors to the login form
					->withInput(Input::except('pswd')); // send back the input (not the password) so that we can repopulate the form
        } else {
			// create our user data for the authentication
			$userdata = array(
					'username' 	=> Input::get('username'),
					'password' 	=> Input::get('pswd')
			);
			// attempt to do the login
			if (Auth::attempt($userdata)) {
				// validation successful!
				$obj = UserInfoDao::where('username', '=', $userdata['username'])->firstOrFail();
				$obj->last_login = date('Y/m/d H:i:s');
				$obj->save();
				
				return Redirect::to('prototype/homepage')->with('justLogin', true);
            } else {
                // validation not successful, send back to form	
                return Redirect::to('/')->withInput(Input::except('pswd')); // send back the input (not the password) so that we can repopulate the form;
            }
        }
    }
    
    public function doLogout(){
        Auth::logout(); // log the user out of our application
		Session::forget('functioninfo');
		Session::forget('privilegeinfo');
        return Redirect::to('/'); // redirect the user to the login screen
    }
    
    public function showForbidden(){
        $this->mFunctionInfoId = "Forbidden";
        //$this->page_data['_MODULE_NAME'] = $this->getFunctionName();
        if (!$this->isLogin()){ return Redirect::to('/'); }
        return $this->makeView('shared/forbidden',array());
    }
	
	/*
		Private Method
	*/
	
	private function loadDefaultValue(){
        
    }

    private function loadServices(){
		
    }
	
}