<?php
class LookupDetailService extends DevetekService {
    
	function __construct(array $attributes = array()) {
        parent::__construct($attributes);
		$this->mDao = new LookupDetailDao();
    }

    public function insertLookupDetail($obj){
        try {
			if($this->validateObjectOnInsert($obj)==false) return false;
			$this->mDao->BeginTransaction();
				$obj = $this->mDao->insertObject($obj);
			$this->mDao->CommitTransaction();
            return $obj;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }

    public function updateLookupDetail($obj,$oldId){
        try {
			if($this->validateObjectOnUpdate($obj)==false) return false;
            $this->mDao->BeginTransaction();
                if ($this->smartUpdate($obj,$oldId)==false) return null;
            $this->mDao->CommitTransaction();

            return true;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }

    public function getLookupDetails($filter=null) {
        try {
            return $this->mDao->getList($filter);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }
    
    public function getLookupDetail($id){
        try {
            return $this->mDao->getObject($id);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }

    public function deleteLookupDetail($id){
        try {
            $lookupDetail = $this->mDao->getObject($id);
            $this->mDao->BeginTransaction();
                if($this->mDao->deleteObject($lookupDetail->getId())==false) return false;
            $this->mDao->CommitTransaction();
            return true;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }
    
    public function smartUpdate($obj,$oldId){
		if($this->mDao->updateObject($obj,$oldId)==false) return false;
		return true;
    }
    
    private function validateBase($obj) {
		// $oldId = $obj->getId();
		// $newId = str_replace(" ", "_", strtolower($obj->getName()));
		// if($obj->getId() != $newId){
			// $obj->setId($newId);
			 // $lookupDetailFilter = new LookupDetailFilter();
			// $lookupDetailFilter->setId($obj->getId());
			// $lookupDetailFilter->setName($obj->getName());
			// if($this->getLookupDetails($lookupDetailFilter)!=null){
				// $this->addError("Entry with Same Name is Existed.");
			// }
		// }
		
        return $this->getServiceState();
    }
    
    private function validateObjectOnInsert($model) {
        $this->validateBase($model);
        
		return $this->getServiceState();
    }
    
    private function validateObjectOnUpdate($model) {
        $this->validateBase($model);
        
		return $this->getServiceState();
    }
    
}
?>