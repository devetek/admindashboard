<?php
class PrivilegeInfoService extends DevetekService {
    
	function __construct(array $attributes = array()) {
        parent::__construct($attributes);
		$this->mDao = new PrivilegeInfoDao();
    }

    public function insertPrivilegeInfo($obj){
        try {
			if($this->validateObjectOnInsert($obj)==false) return false;
			if($this->UserInformation() != null){
				$userInfo = new UserInfo();
				$userInfo->setId($this->UserInformation()->getId());
				$userInfo->setIsLoaded(true);
				$obj->setCreatedBy($userInfo);
			}
			$obj->setCreatedDate(new DateTime());
			$this->mDao->BeginTransaction();
				$obj = $this->mDao->insertObject($obj);
			$this->mDao->CommitTransaction();
            return $obj;

        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("Privilege Info Service: ".$exc->getMessage());
        }
    }

    public function updatePrivilegeInfo($obj,$oldId){
        try {
			if($this->validateObjectOnUpdate($obj)==false) return false;
			$oldObj = $this->getModule($oldId);
			if($oldObj!=null){
				$obj->setCreatedBy($oldObj->getCreatedBy());
				$obj->setCreatedDate($oldObj->getCreatedDate());
			}
			if($this->UserInformation() != null){
				$userInfo = new UserInfo();
				$userInfo->setId($this->UserInformation()->getId());
				$userInfo->setIsLoaded(true);
				$obj->setUpdatedBy($userInfo);
			}
			$obj->setUpdatedDate(new DateTime());
            $this->mDao->BeginTransaction();
                if ($this->smartUpdate($obj,$oldId)==false) return null;
            $this->mDao->CommitTransaction();

            return true;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("Privilege Info Service: ".$exc->getMessage());
        }
    }

    public function getPrivilegeInfos($criteria=null) {
        try {
            return $this->mDao->getList($criteria);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("Privilege Info Service: ".$exc->getMessage());
        }
    }
    
    public function getPrivilegeInfo($id){
        try {
            return $this->mDao->getObject($id);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("Privilege Info Service: ".$exc->getMessage());
        }
    }

    public function deletePrivilegeInfo($id){
        try {
            $this->mDao->BeginTransaction();
				$privilegeInfoDao = new PrivilegeInfoDao();
				if($userGroup->getPrivilegeInfo()!=null){
					foreach ($userGroup->getPrivilegeInfo() as $item){
						if($privilegeInfoDao->deleteObject($item->getId())==false) return false;
					}
				}
				if($this->mDao->deleteObject($userGroup->getId())==false) return false;
                
            $this->mDao->CommitTransaction();
            return true;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("Privilege Info Service: ".$exc->getMessage());
        }
    }
    
    public function smartUpdate($obj,$oldId){
        $oldObj = $this->getPrivilegeInfo($oldId);
		if($oldObj!=null){
			$obj->setCreatedBy($oldObj->getCreatedBy());
			$obj->setCreatedDate($oldObj->getCreatedDate());
		}
		if($this->UserInformation() != null){
			$userInfo = new UserInfo();
			$userInfo->setId($this->UserInformation()->getId());
			$userInfo->setIsLoaded(true);
			$obj->setUpdatedBy($userInfo);
		}
		$obj->setUpdatedDate(new DateTime());
		$this->mDao->BeginTransaction();
			if($this->mDao->updateObject($obj,$oldId)==false) return false;
		$this->mDao->CommitTransaction();
		return true;
        
    }

    private function validateBase($obj) {
		
        return $this->getServiceState();
    }
    
    private function validateObjectOnInsert($model) {
        $this->validateBase($model);
        
		return $this->getServiceState();
    }
    
    private function validateObjectOnUpdate($model) {
        $this->validateBase($model);
        
		return $this->getServiceState();
    }
    
}
?>