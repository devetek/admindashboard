<?php
class UserInfoService extends DevetekService {
    
	function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->mDao = new UserInfoDao();
    }

    public function insertUserInfo($obj){
        try {
            if($this->validateObjectOnInsert($obj)==false) return false;
			if($this->UserInformation() != null){
				$user = new UserInfo();
				$user->setId($this->UserInformation()->getId());
				$user->setIsLoaded(true);
				$obj->setCreatedBy($user);
			}
			$obj->setCreatedDate(new DateTime());
        
			$this->mDao->BeginTransaction();
                $obj = $this->mDao->insertObject($obj);
                if ($obj==null) return null;
            $this->mDao->CommitTransaction();

			return $obj;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }

    public function updateUserInfo($obj,$oldId){
        try {
			if($this->validateObjectOnUpdate($obj)==false) return false;
			$oldObj = $this->getUserInfo($oldId);
			if($oldObj!=null){
				$obj->setCreatedBy($oldObj->getCreatedBy());
				$obj->setCreatedDate($oldObj->getCreatedDate());
			}
			if($this->UserInformation() != null){
				$user = new UserInfo();
				$user->setId($this->UserInformation()->getId());
				$user->setIsLoaded(true);
				$obj->setUpdatedBy($user);
			}
			
			$obj->setUpdatedDate(new DateTime());
            $this->mDao->BeginTransaction();
                if ($this->smartUpdate($obj,$oldId)==false) return null;
            $this->mDao->CommitTransaction();

            return true;
		} catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }

    public function getUserInfos() {
        try {
            return $this->mDao->getList();
        } catch (Exception $exc) {
			throw new Exception(get_class($this). ": " .$exc->getMessage());
        }
    }
    
    public function getUserInfo($id){
        try {
            return $this->mDao->getObject($id);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }

    public function deleteUserInfo($id){
        try {
            $user = $this->mDao->getObject($id);
			if($user!=null){               
				$this->mDao->BeginTransaction();
					if($this->mDao->deleteObject($user->getId())==false) return false;
				$this->mDao->CommitTransaction();
			}
            return true;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }   
    
	public function smartUpdate($obj,$oldId){
        if($this->mDao->updateObject($obj,$oldId)==false) return false;

        return $obj;
    }
	
    public function resetPassword($obj,$oldId){
        try {
            if($this->validatePassword($oldId,$obj->getPassword())==false) {
                $this->addModelError("Invalid Password");
				return false;
            }
			return true;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }
    
    public function validatePassword($username,$password){
        try {
            return $this->mDao->validatePassword($username,$password);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }
 
    private function validateBase($obj) {
		
        return $this->getServiceState();
    }
    
    private function validateObjectOnInsert($obj) {
		$this->validateBase($obj);
        
		$userInfoFilter = new UserInfoFilter();
		$userInfoFilter->setUsername($obj->getUsername());
		if($this->mDao->getList($userInfoFilter)!=null){
			$this->addError("Entry with Same Username is Existed.");
		}
		
		$userInfoFilter = new UserInfoFilter();
		$userInfoFilter->setEmail($obj->getEmail());
		if($this->mDao->getList($userInfoFilter)!=null){
			$this->addError("Entry with Same Email is Existed.");
		}
		
		return $this->getServiceState();
    }
    
    private function validateObjectOnUpdate($obj) {
        $this->validateBase($obj);
        
		return $this->getServiceState();
    }
	 
	public function CheckUser($username,$id){
        try {
            return $this->mDao->CheckUser($username,$id);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }   

    public function updateUserGroupId($newId,$oldId){
        try {
            return $this->mDao->updateUserGroupId($newId,$oldId);
        } catch (Exception $exc) {
            $this->addError($exc->getMessage());
            throw new Exception("[".__CLASS__ .":".__FUNCTION__."]:".$exc->getMessage());
        }           
    }
}
?>