<?php
class UserGroupService extends DevetekService {
    
	function __construct(array $attributes = array()) {
        parent::__construct($attributes);
		$this->mDao = new UserGroupDao();
    }

    public function insertUserGroup($obj){
        try {
            if($this->validateObjectOnInsert($obj)==false) return false;
			if($this->UserInformation() != null){
				$userInfo = new UserInfo();
				$userInfo->setId($this->UserInformation()->getId());
				$userInfo->setIsLoaded(true);
				$obj->setCreatedBy($userInfo);
			}
			$obj->setCreatedDate(new DateTime());
			
			$this->mDao->BeginTransaction();
                $obj = $this->mDao->insertObject($obj);
                if ($obj==null) return null;
                if($obj->getPrivilegeInfo()!=null){
                    $privilegeInfoDao = new PrivilegeInfoDao();
                    foreach($obj->getPrivilegeInfo() as $item){
                        $Group = new UserGroup();
                        $Group->setId($obj->getId());
                        $Group->IsLoaded(true);
                        $item->setUserGroup($Group);
                        
                        if($privilegeInfoDao->insertObject($item)==null) return null;
                    }
                }
            $this->mDao->CommitTransaction();
            
            return $obj;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }

    public function updateUserGroup($obj,$oldId){
        try {
			if($this->validateObjectOnUpdate($obj)==false) return false;
			$oldObj = $this->getUserGroup($oldId);
			if($oldObj!=null){
				$obj->setCreatedBy($oldObj->getCreatedBy());
				$obj->setCreatedDate($oldObj->getCreatedDate());
			}
			if($this->UserInformation() != null){
				$userInfo = new UserInfo();
				$userInfo->setId($this->UserInformation()->getId());
				$userInfo->setIsLoaded(true);
				$obj->setUpdatedBy($userInfo);
			}
			$obj->setUpdatedDate(new DateTime());
            $this->mDao->BeginTransaction();
                if ($this->smartUpdate($obj,$oldId)==false) return null;
            $this->mDao->CommitTransaction();

            return true;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }

    public function getUserGroups($filter=null) {
        try {
            return $this->mDao->getList($filter);
        } catch (Exception $exc) {
            PageData::addModelError($exc->getMessage());
            throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }
    
    public function getUserGroup($id){
        try {
            return $this->mDao->getObject($id);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }

    public function deleteUserGroup($id){
        try {
            $userGroup = $this->mDao->getObject($id);
            $this->mDao->BeginTransaction();
                if($userGroup!=null){               
                    $privilegeInfoDao = new PrivilegeInfoDao();
                    if($userGroup->getPrivilegeInfo()!=null){
                        foreach ($userGroup->getPrivilegeInfo() as $item){
                            if($privilegeInfoDao->deleteObject($item->getId())==false) return false;
                        }
                    }
                    if($this->mDao->deleteObject($userGroup->getId())==false) return false;
                }
                
            $this->mDao->CommitTransaction();
            return true;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }
    
    public function smartUpdate($obj,$oldId){
		if($this->mDao->updateObject($obj,$oldId)==false) return false;
        
        $userInfoService = new UserInfoService();
        if($userInfoService->updateUserGroupId($obj->getId(), $oldId)==false) return false;
        
        $privilegeInfoService = new PrivilegeInfoService();
        $privilegeInfoFilter = new PrivilegeInfoFilter();
        $privilegeInfoFilter->setUserGroupId($obj->getId());
        $privilegeInfos = $privilegeInfoService->getPrivilegeInfos($privilegeInfoFilter);

        if($obj->getPrivilegeInfo()!=null){
            $privilegeInfoDao = new PrivilegeInfoDao();
            foreach($obj->getPrivilegeInfo() as $item){
                $Group = new UserGroup();
                $Group->setId($obj->getId());
                $Group->IsLoaded(true);
                $item->setUserGroup($Group);
				
                if(($item->getId() == null) || ($item->getId() == '')){
                    if($privilegeInfoDao->insertObject($item)==null) {
                        throw new Exception('Gagal menambahkan privilege info.');
                    }
                } else {
                    if(!$privilegeInfoDao->updateObject($item,$item->getId())) {
                        throw new Exception('Gagal mengupdate privilege info.');
                    }
                    if (($privilegeInfos != null) && (count($privilegeInfos) > 0)) {
                        $index_remove = -1;

                        foreach($privilegeInfos as $index=>$privilegeinfo) {
                            if ($privilegeinfo->getId() == $item->getId()) {
                                $index_remove = $index;
                                break;
                            }
                        }
                        if ($index_remove > -1) {
                            unset($privilegeInfos[$index_remove]);
                        }
                    }
                }
            }
		   
            if (($privilegeInfos != null) && (count($privilegeInfos) > 0)) {
                foreach($privilegeInfos as $item) {
                    if (!$privilegeInfoDao->deleteObject($item->getId())) {
                        throw new Exception('Gagal menghapus unused privilege info.');
                    }
                }
            }
        }
        
        return $obj;
        
    }
    
    private function validateBase($obj) {
		$oldId = $obj->getId();
		$newId = str_replace(" ", "_", strtolower($obj->getName()));
		if($obj->getId() != $newId){
            $obj->setId($newId);
			$userGroupFilter = new UserGroupFilter();
			$userGroupFilter->setId($obj->getId());
			if($this->mDao->getList($userGroupFilter)!=null){
				$this->addError("Entry with Same Name is Existed.");
			}
		}
		
        return $this->getServiceState();
    }
    
    private function validateObjectOnInsert($model) {
        $this->validateBase($model);
        
		return $this->getServiceState();
    }
    
    private function validateObjectOnUpdate($model) {
        $this->validateBase($model);
        
		return $this->getServiceState();
    }
    
}
?>