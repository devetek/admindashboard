<?php
class FunctionInfoService extends DevetekService {
    
	function __construct(array $attributes = array()) {
        parent::__construct($attributes);
		$this->mDao = new FunctionInfoDao();
    }

    public function insertFunctionInfo($obj){
        try {
			if($this->validateObjectOnInsert($obj)==false) return false;
			if($this->UserInformation() != null){
				$userInfo = new UserInfo();
				$userInfo->setId($this->UserInformation()->getId());
				$userInfo->setIsLoaded(true);
				$obj->setCreatedBy($userInfo);
			}
			$obj->setCreatedDate(new DateTime());
			$this->mDao->BeginTransaction();
				$obj = $this->mDao->insertObject($obj);
			$this->mDao->CommitTransaction();
            return $obj;

        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ .":".__FUNCTION__."]:".$exc->getMessage());
        }
    }

    public function updateFunctionInfo($obj,$oldId){
        try {
			if($this->validateObjectOnUpdate($obj)==false) return false;
			$oldObj = $this->getFunctionInfo($oldId);
			if($oldObj!=null){
				$obj->setCreatedBy($oldObj->getCreatedBy());
				$obj->setCreatedDate($oldObj->getCreatedDate());
			}
			if($this->UserInformation() != null){
				$userInfo = new UserInfo();
				$userInfo->setId($this->UserInformation()->getId());
				$userInfo->setIsLoaded(true);
				$obj->setUpdatedBy($userInfo);
			}
			$obj->setUpdatedDate(new DateTime());
            $this->mDao->BeginTransaction();
                if ($this->smartUpdate($obj,$oldId)==false) return null;
            $this->mDao->CommitTransaction();

            return true;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ .":".__FUNCTION__."]:".$exc->getMessage());
        }
    }

    public function getFunctionInfos($filter=null) {
        try {
            return $this->mDao->getList($filter);
        } catch (Exception $exc) {
            PageData::addModelError($exc->getMessage());
            throw new Exception($exc->getMessage());
        }
    }
    
    public function getFunctionInfo($id){
        try {
            return $this->mDao->getObject($id);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ .":".__FUNCTION__."]:".$exc->getMessage());
        }
    }

    public function deleteFunctionInfo($id){
        try {
            $functionInfo = $this->mDao->getObject($id);
            $this->mDao->BeginTransaction();
                if($functionInfo!=null){               
                    if($this->mDao->deleteObject($functionInfo->getId())==false) return false;
                }
            $this->mDao->CommitTransaction();
            return true;
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ .":".__FUNCTION__."]:".$exc->getMessage());
        }
    }
    
    public function smartUpdate($obj,$oldId){
       $oldObj = $this->getFunctionInfo($oldId);
		if($oldObj!=null){
			$obj->setCreatedBy($oldObj->getCreatedBy());
			$obj->setCreatedDate($oldObj->getCreatedDate());
		}
		if($this->UserInformation() != null){
			$userInfo = new UserInfo();
			$userInfo->setId($this->UserInformation()->getId());
			$userInfo->setIsLoaded(true);
			$obj->setUpdatedBy($userInfo);
		}
		$obj->setUpdatedDate(new DateTime());
		if($this->mDao->updateObject($obj,$oldId)==false) return false;
		
		$privilegeInfoDao = new PrivilegeInfoDao();
		if($privilegeInfoDao->updateModuleIdFunction($obj->getModule()->getId(), $oldId)==false) return false;

		if($privilegeInfoDao->updateFunctionInfoId($obj->getId(), $oldId)==false) return false;
		
		return true;
    }
    
    private function validateBase($obj) {
		$oldId = $obj->getId();
		$newId = str_replace(" ", "_", strtolower($obj->getName()));
		if($obj->getId() != $newId){
            $obj->setId($newId);
			$functionInfoFilter = new FunctionInfoFilter();
			$functionInfoFilter->setId($obj->getId());
			if($this->getFunctionInfos($functionInfoFilter)!=null){
				$this->addError("Entry with Same Name is Existed.");
			}
		}
		
        return $this->getServiceState();
    }
    
    private function validateObjectOnInsert($model) {
        $this->validateBase($model);
        
		return $this->getServiceState();
    }
    
    private function validateObjectOnUpdate($model) {
        $this->validateBase($model);
        
		return $this->getServiceState();
    }
    
}
?>