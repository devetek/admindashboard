<?php
class SDKService extends DevetekService {
    
	function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }

	public function 
	
    public function getKapals($filter=null) {
        try {
            return $this->mDao->getList($filter);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }
    
    public function getKapal($id){
        try {
            return $this->mDao->getObject($id);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }

}
?>