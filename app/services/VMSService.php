<?php
class VMSService extends DevetekService {
    
	function __construct(array $attributes = array()) {
        parent::__construct($attributes);
		$this->mDao = new VMSDao();
    }

	// PARAM
	// $aktif : 1 (aktif), 2 (nonaktif) 3 (akan habis), 0 (ALL)
	public function getJumlahSKAT($status = 0){
		try {
            return $this->mDao->getCountSKAT($status);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
	}

	// PARAM
	// $aktif : 1 (aktif), 2 (nonaktif), 0 (ALL)
	public function getJumlahKapalTerpantau($status = 0){
		try {
			switch($status){
				case 0:
					return 6283;
					break;
				case 1:
					return 6023;
					break;
				case 2:
					return 250;
					break;
			}
			return 0;
            return $this->mDao->getList($filter);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
	}
	
	public function getListIndikasi(){
		try {
			return Dummy::ListIndikasi();
            return $this->mDao->getList($filter);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
	}
	
	public function getDataChartPelanggaran($startDate,$endDate,$periode){
		try {
			return Dummy::dataPelanggaran();
            return $this->mDao->getList($filter);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
	}
	
	public function getDataChartKomposisi($type){
		try {
			return Dummy::dataKomposisi();
            return $this->mDao->getList($filter);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
	}
	
    public function getKapals($filter=null) {
        try {

            return $this->mDao->getList($filter);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }
    
    public function getKapal($id){
        try {
            return $this->mDao->getObject($id);
        } catch (Exception $exc) {
			$this->addError($exc->getMessage());
			throw new Exception("[".__CLASS__ ."-".__FUNCTION__.":".$exc->getMessage()."]");
        }
    }

}
?>