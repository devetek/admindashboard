<?php

use yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class DevetekDao extends Eloquent{

	public $useWritePdo = false;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table;
	protected $debug = false;
	
	/**
	 * The database table primary key field.
	 *
	 * @var string
	 */
    protected $primary_key = "id";
    
	/**
	 * The database table incrementing setting.
	 *
	 * @var string
	 */
    public $incrementing=true;

	/**
	 * The database table editable column.
	 *
	 * @var string
	 */
    protected $fillable;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden;
    
    protected $transactionCounter = 0; 

    protected $sequence = null;
	
	protected $connectionName = 'oracle';
	protected $sequenceName = '';
	
    function __construct() {
        parent::__construct();
		$this->sequenceName = $this->table."_SEQ";
    }
	
	public function InsertObject($object) {
		if($this->debug){
			D::p($object->toArray());
		}
		
		if($this->incrementing){
			$seq = DB::connection($this->connectionName)->getSequence();
			$object->setId($seq->nextValue($this->sequenceName));
		}

		if (is_null($object) || empty($object)) { return null; }
		if(false){
			DB::table($this->table)->insert($object->toArray());
			$result = DB::connection($this->connectionName)->getPdo()->lastInsertId();
		} else {
			$result = DB::connection($this->connectionName)->table($this->table)->insertGetId($object->toArray());
		}
		if (is_null($result) || empty($result)) { return null; }
		$object->setId($result);
				
		return $object;
    }
	
    public function UpdateObject($object, $old_id) {
		if($this->debug){
			D::p(DB::connection($this->connectionName)->table($this->table)->where($this->primary_key, $old_id)->toSql() . $old_id);
			D::p($object->toArray());
		}
		
        if (is_null($object) || empty($object)) { return false; }
        $result = DB::connection($this->connectionName)->table($this->table)->where($this->primary_key, $old_id)->update($object->toArray());
		if ($result < 0) { return false; }
		
        return true;
    }
    
    public function GetObject($id) {
		if (is_null($id) || empty($id)) { return null; }
        $result = DB::connection($this->connectionName)->table($this->table)->where($this->primary_key, $id)->get();
        if (is_null($result) || empty($result) || count($result) <= 0) { return null; }
        $result = get_object_vars($result[0]);
        return $this->toObject($result);
    }

    
    public function GetList($criteria=null) {
	
		//D::p($this->GetQuery($criteria));
	
        $result = null;
        if (is_null($criteria)) {
            $result = DB::connection($this->connectionName)->table($this->table)->get();
        } else {
			$whereQuery = $criteria->getWhereQuery();
            if (!empty($whereQuery)) {
				$result = DB::connection($this->connectionName)->table($this->table)->whereRaw($whereQuery)->get();
            } else {
                $result = DB::connection($this->connectionName)->table($this->table)->get();
            }
        }	
		
        $obj_arr = array();
        if (!is_null($result)) {
            $list_arr = array();
            if (is_array($result)) {
                $list_arr = $result;
            } else {
                $list_arr = $result->toArray();
            }

            foreach ($list_arr as $item) {
                if(!is_array($item)) { $item = get_object_vars($item); }
                $obj_arr[] = $this->toObject($item);
            }
        }
        return $obj_arr;
    }
    
    public function DeleteObject($id) {
		if (is_null($id) || empty($id)) { return false; }
        DB::connection($this->connectionName)->table($this->table)->where($this->primary_key, $id)->delete();
        return true;
	}
	
    public function GetListCount($criteria=null) {
        $count = 0;
        if (is_null($criteria)) {
            $count = DB::connection($this->connectionName)->table($this->table)->count();
        } else {
            $count = DB::connection($this->connectionName)->table($this->table)->whereRaw($criteria->getWhereQuery())->count();
        }
        return $count;
    }
	
	public function GetCriteriaQuery($criteria=null){
		$result = null;
        if (is_null($criteria)) {
            $result = DB::connection($this->connectionName)->table($this->table)->get();
        } else {
			$whereQuery = $criteria->getWhereQuery();
            if (!empty($whereQuery)) {
				echo DB::connection($this->connectionName)->table($this->table)->whereRaw($whereQuery)->toSql(); die();
            } else {
                $result = DB::table($this->table)->toSql(); die();
            }
        }
	}
	
    public function GetDB(){
        return DB::connection($this->connectionName)->table($this->table);
    }

    public function ReadRawQuery($select_query=""){
        DB::connection($this->connectionName)->select($select_query);        
    }

    public function UpdateRawQuery($update_query=""){
        DB::connection($this->connectionName)->update($update_query);        
    }

    public function DeleteRawQuery($delete_query=""){
        DB::connection($this->connectionName)->delete($delete_query);        
    }

    public function BeginTransaction() {
        if(!$this->transactionCounter++) 
            return DB::connection($this->connectionName)->getPdo()->BeginTransaction();
        return $this->transactionCounter >= 0;
    }
    
    public function CommitTransaction() {
       if(!--$this->transactionCounter) 
            return DB::connection($this->connectionName)->getPdo()->commit();
       return $this->transactionCounter >= 0; 
    }

    public function RollbackTransaction() {
        if($this->transactionCounter >= 0) 
        { 
            $this->transactionCounter = 0; 
            return DB::connection($this->connectionName)->getPdo()->rollBack();
        } 
        $this->transactionCounter = 0; 
        return false; 
    }
    
    protected function AddError($message) {
        $this->error = $message;
    }
    
    public function GetError() {
        return $this->error;
    }

    protected function toObject($rowdata=null) {
        throw new Exception('There is no implementation of toObject() method in current dao instance!'); 
    }
	
}
