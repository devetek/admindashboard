<?php

class DevetekService {

    private $arr_error = null;
    protected $mDao = null;
    protected $mUserInfo = null;  
    
    function __construct(){
    }
    
    protected function AddError($message) {
		if ($message == null) return;

		if(is_array($message)){
			$this->arr_error = array_merge($this->arr_error,$message);
		}else{
			$this->arr_error[] = $message;
		}
    }
    
    public function GetCriteriaQuery($criteria=null) {
        return $this->mDao->GetCriteriaQuery($criteria);
    }
	
    public function GetErrors() {
        return $this->arr_error;
    }
	
	public function GetServiceState(){
		return !is_array($this->arr_error);
	}
	
    protected function UserInformation(){
        return $this->mUserInfo;
    }
    
    public function SetUserInformation($value){
        $this->mUserInfo = $value;
    }
    
}
