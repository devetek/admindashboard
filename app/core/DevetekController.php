<?php

class DevetekController extends Controller {
    
	/*
		Variable Declaration
	*/
	
	protected $mUserInfo;		// menampung data userinfo
	protected $mFunctionInfoId; // menyimpan id dari functionInfo id
	protected $mActiveFunctionInfo;	// menampung data functionInfo
	protected $mModelState;		// menampung data error
	private $privilegeInfoList;
	
	/*
		Constructor
	*/
	
	function __construct(){
		date_default_timezone_set("Asia/Jakarta");
		$this->LoadLoginInfo();
		return;
	}
	
	/*
		Privilege Method
	*/
	
	function UserInformation(){
		return $this->mUserInfo;
	}
	
	protected function setFunctionId($value){
        $this->mFunctionInfoId = $value;
		return;
    }
	
	protected function IsLogin(){
        return Auth::check();
    }
	
	private function LoadLoginInfo(){
		if ($this->isLogin()) {
			$UserInfoService = new UserInfoService();
			$AuthUser = Auth::user();
			$this->mUserInfo = $UserInfoService->getUserInfo($AuthUser->id);
			if (!Session::has('userInfo')){
				Session::put('privilegeinfo',unserialize(serialize($this->loadPrivilegeInfoList())));
				Session::put('functioninfo',unserialize(serialize($this->loadFunctionInfoList())));
			}
			View::share('userInfo', $this->mUserInfo);
			View::share('functioninfo', Session::get('functioninfo'));
        } else {
            View::share('userInfo', new UserInfo());
			View::share('functioninfo', null);
        }
		
		//D::p(Session::get('functioninfo'),true);
		return;
    }
	
	private function LoadPrivilegeInfoList(){
		$privilegeInfoService = new PrivilegeInfoService();
		$privilegeInfoFilter = new PrivilegeInfoFilter();
		$userGroupId = "-";
		if($this->mUserInfo != null){
			$userGroupId = $this->mUserInfo->getUserGroup()!=NULL ? $this->mUserInfo->getUserGroup()->getId() : "";
		}
		$privilegeInfoFilter->setUserGroupId($userGroupId);
        $this->privilegeInfoList = $privilegeInfoService->getPrivilegeInfos($privilegeInfoFilter);
		return $this->privilegeInfoList;
    }
	
    private function LoadFunctionInfoList(){
		$privilegeInfoList = Session::get('privilegeinfo');
		$privilegeInfoData = new ArrayObject(array(), ArrayObject::STD_PROP_LIST);
		$privilegeInfoData['module'] = array();
		if($privilegeInfoList!=null){
            foreach($privilegeInfoList as $item){
				$module = $item->getModule();
                $functions = $item->getFunctionInfo();
                if($module!=null && $functions!=null){
                    if($module!=null && $functions->IsShow()!=false && $functions->IsEnabled()!=false){
						$functions->setIsLoaded = true;
                        if(!isset($privilegeInfoData[$module->getId()])) {
                            $privilegeInfoData['module'][] = $module;
                            $privilegeInfoData[$module->getId()] = array();
                        }
						$privilegeInfoData[$module->getId()][] = unserialize(serialize($functions));
                    }
                }
            }
			usort($privilegeInfoData['module'], function($a, $b)
			{
				return strcmp($a->getSequence(), $b->getSequence());
			});
			foreach($privilegeInfoData['module'] as $module){
				//D::p($privilegeInfoData[$module->getId()],true);
				usort($privilegeInfoData[$module->getId()], function($a, $b)
				{
					return strcmp($a->getSequence(), $b->getSequence());
				});
			}
		}
		return $privilegeInfoData;
    }
	
	protected function isAllowCreate(){
		return $this->checkPrivilege('C');
    }

	protected function isAllowRead(){
		return $this->checkPrivilege('R');
    }

	protected function isAllowUpdate(){
		return $this->checkPrivilege('U');
    }

	protected function isAllowDelete(){
		return $this->checkPrivilege('D');
    }

	private function checkPrivilege($privilege){
		$hasAccess = false;
		$privilegeInfo = null;
		if($this->mUserInfo!=NULL){
			$privilegeInfoList = Session::get('privilegeinfo');
			if($privilegeInfoList!=null){
                foreach($privilegeInfoList as $item){
					if($item->getFunctionInfo() != null && $item->getFunctionInfo()->getId() == $this->mFunctionInfoId){
						$privilegeInfo = $item;
					}
                }
            }
			if($privilegeInfo!=null){
				switch($privilege){
					case 'C':
						$hasAccess = $privilegeInfo->IsAllowCreate();
						break;
					case 'R':
						$hasAccess = $privilegeInfo->IsAllowRead();
						break;
					case 'U':
						$hasAccess = $privilegeInfo->IsAllowUpdate();
						break;
					case 'D':
						$hasAccess = $privilegeInfo->IsAllowDelete();
						break;
				}
				$this->mActiveFunctionInfo = $privilegeInfo->getFunctionInfo();
			}
		}
		
		return $hasAccess;
	}
	
	/*
		Devetek Web Apps Method
	*/
	
	private function LoadWebData(){
		View::share('DevData', include('DevetekConfig.php')); // Nanti Confignya bakal dimasukin kedalam database CONFIGLOOKUP
		View::share('DevMessage', $this->loadMessage()); // Untuk Jika ada error messages
		View::share('ActiveFunction', $this->mActiveFunctionInfo); 
		return;
	}
	
	private function LoadMessage(){
		$messages = "";
		if($this->mModelState!=null&&count($this->mModelState)>0){
		$messages = "<div class='card'>" .
            "<div class='card-header'>" .
                "<h2>Messages <small>Alert Messages</small></h2>" .
            "</div>" .
            "<div class='card-body card-padding'>" .
                "<div class='alert alert-danger' role='alert'>";

		foreach($this->mModelState as $item){
			$messages .= "<li>".$item."</li> ";
		}
		
		$messages .=  "</div>".
        		    "</div>" .
		        "</div>";
		}

		return $messages;
	}
	
    public static function GetModelState(){
        return is_array($this->mModelState);
    }
	
	protected function AddModelError($errors) {
        if ($errors == null) return;
		if (!is_array($this->mModelState)) {
            $this->mModelState = array();
        }
		
		if (is_array($errors)) {
            $this->mModelState = array_merge($this->mModelState, $errors);
        } else {
            $this->mModelState[] = $errors;
        }
		return;
    }
    
    protected function AssignValidatorToModelState($validator){
        $this->addModelError($validator->messages()->all());
		return;
    }
	
	protected function AssignToTempData(){
		if (!Session::has('users'))
		{
			Session::put('ModelState', '');
		}
		Session::push('ModelState', $this->mModelState );
		return;
	}
	
	protected function RestoreTempData(){
		$this->addModelError(Session::pull('ModelState', null)[0]);
		View::share('DevMessage', $this->loadMessage());
		return;
	}
	
	public function MissingMethod($parameters = array())
	{
		return View::make('hello');
	}
	
	protected function makeView($url,$data){
		$this->LoadWebData();
		if (is_null($this->layout))
		{
			$this->layout = View::make($url, $data);
		}
	}
	
}
        
