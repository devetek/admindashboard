<?php

class LookupDetail {

    private $mId;
    private $mName;
    private $mValue;
    private $mLookup;
    private $mIsLoaded;
    
    public function getId(){
        return $this->mId;
    }
    
    public function setId($value){
        $this->mId = $value;
    }
    
    public function getName(){
        return $this->mName;
    }

    public function setName($value){
        $this->mName = $value;
    }    
    
    public function getValue(){
        return $this->mValue;
    }

    public function setValue($value){
        $this->mValue = $value;
    }    
    
    public function IsEditable(){
        return $this->mIsEditable;
    }

    public function setIsEditable($value){
        $this->mIsEditable = $value;
    }    

    public function getLookup(){
        if (($this->mLookup != null) && (!$this->mLookup->IsLoaded())) {
            $LookupDao = new LookupDao();
            $this->mLookup = $LookupDao->getObject($this->mLookup->getId());
            if ($this->mLookup != null) $this->mLookup->setIsLoaded(true);
            }
        return $this->mLookup;
    }

    public function setLookup($value){
        $this->mLookup = $value;
    }    
	
    public function IsLoaded() {
        return $this->mIsLoaded;
    }

    public function setIsLoaded($value){
        $this->mIsLoaded = $value;
    }

    public function toArray() {
        $data = array(
                'id' => $this->mId,
                'name' => $this->mName,
                'value' => $this->mValue,
                'is_editable' => $this->mIsEditable,
                'lookup_id' => (!is_null($this->mLookup) ? $this->mLookup->getId() : null),
        );
        
        return $data;
    }
    
}
