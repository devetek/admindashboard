<?php

class UserInfo {

    private $mId;
    private $mUsername;
    private $mEmail;
    private $mName;
    private $mPassword;
    private $mUserGroup;
    private $mIsEnabled;
    private $mLastLogin;
    private $mCreatedDate;
    private $mCreatedBy;
    private $mUpdatedDate;
    private $mUpdatedBy;
    private $mIsLoaded;

    public function setId($value){
        $this->mId = $value;
    }
    
    public function getId(){
        return $this->mId;
    }

    public function setUsername($value){
        $this->mUsername = $value;
    }

    public function getUsername(){
        return $this->mUsername;
    }
    
    public function setEmail($value){
        $this->mEmail = $value;
    }
    
    public function getEmail(){
        return $this->mEmail;
    }
    
    public function setName($value){
        $this->mName = $value;
    }
    
    public function getName(){
        return $this->mName;
    }
    
    public function setHashedPassword($value){
        $this->mPassword = $value;
    }
    public function setPassword($value){
        $this->mPassword = Hash::make($value);
    }
    
    public function getPassword(){
        return $this->mPassword;
    }

    public function setGroup($value){
        $this->mUserGroup = $value;
    }

    public function getUserGroup(){
         if (($this->mUserGroup != null) && (!$this->mUserGroup->IsLoaded())) {
            $GroupDao = new UserGroupDao();
            $this->mUserGroup = $GroupDao->getObject($this->mUserGroup->getId());
            if ($this->mUserGroup != null) $this->mUserGroup->setIsLoaded(true);
            }
        return $this->mUserGroup;
    }

    public function setIsEnabled($value){
        $this->mIsEnabled = $value;
    }
    
    public function IsEnabled(){
        return $this->mIsEnabled;
    }
      
    public function setLastLogin($value){
        $this->mLastLogin = $value;
    }
    
    public function getLastLogin(){
        return $this->mLastLogin;
    }

    public function setCreatedDate($value){
        $this->mCreatedDate = $value;
    }
    
    public function getCreatedDate(){
        return $this->mCreatedDate;
    }

    public function setCreatedBy($value){
        $this->mCreatedBy = $value;
    }
    
    public function getCreatedBy(){
        return $this->mCreatedBy;
    }
    
	public function setUpdatedDate($value){
        $this->mUpdatedDate = $value;
    }
    
    public function getUpdatedDate(){
        return $this->mUpdatedDate;
    }

    public function setUpdatedBy($value){
        $this->mUpdatedBy = $value;
    }
    
    public function getUpdatedBy(){
        return $this->mUpdatedBy;
    }
	
    public function setIsLoaded($value){
        $this->mIsLoaded = $value;
    }
    
    public function IsLoaded(){
        return $this->mIsLoaded;
    }
    
    public function toArray() {
        $data = array(
			'id' => $this->mId,
			 'username' => $this->mUsername,
			 'name' => $this->mName,
			 'email' => $this->mEmail,
			 'user_group_id' => (!is_null($this->mUserGroup) ? $this->mUserGroup->getId() : ""),
			 'is_enabled' => $this->mIsEnabled,
			 'created_at' => $this->mCreatedDate,
			 'created_by' => (!is_null($this->mCreatedBy) ? $this->mCreatedBy->getId() : ""),
			 'updated_at' => $this->mUpdatedDate,
			 'updated_by' => (!is_null($this->mUpdatedBy) ? $this->mUpdatedBy->getId() : "")
        );

		if(!is_null($this->mPassword) && !empty($this->mPassword)){
			$data['password'] = $this->mPassword;
		}
		
        return $data;
    }
}
