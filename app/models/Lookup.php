<?php

class Lookup {

    private $mId;
    private $mName;
    private $mIsLoaded;
    private $mLookupDetail;
	
    public function getId(){
        return $this->mId;
    }
    
    public function setId($value){
        $this->mId = $value;
    }
    
    public function getName(){
        return $this->mName;
    }

    public function setName($value){
        $this->mName = $value;
    }    

    public function IsLoaded() {
        return $this->mIsLoaded;
    }

    public function setIsLoaded($value){
        $this->mIsLoaded = $value;
    }

    public function setLookupDetail($value){
        $this->mLookupDetail = $value;
    }
    
    public function getLookupDetail(){
        if (($this->mLookupDetail == null)) {
            $LookupDetailDao = new LookupDetailDao();
			$LookupDetailFilter = new LookupDetailFilter();
			$LookupDetailFilter->setLookupId($this->mId);
            $this->mLookupDetail = $LookupDetailDao->getList($LookupDetailFilter);
            }
        return $this->mLookupDetail;
    }
	
    public function toArray() {
        $data = array(
                'id' => $this->mId,
                'name' => $this->mName,
        );
        
        return $data;
    }
    
}
