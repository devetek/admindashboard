<?php

class Module {

    private $mId;
    private $mName;
    private $mSequence;
    private $mIcon;
    private $mIsEnabled;
    private $mIsShow;
    private $mParentModule;
    private $mCreatedBy;
    private $mCreatedAt;
    private $mUpdatedBy;
    private $mUpdatedAt;
    private $mIsLoaded;
    
    private $mFunctionList;

    public function getId() {
        return $this->mId;
    }

    public function setId($value) {
        $this->mId = $value;
    }

    public function getName() {
        return $this->mName;
    }

    public function setName($value) {
        $this->mName = $value;
    }

    public function getSequence() {
        return $this->mSequence;
    }

    public function setSequence($value) {
        $this->mSequence = $value;
    }

    public function getIcon() {
        return $this->mIcon;
    }

    public function setIcon($value) {
        $this->mIcon = $value;
    }

    public function IsEnabled() {
        return $this->mIsEnabled;
    }

    public function setIsEnabled($value) {
        $this->mIsEnabled = $value;
    }

    public function IsShow() {
        return $this->mIsShow;
    }

    public function setIsShow($value) {
        $this->mIsShow = $value;
    }
    
    public function getParentModule() {
        if(($this->mParentModule != null)&&(!$this->mParentModule->IsLoaded())){
            $ModuleDao = new ModuleDao();
            $this->mParentModule = $ModuleDao->getObject($this->mParentModule->getId());
            if ($this->mParentModule != null) $this->mParentModule->setIsLoaded(true);
        }
        return $this->mParentModule;
    }

    public function setParentModule($value) {
        $this->mParentModule = $value;
    }

	public function getCreatedBy() {
        if(($this->mCreatedBy != null)&&(!$this->mCreatedBy->IsLoaded())){
            $userInfoDao = new UserInfoDao();
            $this->mCreatedBy = $userInfoDao->getObject($this->mCreatedBy->getId());
            if ($this->mCreatedBy != null) $this->mCreatedBy->setIsLoaded(true);
        }
        return $this->mCreatedBy;
    }

    public function setCreatedBy($value) {
        $this->mCreatedBy = $value;
    }
    
    public function getCreatedDate() {
        return $this->mCreatedAt;
    }

    public function setCreatedDate($value) {
        $this->mCreatedAt = $value;
    }
	
    public function getUpdatedBy() {
        if(($this->mUpdatedBy != null)&&(!$this->mUpdatedBy->IsLoaded())){
            $userInfoDao = new UserInfoDao();
            $this->mUpdatedBy = $userInfoDao->getObject($this->mUpdatedBy->getId());
            if ($this->mUpdatedBy != null) $this->mUpdatedBy->setIsLoaded(true);
        }
        return $this->mUpdatedBy;
    }

    public function setUpdatedBy($value) {
        $this->mUpdatedBy = $value;
    }

    public function getUpdatedDate() {
        return $this->mUpdatedAt;
    }
    
    public function setUpdatedDate($value) {
        $this->mUpdatedAt = $value;
    }

    public function IsLoaded(){
        return $this->mIsLoaded;
    }
    
    public function setIsLoaded($value) {
        $this->mIsLoaded = $value;
    }
    
    public function getFunctionInfos(){
        if (($this->mFunctionList == null) && ($this->mId!=null)) {
			$functionInfoDao = new FunctionInfoDao();
			$functionInfoFilter = new FunctionInfoFilter();
			$functionInfoFilter->setModuleId($this->mId);
			$this->mFunctionList = $functionInfoDao->getList($functionInfoFilter);
		}
        return $this->mFunctionList;
    }
    
    public function toArray(){
        $model = array(
            'id' => $this->mId,
             'name' => $this->mName,
            'icon' => $this->mIcon,
            'sequence' => $this->mSequence,
            'is_enabled' => $this->mIsEnabled,
            'is_show' => $this->mIsShow,
            'parent_module_id' => (!is_null($this->mParentModule) ? $this->mParentModule->getId() : null),
            "created_at" => $this->mCreatedAt,
            "created_by" => (!is_null($this->mCreatedBy) ? $this->mCreatedBy->getId() : null),
            "updated_at" => $this->mUpdatedAt,
            "updated_by" => (!is_null($this->mUpdatedBy) ? $this->mUpdatedBy->getId() : null),
        );
		
        return $model;
    }
    
}
