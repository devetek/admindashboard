<?php

class PrivilegeInfo{

    private $mId;
    private $mUserGroup;
    private $mModule;
    private $mFunctionInfoInfo;
    private $mIsAllowCreate;
    private $mIsAllowRead;
    private $mIsAllowUpdate;
    private $mIsAllowDelete;
    private $mIsLoaded;

    public function getId() {
        return $this->mId;
    }
    
    public function setId($value){
        $this->mId = $value;
    }
    
    public function getUserGroup(){
        if(($this->mUserGroup != null)&&(!$this->mUserGroup->isLoaded())){
            $groupDao = new UserGroupDao();
            $this->mUserGroup = $groupDao->getObject($this->mUserGroup->getId());
            if ($this->mUserGroup != null) $this->mUserGroup->setIsLoaded(true);
        }
        return $this->mUserGroup;
    }

    public function setUserGroup($value){
        $this->mUserGroup = $value;
    }
    
    public function getModule(){
        if(($this->mModule != null)&&(!$this->mModule->isLoaded())){
            $moduleDao = new ModuleDao();
            $this->mModule = $moduleDao->getObject($this->mModule->getId());
            if ($this->mModule != null) $this->mModule->setIsLoaded(true);
        }
        return $this->mModule;
    }
    
    public function setModule($value){
        $this->mModule = $value;
    }
    
    public function getFunctionInfo(){
		if(($this->mFunctionInfo != null)&&(!$this->mFunctionInfo->isLoaded())){
            $functionInfoDao = new FunctionInfoDao();
            $this->mFunctionInfo = $functionInfoDao->getObject($this->mFunctionInfo->getId());
            if ($this->mFunctionInfo != null) $this->mFunctionInfo->setIsLoaded(true);
        }
        return $this->mFunctionInfo;
    }
    
    public function setFunctionInfo($value){
        $this->mFunctionInfo = $value;
    }
    
    public function IsAllowCreate(){
        return $this->mIsAllowCreate;
    }
    
    public function setIsAllowCreate($value){
        $this->mIsAllowCreate = $value;
    }

    public function IsAllowRead(){
        return $this->mIsAllowRead;
    }
    
    public function setIsAllowRead($value){
        $this->mIsAllowRead = $value;
    }

    public function IsAllowUpdate(){
        return $this->mIsAllowUpdate;
    }
    
    public function setIsAllowUpdate($value){
        $this->mIsAllowUpdate = $value;
    }

    public function IsAllowDelete(){
        return $this->mIsAllowDelete;
    }
    
    public function setIsAllowDelete($value){
        $this->mIsAllowDelete = $value;
    }

    public function IsLoaded($param) {
        return $this->mIsLoaded;
    }
    
    public function setIsLoaded($value) {
        $this->mIsLoaded = $value;
    }
    
    public function toArray() {
        $data = array(
            'id' => $this->mId,
            'user_group_id' => (!is_null($this->mUserGroup) ? $this->mUserGroup->getId() : null),
            'module_id' => (!is_null($this->mModule) ? $this->mModule->getId() : null),
            'function_info_id' => (!is_null($this->mFunctionInfo) ? $this->mFunctionInfo->getId() : null),
            'is_allow_read' => $this->mIsAllowRead,
            'is_allow_create' => $this->mIsAllowCreate,
            'is_allow_update' => $this->mIsAllowUpdate,
            'is_allow_delete' => $this->mIsAllowDelete,
            "created_ats" => new DateTime(),
            "updated_ats" => new DateTime(),
        );
        
        return $data;
    }
    
}
