<?php

class FunctionInfo {

    private $mId;
    private $mName;
    private $mModule;
    private $mUrl;
    private $mIcon;
    private $mSequence;
    private $mIsEnabled;
    private $mIsShow;
    private $mCreatedBy;
    private $mCreatedAt;
    private $mUpdatedBy;
    private $mUpdatedAt;
    private $mIsLoaded;
    
    public function getId(){
        return $this->mId;
    }
    
    public function setId($value){
        $this->mId = $value;
    }
    
    public function getName(){
        return $this->mName;
    }

    public function setName($value){
        $this->mName = $value;
    }    

    public function getModule(){
        if (($this->mModule != null) && (!$this->mModule->IsLoaded())) {
            $ModuleDao = new ModuleDao();
            $this->mModule = $ModuleDao->getObject($this->mModule->getId());
            if ($this->mModule != null) $this->mModule->setIsLoaded(true);
            }
        return $this->mModule;
    }

    public function setModule($value){
        $this->mModule = $value;
    }    

    public function getUrl(){
        return $this->mUrl;
    }

    public function setUrl($value){
        $this->mUrl = $value;
    }
    
    public function getIcon(){
        return $this->mIcon;
    }

    public function setIcon($value){
        $this->mIcon = $value;
    }
    
    public function getSequence(){
        return $this->mSequence;
    }

    public function setSequence($value){
        $this->mSequence = $value;
    }
    
    public function IsEnabled(){
        return $this->mIsEnabled;
    }

    public function setIsEnabled($value){
        $this->mIsEnabled = $value;
    }

    public function IsShow(){
        return $this->mIsShow;
    }
    
    public function setIsShow($value){
        $this->mIsShow = $value;
    }
    
	public function getCreatedBy() {
        if(($this->mCreatedBy != null)&&(!$this->mCreatedBy->IsLoaded())){
            $userInfoDao = new UserInfoDao();
            $this->mCreatedBy = $userInfoDao->getObject($this->mCreatedBy->getId());
            if ($this->mCreatedBy != null) $this->mCreatedBy->setIsLoaded(true);
        }
        return $this->mCreatedBy;
    }

    public function setCreatedBy($value) {
        $this->mCreatedBy = $value;
    }
    
    public function getCreatedDate() {
        return $this->mCreatedAt;
    }

    public function setCreatedDate($value) {
        $this->mCreatedAt = $value;
    }
	
    public function getUpdatedBy() {
        if(($this->mUpdatedBy != null)&&(!$this->mUpdatedBy->IsLoaded())){
            $userDao = new UserDao();
            $this->mUpdatedBy = $userDao->getObject($this->mUpdatedBy->getId());
            if ($this->mUpdatedBy != null) $this->mUpdatedBy->setIsLoaded(true);
        }
        return $this->mUpdatedBy;
    }

    public function setUpdatedBy($value) {
        $this->mUpdatedBy = $value;
    }

    public function getUpdatedDate() {
        return $this->mUpdatedAt;
    }
    
    public function setUpdatedDate($value) {
        $this->mUpdatedAt = $value;
    }
	
    public function IsLoaded() {
        return $this->mIsLoaded;
    }

    public function setIsLoaded($value){
        $this->mIsLoaded = $value;
    }

    public function toArray() {
        $data = array(
                'id' => $this->mId,
                'name' => $this->mName,
                'module_id' => (!is_null($this->mModule) ? $this->mModule->getId() : null),
                'url' => $this->mUrl,
                'icon' => $this->mIcon,
                'sequence' => $this->mSequence,
                'is_enabled' => $this->mIsEnabled,
                'is_show' => $this->mIsShow,
				"created_at" => $this->mCreatedAt,
				"created_by" => (!is_null($this->mCreatedBy) ? $this->mCreatedBy->getId() : null),
				"updated_at" => $this->mUpdatedAt,
				"updated_by" => (!is_null($this->mUpdatedBy) ? $this->mUpdatedBy->getId() : null),
        );
        
        return $data;
    }
    
}
