<?php

class ModuleFilter {

    private $mId;
    private $mNotId;
	private $mIsEnabled;
	private $mIsShow;
	
    public function setId($value) {
        $this->mId = $value;
    }

    public function getId() {
        return $this->mId;
    }
    public function setNotId($value) {
        $this->mNotId = $value;
    }

    public function getNotId() {
        return $this->mNotId;
    }
    
    public function getWhereQuery() {
        $where = "";
        
        if (!is_null($this->mId) && !empty($this->mId)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " id = '".stripslashes($this->mId)."'";
        }
        
        if (!is_null($this->mNotId) && !empty($this->mNotId)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " id <> '".stripslashes($this->mNotId)."'";
        }
        
		if (!is_null($this->mIsEnabled) && !empty($this->mIsEnabled)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " is_enabled = ".stripslashes($this->mIsEnabled)."";
        }
		
		if (!is_null($this->mIsShow) && !empty($this->mIsShow)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " is_show = ".stripslashes($this->mIsShow)."";
        }
		
        return $where; 
    }
}