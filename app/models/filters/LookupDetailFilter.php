<?php

class LookupDetailFilter {

	private $mId;
	private $mNotId;
	private $LookupId;
	
	public function setId($value) {
        $this->mId = $value;
    }

    public function getId() {
        return $this->mId;
    }

	public function setNotId($value) {
        $this->mNotId = $value;
    }

    public function getNotId() {
        return $this->mNotId;
    }

	public function setLookupId($value) {
        $this->mLookupId = $value;
    }

    public function getLookupId() {
        return $this->mLookupId;
    }
    
    public function getWhereQuery() {
        $where = "";
        
		if (!is_null($this->mId) && !empty($this->mId)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " id = '".stripslashes($this->mId)."'";
        }
		
		if (!is_null($this->mNotId) && !empty($this->mNotId)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " NOT id = '".stripslashes($this->mNotId)."'";
        }
		
		if (!is_null($this->mLookupId) && !empty($this->mLookupId)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " lookup_id = '".stripslashes($this->mLookupId)."'";
        }
        
        return $where; 
    }
}