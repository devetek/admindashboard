<?php

class FunctionInfoFilter {

	private $mId;
	private $mModuleId;
	
	public function setId($value) {
        $this->mId = $value;
    }

    public function getId() {
        return $this->mId;
    }

	public function setModuleId($value) {
        $this->mModuleId = $value;
    }

    public function getModuleId() {
        return $this->mModuleId;
    }
    
    public function getWhereQuery() {
        $where = "";
        
		if (!is_null($this->mId) && !empty($this->mId)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " id = '".stripslashes($this->mId)."'";
        }
		
		if (!is_null($this->mModuleId) && !empty($this->mModuleId)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " module_id = '".stripslashes($this->mModuleId)."'";
        }
        
        return $where; 
    }
}