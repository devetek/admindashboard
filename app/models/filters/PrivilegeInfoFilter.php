<?php

class PrivilegeInfoFilter {

	private $mId;
	private $mUserGroupId;
	
	public function setId($value) {
        $this->mId = $value;
    }

    public function getId() {
        return $this->mId;
    }

	public function setUserGroupId($value) {
        $this->mUserGroupId = $value;
    }

    public function getUserGroupId() {
        return $this->mUserGroupId;
    }
    
    public function getWhereQuery() {
        $where = "";
        
		if (!is_null($this->mId) && !empty($this->mId)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " id = '".stripslashes($this->mId)."'";
        }
		
		if (!empty($where)) { $where .= " AND "; }
		$where .= " user_group_id = '".stripslashes($this->mUserGroupId)."'";
	
        return $where; 
    }
}