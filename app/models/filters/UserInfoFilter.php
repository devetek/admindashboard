<?php

class UserInfoFilter {

    private $mId;
	private $mNotId;
	private $mUsername;
	private $mEmail;
	
	public function setId($value) {
        $this->mId = $value;
    }

    public function getId() {
        return $this->mId;
    }

	public function setNotId($value) {
        $this->mNotId = $value;
    }

    public function getNotId() {
        return $this->mNotId;
    }

	public function setUsername($value) {
        $this->mUsername = $value;
    }

    public function getUsername() {
        return $this->mUsername;
    }

	public function setEmail($value) {
        $this->mEmail = $value;
    }

    public function getEmail() {
        return $this->mEmail;
    }
    
    public function getWhereQuery() {
        $where = "";
        
		if (!is_null($this->mId) && !empty($this->mId)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " id = '".stripslashes($this->mId)."'";
        }
		
		if (!is_null($this->mNotId) && !empty($this->mNotId)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " NOT id = '".stripslashes($this->mNotId)."'";
        }
		
		if (!is_null($this->mUsername) && !empty($this->mUsername)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " username = '".stripslashes($this->mUsername)."'";
        }
		
		if (!is_null($this->mEmail) && !empty($this->mEmail)) {
            if (!empty($where)) { $where .= " AND "; }
            $where .= " email = '".stripslashes($this->mEmail)."'";
        }
        
        return $where; 
    }
}
