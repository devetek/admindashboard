<?php


class UserGroup {

    private $mId;
    private $mName;
    private $mDescription;
    private $mIsEnabled;
    private $mCreatedBy;
    private $mCreatedAt;
    private $mUpdatedBy;
    private $mUpdatedAt;
	
    private $mIsLoaded;
    private $mPrivilegeInfo;
    
    public function setId($value){
        $this->mId = $value;
    }
    
    public function getId(){
        return $this->mId;
    }

    public function setName($value){
        $this->mName = $value;
    }
    
    public function getName(){
        return $this->mName;
    }
    
    public function setDescription($value){
        $this->mDescription = $value;
    }
    
    public function getDescription(){
        return $this->mDescription;
    }
    
    public function setIsEnabled($value){
        $this->mIsEnabled = $value;
    }
    
    public function IsEnabled(){
        return $this->mIsEnabled;
    }
    
    public function setPrivilegeInfo($value){
        $this->mPrivilegeInfo = $value;
    }
    
    public function getPrivilegeInfo(){
        if (($this->mPrivilegeInfo == null)) {
            $privilegeInfoDao = new PrivilegeInfoDao();
			$privilegeInfoFilter = new PrivilegeInfoFilter();
			$privilegeInfoFilter->setUserGroupId($this->mId);
            $this->mPrivilegeInfo = $privilegeInfoDao->getList($privilegeInfoFilter);
            }
        return $this->mPrivilegeInfo;
    }

	public function getCreatedBy() {
        if(($this->mCreatedBy != null)&&(!$this->mCreatedBy->IsLoaded())){
            $userInfoDao = new UserInfoDao();
            $this->mCreatedBy = $userInfoDao->getObject($this->mCreatedBy->getId());
            if ($this->mCreatedBy != null) $this->mCreatedBy->setIsLoaded(true);
        }
        return $this->mCreatedBy;
    }

    public function setCreatedBy($value) {
        $this->mCreatedBy = $value;
    }
    
    public function getCreatedDate() {
        return $this->mCreatedAt;
    }

    public function setCreatedDate($value) {
        $this->mCreatedAt = $value;
    }
	
    public function getUpdatedBy() {
        if(($this->mUpdatedBy != null)&&(!$this->mUpdatedBy->IsLoaded())){
            $userInfoDao = new UserInfoDao();
            $this->mUpdatedBy = $userInfoDao->getObject($this->mUpdatedBy->getId());
            if ($this->mUpdatedBy != null) $this->mUpdatedBy->setIsLoaded(true);
        }
        return $this->mUpdatedBy;
    }

    public function setUpdatedBy($value) {
        $this->mUpdatedBy = $value;
    }

    public function getUpdatedDate() {
        return $this->mUpdatedAt;
    }
    
    public function setUpdatedDate($value) {
        $this->mUpdatedAt = $value;
    }
    
    public function setIsLoaded($value){
        $this->mIsLoaded = $value;
    }
    
    public function IsLoaded(){
        return $this->mIsLoaded;
    }
    
	public function toArray() {
        return array(
			"id" => $this->mId,
            "name" => $this->mName,
            "description" => $this->mDescription,
            "is_enabled" => $this->mIsEnabled,
            "created_at" => $this->mCreatedAt,
            "created_by" => (!is_null($this->mCreatedBy) ? $this->mCreatedBy->getId() : null),
            "updated_at" => $this->mUpdatedAt,
            "updated_by" => (!is_null($this->mUpdatedBy) ? $this->mUpdatedBy->getId() : null),
        );
    }
}
