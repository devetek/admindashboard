<?php

class Dummy {

	public static function ListIndikasi(){
		return 	Array(
		(object) array
		(                      
			"nama"=>"MITRA NELAYAN",
			"gt"=>130,
			"lon"=>18.5294,
			"lat"=>118.2078,
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Pukat Ikan",
			"jenis_pelanggaran"=>"Pelanggaran Wilayah"
		  ),
		  (object) array (
			"nama"=>"MUTIARA MINA 1",
			"gt"=>115,
			"lon"=>-7.1722 ,
			"lat"=>112.7314,
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Pukat Ikan",
			"jenis_pelanggaran"=>"Pelanggaran Operasional"
		  ),
		  (object) array (
			"nama"=>"WOGEKEL - 11",
			"gt"=>195,
			"lon"=>-8.7431,
			"lat"=>115.2075,
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Pukat Ikan",
			"jenis_pelanggaran"=>"Pelanggaran Wilayah"
		  ),
		  (object) array (
			"nama"=>"OMBRE 62",
			"gt"=>305,
			"lon"=>1,
			"lon"=>140.309, 
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Pukat Ikan",
			"jenis_pelanggaran"=>"Pelanggaran Wilayah"
		  ),
		  (object) array (
			"nama"=>"KOFIAU 32",
			"gt"=>298,
			"lon"=>-9.564 ,
			"lat"=>130.309,
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Pukat Ikan",
			"jenis_pelanggaran"=>"Pelanggaran Operasional"
		  ),
		  (object) array (
			"nama"=>"DOFIOR 156",
			"gt"=>298,
			"lon"=>3.78 ,
			"lat"=>98.713,
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Pukat Ikan",
			"jenis_pelanggaran"=>"Pelanggaran Wilayah"
		  ),
		  (object) array (
			"nama"=>"DOFIOR 158",
			"gt"=>298,
			"lon"=>-6.1 ,
			"lat"=>106.804,
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Pukat Ikan",
			"jenis_pelanggaran"=>"Pelanggaran Wilayah"
		  ),
		  (object) array (
			"nama"=>"ASIA BAGUS - 28",
			"gt"=>127,
			"lon"=>1.4383,
			"lat"=>125.2136,
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Pancing Cumi (squid jigging)",
			"jenis_pelanggaran"=>"Pelanggaran Wilayah"
		  ),
		  (object) array (
			"nama"=>"SUMBER BARU -II",
			"gt"=>38,
			"lon"=>1.8506,
			"lat"=>104.64,
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Jaring insang oseanik",
			"jenis_pelanggaran"=>"Pelanggaran Wilayah"
		  ),
		  (object) array (
			"nama"=>"MITRAMAS 9",
			"gt"=>628,
			"lon"=>-8.7419 ,
			"lat"=>115.2075,
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Pengangkut",
			"jenis_pelanggaran"=>"Pelanggaran Wilayah"
		  ),
		  (object) array (
			"nama"=>"TRANS MITRAMAS 111",
			"gt"=>425,
			"lon"=> -4.7994 ,
			"lat"=> 136.6356,
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Pengangkut",
			"jenis_pelanggaran"=>"Pelanggaran Wilayah"
		  ),
		  (object) array (
			"nama"=>"BANDAR NELAYAN - 126",
			"gt"=>133,
			"lon"=>4.2072,
			"lat"=>123.6611,
			"jenis"=>"penangkap ikan",
			"alat_tangkap"=>"Rawai Tuna",
			"jenis_pelanggaran"=>"Pelanggaran Wilayah"
		  )
		);
	}
    
	public static function dataPelanggaran(){
		return Array(
		(object) array ("date"=> "2015-01","Wilayah"=> 2,"Operasional"=> 2),
		(object) array ("date"=> "2015-02","Wilayah"=> 5,"Operasional"=> 1),
		(object) array ("date"=> "2015-03","Wilayah"=> 4,"Operasional"=> 5),
		(object) array ("date"=> "2015-04","Wilayah"=> 2,"Operasional"=> 5),
		(object) array ("date"=> "2015-05","Wilayah"=> 1,"Operasional"=> 6),
		(object) array ("date"=> "2015-06","Wilayah"=> 7,"Operasional"=> 5),
		(object) array ("date"=> "2015-07","Wilayah"=> 5,"Operasional"=> 8),
		(object) array ("date"=> "2015-08","Wilayah"=> 2,"Operasional"=> 3),
		(object) array ("date"=> "2015-09","Wilayah"=> 6,"Operasional"=> 1),
		(object) array ("date"=> "2015-10","Wilayah"=> 2,"Operasional"=> 1));
	}
	
	public static function dataKomposisi(){
		return Array((object) array ("category"=>"Purse Seine (Pukat Cincin) Pelagis Kecil","column-1"=>"29.224157"),(object) array ("category"=>"Rawai Tuna","column-1"=>"14.612079"),(object) array ("category"=>"Pukat Ikan","column-1"=>"13.865641"),(object) array ("category"=>"Pengangkut","column-1"=>"12.621579"),(object) array ("category"=>"Bouke ami","column-1"=>"10.517982"),(object) array ("category"=>"Lainnya","column-1"=>"19.158562"));
	}
	
	public static function dataPP(){
		return Array((object) array ("date"=> "Jan","2015"=> 3,"2014"=> 1), (object) array ("date"=> "Feb","2015"=> 3,"2014"=> 2), (object) array ("date"=> "Mar","2015"=> 4,"2014"=> 2), (object) array ("date"=> "Apr","2015"=> 2,"2014"=> 2), (object) array ("date"=> "Mei","2015"=> 1,"2014"=> 3), (object) array ("date"=> "Jun","2015"=> 3,"2014"=> 2), (object) array ("date"=> "Jul","2015"=> 5,"2014"=> 1), (object) array ("date"=> "Agu","2015"=> 3,"2014"=> 1), (object) array ("date"=> "Sep","2015"=> 4,"2014"=> 1), (object) array ("date"=> "Okt","2015"=> 2,"2014"=> 1));
	}
	
}


