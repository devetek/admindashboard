<?php

	/*
	|--------------------------------------------------------------------------
	| Application Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register all of the routes for an application.
	| It's a breeze. Simply tell Laravel the URIs it should respond to
	| and give it the Closure to execute when that URI is requested.
	|
	*/

    Route::controller('apiv1', 'IpinCont');
    Route::controller('apisdk', 'IpinSdk');
    Route::controller('apikp', 'IpinKp');
    Route::controller('apisdp', 'IpinSdp');
    Route::controller('apipp', 'IpinPp');
    Route::controller('apivms', 'IpinVms');

    // <editor-fold defaultstate="collapsed" desc="Routes For Login">
        
    Route::get('/', array('as' => 'index','uses' => 'LoginController@index'));
    Route::post('/', array('as' => 'index','uses' => 'LoginController@doLogin'));
    Route::get('/login', array('as' => 'index','uses' => 'LoginController@index'));
    Route::post('/login', array('as' => 'index','uses' => 'LoginController@doLogin'));
    Route::get('/logout', array('as' => 'logout','uses' => 'LoginController@doLogout'));
    Route::get('/forbidden', array('as' => 'login','uses' => 'LoginController@showForbidden'));
    
    // </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="Routes For User">

    Route::get('/user', array('uses' => 'UserController@index'));
    Route::get('/user/detail/{id?}' ,array('uses' => 'UserController@detail'));
    Route::get('/user/delete/{id?}' ,array('uses' => 'UserController@delete'));
    Route::get('/user/create' ,array('uses' => 'UserController@create'));
    Route::post('/user/create' ,array('uses' => 'UserController@processcreate'));
    Route::get('/user/edit/{id?}' ,array('uses' => 'UserController@edit'));
    Route::post('/user/edit/{id?}' ,array('uses' => 'UserController@processedit'));
    
    Route::get('/profile', array('uses' => 'UserController@profile'));
    Route::get('/profile/edit/{id?}' ,array('uses' => 'UserController@updateprofile'));
    Route::post('/profile/edit/{id?}' ,array('uses' => 'UserController@processupdateprofile'));
    // </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="Routes For User Group">

    Route::get('/usergroup', array('uses' => 'UserGroupController@index'));
    Route::get('/usergroup/detail/{id?}' ,array('uses' => 'UserGroupController@detail'));
    Route::get('/usergroup/delete/{id?}' ,array('uses' => 'UserGroupController@delete'));
    Route::get('/usergroup/create' ,array('uses' => 'UserGroupController@create'));
    Route::post('/usergroup/create' ,array('uses' => 'UserGroupController@processcreate'));
    Route::get('/usergroup/edit/{id?}' ,array('uses' => 'UserGroupController@edit'));
    Route::post('/usergroup/edit/{id?}' ,array('uses' => 'UserGroupController@processedit'));
    
    // </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="Routes For Module">

    Route::get('/module', array('uses' => 'ModuleController@index'));
    Route::get('/module/detail/{id?}' ,array('uses' => 'ModuleController@detail'));
    Route::get('/module/delete/{id?}' ,array('uses' => 'ModuleController@delete'));
    Route::get('/module/create' ,array('uses' => 'ModuleController@create'));
    Route::post('/module/create' ,array('uses' => 'ModuleController@processcreate'));
    Route::get('/module/edit/{id?}' ,array('uses' => 'ModuleController@edit'));
    Route::post('/module/edit/{id?}' ,array('uses' => 'ModuleController@processedit'));
    
    // </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="Routes For Function Info">

    Route::get('/functioninfo', array('uses' => 'FunctionInfoController@index'));
    Route::get('/functioninfo/detail/{id?}' ,array('uses' => 'FunctionInfoController@detail'));
    Route::get('/functioninfo/delete/{id?}' ,array('uses' => 'FunctionInfoController@delete'));
    Route::get('/functioninfo/create' ,array('uses' => 'FunctionInfoController@create'));
    Route::post('/functioninfo/create' ,array('uses' => 'FunctionInfoController@processcreate'));
    Route::get('/functioninfo/edit/{id?}' ,array('uses' => 'FunctionInfoController@edit'));
    Route::post('/functioninfo/edit/{id?}' ,array('uses' => 'FunctionInfoController@processedit'));
    
    // </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="Routes For Function Info">

    Route::get('/lookup', array('uses' => 'LookupController@index'));
    Route::get('/lookup/detail/{id?}' ,array('uses' => 'LookupController@detail'));
    Route::get('/lookup/delete/{id?}' ,array('uses' => 'LookupController@delete'));
    Route::get('/lookup/create' ,array('uses' => 'LookupController@create'));
    Route::post('/lookup/create' ,array('uses' => 'LookupController@processcreate'));
    Route::get('/lookup/edit/{id?}' ,array('uses' => 'LookupController@edit'));
    Route::post('/lookup/edit/{id?}' ,array('uses' => 'LookupController@processedit'));
    
    // </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="Routes For DashboardEngine">

    Route::get('/dashboard/{id?}', array('uses' => 'DashboardController@index'));
    Route::get('/dashboard/getJson/{id?}', array('uses' => 'DashboardController@getJson'));
    Route::get('/dashboard/getChart/{id?}', array('uses' => 'DashboardController@getChart'));
	
    // </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="Routes For Prototype">

    Route::get('/prototype/{id?}', array('uses' => 'PrototypeController@index'));
    Route::get('/prototype/{folder?}/{page?}', array('uses' => 'PrototypeController@page'));
	
    // </editor-fold>

	// <editor-fold defaultstate="collapsed" desc="Routes For Prototype">

    Route::get('/p/{id?}', array('uses' => 'PrototypeController@index'));
    Route::get('/p/{folder?}/{page?}', array('uses' => 'PrototypeController@page'));
	
    // </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="Routes For Prototype">

    Route::get('/vms', array('uses' => 'VMSController@index'));
    Route::get('/vms/{id?}', array('uses' => 'VMSController@getPage'));
    Route::get('/vms/getJson/{id?}', array('uses' => 'VMSController@getJson'));
	
    // </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="Routes For Prototype">

    Route::get('/sumber_daya_kelautan', array('uses' => 'PPController@index'));
    Route::get('/sumber_daya_kelautan/{id?}', array('uses' => 'PPController@getPage'));
    Route::get('/sumber_daya_kelautan/getJson/{id?}', array('uses' => 'PPController@getJson'));
	
    // </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="Routes For Prototype">

    Route::get('/sumber_daya_perikanan', array('uses' => 'PPController@index'));
    Route::get('/sumber_daya_perikanan/{id?}', array('uses' => 'PPController@getPage'));
    Route::get('/sumber_daya_perikanan/getJson/{id?}', array('uses' => 'PPController@getJson'));
	
    // </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="Routes For Prototype">

    Route::get('/penanganan_pelanggaran', array('uses' => 'PPController@index'));
    Route::get('/penanganan_pelanggaran/{id?}', array('uses' => 'PPController@getPage'));
    Route::get('/penanganan_pelanggaran/getJson/{id?}', array('uses' => 'PPController@getJson'));
	
    // </editor-fold>
	
	// <editor-fold defaultstate="collapsed" desc="Routes For Prototype">

    Route::get('/kapal_pengawas', array('uses' => 'PPController@index'));
    Route::get('/kapal_pengawas/{id?}', array('uses' => 'PPController@getPage'));
    Route::get('/kapal_pengawas/getJson/{id?}', array('uses' => 'PPController@getJson'));
	
    // </editor-fold>
	
	Route::get('/404', function() { return View::make('hello'); });
	Route::get('/maintenance', function() { return View::make('hello'); });
	Route::get('/page/forbidden', function() { return View::make('shared/forbidden'); });
	Route::get('/home', function(){
		return Redirect::to('user'); 
    });
	
	// Route::get('/sandbox', function() { return View::make('login'); });
	// Route::get('/test', function()
	// {
		// $userInfoService = new UserInfoService();
		// $UserInfoData = $userInfoService->getUserByUsername($userdata['username']);
		// $UserInfoData->setLastLogin();
	
		// //Test Dao
	
        // $Obj = new ModuleDao();
        // $Data = $Obj->GetList();
        // if($Data != null && count($Data) >0){
            // foreach ($Data as $key => $value) {
                // D::p($value->toArray());
            // }
        // }

        // echo "end of file";
	// });
	
	Route::get('/change2/{id?}/{pass?}', function($id,$pass){
	
		try{
			$dao = UserInfoDao();
			$obj = $dao->getObject($id);
			$obj->setPassword(Hash::make($pass));
			$dao->updateObject($obj);
			echo "Success";
		}
		catch(Exception $exc){
			echo $exc;
		}
        
    });

	// Route::get('/change/{id?}/{pass?}', function($id,$pass){
		
		// try{
			// $obj = UserInfoDao::where('username', '=', $id)->firstOrFail();
			// $obj->last_login = date('Y/m/d H:i:s');
			// $obj->password = Hash::make($pass);
			// $obj->save();
			// echo "Success";
		// }
		// catch(Exception $exc){
			// echo $exc;
		// }
        
    // });
	
?>