<?php

class FunctionInfoDao extends DevetekDao {

    function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->table = 'adf_d_functioninfo';
		$this->incrementing = false;
    }
    
    public function insertObject($Obj){
		return parent::InsertObject($Obj);
    }
    
    public function getObject($id){
        return parent::GetObject($id);
    }
    
    public function getList($criteria = NULL){
        return parent::GetList($criteria);
    }

    public function updateObject($Obj, $id){
        return parent::UpdateObject($Obj, $id);
    }

    public function deleteObject($id){
        return parent::DeleteObject($id);
    }
    
    public function updateModuleId($newId,$oldId){
        $this->UpdateRawQuery("update ".$this->table." set module_id = '".$newId."' where module_id='".$oldId."'");
        return true;        
    }

     protected function toObject($rowdata=null){
		$Obj = new FunctionInfo();
		
        $Obj->setId($rowdata['id']);
        $Obj->setName($rowdata['name']);
			$module = new Module();
			$module->setId($rowdata['module_id']);
			$module->isLoaded(false);
		$Obj->setModule($module);
		$Obj->setUrl($rowdata['url']);
        $Obj->setIcon($rowdata['icon']);
        $Obj->setSequence($rowdata['sequence']);
        $Obj->setIsEnabled($rowdata['is_enabled']);
        $Obj->setIsShow($rowdata['is_show']);
			$createdBy = new UserInfo();
			$createdBy->setId($rowdata['created_by']);
			$createdBy->isLoaded(false);
		$Obj->setCreatedBy($createdBy);
        $Obj->setCreatedDate($rowdata['created_at']);
			$updatedBy = new UserInfo();
			$updatedBy->setId($rowdata['updated_by']);
			$updatedBy->isLoaded(false);
		$Obj->setUpdatedBy($updatedBy);
		$Obj->setUpdatedDate($rowdata['updated_at']);

        return $Obj;
    }
}
