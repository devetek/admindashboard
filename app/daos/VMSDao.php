<?php

class VMSDao extends DevetekDao {

    function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->table = 'ADF_DW_J_TVESSEL_STATUS';
		$this->incrementing = false;
    }
    
	public function getDataKapal($id){
		$select_query = "";
		$result = DB::connection($this->connectionName)->select($select_query);  
		if (is_null($result) || empty($result) || count($result) <= 0) { return null; }
		$result = get_object_vars($result[0]);
        
		return $result;
	}

	public function getDataSKAT($id){
		$select_query = "";
		$result = DB::connection($this->connectionName)->select($select_query);  
		if (is_null($result) || empty($result) || count($result) <= 0) { return null; }
		$result = get_object_vars($result[0]);
        
		return $result;
	}

	public function getListSKAT(){
		$result = DB::connection($this->connectionName)->select($select_query);  
		if (is_null($result) || empty($result) || count($result) <= 0) { return null; }
		$result = get_object_vars($result[0]);
        
		return $result;
	}

	public function getCountSKAT($status = 0){
		$select_query = "SELECT COUNT(SHIP_ID) \"JUMLAH\" FROM ( SELECT
			MAX(TRANSMITTER_PAIRING_BEGIN_DATE) \"LAST_PAIRING\", SHIP_ID
			FROM ADF_VMS_SKAT_SHIP WHERE 
			{where}
			GROUP BY SHIP_ID)";
		$where_query = "";
		switch($status){
			case 1: 
				$where_query .= "SYSDATE < TRANSMITTER_PAIRING_END_DATE";
				break;	
			case 2: 
				$where_query .= "SYSDATE > TRANSMITTER_PAIRING_END_DATE";
				break;	
			case 3: 
				$where_query .= "TRANSMITTER_PAIRING_END_DATE BETWEEN 
									SYSDATE AND 
								SYSDATE + (SELECT SUM(value) FROM ADF_D_LOOKUP_DETAIL WHERE ID = 'VMS_SKAT_PERIOD')";
				break;	
		}
		$select_query = str_replace("{where}",$where_query,$select_query);
		
		$result = DB::connection($this->connectionName)->select($select_query);  
		if (is_null($result) || empty($result) || count($result) <= 0) { return null; }
		$result = get_object_vars($result[0]);
        
		return $result['jumlah'];
	}
	
	public function getCountTerpantau($status = 0){
		$select_query = "SELECT * FROM ADF_DW_J_TVESSEL_STATUS
						WHERE 1 = 1 AND
						{where}";
		$where_query = "";
		switch($status){
			case 1: 
				$where_query .= "REPORTDATE BETWEEN SYSDATE - INTERVAL (SELECT SUM(value) FROM ADF_D_LOOKUP_DETAIL WHERE ID = 'VMS_ACTIVE_HOURS') HOUR AND SYSDATE
						AND REPORTDATE BETWEEN SYSDATE - INTERVAL '24' HOUR AND SYSDATE";
				break;	
			case 2: 
				$where_query .= "REPORTDATE BETWEEN SYSDATE - INTERVAL (SELECT SUM(value) FROM ADF_D_LOOKUP_DETAIL WHERE ID = 'VMS_ACTIVE_HOURS') HOUR AND SYSDATE - INTERVAL '24' HOUR
						AND REPORTDATE BETWEEN SYSDATE - INTERVAL '24' HOUR AND SYSDATE";
				break;	
			case 3: 
				$where_query .= "TRANSMITTER_PAIRING_END_DATE BETWEEN 
									SYSDATE AND 
								SYSDATE + (SELECT MAX(value) FROM ADF_D_LOOKUP_DETAIL WHERE ID = 'VMS_SKAT_PERIOD')";
				break;	
		}
		$select_query = str_replace("{where}",$where_query,$select_query);
		
		$result = DB::connection("oraclewarehouse")->select($select_query);  
		if (is_null($result) || empty($result) || count($result) <= 0) { return null; }
		$result = get_object_vars($result[0]);
        
		return $result['jumlah'];
	}
	
}
