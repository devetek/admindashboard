<?php

class PrivilegeInfoDao extends DevetekDao {

    function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->table = 'adf_d_privilegeinfo';
		$this->sequenceName = 'PRIVILEGEINFO_SEQ';
    }
    
    public function insertObject($Obj){
		return parent::InsertObject($Obj);
    }
    
    public function getObject($id){
        return parent::GetObject($id);
    }
    
    public function getList($criteria = NULL){
		return parent::GetList($criteria);
    }

    public function updateObject($Obj, $id){
        return parent::UpdateObject($Obj, $id);
    }

    public function deleteObject($id){
        return parent::DeleteObject($id);
    }
    
    public function updateModuleId($newId,$oldId){
        $this->UpdateRawQuery("update ".$this->table." set module_id = '".$newId."' where module_id='".$oldId."'");
        return true;        
    }
    
    public function updateFunctionInfoId($newId,$oldId){
        $this->UpdateRawQuery("update ".$this->table." set function_info_id = '".$newId."' where function_info_id='".$oldId."'");
        return true;        
    }
    
    public function updateModuleIdFunction($newId,$functionId){
        $this->UpdateRawQuery("update ".$this->table." set module_id = '".$newId."' where function_info_id='".$functionId."'");
        return true;        
    }

     protected function toObject($rowdata=null){
        $Obj = new PrivilegeInfo();
		
        $Obj->setId($rowdata['id']);
			$userGroup = new UserGroup();
			$userGroup->setId($rowdata['user_group_id']);
			$userGroup->setIsLoaded(false);
        $Obj->setUserGroup($userGroup);
			$module = new Module();
			$module->setId($rowdata['module_id']);
			$module->setIsLoaded(false);
        $Obj->setModule($module);
			$functionInfo = new FunctionInfo();
			$functionInfo->setId($rowdata['function_info_id']);
			$functionInfo->setIsLoaded(false);
        $Obj->setFunctionInfo($functionInfo);
        $Obj->setIsAllowCreate($rowdata['is_allow_create']);
        $Obj->setIsAllowRead($rowdata['is_allow_read']);
        $Obj->setIsAllowUpdate($rowdata['is_allow_update']);
        $Obj->setIsAllowDelete($rowdata['is_allow_delete']);
        $Obj->setIsLoaded(true);
        
        return $Obj;
    }
}
