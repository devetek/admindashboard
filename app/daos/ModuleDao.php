<?php

class ModuleDao extends DevetekDao {

    function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->table = 'adf_d_module';
		$this->incrementing = false;
    }
    
    public function insertObject($Obj){
		return parent::InsertObject($Obj);
	}
    
    public function getObject($id){
        return parent::GetObject($id);
    }
    
    public function getList($criteria = NULL){
        return parent::GetList($criteria);
    }

    public function updateObject($Obj, $id){
        return parent::UpdateObject($Obj, $id);
    }

    public function deleteObject($id){
        return parent::DeleteObject($id);
    }
    
     protected function toObject($rowdata=null){
		$Obj = new Module();
		
        $Obj->setId($rowdata['id']);
        $Obj->setName($rowdata['name']);
        $Obj->setSequence($rowdata['sequence']);
        $Obj->setIcon($rowdata['icon']);
            $parentModule = new Module();
            $parentModule->setId($rowdata['parent_module_id']);
        $Obj->setParentModule($parentModule);
        $Obj->setIsShow($rowdata['is_show']);
        $Obj->setIsEnabled($rowdata['is_enabled']);
			$createdBy = new UserInfo();
			$createdBy->setId($rowdata['created_by']);
		$Obj->setCreatedBy($createdBy);
        $Obj->setCreatedDate(new DateTime($rowdata['created_at']));
			$updatedBy = new UserInfo();
			$updatedBy->setId($rowdata['updated_by']);
		$Obj->setUpdatedBy($updatedBy);
		$Obj->setUpdatedDate(new DateTime($rowdata['updated_at']));
        
        return $Obj;
    }
}
