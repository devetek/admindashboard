<?php

class UserGroupDao extends DevetekDao {

    function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->table = 'adf_d_usergroup';
		$this->incrementing = false;
    }
    
    public function insertObject($Obj){
		return parent::InsertObject($Obj);
    }
    
    public function getObject($id){
        return parent::GetObject($id);
    }
    
    public function getList($criteria = NULL){
        return parent::GetList($criteria);
    }

    public function updateObject($Obj, $id){
        return parent::UpdateObject($Obj, $id);
    }

    public function deleteObject($id){
        return parent::DeleteObject($id);
    }
    
     protected function toObject($rowdata=null){
        $obj = new UserGroup();
        $obj->setId($rowdata['id']);
        $obj->setName($rowdata['name']);
        $obj->setDescription($rowdata['description']);
        $obj->setIsEnabled($rowdata['is_enabled']);
			$createdBy = new UserInfo();
			$createdBy->setId($rowdata['created_by']);
			$createdBy->setIsLoaded(false);
		$obj->setCreatedBy($createdBy);
        $obj->setCreatedDate($rowdata['created_at']);
			$updatedBy = new UserInfo();
			$updatedBy->setId($rowdata['updated_by']);
			$updatedBy->setIsLoaded(false);
		$obj->setUpdatedBy($updatedBy);
		$obj->setUpdatedDate($rowdata['updated_at']);
        $obj->setIsLoaded(true);
        
        return $obj;
    }
}
