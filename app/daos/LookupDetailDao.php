<?php

class LookupDetailDao extends DevetekDao {

    function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->table = 'adf_d_lookup_detail';
		$this->incrementing = false;
    }
    
    public function insertObject($Obj){
		return parent::InsertObject($Obj);
	}
    
    public function getObject($id){
        return parent::GetObject($id);
    }
    
    public function getList($criteria = NULL){
        return parent::GetList($criteria);
    }

    public function updateObject($Obj, $id){
        return parent::UpdateObject($Obj, $id);
    }

    public function deleteObject($id){
        return parent::DeleteObject($id);
    }
    
     protected function toObject($rowdata=null){
		$Obj = new LookupDetail();
		
        $Obj->setId($rowdata['id']);
        $Obj->setName($rowdata['name']);
        $Obj->setValue($rowdata['value']);
        $Obj->setIsEditable($rowdata['is_editable']);
            $Lookup = new Lookup();
            $Lookup->setId($rowdata['lookup_id']);
        $Obj->setLookup($Lookup);
        
        return $Obj;
    }
}
