<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserInfoDao extends DevetekDao implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->table = 'adf_d_userinfo';
		$this->sequenceName = 'USERINFO_SEQ';
	}

	public function getObject($id){
		return parent::getObject($id);
	}
 
	public function getList($filter = null) {
        return parent::getList($filter);
    }
	
	public function InsertObject($Obj) {
		return parent::InsertObject($Obj);
    }

	public function UpdateObject($Obj, $Id) {
        return parent::UpdateObject($Obj, $Id);
    }

	public function DeleteObject($Id) {
        return parent::DeleteObject($Id);
    }

    public function updateUserGroupId($newId,$oldId){
        $this->UpdateRawQuery("update ".$this->table." set user_group_id = '".$newId."' where user_group_id='".$oldId."'");
        return true;        
    }
	
	public function toObject($rowdata=null) {
        $user = new UserInfo();
        
        $user->setId($rowdata['id']);
        $user->setName($rowdata['name']);
        $user->setUsername($rowdata['username']);
        //$user->setHashedPassword ($rowdata['password']);
        $user->setEmail($rowdata['email']);
			$group = new UserGroup();
			$group->setId($rowdata['user_group_id']);
			$group->setIsLoaded(false);
        $user->setGroup($group);
        $user->setIsEnabled($rowdata['is_enabled']);
        $user->setLastLogin($rowdata['last_login']);
			$createdBy = new UserInfo();
			$createdBy->setId($rowdata['created_by']);
			$createdBy->setIsLoaded(false);
        $user->setCreatedBy($createdBy);
        $user->setCreatedDate($rowdata['created_at']);
			$updatedBy = new UserInfo();
			$updatedBy->setId($rowdata['updated_by']);
			$updatedBy->setIsLoaded(false);
        $user->setUpdatedBy($updatedBy);
        $user->setUpdatedDate($rowdata['updated_at']);
        $user->setIsLoaded(true);

        return $user;
    }

	
}
