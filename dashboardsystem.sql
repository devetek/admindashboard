-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.1.0.4545
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for dbdevetek
CREATE DATABASE IF NOT EXISTS `dbdevetek` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `dbdevetek`;


-- Dumping structure for table dbdevetek.functioninfo
CREATE TABLE IF NOT EXISTS `functioninfo` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `is_enabled` bit(1) DEFAULT NULL,
  `is_show` bit(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dbdevetek.functioninfo: ~9 rows (approximately)
DELETE FROM `functioninfo`;
/*!40000 ALTER TABLE `functioninfo` DISABLE KEYS */;
INSERT INTO `functioninfo` (`id`, `name`, `module_id`, `url`, `icon`, `sequence`, `is_enabled`, `is_show`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	('function_info', 'Function Info', 'user_privilege', 'functioninfo', 'zmdi-view-list', 4, b'1', b'1', '2014-11-23 07:29:59', NULL, '2015-10-25 08:53:29', 1),
	('kapal_pengawas', 'Kapal Pengawas', 'kapal_pengawas', 'prototype/kapal_pengawas', 'zmdi-portable-wifi-changes', 1, b'1', b'1', NULL, NULL, '2015-10-25 08:50:33', 1),
	('module', 'Module', 'user_privilege', 'module', 'zmdi-puzzle-piece', 3, b'1', b'1', '2014-11-23 07:29:59', NULL, '2015-10-25 08:51:39', 1),
	('penanganan_pelanggaran', 'Penanganan Pelanggaran', 'penanganan_pelanggaran', 'prototype/penanganan_pelanggaran', 'zmdi-balance', 1, b'1', b'1', NULL, NULL, '2015-10-25 08:55:20', 1),
	('sumber_daya_kelautan', 'Sumber Daya Kelautan', 'sumber_daya_kelautan', 'prototype/sumber_daya_kelautan', 'zmdi-globe', 1, b'1', b'1', NULL, NULL, '2015-10-25 08:53:50', 1),
	('sumber_daya_perikanan', 'Sumber Daya Perikanan', 'sumber_daya_perikanan', 'prototype/sumber_daya_perikanan', 'zmdi-boat', 1, b'1', b'1', NULL, NULL, '2015-10-25 08:54:27', 1),
	('user_group', 'User Group', 'user_privilege', 'usergroup', 'zmdi-accounts', 2, b'1', b'1', '2014-11-23 07:29:59', NULL, '2015-10-25 08:54:35', 1),
	('user_info', 'User Info', 'user_privilege', 'user', 'zmdi-account', 1, b'1', b'1', '2014-11-23 07:29:59', NULL, '2015-10-25 08:54:42', 1),
	('vms', 'VMS', 'vms', 'prototype/vms', 'zmdi-eye', 1, b'1', b'1', NULL, NULL, '2015-10-25 08:54:53', 1);
/*!40000 ALTER TABLE `functioninfo` ENABLE KEYS */;


-- Dumping structure for table dbdevetek.module
CREATE TABLE IF NOT EXISTS `module` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_module_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(1) DEFAULT NULL,
  `is_show` tinyint(1) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dbdevetek.module: ~6 rows (approximately)
DELETE FROM `module`;
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` (`id`, `name`, `sequence`, `icon`, `parent_module_id`, `is_enabled`, `is_show`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
	('kapal_pengawas', 'Kapal Pengawas', 5, 'zmdi-portable-wifi-changes', '', 1, 1, 1, '2015-10-24 05:00:54', 1, '2015-10-25 08:47:27'),
	('penanganan_pelanggaran', 'Penanganan Pelanggaran', 6, 'zmdi-balance', '', 1, 1, 1, '2015-10-24 05:01:04', 1, '2015-10-25 05:20:21'),
	('sumber_daya_kelautan', 'Sumber Daya Kelautan', 3, 'zmdi-globe', '', 1, 1, 1, '2015-10-24 05:00:02', 1, '2015-10-25 08:42:32'),
	('sumber_daya_perikanan', 'Sumber Daya Perikanan', 4, 'zmdi-boat', '', 1, 1, 1, '2015-10-24 05:00:15', 1, '2015-10-25 08:46:38'),
	('user_privilege', 'User Privilege', 1, 'zmdi-assignment-account', '', 1, 1, NULL, '2014-11-23 07:29:58', 1, '2015-10-25 08:44:45'),
	('vms', 'VMS', 2, 'zmdi-pin-drop', '', 1, 1, NULL, '2015-10-11 00:49:00', 1, '2015-10-25 05:19:47');
/*!40000 ALTER TABLE `module` ENABLE KEYS */;


-- Dumping structure for table dbdevetek.privilegeinfo
CREATE TABLE IF NOT EXISTS `privilegeinfo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_group_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `function_info_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_allow_read` bit(1) NOT NULL,
  `is_allow_create` bit(1) NOT NULL,
  `is_allow_update` bit(1) NOT NULL,
  `is_allow_delete` bit(1) NOT NULL,
  `created_ats` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_ats` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dbdevetek.privilegeinfo: ~17 rows (approximately)
DELETE FROM `privilegeinfo`;
/*!40000 ALTER TABLE `privilegeinfo` DISABLE KEYS */;
INSERT INTO `privilegeinfo` (`id`, `user_group_id`, `module_id`, `function_info_id`, `is_allow_read`, `is_allow_create`, `is_allow_update`, `is_allow_delete`, `created_ats`, `updated_ats`) VALUES
	(0, 'administrator', 'vms', 'vms', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(1, 'administrator', 'user_privilege', 'user_group', b'1', b'1', b'1', b'1', '2014-11-23 07:29:59', '2014-11-23 07:29:59'),
	(2, 'administrator', 'user_privilege', 'user_info', b'1', b'1', b'1', b'1', '2014-11-23 07:30:00', '2014-11-23 07:30:00'),
	(3, 'administrator', 'user_privilege', 'module', b'1', b'1', b'1', b'1', '2014-11-23 07:30:00', '2014-11-23 07:30:00'),
	(4, 'administrator', 'user_privilege', 'function_info', b'1', b'1', b'1', b'1', '2014-11-23 07:30:00', '2014-11-23 07:30:00'),
	(5, 'administrator', 'user_privilege', 'privilege_info', b'1', b'1', b'1', b'1', '2014-11-23 07:30:00', '2014-11-23 07:30:00'),
	(7, 'administrator', 'vms', 'dashboard', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(8, 'administrator', 'sumber_daya_kelautan', 'sumber_daya_kelautan', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(9, 'administrator', 'sumber_daya_perikanan', 'sumber_daya_perikanan', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(10, 'administrator', 'kapal_pengawas', 'kapal_pengawas', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(11, 'administrator', 'penanganan_pelanggaran', 'penanganan_pelanggaran', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(12, 'operator_-_vms', 'vms', 'vms', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(13, 'operator_-_vms', 'kapal_pengawas', 'kapal_pengawas', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(14, 'operator', 'kapal_pengawas', 'kapal_pengawas', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(15, 'operator', 'penanganan_pelanggaran', 'penanganan_pelanggaran', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(16, 'operator', 'vms', 'vms', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(17, 'operator', 'sumber_daya_perikanan', 'sumber_daya_perikanan', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(18, 'operator', 'sumber_daya_kelautan', 'sumber_daya_kelautan', b'1', b'1', b'1', b'1', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `privilegeinfo` ENABLE KEYS */;


-- Dumping structure for table dbdevetek.usergroup
CREATE TABLE IF NOT EXISTS `usergroup` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dbdevetek.usergroup: ~3 rows (approximately)
DELETE FROM `usergroup`;
/*!40000 ALTER TABLE `usergroup` DISABLE KEYS */;
INSERT INTO `usergroup` (`id`, `name`, `description`, `is_enabled`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
	('administrator', 'Administrator', 'Administrator', 1, 1, '2014-11-23 07:29:58', 11, '2015-05-30 06:07:03'),
	('operator', 'Operator', '', 1, 1, '2015-10-25 10:25:25', 1, '2015-10-27 08:25:24'),
	('operator_-_vms', 'Operator - VMS', '', 1, 1, '2015-10-25 10:21:09', NULL, '2015-10-25 17:21:09');
/*!40000 ALTER TABLE `usergroup` ENABLE KEYS */;


-- Dumping structure for table dbdevetek.userinfo
CREATE TABLE IF NOT EXISTS `userinfo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_group_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_enabled` tinyint(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table dbdevetek.userinfo: ~3 rows (approximately)
DELETE FROM `userinfo`;
/*!40000 ALTER TABLE `userinfo` DISABLE KEYS */;
INSERT INTO `userinfo` (`id`, `username`, `email`, `name`, `password`, `user_group_id`, `is_enabled`, `remember_token`, `last_login`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
	(1, 'admin', 'admin@coderkontrakan.com', 'Administrator', '$2y$10$5OJ1bJ/ZOxVENUiE4Pa/A.RL4xK.rLDIpJufSRlHbHzPc61..qpIm', 'administrator', 1, 'Gn5Fnd5NOsE4wmWbp2lntXNbPbqShLVM9fw68bA4TCqqYSXAmx2UqZYgCtPg', '2015-10-27 08:23:22', 0, '0000-00-00 00:00:00', 0, '2015-10-27 08:25:42'),
	(2, 'aning', 'AningLyandhaniSiaroto@gmail.com', 'Aning L S', '$2y$10$i/XOn646jheuWU/SxCQaW.owYB89iYnDIALQmipWam/XIm0tXeY5C', 'operator', 1, 'TDUZbzXpwijZnH1hBTs35CGklTecDn1Hzh9NvRSm7ZEUJry8VgRe8c4CLuNo', '2015-10-25 16:18:50', 1, '2015-10-24 05:10:01', 1, '2015-10-27 08:23:37'),
	(3, 'Agus', 'agus_cahbagus@ymail.com', 'Agus', '$2y$10$3Q/pDTtT0pl0DeqCrzI3HunBwC1ikUrxyf/4ZcSjsUdo7JqrjDX.q', 'administrator', 1, NULL, '0000-00-00 00:00:00', 1, '2015-10-24 05:15:10', 1, '2015-10-24 05:16:45'),
	(4, 'Operator', 'operator@kkp.go.id', 'Operator', '$2y$10$p9g7ANq4OwRK2aj8vTRUOe/W7lSk0T5BRYsOCfOEukm5Woz3qBJ9i', 'operator', 1, 'hM1bDJx6p5GVTDdEB5Nc49vtU6T0QdKVhDhFFl9pMF1jwRs6vO17HxQg9ioi', '2015-10-27 08:54:47', 1, '2015-10-27 08:24:43', 0, '2015-10-27 08:54:47');
/*!40000 ALTER TABLE `userinfo` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
