function selectFrancePoints(feature, resolution)
  {
  var radiusPoints = 8;
   
  var textKdisisi = feature.get('name');
  var styles = [new ol.style.Style({
      fill: new ol.style.Fill({
        color: 'rgba(255, 255, 255, 0.2)'
        }),
        stroke: new ol.style.Stroke({
        color: '#ffcc33',
        width: 3
        }),
  
        image: new ol.style.Circle({
          fill: new ol.style.Fill({
            color: '#ff0000'
          }),
          stroke: new ol.style.Stroke({
            color: '#ffccff'
          }),
          radius: 8
          }) 

        })
      
      ];
  return styles;
  }

function pointVms(feature, resolution)
	{
		var radiusPoints = 8;
	   
		var textKdisisi = feature.get('name');
		var styles = [new ol.style.Style({
				fill: new ol.style.Fill({
					color: 'rgba(255, 255, 255, 0.2)'
				  }),
				  stroke: new ol.style.Stroke({
					color: '#ffcc33',
					width: 3
				  }),
		
					image: new ol.style.Circle({
						fill: new ol.style.Fill({
						  color: '#ff0000'
						}),
						stroke: new ol.style.Stroke({
						  color: '#ffccff'
						}),
						radius: 8
					  }),
					  text: 'bla'

					})
				
			  ];
		return styles;
	}

 
 var stylePengawas = function(feature,resolution) { 
    return [new ol.style.Style({
            
       image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [0.5, 20],
            anchorXUnits: 'pixels',
            anchorYUnits: 'pixels',
            scale : 0.7, 
            src: urlServer + 'public/img/Boat-icon.png'
          }))   
      })];
}; 
 

 var styleFunctionZone = function(feature, resolution) {
    var hexColor = d2h(feature.get('fill-color')); //feature.get('color');
    var fill_color = hexToRgb(hexColor,feature.get('fill-opacity')); 
     
    var hexColor = d2h(feature.get('line-color')); //feature.get('color');
    var line_color = hexToRgb(hexColor,255); 
     
    var opacity = (feature.get('fill-opacity')-5) * 4/1000;
     
    return [new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: line_color,
          width: feature.get('line-width')
        }),
        fill: new ol.style.Fill({
          color: fill_color
        }),   
       image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [0.5, 20],
            anchorXUnits: 'pixels',
            anchorYUnits: 'pixels',
            scale : 1,
            opacity: opacity,
            src: server + 'Styles/Icons/Transportation/boat.png'
          })),
                 
        text: new ol.style.Text({
          text: feature.get('name'),
          scale: 1,
          fill: new ol.style.Fill({
            color: '#000000'
          }),
          stroke: new ol.style.Stroke({
            color: '#FFFF99',
            width: 3.5
          })
        })
       

      })];
};


function d2h(d) { return (+d).toString(16); };
function hexToRgb(hex,opacity) {

    opacity = opacity * 4/1000;

    var bigint = parseInt(hex, 16);
    var r = (bigint >> 16) & 255;
    var g = (bigint >> 8) & 255;
    var b = bigint & 255;

    return [r , g , b,opacity];
}


 
 var stylePoint = function(feature,resolution) { 
     
    return [new ol.style.Style({ 
       image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
            anchor: [0.5, 20],
            anchorXUnits: 'pixels',
            anchorYUnits: 'pixels',
            scale : 1, 
            src:feature.get('icon')
          }))   
      })];
}; 

function pointDss(feature, resolution)
{ 
    var warna= feature.get('kelas'); 
    var styles = [new ol.style.Style({
        fill: new ol.style.Fill({
          color: 'rgba(255, 255, 255, 0.2)'
          }),
          stroke: new ol.style.Stroke({
          color: '#ffcc33',
          width: 3
          }),
    
          image: new ol.style.Circle({
            fill: new ol.style.Fill({
              color: warna
            }),
            stroke: new ol.style.Stroke({
              color: warna
            }),
            radius: 4
            })

          })
        
        ];
    return styles;
  }


  
 

