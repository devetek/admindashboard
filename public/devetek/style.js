var warnaBg = [
				  '#6D8FFF', 
				  '#F57D5A',
				  '#0DE23C',
				  '#57E8EB',
				  '#FFFB6C',
				  '#8960FF',
				  '#EC50B1'
				  ];

var warnaBg2 = [
				  '#f4cccc', 
				  '#efb2b2',
				  '#ea9999',
				  '#e57f7f',
				  '#e06666',
				  '#db4c4c',
				  '#d63232',
				  '#d11919'
				  ];
						  
var MaxHeight=900; 
var MaxHeight600=600; 
var MaterHeight=$(window).height() - 80;
var standatHeight = 400;

var lokasiPie='';
var urlPie='';

//menganti ukuran
$(window).resize(function() {
	TriggerResize();    
}); 
$(window).trigger('resize');

function TriggerResize()
{
	if($(window).height() > MaxHeight)
	{
		console.log($(window).height());
		$('#chartKapal').height(MaterHeight/2 - 150);
		$('.card').height(MaterHeight/2 - 50);
		$('#site-visits').height(MaterHeight/2 - 50);

		$('.card2').height((MaterHeight/2 -45  ) * 3)  ;

		$('.table-responsive2').height((MaterHeight/2 -145  ) * 3)  ;

		$('#hostlistWG').height(MaterHeight/2 - 150);
		$('.mini-charts-item').height( (MaterHeight/2 - 53)/4);
	}
	else if($(window).height() > MaxHeight600)
	{
		console.log($(window).height());
		$('#chartKapal').height(MaterHeight * 2/3 - 400);
		$('.card').height(MaterHeight * 2/3 - 50);
		$('#site-visits').height(MaterHeight * 2/3 - 50);

		$('.card2').height( (MaterHeight * 2/3 -45) *3 );

		$('.table-responsive2').height( (MaterHeight * 2/3 -150) *3 );

		$('#hostlistWG').height(MaterHeight * 2/3 - 150);
		$('.mini-charts-item').height( (MaterHeight * 2/3 - 53)/4);
	}
	else
	{
		console.log('standar');
		$('#chartKapal').height(standatHeight);
		//$('.card').height(standatHeight);
		$('#hostlistWG').height(standatHeight-50);
		$('.mini-charts-item').height( (standatHeight-50)/4);
	}
}

function AutoResizeDiv(jenis,cal)
{ 

	if ($(jenis).length !== 0) {
		if($(window).height() > MaxHeight)
		{
			console.log($(window).height());
			$(jenis).height(MaterHeight/2 - cal -100);			 
		}
		else if($(window).height() > MaxHeight600)
		{
			console.log($(window).height());
			$(jenis).height(MaterHeight /2 - cal); 
			 
		}
		else
		{ 
			$(jenis).height(standatHeight); 
		}
	}
}

function AutoResizeTable(jenis)
{ 

	if ($(jenis).length !== 0) 
		{		  
			$(jenis).height(MaterHeight * 2/3);			 
		}
		else if($(window).height() > MaxHeight600)
		{ 
			$(jenis).height(MaterHeight * 1/2 ); 			 
		}
		else
		{ 
			$(jenis).height(standatHeight); 
		}
	
}

function ContentStyle2Height()
{
	var tinggi1 = $('.card-header').height();
	var tinggi2 = $('.card-sub-header').height();
	var windowHeight = $(window).height();

	return windowHeight - tinggi1 - tinggi2 - 50*1.2; //50 padding tinggi1
}

function heightFull()
{
	$('.card').height($(window).height());
	$('#' + lokasiPie).height(ContentStyle2Height());
	
	//untuk map
	if ($('.tab-content').length !== 0) 
	{
		$('.tab-content').height(MaterHeight-70); 
	}

	//sdk
	if ($('#mapSDK').length !== 0) 
	{
		$('#mapSDK').height(MaterHeight); 
	}
	if ($('.card1 .autoOverflow').length !== 0) 
	{
		$('.card1 .autoOverflow').height(MaterHeight); 
	}
 
 	
 	//SDP
	if ($('#PengawasanBerpangkalan').length !== 0) 
	{
		$('#PengawasanBerpangkalan').height(MaterHeight); 
	}

	//KP
	if ($('#mapKp').length !== 0) 
	{
		$('#mapKp').height(MaterHeight); 
	}
	if ($('.card .autoOverflow').length !== 0) 
	{
		$('.card .autoOverflow').height(MaterHeight); 
	}
	if ($('#pieKp').length !== 0) 
	{
		$('#pieKp').height(MaterHeight); 
	}
 
	
	//home
	if ($('#mapHomepage').length !== 0) 
	{
		$('#mapHomepage').height(MaterHeight); 
	}

	if ($('#lastPosisi').length !== 0) 
	{
		$('#lastPosisi').height(MaterHeight); 
	}

	$('table.table-inner').removeClass('hidden');

}

function heightKecil()
{
	$('#' + lokasiPie).height(MaterHeight/2 - 50);

	if($(window).height() > MaxHeight)
	{ 
		$('.card').height(MaterHeight/2 - 50); 
	}
	else if($(window).height() > MaxHeight600)
	{ 
		$('.card').height(MaterHeight * 2/3 - 50); 
	}
	else
	{
		$('.card').height(400);
	}
	
	//home
	if ($('#mapHomepage').length !== 0) 
	{ 
		customHeight('#mapHomepage',2,50);
	}
	
	//SDP
	if ($('#PengawasanBerpangkalan').length !== 0) 
	{
		AutoResizeDiv('#PengawasanBerpangkalan',20);
	}

	//sdk
	if ($('#mapSDK').length !== 0) 
	{
		AutoResizeDiv('#mapSDK',20);
	} 
	if ($('.card1 .autoOverflow').length !== 0) 
	{
		customHeight('.card1 .autoOverflow',2,80);  
	} 

	
	//Kp
	if ($('#mapKp').length !== 0) 
	{
		AutoResizeDiv('#mapKp',20);
	} 
	if ($('.card .autoOverflow').length !== 0) 
	{
		AutoResizeDiv('.card .autoOverflow',20);  
	} 
	if ($('#pieKp').length !== 0) 
	{
		AutoResizeDiv('#pieKp',70);  
	}


	if ($('#lastPosisi').length !== 0) 
	{
		AutoResizeDiv('#lastPosisi',20);
	}

	$('table.table-inner').addClass('hidden');

}

function customHeight(id,kali,kurang)
{
	if($(window).height() > MaxHeight)
	{		 
		$(id).height((MaterHeight/2 - kurang  ) * kali)  ; 
	}
	else if($(window).height() > MaxHeight600)
	{ 
		$(id).height((MaterHeight *2/3 - kurang  ) * kali)  ;
	}
	else
	{ 

	}
}

$(function(){
    $.ajaxSetup({
        beforeSend: function() {
            if ($("#loadingbar").length === 0) {
                $("body").append("<div id='loadingbar'></div>")
                $("#loadingbar").addClass("waiting").append($("<dt/><dd/>"));
                $("#loadingbar").width((50 + Math.random() * 30) + "%");
                //$("#loading").fadeIn('slow');
                console.log('sdsa');
                //$("#mutar").fadeIn('slow'); 
            }
        },
        complete : function() {
            $("#loadingbar").width("101%").delay(200).fadeOut(400, function() {
                $(this).remove();
                //$("#loading").fadeOut('slow');
                //$("#mutar").fadeOut('slow');
            });

            //AutoResizeTable('.table-responsive');
        }
    }); 
}); 

$(document).ready(function(){
    //Welcome Message (not for login page)
    function notify(from, align, icon, type, animIn, animOut, message){
                $.growl({
                    icon: icon,
                    title: ' Pelanggaran: ',
                    message: message,
                    url: ''
                },{
                        element: 'body',
                        type: type,
                        allow_dismiss: true,
                        placement: {
                                from: from,
                                align: align
                        },
                        offset: {
                            x: 20,
                            y: 85
                        },
                        spacing: 10,
                        z_index: 1031,
                        delay: 0,
                        timer: 0,
                        url_target: '_blank',
                        mouse_over: false,
                        animate: {
                                enter: animIn,
                                exit: animOut
                        },
                        icon_type: 'class',
                        template: '<div data-growl="container" class="alert" role="alert">' +
							'<button type="button" class="close" data-growl="dismiss">' +
								'<span aria-hidden="true">&times;</span>' +
								'<span class="sr-only">Close</span>' +
							'</button>' +
							'<i data-growl="icon"></i>' +
							'<span data-growl="title"></span>' +
							'<span data-growl="message"></span>' +
							'<a href="#" data-growl="url"></a>' +
						'</div>'
                });
            };

    
	notify('top', 'right','zmdi zmdi-alert-polygon zmdi-hc-fw', 'danger', "animated fadeInUp", "animated fadeOutUp", "Pelanggaran Wilayah Kapal XYZ di Wilayah ABC");
});


