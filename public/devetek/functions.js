 
function startTime() {
    var today = new Date();
	var days = ["Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"];
	var months = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
	var tanggal = days[today.getDay()] + ", " + checkTime(today.getDate()) + " " + months[today.getMonth()] + " " + today.getFullYear();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('clock-widget').innerHTML =
    tanggal + " [" + h + ":" + m + ":" + s + "]";
    var t = setTimeout(startTime, 500);
}

function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function LoadLayer(layerId,url)
{
	$(layerId).load(url);
	$('#layer1').hide();
}

function closeLayer(layerId){
	$(layerId).html('');
	$('#layer1').show();
}

function hideToggle(id)
{
	if($('#' + id).hasClass('active'))
	{
		$('#' + id).removeClass('active');
		$('#' + id +'.additional').hide();
	}
	else
	{
		$('#' + id).addClass('active')
		$('#' + id +'.additional').show();
	}
}


var legenPie='';
var JudulPie='';
	
function reloadWidget(){
	var $widgets = $('.widget');
	$widgets.widgster();
 

	$widgets.on("fullscreen.widgster", function(){
		
		heightFull();

		//ganti legen pie ka full
		legenPie = {
					"align": "center",
					"markerType": "circle"
				};
		ChartPie();

		$('.content-wrap').css({
			'-webkit-transform': 'none',
			'-ms-transform': 'none',
			transform: 'none',
			'margin': 0,
			'z-index': 2
		});

	}).on("restore.widgster closed.widgster", function(){
		
		heightKecil();

		legenPie = '';
		ChartPie();


		$('.content-wrap').css({
			'-webkit-transform': '',
			'-ms-transform': '',
			transform: '',
			margin: '',
			'z-index': ''
		});
	});
}

function LoadModal(url)
{ 
	$('#myModal').removeData("modal"); 
	$('#myModal').modal({remote: url});       
}
 
	
//remove data lamo
$('body').on('hidden.bs.modal', '.modal', function () {
	$(this).removeData('bs.modal');
});

$( document ).ready(function() {
	reloadWidget();
	startTime();
});

function form_sumbit()
{

}

//untuak lokasi server

 if(window.location.host =='localhost')
 {
    var urlServer= 'http://localhost/kkp/v3/admindashboard/';  
 }
 else
 {
    var urlServer= window.location.protocol + "//" + window.location.host + "/kkp/v3/admindashboard/"; 
 }


function  ChartPie()
{   
	AmCharts.makeChart(lokasiPie,
		 {
			"type": "pie",
			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			"innerRadius": "40%",
			"labelText": " [[percents]]%",
			"minRadius": 7,
			"titleField": "category",
			"valueField": "column-1",
			"theme": "light",
			"allLabels": [],
			"balloon": {},
			"legend": legenPie,
			"export": {
				"enabled": true
			},
			"titles": [
				{
					"id": "Title-1",
					"size": 12,
					"text": JudulPie
				}
			],
			 "dataLoader": { 
			    "url": urlPie,
			    "format": "json"
			  }


		}
	);
}
